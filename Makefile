cljs_sources := $(shell find src/cljs      -name \*.cljs)
clj_sources  := $(shell find src/clj       -name \*.clj)
spec_sources := $(shell find spec          -name \*.clj)
e2e_sources  := $(shell find integration   -name \*.clj)
resources    := $(shell find resources     -name \*.clj)
ebextensions := $(shell find .ebextensions -type f)
platform     := $(shell find .platform     -type f)

.min-prod: min-prod.cljs.edn $(cljs_sources)
	clojure -M:fig -bo min-prod
	touch $@

.min-local: min-local.cljs.edn $(cljs_sources)
	clojure -M:fig -bo min-local
	touch $@

.min-e2e: min-e2etest.cljs.edn $(cljs_sources)
	clojure -M:fig -bo min-e2etest
	touch $@

backend: FORCE
	clojure -M:dev:backend

figwheel: FORCE
	clojure -M:fig:frontend-dev

.test: $(clj_sources) $(spec_sources)
	clojure -M:test
	touch $@

autotest: FORCE
	clojure -M:test -a

.e2e: .min-e2e $(clj_sources) $(cljs_sources) $(e2e_sources) $(resources)
	clojure -M:e2e
	touch $@

.deploy: .test .e2e .min-prod $(ebextensions) $(platform) \
         deploy-helper.sh Makefile Procfile
	./deploy-helper.sh
	touch $@

.min-prod .min-local .min-e2e .test .e2e .deploy: deps.edn

FORCE:
