#!/bin/sh

if [ $(which clojure 2> /dev/null) ]; then exit 0; else echo "Installing Clojure CLI"; fi
curl -O https://download.clojure.org/install/linux-install-1.10.1.739.sh
chmod +x linux-install-1.10.1.739.sh
sudo ./linux-install-1.10.1.739.sh
