;; These are "per-directory local variables" for emacs.
;; See https://www.gnu.org/software/emacs/manual/html_node/emacs/Directory-Variables.html

;; Always use the e2e alias (apart from the main options), so that Etaoin is
;; available in the REPL in case we want to debug an integration test.
((clojure-mode
  (cider-clojure-cli-global-options . "-R:e2e -C:e2e -R:fig -R:frontend-dev")))
