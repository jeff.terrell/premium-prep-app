(ns com.premiumprep.app.server.session-spec
  (:require [clojure.string :as str]
            (com.premiumprep.app
              [middleware :as mdw]
              [schema :as schema]
              [server :as sut])
            [datomic.client.api :as d]
            [speclj.core :refer [describe it should-be should-contain should=
                                 with]])
  (:import java.util.UUID))

(describe "the main handler of the server"
  (with db-name (name ::db))
  (with client (doto (d/client {:server-type :dev-local
                                :storage-dir :mem
                                :system "local"})
                 (d/create-database {:db-name @db-name})))
  (with conn (doto (d/connect @client {:db-name @db-name})
               schema/install-schema!))

  (it "sets a cookie header given :session data in the response"
    (let [session-data 12341234
          base-handler (constantly {:status 200
                                    :body ""
                                    :session session-data})
          top-handler (sut/make-handler base-handler :none @conn)
          response (top-handler {:uri "/", :request-method :get})
          valid-cookie-data? (fn [c]
                               (.startsWith c "ring-session="))]
      (should-contain "Set-Cookie" (:headers response))
      (should-be valid-cookie-data?
                 (first (get-in response [:headers "Set-Cookie"])))))

  (it "finds session data given a cookie header in the request"
    (let [session-id (UUID/randomUUID)
          email "an-email-address"
          account-type :account-type/student
          tx-data [{:session/id session-id, :session/account "account"}
                   {:db/id "account"
                    :account/email email
                    :account/type account-type
                    :account/state :account-state/initialized
                    :account/active? true}]
          {:keys [tempids]} (d/transact @conn {:tx-data tx-data})
          account-eid (tempids "account")
          base-handler #(if (= "" (:body %))
                          {:status 200, :body "", :session account-eid}
                          {:status 200, :body (pr-str (:session %))})
          top-handler (sut/make-handler base-handler :none @conn)
          response (top-handler {:uri "/", :request-method :get, :body ""})
          cookie (first (get-in response [:headers "Set-Cookie"]))
          valid-cookie-data? (fn [c]
                               (.startsWith c "ring-session="))]

      (should-contain "Set-Cookie" (:headers response))
      (should-be valid-cookie-data? cookie)
      (let [cookie-data (first (str/split cookie #";"))
            req {:uri "/"
                 :request-method :get
                 :headers {"cookie" cookie-data}
                 :body "1"}
            response (top-handler req)
            session-data (read-string (:body response))]
        (should= {:db/id (str account-eid)
                  :account/email email
                  :account/type account-type
                  :account/state :account-state/initialized}
                 session-data)))))
