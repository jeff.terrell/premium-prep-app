(ns com.premiumprep.app.model.student-spec
  (:require [com.premiumprep.app.model.student :as sut]
            [speclj.core :refer [describe it should=]]))

(def entry
  #(hash-map :college-list-entry/category {:db/ident
                                           (keyword "college-list-category"
                                                    (name %1))}
             :college-list-entry/index %2
             :attr %3
             :college-list-entry/college %4))

(defn result [attr college] {:attr attr, :college-list-entry/college college})

(describe "listify-college-list-entries"
  (it "works correctly"
    (should= {:student/college-lists
              {:high-reach [(result :a :college1)]
               :reach [(result :b :college2) (result :c :college3)]
               :target [(result :d :college4)
                        (result :e :college5)
                        (result :f :college6)]
               :likely [(result :g :college7) (result :h :college8)]}}
             (sut/listify-college-list-entries
               {:student/college-list-entries
                [(entry :high-reach 0 :a :college1)
                 (entry :reach 0 :b :college2)
                 (entry :reach 1 :c :college3)
                 (entry :target 0 :d :college4)
                 (entry :target 1 :e :college5)
                 (entry :target 2 :f :college6)
                 (entry :likely 0 :g :college7)
                 (entry :likely 1 :h :college8)]}))))
