(ns com.premiumprep.app.handler.create-student-spec
  (:require
   [com.premiumprep.app.handler.create-student :as sut]
   [com.premiumprep.app.model.student :as student]
   [com.premiumprep.app.util :as util]
   [datomic.client.api :as d]
   [speclj.core :refer [around context describe it should-be should-contain
                        should-have-invoked should-not-have-invoked should= stub
                        with with-stubs]]))

(describe "create-student/params->tx-data"
  (with-stubs)

  ;; "function under test"
  (with fut @(ns-resolve 'com.premiumprep.app.handler.create-student
                         'params->tx-data))

  (it "returns an error message if the email address is already taken"
    (let [db-val :the-database
          cid "123412342134"
          email "an-email-address@domain.com"
          stub-name :duplicate-email-msg
          expected-msg (format "The given email (%s) is already taken" email)
          e-stub (stub stub-name {:return expected-msg})]
      (with-redefs [sut/duplicate-email-msg e-stub
                    d/q (stub :q {:return #{[cid]}})]
        (should= [false [expected-msg]]
                 (@fut db-val {"email" email, "name" "n", "counselor-id" cid}))
        (should-have-invoked stub-name {:times 1, :with [db-val email]}))))

  (context "ignoring email uniqueness checks"
    (with fut2
      (fn [& args]
        (with-redefs [sut/duplicate-email-msg (stub :duplicate-email-msg
                                                    {:return nil})]
          (apply @fut args))))

    (it "returns an error message if required params are missing"
      (should= [false [(str "The following required parameters are missing:"
                            " counselor-id, email")]]
               (@fut2 :db {}))
      (should= [false
                ["The following required parameter is missing: counselor-id"]]
               (@fut2 :db {"email" "e", "name" "n"})))

    (it "returns an error message if counselor-id is not numeric"
      (let [counselor-id "cid"]
        (should= [false [(format "The given counselor-id value (%s) is not numeric"
                                 counselor-id)]]
                 (@fut2 :db
                  {"email" "e", "name" "n", "counselor-id" counselor-id}))))

    (it "returns an error message if counselor-id is not found in db"
      (let [db-val :the-database
            cid "1234"
            q-stub (stub :q {:return #{}})]
        (with-redefs [d/q q-stub]
          (should= [false [(str "The given counselor-id value (" cid
                                ") is not found in the database")]]
                   (@fut2 db-val {"email" "e", "name" "n", "counselor-id" cid}))
          (should-have-invoked :q))))

    (it "returns multiple error messages if there are multiple errors"
      (let [counselor-id "cid"]
        (should= [false ["The following required parameter is missing: email"
                         (format "The given counselor-id value (%s) is not numeric"
                                 counselor-id)]]
                 (@fut2 :db {"counselor-id" counselor-id}))))

    (it "does not include multiple error messages for the same field"
      (should= [false
                ["The following required parameter is missing: counselor-id"]]
               (@fut2 :db {"email" "e", "name" "n"})))

    (context "with a found counselor-id"
      (with db-val :the-database)
      (with counselor-id 1234)
      (with q-stub-name :datomic-query)
      (with q-stub (stub @q-stub-name {:return #{[@counselor-id]}}))
      (around [it] (with-redefs [d/q @q-stub] (it)))
      (with email "person@email.com")
      (with student-name "Homer Price")

      (with base-params {"email" @email
                         "name" @student-name
                         "counselor-id" (str @counselor-id)})
      (with base-account-tx-map {:db/id "account"
                                 :account/email @email
                                 :account/active? true
                                 :account/state :account-state/uninitialized
                                 :account/type :account-type/student})
      (with base-student-tx-map {:db/id "student"
                                 :student/account "account"
                                 :student/counselor @counselor-id})

      (it "returns a tx-map for a new account entity with given values"
        (let [tx-data (second (@fut2 @db-val @base-params))
              account-tx-map (first (filter #(= "account" (:db/id %)) tx-data))]
          (should-be some? account-tx-map)
          (should= @base-account-tx-map
                   (dissoc account-tx-map :account/token))))

      (it "includes a token attribute in the account tx-map of returned value"
        (let [tx-data (second (@fut2 @db-val @base-params))
              account-tx-map (first (filter #(= "account" (:db/id %)) tx-data))]
          (should-contain :account/token account-tx-map)
          (should-be string? (:account/token account-tx-map))))

      (it "returns a tx-map for a new student entity with given values"
        (should-contain @base-student-tx-map
                        (map #(if (map? %) (dissoc % :student/joined-at) %)
                             (second (@fut2 @db-val @base-params)))))

      (it "includes a joined-at date"
        (let [instant? #(= java.util.Date (type %))
              student-tx-map (first (filter #(= "student" (:db/id %))
                                            (second
                                              (@fut2 @db-val @base-params))))]
          (should-be instant? (:student/joined-at student-tx-map)))))))

(describe "the create-student handler"
  (with-stubs)
  (with conn :datomic-connection)
  (with db-stub-name :datomic-db)
  (with db-val :the-database-value)
  (with db-stub (stub @db-stub-name {:return @db-val}))
  (with counselor-id 1234)
  (with student-id 4321)
  (with q-stub-name :datomic-q)
  (with q-stub (stub @q-stub-name {:return #{[@counselor-id]}}))
  (with find-stub-name :student-find)
  (with find-stub (stub @find-stub-name {:return :student-data}))
  (with result-stub-name :student-result)
  (with result-stub (stub @result-stub-name {:return :student-result}))
  (with password "abcdefghijklmnop")
  (with tx-data [{:db/id "account"}])
  (with tx-data-stub-name :params->tx-data)
  (with tx-data-stub (stub @tx-data-stub-name {:return @tx-data}))
  (with transact-stub-name :datomic-transact)
  (with transact-stub (stub @transact-stub-name
                            {:return {:tempids {"student" @student-id}}}))
  (with duplicate-email-msg-stub-name :duplicate-email-msg)
  (with duplicate-email-msg-stub (stub @duplicate-email-msg-stub-name
                                       {:return nil}))
  (around [it] (with-redefs [d/db @db-stub
                             d/q @q-stub
                             d/transact @transact-stub
                             sut/duplicate-email-msg @duplicate-email-msg-stub
                             sut/params->tx-data* @tx-data-stub
                             student/find @find-stub
                             util/student-result @result-stub]
                 (it)))

  (it "refuses unauthorized requests"
    (let [req {:datomic/conn @conn, :multipart-params {}}
          result (sut/handler req)]
      (should-not-have-invoked @find-stub-name)
      (should-not-have-invoked @result-stub-name)
      (should= 403 (:status result))))

  (it "returns an error response if given invalid data"
    (should= {:status 400
              :body {:error (str "The given data generated the following errors:\n"
                                 "- The following required parameter is missing:"
                                 " counselor-id\n")}}
             (sut/handler {:datomic/conn @conn
                           :multipart-params {"email" "e"
                                              "name" "n"}
                           :session {:account/type :account-type/superuser}})))

  (it "calls transact with generated tx-data if given valid data"
    (sut/handler {:datomic/conn @conn
                  :email-send-fn (constantly false)
                  :multipart-params {"email" "e"
                                     "name" "n"
                                     "counselor-id" (str @counselor-id)}
                  :session {:account/type :account-type/superuser}})
    (should-have-invoked @transact-stub-name
                         {:times 1, :with [@conn {:tx-data @tx-data}]}))

  (it "returns a 201 response with student data given valid data"
    (let [params {"email" "e", "name" "n", "counselor-id" (str @counselor-id)}
          req {:datomic/conn @conn
               :email-send-fn (constantly false)
               :multipart-params params
               :session {:account/type :account-type/superuser}}
          result (sut/handler req)]
      (should-have-invoked @find-stub-name {:times 1, :with [@db-val @student-id]})
      (should-have-invoked @result-stub-name {:times 1, :with [:student-data]})
      (should= {:status 201, :body :student-result}
               result))))
