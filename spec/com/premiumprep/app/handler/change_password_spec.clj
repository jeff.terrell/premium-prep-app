(ns com.premiumprep.app.handler.change-password-spec
  (:require [com.premiumprep.app.handler.change-password :as sut]
            [com.premiumprep.app.model.account :as account]
            [crypto.random :as random]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-have-invoked
                                 should= stub with with-stubs]]))

(describe "the change-password handler"
  (with-stubs)
  (around [it]
    (with-redefs [account/save-password (stub :save-password)]
      (it)))

  (it "returns a 403 when no session data"
    (should= {:status 403, :headers {}}
             (sut/handler nil)))

  (context "with a valid session"
    (it "returns a 400 given no password"
      (should= {:status 400
                :headers {}
                :body "Error: you did not provide a password."}
               (sut/handler {:multipart-params {}
                             :session {:db/id "1234"}})))

    (it "returns a 400 given a password of 7 characters"
      (should= {:status 400
                :headers {}
                :body "Error: your password must be at least 8 characters."}
               (sut/handler {:multipart-params {"password" "hello"}
                             :session {:db/id "1234"}})))

    (context "given a password of 8 characters"
      (it "returns a 204"
        (should= {:status 204, :headers {}}
                 (sut/handler {:multipart-params {"password" "hello there"}
                               :session {:db/id "123"}})))

      (it "transacts a new password"
        (let [pass "hello there"
              account-id 123454321]
          (sut/handler {:datomic/conn :db-conn
                        :multipart-params {"password" pass}
                        :session {:db/id (str account-id)}})
          (should-have-invoked :save-password
                               {:times 1
                                :with [:db-conn account-id pass]}))))))

