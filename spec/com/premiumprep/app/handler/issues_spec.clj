(ns com.premiumprep.app.handler.issues-spec
  (:require [com.premiumprep.app.handler.issues :as sut]
            [com.premiumprep.app.model.issue :as issue]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-be
                                 should-have-invoked should= should-not= stub
                                 with with-stubs]]))

(describe "issue-result"
  (with issue {:db/id 1234
               :issue/student {:db/id 7890}
               :issue/state {:db/ident :issue-state/new}})
  (with sut @(ns-resolve 'com.premiumprep.app.handler.issues 'issue-result))
  (with ret (@sut @issue))

  (it "stringifies the :db/id attribute"
    (should= "1234" (:db/id @ret)))

  (it "flattens and stringifies the [:issue/student :db/id] attribute"
    (should= "7890" (:db/id (:issue/student @ret))))

  (it "flattens the :issue/state enum"
    (should= :issue-state/new (:issue/state @ret))))

(describe "authorized?"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.issues 'authorized?))
  (it "returns true when active account type is superuser or counselor"
    (should= true (@sut {:account/type :account-type/superuser}))
    (should= true (@sut {:account/type :account-type/counselor})))
  (it "returns false when active account type is not superuser nor counselor"
    (should= false (@sut {:account/type :account-type/student}))))

(describe "the get issues handler"
  (with-stubs)

  (it "checks authorization and returns 403 if not authorized"
    (let [session :a-session]
      (with-redefs [sut/authorized? (stub :authorized? {:return false})]
        (sut/handler {:session session})
        (should-have-invoked :authorized? {:times 1, :with [session]}))))

  (context "when authorized"
    (with issue {:db/id 4321
                 :issue/text "issue text"
                 :issue/created-at ::created-at
                 :issue/state {:db/ident :issue-state/new}
                 :issue/student {:db/id 7890}})
    (around [it]
      (with-redefs [sut/authorized? (stub :authorized? {:return true})
                    d/db (stub :db {:return :db-val})
                    issue/all (stub :all-issues {:return [@issue]})
                    sut/filter-issues (stub :filter {:return [@issue]})]
        (it)))

    (it "returns a 200 status"
      (should= 200 (:status (sut/handler nil))))

    (it "returns a response with issue data"
      (with-redefs [sut/issue-result (stub :result {:return ::result})]
        (let [resp (sut/handler nil)
             expected [::result]]
         (should-have-invoked :all-issues {:times 1, :with [:db-val]})
         (should= expected (:body resp)))))))
