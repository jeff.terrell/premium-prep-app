(ns com.premiumprep.app.handler.student-spec
  (:require [com.premiumprep.app.handler.student :as sut]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.schema :as schema]
            [com.premiumprep.app.util :as util]
            [datomic.client.api :as d]
            [speclj.core :refer [describe it should-be should-have-invoked
                                 should= stub with with-stubs]])
  (:import java.util.Date))

(describe "the student handler"
  (with-stubs)
  (with db :datomic-db)

  (it "gets a database value from the given connection"
    (let [conn :a-datomic-connection,
          db-stub (stub :db {:return @db})
          student-find-stub (stub :student-find {:return true})
          student-id "123412341234"
          match {:path-params {:id student-id}}
          req {:datomic/conn conn
               :reitit.core/match match
               :session {:account/type :account-type/counselor}}]
      (with-redefs [d/db db-stub
                    student/find student-find-stub
                    student/listify-college-list-entries (stub :listify)
                    util/maybe-remove-eliminated-entries (stub :maybe-remove
                                                               {:invoke
                                                                (fn [_ s] s)})
                    util/student-result (stub :student-result)]
        (sut/handler req)
        (should-have-invoked :db {:times 1, :with [conn]})
        (should-have-invoked :listify {:times 1, :with [true]}))))

  (it "calls model.student/find"
    (let [conn :a-datomic-connection,
          db-stub (stub :db {:return @db})
          find-stub (stub :find)
          student-id "123412341234"
          match {:path-params {:id student-id}}
          req {:datomic/conn conn
               :reitit.core/match match
               :session {:account/type :account-type/counselor}}]
      (with-redefs [d/db db-stub
                    student/find find-stub]
        (sut/handler req)
        (should-have-invoked :find {:times 1}))))

  (it "refuses unauthorized requests"
    (let [client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"})
          opts {:db-name "student-spec"}
          _ (d/delete-database client opts)
          _ (d/create-database client opts)
          conn (d/connect client opts)
          _ (schema/install-schema! conn)
          match {:path-params {:id "1"}}
          req {:datomic/conn conn, :reitit.core/match match}
          actual (sut/handler req)]
      (should= 403 (:status actual))))

  ;; TODO: (it "returns 404 when the student could not be found")

  (it "returns a 200 response with student info"
    (let [client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"})
          opts {:db-name "student-spec"}
          _ (d/delete-database client opts)
          _ (d/create-database client opts)
          conn (d/connect client opts)
          _ (schema/install-schema! conn)
          account {:db/id "account"
                   :account/email "email@email.com"
                   :account/password "should NOT be returned"
                   :account/full-name "Joseph Student"
                   :account/active? true
                   :account/state :account-state/uninitialized
                   :account/type :account-type/student}
          caccount {:db/id "caccount"
                    :account/password "should NOT be returned"
                    :account/email "counselor@email.com"
                    :account/full-name "The Counselor (dum dum DUM)"
                    :account/active? true
                    :account/state :account-state/initialized
                    :account/type :account-type/counselor}
          counselor {:db/id "counselor"
                     :counselor/account "caccount"}
          doc1 {:db/id "doc1"
                :document/uploaded-at (Date. 120 5 13)
                :document/s3-path "an-s3-path"
                :document/name "My Essay"}
          student {:db/id "student"
                   :student/account "account"
                   :student/counselor "counselor"
                   :student/joined-at (Date. 119 7 4)
                   :student/graduation-year 2024
                   :student/high-school "East Chapel Hill High"
                   :student/documents [doc1]
                   :student/profile-comments []
                   :student/document-list-comments []}
          tx-data [account caccount counselor student]
          caccount (dissoc caccount :account/active?)
          {tempids :tempids} (d/transact conn {:tx-data tx-data})
          eid (tempids "student")
          aid (tempids "account")
          cid (tempids "counselor")
          did (tempids "doc1")
          account (assoc account :db/id aid)
          estudent (assoc student
                          :student/account account
                          :student/counselor counselor
                          :db/id (str eid)
                          :student/college-lists {}
                          :issue/_student [])
          estudent (assoc-in estudent [:student/documents 0 :db/id] (str did))
          expected {:status 200
                    :body [estudent]}
          match {:path-params {:id (str eid)}}
          req {:datomic/conn conn
               :reitit.core/match match
               :session {:account/type :account-type/counselor}}
          actual (sut/handler req)
          abody (-> actual (:body "nil"))
          abody (update-in abody [:student/documents 0 :db/id] str)
          aaccount (:student/account abody)]

      (should-be map? actual)
      (should= (-> expected keys sort) (-> actual keys sort))
      (should= (:status expected) (:status actual))
      (should= (:headers expected) (:headers actual))
      (should-be map? abody)
      (should= (-> estudent keys set) (-> abody keys set))
      (should= (:db/id estudent) (:db/id abody))
      (should= (:student/joined-at estudent) (:student/joined-at abody))
      (should= (:student/graduation-year estudent) (:student/graduation-year abody))
      (should= (:student/high-school estudent) (:student/high-school abody))
      (should= (:student/documents estudent) (:student/documents abody))
      (should= (-> estudent :student/account keys set (disj :account/password))
               (-> abody :student/account keys set))
      (should= (:account/email account) (:account/email aaccount))
      (should= (:account/full-name account) (:account/full-name aaccount))
      (should= (:account/type account) (:account/type aaccount))))

  (it "returns student info when the same student requests it"
    (let [client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"})
          opts {:db-name "student-spec"}
          _ (d/delete-database client opts)
          _ (d/create-database client opts)
          conn (d/connect client opts)
          _ (schema/install-schema! conn)
          account {:db/id "account"
                   :account/email "email@email.com"
                   :account/password "should NOT be returned"
                   :account/full-name "Joseph Student"
                   :account/active? true
                   :account/state :account-state/uninitialized
                   :account/type :account-type/student}
          caccount {:db/id "caccount"
                    :account/password "should NOT be returned"
                    :account/email "counselor@email.com"
                    :account/full-name "The Counselor (dum dum DUM)"
                    :account/active? true
                    :account/state :account-state/initialized
                    :account/type :account-type/counselor}
          counselor {:db/id "counselor"
                     :counselor/account "caccount"}
          doc1 {:db/id "doc1"
                :document/uploaded-at (Date. 120 5 13)
                :document/s3-path "an-s3-path"
                :document/name "My Essay"}
          student {:db/id "student"
                   :student/account "account"
                   :student/counselor "counselor"
                   :student/joined-at (Date. 119 7 4)
                   :student/graduation-year 2024
                   :student/high-school "East Chapel Hill High"
                   :student/documents [doc1]
                   :student/profile-comments []
                   :student/document-list-comments []}
          tx-data [account caccount counselor student]
          caccount (dissoc caccount :account/active?)
          {tempids :tempids} (d/transact conn {:tx-data tx-data})
          eid (tempids "student")
          aid (tempids "account")
          cid (tempids "counselor")
          did (tempids "doc1")
          account (assoc account :db/id aid)
          estudent (assoc student
                          :student/account account
                          :student/counselor counselor
                          :student/college-lists {}
                          :db/id (str eid))
          estudent (assoc-in estudent [:student/documents 0 :db/id] (str did))
          expected {:status 200
                    :body [estudent]}
          match {:path-params {:id (str eid)}}
          req {:datomic/conn conn
               :reitit.core/match match
               :session {:account/type :account-type/student
                         :db/id (str aid)}}
          actual (sut/handler req)
          abody (-> actual (:body "nil"))
          abody (update-in abody [:student/documents 0 :db/id] str)
          aaccount (:student/account abody)]

      (should-be map? actual)
      (should= (-> expected keys sort) (-> actual keys sort))
      (should= (:status expected) (:status actual))
      (should= (:headers expected) (:headers actual))
      (should-be map? abody)
      (should= (-> estudent keys set) (-> abody keys set))
      (should= (:db/id estudent) (:db/id abody))
      (should= (:student/joined-at estudent) (:student/joined-at abody))
      (should= (:student/graduation-year estudent) (:student/graduation-year abody))
      (should= (:student/high-school estudent) (:student/high-school abody))
      (should= (-> estudent :student/account keys set (disj :account/password))
               (-> abody :student/account keys set))
      (should= (:account/email account) (:account/email aaccount))
      (should= (:account/full-name account) (:account/full-name aaccount))
      (should= (:account/type account) (:account/type aaccount)))))
