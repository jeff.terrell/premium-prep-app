(ns com.premiumprep.app.handler.delete-college-list-entry-spec
  (:require
   [com.premiumprep.app.handler.delete-college-list-entry :as sut]
   [com.premiumprep.app.model.student :as student]
   [com.premiumprep.app.util :as util]
   [datomic.client.api :as d]
   [speclj.core :refer [around context describe it should-contain
                        should-have-invoked should= stub with with-stubs]]
   [speclj.stub :refer [first-invocation-of]]))

(describe "delete-cle/missing-required-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.delete-college-list-entry
               'missing-required-params-error))
  (with good-params {"student-id" "123"
                     "college-id" "234"})

  (it "detects a missing param"
    (should= "Missing parameter: student-id"
             (@sut (dissoc @good-params "student-id")))
    (should= "Missing parameter: college-id"
             (@sut (dissoc @good-params "college-id"))))

  (it "detects multiple missing params"
    (should= "Missing parameters: college-id, student-id"
             (@sut (dissoc @good-params "college-id" "student-id")))))

(describe "delete-cle/nonnumeric-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.delete-college-list-entry
               'nonnumeric-params-error))
  (with good-params {"student-id" "123"
                     "college-id" "234"})

  (it "detects a nonnumeric param"
    (should= "Non-numeric parameter: student-id"
             (@sut (assoc @good-params "student-id" "abc")))
    (should= "Non-numeric parameter: college-id"
             (@sut (assoc @good-params "college-id" "12e4"))))

  (it "detects multiple nonnumeric params"
    (should= "Non-numeric parameters: student-id, college-id"
             (@sut {}))))

(describe "delete-cle/authorized?"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.delete-college-list-entry
               'authorized?))

  (it "rejects unauthenticated users"
    (should= false (boolean (@sut nil))))

  (it "rejects students"
    (should= false (@sut {:account/type :account-type/student})))

  (it "allows authorized users"
    (should= true (@sut {:account/type :account-type/superuser}))
    (should= true (@sut {:account/type :account-type/counselor}))))

(describe "the delete-college-list-entry handler"
  (with-stubs)
  (it "returns status 400 if required categories are missing"
    (with-redefs [sut/missing-required-params-error (stub :stub
                                                          {:return "errmsg"})]
      (should= 400 (:status (sut/handler {:multipart-params :the-params})))
      (should-have-invoked :stub {:times 1, :with [:the-params]})))

  (context "with no missing categories"
    (around [it]
      (with-redefs [sut/missing-required-params-error (stub :mrpe)]
        (it)))

    (it "returns status 400 if any id params are not numeric"
      (with-redefs [sut/nonnumeric-params-error (stub :stub {:return "errmsg"})]
        (should= 400 (:status (sut/handler {:multipart-params :the-params})))
        (should-have-invoked :stub {:times 1, :with [:the-params]})))

    (context "with numeric id params"
      (around [it]
        (with-redefs [sut/nonnumeric-params-error (stub :nnpe)]
          (it)))

      (it "returns status 403 if request not authorized"
        (let []
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :stub {:return nil})]
            (should= 403 (:status (sut/handler {:datomic/conn :db-conn
                                                :multipart-params :the-params
                                                :session :the-session})))
            (should-have-invoked :db {:times 1, :with [:db-conn]})
            (should-have-invoked :stub
                                 {:times 1
                                  :with [:the-session]}))))

      (context "with an authorized request"
        (around [it]
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :authorized? {:return true})]
            (it)))

        (it "returns status 404 if referenced college-list-entry not found"
          (let [student-id 1234
                college-id 2345
                params {"student-id" (str student-id)
                        "college-id" (str college-id)
                        "category" "likely"}]
            (with-redefs [sut/find-entry (stub :stub)]
              (should= 404 (:status (sut/handler {:multipart-params params})))
              (should-have-invoked :stub
                                   {:times 1
                                    :with [:db-val student-id college-id]}))))

        (context "with a found college-list-entry"
          (with student-id 1234)
          (with college-id 2345)
          (with params {"student-id" (str @student-id)
                        "college-id" (str @college-id)
                        "category" "target"})
          (with atype :account-type/superuser)
          (with sut #(sut/handler {:datomic/conn :db-conn
                                   :multipart-params @params
                                   :session {:account/type @atype}}))
          (with entry-id 3456)

          (around [it]
            (with-redefs [sut/find-entry (stub :stub {:return @entry-id})]
              (it)))

          (it "calls happy-path fn and returns its returned value"
            (with-redefs [sut/happy-path (stub :stub {:return :hprv})]
              (should= :hprv (@sut))
              (should-have-invoked :stub
                                   {:times 1
                                    :with [@atype :db-conn @student-id
                                           @entry-id]}))))))))

(describe "delete-cle/happy-path"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.delete-college-list-entry
               'happy-path))
  (with student-id 6789)
  (with entry-id 9876)
  (with response (@sut nil :db-conn @student-id @entry-id))

  (around [it]
    (with-redefs [d/db (stub :db {:return :db-val})
                  d/transact (stub :transact)]
      (it)))

  (it "returns status 200 response"
    (with-redefs [d/transact (stub :transact)
                  student/find (stub :find-student)]
      (should= 200 (:status @response))))

  (it "returns an updated college list"
    (with-redefs [d/pull (stub :pull)
                  student/find (stub :student)
                  student/listify-college-list-entries (stub :listify)
                  util/student-result (stub :result
                                            {:return {:student/college-lists
                                                      :new-list}})]
      (should= :new-list (:body @response))))

  (it "includes a datom to update the entry's eliminated? attribute"
    (with-redefs [d/db (stub :db)
                  student/find (stub :find)
                  d/transact (stub :transact)]
      @response
      (should-have-invoked :transact)
      (let [invocation (first-invocation-of :transact)
            tx-data (-> invocation second :tx-data)]
        (should-contain [:db/add @entry-id :college-list-entry/eliminated? true]
                        tx-data)))))
