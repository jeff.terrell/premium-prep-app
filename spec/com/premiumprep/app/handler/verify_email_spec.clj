(ns com.premiumprep.app.handler.verify-email-spec
  (:require [com.premiumprep.app.email :as email]
            [com.premiumprep.app.handler.verify-email :as sut]
            [com.premiumprep.app.model.account :as account]
            [crypto.random :as crypto]
            [datomic.client.api :as d]
            [speclj.core :refer
             [around context describe it should-have-invoked should= stub with
              with-stubs]]))

(describe "the verify-email handler"
  (with-stubs)
  (with token "an-unguessable-token")
  (around [it]
    (with-redefs [account/find-by-token (stub :find-by-token)
                  crypto/url-part (stub :url-part {:return @token})
                  d/db (stub :db {:return :db-val})
                  d/transact (stub :transact)
                  email/verified-email (stub :verified-email)]
      (it)))

  (it "looks up the given token in the database"
    (sut/handler {:datomic/conn :db-conn
                  :query-params {"token" @token}})
    (should-have-invoked :db {:times 1, :with [:db-conn]})
    (should-have-invoked :find-by-token {:times 1, :with [:db-val @token]}))

  (it "returns 400 for requests lacking a valid token"
    (should= {:status 400
              :headers {}
              :body (str "Error: the given token could not be found in the "
                         "database.\n\nPlease ensure that you are at the "
                         "address included in your verification email. If so, "
                         "you may need to send a new verification email.\n")}
             (sut/handler {:query-params {}})))

  (context "given a valid token"
    (with account-id 1234555555)
    (with email "an-email-address@e.com")
    (with account {:db/id @account-id
                   :account/email @email})
    (around [it]
            (with-redefs [account/find-by-token (stub :find-by-token
                                                      {:return @account})]
              (it)))

    (it "returns 204"
      (should= {:status 204, :headers {}}
               (sut/handler {:query-params {"token" @token}})))

    (it "sends a 'your email address has been verified' email"
      (sut/handler {:email-send-fn :send-email
                    :query-params {"token" @token}})
      (should-have-invoked :verified-email {:times 1
                                            :with [:send-email @email]}))

    (it "clears email-unverified state and token on account"
      (let [conn :db-conn
            tx-data [[:db/retract @account-id :account/token @token]
                     [:db/add @account-id :account/state
                      :account-state/initialized]
                     [:db/add "datomic.tx" :transaction/namespace
                      "com.premiumprep.app.handler.verify-email"]]]
        (sut/handler {:datomic/conn conn
                      :email-send-fn :send-email
                      :query-params {"token" @token}})
        (should-have-invoked :transact
                             {:times 1, :with [conn {:tx-data tx-data}]})))))
