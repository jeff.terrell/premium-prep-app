(ns com.premiumprep.app.handler.update-college-list-entry-field-spec
  (:require [clojure.instant :as instant]
            [com.premiumprep.app.handler.update-college-list-entry-field :as sut]
            [com.premiumprep.app.model.student :as student]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-have-invoked
                                 should= stub with with-stubs]]))

(describe "update-cle-field/missing-required-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-field
               'missing-required-params-error))
  (with good-params {"student-id" "123"
                     "college-id" "234"
                     "field" "location"
                     "value" "someplace wonderful"})

  (it "detects a missing param"
    (should= "Missing parameter: student-id"
             (@sut (dissoc @good-params "student-id")))
    (should= "Missing parameter: college-id"
             (@sut (dissoc @good-params "college-id")))
    (should= "Missing parameter: field"
             (@sut (dissoc @good-params "field")))
    (should= "Missing parameter: value"
             (@sut (dissoc @good-params "value"))))

  (it "detects multiple missing fields"
    (should= "Missing parameters: field, value"
             (@sut (dissoc @good-params "field" "value")))))

(describe "update-cle-field/nonnumeric-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-field
               'nonnumeric-params-error))
  (with good-params {"student-id" "123"
                     "college-id" "234"})

  (it "detects a nonnumeric param"
    (should= "Non-numeric parameter: student-id"
             (@sut (assoc @good-params "student-id" "abc")))
    (should= "Non-numeric parameter: college-id"
             (@sut (assoc @good-params "college-id" "12e4"))))

  (it "detects multiple nonnumeric params"
    (should= "Non-numeric parameters: student-id, college-id"
             (@sut {}))))

(describe "update-cle-field/authorized?"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-field
               'authorized?))

  (it "rejects unauthenticated users"
    (should= false (boolean (@sut nil nil nil))))

  (it "rejects students for counselor-only fields"
    (should= false (boolean (@sut nil {:account/type :account-type/student}
                             {"field" "counselor-notes"}))))

  (it "rejects students who aren't the same student as in the request body"
    (let [id 1234
          acct-id 432423
          session {:account/type :account-type/student, :db/id (str acct-id)}
          params {"field" "student-notes", "student-id" (str id)}
          student {:db/id (str id "1")}]
      (with-redefs [student/find-by-account-id (stub :find {:return student})]
        (should= false (@sut :db-val session params))
        (should-have-invoked :find {:times 1, :with [:db-val acct-id]}))))

  (it "allows authorized users"
    (let [id 1234
          acct-id 432423
          session {:account/type :account-type/student, :db/id (str acct-id)}
          params {"field" "student-notes", "student-id" (str id)}
          student {:db/id (str id)}]
      (with-redefs [student/find-by-account-id (stub :find {:return student})]
        (should= true (@sut :db-val session params))
        (should-have-invoked :find {:times 1, :with [:db-val acct-id]})))))

(describe "update-cle-field/invalid-field-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-field
               'invalid-field-error))
  (it "detects invalid field names"
    (should= "Invalid field" (@sut {"field" "abc"})))
  (it "accepts valid field names"
    (should= nil (@sut {"field" "student-notes"}))))

(describe "update-cle-field/invalid-value-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-field
               'invalid-value-error))

  (it "detects invalid booleans"
    (should= "Invalid boolean value"
             (@sut {"field" "visited?", "value" "abc"})))

  (it "detects invalid instants"
    (should= "Invalid instant value"
             (@sut {"field" "app-deadline", "value" "bca"})))

  (it "accepts valid values"
    (should= nil (@sut {"field" "app-deadline", "value" "2020-04-04"}))
    (should= nil (@sut {"field" "visited?", "value" "true"}))
    (should= nil (@sut {"field" "visited?", "value" "false"}))))

(describe "update-cle-field/happy-path"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-field
               'happy-path))
  (with entry-id 9876)
  (with params {"field" "student-notes", "value" "new notes"})
  (with response (@sut :db-conn @entry-id @params))

  (around [it]
    (with-redefs [d/transact (stub :transact)]
      (it)))

  (it "returns status 204 response"
    (should= 204 (:status @response)))

  (it "calls transact"
    @response
    (let [datom [:db/add @entry-id :college-list-entry/student-notes
                 "new notes"]
          tx-datom [:db/add "datomic.tx" :transaction/namespace
                    "com.premiumprep.app.handler.update-college-list-entry-field"]]
      (should-have-invoked :transact
                           {:times 1
                            :with [:db-conn {:tx-data [datom tx-datom]}]})))

  (it "parses boolean fields"
    (let [response (@sut :db-conn @entry-id {"field" "visited?", "value" "true"})
          datom [:db/add @entry-id :college-list-entry/visited? true]
          tx-datom [:db/add "datomic.tx" :transaction/namespace
                    "com.premiumprep.app.handler.update-college-list-entry-field"]]
      (should-have-invoked :transact
                           {:times 1
                            :with [:db-conn {:tx-data [datom tx-datom]}]})))

  (it "parses instant fields"
    (let [ts "2020-04-04"
          inst (instant/read-instant-date (str ts "Z"))
          response (@sut :db-conn @entry-id {"field" "app-deadline", "value" ts})
          datom [:db/add @entry-id :college-list-entry/app-deadline inst]
          tx-datom [:db/add "datomic.tx" :transaction/namespace
                    "com.premiumprep.app.handler.update-college-list-entry-field"]]
      (should-have-invoked :transact
                           {:times 1
                            :with [:db-conn {:tx-data [datom tx-datom]}]}))))

(describe "the update-college-list-entry-field handler"
  (with-stubs)
  (it "returns status 400 if required fields are missing"
    (with-redefs [sut/missing-required-params-error (stub :stub
                                                          {:return "errmsg"})]
      (should= 400 (:status (sut/handler {:multipart-params :the-params})))
      (should-have-invoked :stub {:times 1, :with [:the-params]})))

  (context "with no missing fields"
    (around [it]
      (with-redefs [sut/missing-required-params-error (stub :mrpe)]
        (it)))

    (it "returns status 400 if any id fields are not numeric"
      (with-redefs [sut/nonnumeric-params-error (stub :stub {:return "errmsg"})]
        (should= 400 (:status (sut/handler {:multipart-params :the-params})))
        (should-have-invoked :stub {:times 1, :with [:the-params]})))

    (context "with numeric id fields"
      (around [it]
        (with-redefs [sut/nonnumeric-params-error (stub :nnpe)]
          (it)))

      (it "returns status 403 if request not authorized"
        (let []
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :stub {:return nil})]
            (should= 403 (:status (sut/handler {:datomic/conn :db-conn
                                                :multipart-params :the-params
                                                :session :the-session})))
            (should-have-invoked :db {:times 1, :with [:db-conn]})
            (should-have-invoked :stub
                                 {:times 1
                                  :with [:db-val :the-session :the-params]}))))

      (context "with an authorized request"
        (around [it]
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :authorized? {:return true})]
            (it)))

        (it "returns status 400 if field name invalid"
          (with-redefs [sut/invalid-field-error (stub :stub {:return "err"})]
            (should= 400
                     (:status (sut/handler {:multipart-params :the-params})))
            (should-have-invoked :stub {:times 1, :with [:the-params]})))

        (context "with a valid field name"
          (around [it]
            (with-redefs [sut/invalid-field-error (stub :ife)]
              (it)))

          (it "returns status 404 if referenced college-list-entry not found"
            (let [student-id 1234
                  college-id 2345
                  params {"student-id" (str student-id)
                          "college-id" (str college-id)
                          "field" "student-notes"
                          "value" "new notes"}]
              (with-redefs [sut/find-entry (stub :stub)]
                (should= 404 (:status (sut/handler {:multipart-params params})))
                (should-have-invoked :stub
                                     {:times 1
                                      :with [:db-val student-id college-id]}))))

          (context "with a found college-list-entry"
            (with student-id 1234)
            (with college-id 2345)
            (with params {"student-id" (str @student-id)
                          "college-id" (str @college-id)
                          "field" "student-notes"
                          "value" "new notes"})
            (with sut #(sut/handler {:datomic/conn :db-conn
                                     :multipart-params @params}))
            (with entry-id 3456)

            (around [it]
              (with-redefs [sut/find-entry (stub :stub {:return @entry-id})]
                (it)))

            (it "returns status 400 if value is invalid"
              (with-redefs [sut/invalid-value-error (stub :stub {:return "err"})]
                (should= 400 (:status (@sut)))
                (should-have-invoked :stub {:times 1, :with [@params]})))

            (context "with a valid value"
              (around [it]
                (with-redefs [sut/invalid-value-error (stub :ive)]
                  (it)))

              (it "calls happy-path fn and returns its returned value"
                (with-redefs [sut/happy-path (stub :stub {:return :hprv})]
                  (should= :hprv (@sut))
                  (should-have-invoked :stub
                                       {:times 1
                                        :with [:db-conn @entry-id @params]}))))))))))
