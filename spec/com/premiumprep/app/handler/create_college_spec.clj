(ns com.premiumprep.app.handler.create-college-spec
  (:require [com.premiumprep.app.email :as email]
            [com.premiumprep.app.handler.create-college :as sut]
            [com.premiumprep.app.model.college :as college]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should= should-be
                                 should-not= should-have-invoked stub with
                                 with-stubs]]))

(describe "create-college/authorized?"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.create-college
                         'authorized?))
  (it "accepts superusers"
    (should= true (@sut {:account/type :account-type/superuser})))
  (it "rejects non-superusers"
    (should= false (@sut {:account/type :account-type/counselor}))))

(describe "create-college/bad-request-message"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.create-college
                         'bad-request-message))

  (it "detects a missing param"
    (should= "The following required parameter is missing: average-act"
             (@sut {"name" "a"
                    "enrollment" "b"
                    "admit-rate" "c"
                    "average-sat" "d"})))

  (it "detects multiple missing params"
    (should= "The following required parameters are missing: average-act, name"
             (@sut {"enrollment" "b"
                    "admit-rate" "c"
                    "average-sat" "d"}))))

(describe "the create-college handler"
  (with-stubs)

  (it "checks authorization and returns 403 if failed"
    (with-redefs [sut/authorized? (stub :auth {:return false})]
      (should= 403 (:status (sut/handler {:session :the-session})))
      (should-have-invoked :auth {:times 1, :with [:the-session]})))

  (context "with authorization"
    (around [it]
      (with-redefs [sut/authorized? (stub :auth {:return true})]
        (it)))

    (it "checks bad-request-message and returns a 400 response if request is bad"
      (with-redefs [sut/bad-request-message (stub :stub {:return "err"})]
        (let [params {"param1" "hello"}
              resp (sut/handler {:multipart-params params})]
          (should= 400 (:status resp))
          (should= "err" (-> resp :body :error))
          (should-have-invoked :stub {:times 1, :with [params]}))))

    (context "with a valid request"
      (with college-name "<name of university>")
      (with params
        {"name" @college-name
         "enrollment" "b"
         "admit-rate" "c"
         "average-sat" "d"
         "average-act" "e"})
      (with sut
        (fn [req]
          (sut/handler
            (assoc req
                   :email-send-fn :email-sender
                   :multipart-params @params))))

      (it "checks for a college name conflict and returns 409 if so"
        (with-redefs [sut/college-name-conflict? (stub :stub {:return true})
                      d/db (stub :db {:return :db-val})]
          (should= 409 (:status (@sut nil)))
          (should-have-invoked :stub {:times 1, :with [:db-val @college-name]})))

      (context "with no college name conflict"
        (with id 777)
        (with college-result {:db/id id, :college/name "name"})
        (around [it]
          (with-redefs [sut/college-name-conflict? (stub :college-name-conflict?
                                                         {:return false})
                        d/db (stub :db {:return :db-val})
                        d/transact (stub :transact
                                         {:return {:tempids {"college" @id}}})
                        college/find (stub :find-college
                                           {:return @college-result})
                        email/college-added (stub :send-email)]
            (it)))

        (it "returns 201"
          (should= 201 (:status (@sut nil))))

        (it "sends an email"
          (@sut nil)
          (should-have-invoked :send-email {:times 1
                                            :with [:email-sender @params]}))

        (it "transacts a college"
          (with-redefs [d/transact (stub :transact)]
            (@sut {:datomic/conn :db-conn})
            (let [college-tx-map {:db/id "college"
                                  :college/name @college-name
                                  :college/enrollment "b"
                                  :college/admit-rate "c"
                                  :college/average-sat "d"
                                  :college/average-act "e"}
                  tx-data [:db/add "datomic.tx" :transaction/namespace
                           "com.premiumprep.app.handler.create-college"]
                  tx-data {:tx-data [college-tx-map tx-data]}]
              (should-have-invoked :transact {:times 1
                                              :with [:db-conn tx-data]}))))

        (it "returns college info in a standard way"
          (let [resp (@sut nil)]
            (should= (update @college-result :db/id str)
                     (:body resp))
            (should-have-invoked :find-college {:times 1
                                                :with [:db-val @id]})))))))
