(ns com.premiumprep.app.handler.logout-spec
  (:require [com.premiumprep.app.handler.logout :as sut]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-be
                                 should-contain should-have-invoked should= stub
                                 with with-stubs]]
            [speclj.stub :refer [first-invocation-of]]
            [com.premiumprep.app.model.session :as session]))

(describe "the logout handler"
  (with-stubs)

  (it "returns a 204 response"
    (should= 204 (:status (sut/handler nil))))

  (it "destroys the session from the database"
    (let [session-id 987432987
          conn :db-connection]
      (with-redefs [d/db (stub :db)
                    d/q (stub :q)
                    d/transact (stub :transact)
                    session/find-all-for-account-id
                    (stub :find-sessions {:return [{:session/id session-id}]})
                    session/delete (stub :delete)]
        (sut/handler {:datomic/conn conn, :session {:db/id "123454321"}})
        (should-have-invoked :delete {:times 1, :with [conn session-id]}))))

  (it "clears the user's cookie"
    (let [resp (sut/handler nil)]
      (should-contain :session resp)
      (should-be nil? (:session resp)))))
