(ns com.premiumprep.app.handler.create-issue-spec
  (:require [com.premiumprep.app.email :as email]
            [com.premiumprep.app.handler.create-issue :as sut]
            [com.premiumprep.app.model.issue :as issue]
            [com.premiumprep.app.model.student :as student]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should= should-be
                                 should-not= should-have-invoked stub with
                                 with-stubs]]))

(describe "create-issue"
  (with-stubs)

  (it "calls d/transact with reasonable args and returns the new db/id"
    (let [text "this is an issue"
          student-id 1234
          params {"text" text, "student-id" (str student-id)}
          sut @(ns-resolve 'com.premiumprep.app.handler.create-issue
                           'create-issue)
          issue-map {:db/id "issue"
                     :issue/text text
                     :issue/created-at ::created-at
                     :issue/state :issue-state/new
                     :issue/student student-id}
          tx-datom [:db/add "datomic.tx" :transaction/namespace
                    "com.premiumprep.app.handler.create-issue"]
          tx-map {:tx-data [issue-map tx-datom]}
          id 43214321]

      (with-redefs [d/transact (stub :transact
                                     {:return {:tempids {"issue" id}}})
                    sut/created-at (stub :created-at {:return ::created-at})]
        (should= id (sut :db-connection params))
        (should-have-invoked :transact
                             {:times 1, :with [:db-connection tx-map]})))))

(describe "the create-issue handler"
  (with-stubs)
  (with student-id 12)
  (with student-acct-id 123)
  (with counselor-id 1234)
  (with counselor-acct-id 12345)
  (with email "iama-student@email.com")
  (with full-name "Iama Student")
  (with student {:db/id @student-id
                 :student/account {:db/id @student-acct-id
                                   :account/email @email
                                   :account/full-name @full-name}
                 :student/counselor {:db/id @counselor-id
                                     :counselor/account {:db/id
                                                         @counselor-acct-id}}})
  (around [it]
    (with-redefs [d/db (stub :db {:return :db-val})
                  student/find (stub :find-student {:return @student})]
      (it)))

  (it "returns 404 if given student not given or not found"
    (with-redefs [student/find (stub :find-student {:return nil})]
      (should= 404 (:status (sut/handler nil)))
      (should= 404 (:status (sut/handler
                              {:multipart-params {"student-id" "1"}})))))

  (it "rejects counselors who don't counsel the given student"
    (should= 403 (:status (sut/handler
                            {:multipart-params {"student-id" "1"}
                             :session {:db/id (str @counselor-acct-id "0")}}))))

  (it "accepts counselors who do counsel the given student"
    (should-not= 403 (:status (sut/handler
                                {:session {:db/id (str @counselor-acct-id)}}))))

  (it "rejects students who aren't the given student"
    (should= 403 (:status (sut/handler
                            {:multipart-params {"student-id" "1"}
                             :session {:db/id (str @student-acct-id "0")}}))))

  (it "rejects students who are the given student"
    (should= 403 (:status (sut/handler
                            {:multipart-params {"student-id" "1"}
                             :session {:db/id (str @student-acct-id)}}))))

  (it "accepts administrators"
    (should-not= 403 (:status (sut/handler
                                {:multipart-params {"student-id" "1"}
                                 :session {:account/type :account-type/superuser
                                           :db/id "1"}}))))

  (context "with an authorized user"
    (with sut (fn [req]
                (sut/handler (merge {:session
                                     {:account/type :account-type/superuser
                                      :db/id "1"}}
                                    req))))

    (it "returns 400 with explanation if given text is empty"
      (let [params {"student-id" "1"}
            response (@sut {:multipart-params params
                            })]
        (should= 400 (:status response))
        (should= "Error: no issue text supplied."
                 (-> response :body :error))))

    (context "with a valid request"
      (with issue-text "some issue text")
      (with params {"text" @issue-text
                    "student-id" "1"})
      (with new-id 987)
      (with issue {:db/id @new-id
                   :issue/text @issue-text
                   :issue/created-at ::created-at
                   :issue/state {:db/ident :issue-state/new}
                   :issue/student {:db/id @student-id}})
      (with sut2 (fn []
                   (with-redefs [issue/find (stub :find-issue
                                                  {:return @issue})
                                 sut/create-issue (stub :create-issue
                                                        {:return @new-id})
                                 sut/created-at (stub :created-at
                                                      {:return ::created-at})
                                 email/new-issue (stub :email)]
                     (@sut {:datomic/conn :db-connection
                            :multipart-params @params}))))

      (it "creates an issue"
        (@sut2)
        (should-have-invoked :create-issue {:times 1
                                            :with [:db-connection @params]}))

      (it "returns 201"
        (should= 201 (:status (@sut2))))

      (it "sends an email"
        (@sut2)
        (should-have-invoked :email))

      (it "returns created issue as data"
        (let [body-val (:body (@sut2))]
          (should= (-> @issue
                       (update :db/id str)
                       (update-in [:issue/student :db/id] str)
                       (update :issue/state :db/ident))
                   body-val)
          (should-have-invoked :find-issue
                               {:times 1, :with [:db-val @new-id]}))))))
