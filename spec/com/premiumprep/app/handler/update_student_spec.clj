(ns com.premiumprep.app.handler.update-student-spec
  (:require
   [com.premiumprep.app.handler.update-student :as sut]
   [com.premiumprep.app.model.student :as student]
   [com.premiumprep.app.util :as util]
   [datomic.client.api :as d]
   [speclj.core :refer [around context describe it should-be should-contain
                        should-have-invoked should-not-have-invoked should= stub
                        with with-stubs]]))

(describe "create-student/params->tx-data"
  (with-stubs)

  (with student-eid "12341234")
  ;; "function under test"
  (with fut #(@(ns-resolve 'com.premiumprep.app.handler.update-student
                           'params->tx-data) :db @student-eid %))

  (it "returns an error message if required params are missing"
    (should= [false [(str "The following required parameters are missing:"
                          " counselor-id, email")]]
             (@fut {"active" "false"}))
    (should= [false
              ["The following required parameter is missing: counselor-id"]]
             (@fut {"active" "false", "email" "e"})))

  (it "returns an error message if counselor-id is not numeric"
    (let [counselor-id "cid"]
      (should= [false [(format "The given counselor-id value (%s) is not numeric"
                               counselor-id)]]
               (@fut {"email" "e"
                      "counselor-id" counselor-id
                      "active" "true"}))))

  (it "returns an error message if active is not boolean"
    (let [active "not-a-bool"]
      (should-contain
        (format "The given active value (%s) is not boolean" active)
        (second (@fut {"email" "e", "active" active})))))

  (it "returns an error message if counselor-id is not found in db"
    (let [db-val :the-database
          cid "1234"
          q-stub (stub :q {:return #{}})]
      (with-redefs [d/q q-stub]
        (should= [false [(str "The given counselor-id value (" cid
                              ") is not found in the database")]]
                 (@fut {"email" "e", "counselor-id" cid, "active" "true"}))
        (should-have-invoked :q))))

  (it "returns multiple error messages if there are multiple errors"
    (let [counselor-id "cid"]
      (should= [false ["The following required parameter is missing: email"
                       (format "The given counselor-id value (%s) is not numeric"
                               counselor-id)]]
               (@fut {"counselor-id" counselor-id, "active" "true"}))))

  (it "does not include multiple error messages for the same field"
    (should= [false
              ["The following required parameter is missing: counselor-id"]]
             (@fut {"email" "e", "active" "true"})))

  (context "with a found counselor-id"
    (with counselor-id 1234)
    (with q-stub-name :datomic-query)
    (with q-stub (stub @q-stub-name {:return #{[@counselor-id]}}))
    (with account-eid 54321)
    (with find-ret {:student/account {:db/id @account-eid}})
    (with find-stub-name :student-find)
    (with find-stub (stub @find-stub-name {:return @find-ret}))
    (around [it] (with-redefs [d/q @q-stub
                               student/find @find-stub]
                   (it)))
    (with email "person@email.com")
    (with active? true)

    (with params {"email" @email
                  "active" (str @active?)
                  "counselor-id" (str @counselor-id)})

    (it "returns a datom with the new email address"
      (let [tx-data (second (@fut @params))
            account-datom (first (filter #(= :account/email (nth % 2)) tx-data))]
        (should-be some? account-datom)
        (should= [:db/add @account-eid :account/email @email]
                 account-datom)))

    (it "returns a tx-map for a new student entity with given values"
      (should-contain [:db/add @account-eid :account/email @email]
                      (second (@fut @params)))
      (should-contain [:db/add @account-eid :account/active? @active?]
                      (second (@fut @params))))))

(describe "the update-student handler"
  (with-stubs)
  (with conn :datomic-connection)
  (with db-stub-name :datomic-db)
  (with db-val :the-database-value)
  (with db-stub (stub @db-stub-name {:return @db-val}))
  (with counselor-id 1234)
  (with student-id 4321)
  (with q-stub-name :datomic-q)
  (with q-stub (stub @q-stub-name {:return #{[@counselor-id]}}))
  (with find-stub-name :student-find)
  (with find-stub (stub @find-stub-name {:return :student-data}))
  (with result-stub-name :student-result)
  (with result-stub (stub @result-stub-name {:return :student-result}))
  (with tx-data [{:db/id "account"}])
  (with tx-data-stub-name :params->tx-data)
  (with tx-data-stub (stub @tx-data-stub-name {:return @tx-data}))
  (with transact-stub-name :datomic-transact)
  (with transact-stub (stub @transact-stub-name
                            {:return {:tempids {"student" @student-id}}}))
  (with make-req (fn [params atype]
                   {:datomic/conn @conn
                    :multipart-params params
                    :reitit.core/match {:path-params {:id (str @student-id)}}
                    :session {:account/type atype}}))
  (around [it] (with-redefs [d/db @db-stub
                             d/q @q-stub
                             d/transact @transact-stub
                             sut/params->tx-data* @tx-data-stub
                             student/find @find-stub
                             util/student-result @result-stub]
                 (it)))

  (it "refuses unauthorized requests"
    (let [result (sut/handler (@make-req {} :account-type/counselor))]
      (should-not-have-invoked @find-stub-name)
      (should-not-have-invoked @result-stub-name)
      (should= 403 (:status result))))

  (it "returns an error response if given invalid data"
    (should= {:status 400
              :body {:error (str "The given data generated the following errors:\n"
                                 "- The following required parameter is missing:"
                                 " counselor-id\n")}}
             (sut/handler (@make-req {"email" "e", "active" "true"}
                           :account-type/superuser))))

  (it "calls transact with generated tx-data if given valid data"
    (sut/handler (@make-req {"email" "e"
                             "active" "true"
                             "counselor-id" (str @counselor-id)}
                            :account-type/superuser))
    (should-have-invoked @transact-stub-name
                         {:times 1, :with [@conn {:tx-data @tx-data}]}))

  (it "returns a 200 response with student data given valid data"
    (let [params {"email" "e"
                  "active" "true"
                  "counselor-id" (str @counselor-id)}
          result (sut/handler (@make-req params :account-type/superuser))]
      (should-have-invoked @find-stub-name
                           {:times 1, :with [@db-val @student-id]})
      (should-have-invoked @result-stub-name {:times 1, :with [:student-data]})
      (should= {:status 200
                :body :student-result}
               result))))
