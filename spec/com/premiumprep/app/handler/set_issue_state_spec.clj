(ns com.premiumprep.app.handler.set-issue-state-spec
  (:require [com.premiumprep.app.handler.set-issue-state :as sut]
            [com.premiumprep.app.model.issue :as issue]
            [com.premiumprep.app.model.student :as student]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-be
                                 should-have-invoked should= should-not= stub
                                 with with-stubs]]))

(describe "set-issue-state/bad-request-message"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.set-issue-state
                         'bad-request-message))

  (it "returns a message when both params are missing"
    (should= "missing issue-id and to-state params"
             (@sut {})))

  (it "returns a message when the issue-id is missing"
    (should= "missing issue-id param"
             (@sut {"to-state" "closed"})))

  (it "returns a message when the to-state is missing"
    (should= "missing to-state param"
             (@sut {"issue-id" "1234"})))

  (it "returns a message when the issue-id is not numeric"
    (should= "non-numeric issue-id"
             (@sut {"issue-id" "abc"
                    "to-state" "closed"})))

  (it "returns a message when the to-state is invalid"
    (should= "invalid to-state value"
             (@sut {"issue-id" "123"
                    "to-state" "invalid state"})))

  (it "returns nil if the params are fully valid"
    (should= nil
             (@sut {"issue-id" "123"
                    "to-state" "closed"}))))

(describe "set-issue-state/authorized?"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.set-issue-state
                         'authorized?))

  (it "returns false when no session data given"
    (should= false (@sut nil nil)))

  (it "returns true when user is a superuser"
    (should= true (@sut {:account/type :account-type/superuser} nil)))

  (it "returns false when user is a student"
    (should= false (@sut {:account/type :account-type/student} nil)))

  (it "returns false when account type is not superuser, student, or counselor"
    (should= false (@sut {:account/type :account-type/parent} nil)))

  (it "returns true when user is the advising counselor"
    (let [acct-id 12341234
          session {:account/type :account-type/counselor
                   :db/id (str acct-id)}]
      (should= true (@sut session acct-id))))

  (it "returns false when user is a different counselor"
    (let [acct-id 12341234
          session {:account/type :account-type/counselor
                   :db/id (str acct-id "1")}]
      (should= false (@sut session acct-id)))))

(describe "the set-issue-state handler"
  (with-stubs)

  (it "returns 400 when the request is invalid"
    (with-redefs [sut/bad-request-message (stub :stub {:return "errmsg"})]
      (should= {:status 400
                :body {:error "errmsg"}}
               (sut/handler {:multipart-params :mp-params}))
      (should-have-invoked :stub {:times 1, :with [:mp-params]})))

  (context "when the request is valid"
    (with issue-id 4321)
    (around [it]
      (with-redefs [sut/bad-request-message (stub :bad-request-message
                                                  {:return nil})
                    sut/parse-long (stub :parse-long {:return @issue-id})]
        (it)))

    (it "attempts to find the given issue when given valid params"
      (with-redefs [d/db (stub :db {:return :db-val})
                    issue/find (stub :find-issue, {:return {:issue/student {:db/id 789}}})
                    student/find (stub :find-student)]
        (sut/handler {:datomic/conn :db-connection})
        (should-have-invoked :find-issue {:times 1, :with [:db-val @issue-id]})
        (should-have-invoked :find-student {:times 1
                                            :with [:db-val 789]})))

    (it "returns 404 status when given issue was not found"
      (with-redefs [d/db (stub :db {:return :db-val})
                    issue/find (stub :find-issue)]
        (should= 404 (:status (sut/handler {:datomic/conn :db-connection})))))

    (context "with a found issue"
      (with student-id 7890)
      (with counselor-acct-id 9870)
      (with student {:student/counselor {:counselor/account
                                         {:db/id @counselor-acct-id}}})
      (with issue {:db/id @issue-id
                   :issue/text "issue text"
                   :issue/created-at ::created-at
                   :issue/state {:db/ident :issue-state/new}
                   :issue/student {:db/id @student-id}})

      (around [it]
        (with-redefs [d/db (stub :db {:return :db-val})
                      issue/find (stub :find-issue {:return @issue})
                      student/find (stub :find-student {:return @student})]
          (it)))

      (it "rejects unauthenticated requests"
        (with-redefs [sut/authorized? (stub :authorized? {:return false})]
          (should= 403 (:status (sut/handler nil)))))

      (context "with an authenticated request"
        (around [it]
          (with-redefs [sut/authorized? (stub :authorized? {:return true})
                        d/transact (stub :transact)
                        sut/now (stub :now {:return ::current-ts})]
            (it)))

        (it "transacts data to the database"
          (sut/handler {:datomic/conn :db-conn
                        :multipart-params {"issue-id" @issue-id
                                           "to-state" "needs-discussion"}}))

        (it "returns 200"
          (should= 200 (:status (sut/handler
                                  {:multipart-params
                                   {"issue-id" @issue-id
                                    "to-state" "needs-discussion"}}))))

        (it "returns the issue data"
          (let [resp (sut/handler
                       {:multipart-params
                        {"issue-id" @issue-id
                         "to-state" "needs-discussion"}})
                data (:body resp)]
            (should= (-> @issue
                         (assoc :issue/state :issue-state/needs-discussion)
                         (update :db/id str)
                         (update-in [:issue/student :db/id] str))
                     data)))

        (it "updates counselor-touched-at if requester was counselor"
          (let [resp (sut/handler
                       {:multipart-params
                        {"issue-id" @issue-id
                         "to-state" "needs-discussion"}
                        :session {:account/type :account-type/counselor}})
                data (:body resp)]
            (should= ::current-ts (:issue/counselor-touched-at data))))

        (it "does not update counselor-touched-at if requester was superuser"
          (let [resp (sut/handler
                       {:multipart-params
                        {"issue-id" @issue-id
                         "to-state" "needs-discussion"}
                        :session {:account/type :account-type/superuser}})
                data (:body resp)]
            (should= nil (:issue/counselor-touched-at data))))))))
