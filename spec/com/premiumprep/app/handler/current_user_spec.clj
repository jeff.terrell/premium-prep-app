(ns com.premiumprep.app.handler.current-user-spec
  (:require [com.premiumprep.app.schema :as schema]
            [com.premiumprep.app.middleware :as mdw]
            [com.premiumprep.app.handler.current-user :as sut]
            [datomic.client.api :as d]
            [ring.middleware.session :refer [wrap-session]]
            [speclj.core :refer [before describe it should= with with!]])
  (:import java.util.UUID))

(defn- request [session-id]
  {:headers {"cookie" (str "ring-session=" session-id)}})

(describe "the current-user handler"
  (with db-name (name ::db))
  (with client (doto (d/client {:server-type :dev-local
                                :storage-dir :mem
                                :system "local"})
                 (d/create-database {:db-name @db-name})))
  (with conn (doto (d/connect @client {:db-name @db-name})
               schema/install-schema!))

  (with handler
    ;; copied from server ns
    (wrap-session sut/handler
                  {:store (mdw/->DatomicSessionStore @conn)
                   :flash true
                   :cookie-attrs {:http-only true, :same-site :strict}}))

  (with session-id (UUID/randomUUID))
  (with email "an-email-address")
  (with account-type :account-type/student)
  (with tx-data [{:session/id @session-id, :session/account "account"}
                 {:db/id "account"
                  :account/email @email
                  :account/type @account-type
                  :account/state :account-state/initialized
                  :account/active? true}])
  (with account-eid
    (get-in (d/transact @conn {:tx-data @tx-data})
            [:tempids "account"]))

  (it "returns 404 when no cookie provided"
    (let [response (@handler {})]
      (should= 404 (:status response))))

  (it "returns 404 when no corresponding session ID found"
    (let [response (@handler (request (UUID/randomUUID)))]
      (should= 404 (:status response))))

  (it "returns status of 200 and body of current user map when session ID found"
    @account-eid  ; reference this for the side effect of transacting the session
    (let [response (@handler (request @session-id))]
      (should= 200 (:status response))
      (should= {:db/id (str @account-eid)
                :account/email @email
                :account/type @account-type
                :account/state :account-state/initialized}
               (:body response)))))
