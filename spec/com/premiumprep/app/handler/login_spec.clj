(ns com.premiumprep.app.handler.login-spec
  (:require [clojurewerkz.scrypt.core :as sc]
            [com.premiumprep.app.handler.login :as sut]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-have-invoked
                                 should= stub with with-stubs]]))

(describe "the login handler"
  (with-stubs)

  (it "gets a database value from the connection"
    (let [db :datomic-db
          conn :a-datomic-connection,
          db-stub (stub :db {:return db})
          auth-stub (stub :authenticate {:return {:db/id "1234"}})
          req {:datomic/conn conn}]
      (with-redefs [d/db db-stub
                    sut/authenticate auth-stub]
        (sut/handler req)
        (should-have-invoked :db {:times 1, :with [conn]}))))

  (it "queries the database"
    (let [db :datomic-db
          conn :a-datomic-connection,
          q-stub (stub :q)
          db-stub (stub :db {:return db})
          req {:datomic/conn conn}]
      (with-redefs [d/q q-stub
                    d/db db-stub]
        (sut/handler req)
        (should-have-invoked :q {:times 1}))))

  (it "returns 400 when the authentication was not successful"
    (let [db :datomic-db
          conn :a-datomic-connection,
          email "email@email.com"
          password "a really secure password"
          multipart-params {"email" email, "password" password}
          db-stub (stub :db {:return db})
          auth-stub (stub :authenticate {:return false})
          req {:datomic/conn conn, :multipart-params multipart-params}]
      (with-redefs [d/db db-stub
                    sut/authenticate auth-stub]
        (should= 403 (:status (sut/handler req)))
        (should-have-invoked :authenticate {:times 1
                                            :with [db email password]}))))

  (context "when valid credentials are supplied"
    (with db-val :datomic-db-val)
    (with db-stub (stub :db {:return @db-val}))
    (around [it] (with-redefs [d/db @db-stub] (it)))

    (with conn :a-datomic-connection)
    (with id 123412341234)
    (with email "email@email.com")
    (with password "a really secure password")
    (with encrypted-pw (sc/encrypt @password 16384 8 1))
    (with multipart-params {"email" @email, "password" @password})
    (with req {:datomic/conn @conn, :multipart-params @multipart-params})
    (with account-type-ident :account-type/superuser)

    (with q-stub-name :datomic-q)
    (with q-stub (stub @q-stub-name
                       {:return [[{:db/id @id
                                   :account/email @email
                                   :account/password @encrypted-pw
                                   :account/state {:db/ident
                                                   :account-state/initialized}
                                   :account/type {:db/ident
                                                  @account-type-ident}}]]}))
    (around [it] (with-redefs [d/q @q-stub] (it)))

    (it "invokes the authenticate helper function"
      (with-redefs [sut/authenticate (stub :auth {:return nil})]
        (sut/handler @req)
        (should-have-invoked :auth
                             {:times 1, :with [@db-val @email @password]})))

    (it "returns a 200 status code"
      (should= 200 (:status (sut/handler @req))))

    (it "returns a body that includes the id, email, and account type"
      (should= {:db/id (str @id)
                :account/email @email
                :account/type @account-type-ident
                :account/state :account-state/initialized}
               (-> @req sut/handler :body)))

    (it "stringifies the :db/id value"
      (let [acct-id 23452345
            counselor-id 43214321
            acct-type :account-type/counselor
            q-stub (stub @q-stub-name
                         {:return [[{:db/id acct-id
                                     :account/email @email
                                     :account/password @encrypted-pw
                                     :account/type {:db/ident acct-type}}]]})]
        (with-redefs [d/q q-stub]
          (should= (str acct-id)
                   (-> @req sut/handler :body :db/id)))))

    (it "adds session info"
      (let [acct-id :acct-id
            student-id 12341234
            acct-type :account-type/student
            q-stub (stub @q-stub-name
                         {:return [[{:db/id acct-id
                                     :account/password @encrypted-pw
                                     :account/type {:db/ident acct-type}
                                     :account/state {:db/ident
                                                     :account-state/initialized}
                                     :account/email @email}]]})]
        (with-redefs [d/q q-stub]
          (should= acct-id
                   (-> @req sut/handler :session)))))))
