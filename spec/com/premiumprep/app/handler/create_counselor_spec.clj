(ns com.premiumprep.app.handler.create-counselor-spec
  (:require
   [com.premiumprep.app.handler.create-counselor :as sut]
   [com.premiumprep.app.model.counselor :as counselor]
   [com.premiumprep.app.util :as util]
   [datomic.client.api :as d]
   [speclj.core :refer [around context describe it should-be should-contain
                        should-have-invoked should-not-have-invoked should= stub
                        with with-stubs]]))

(describe "create-counselor/params->tx-data"
  (with-stubs)

  ;; "function under test"
  (with fut @(ns-resolve 'com.premiumprep.app.handler.create-counselor
                         'params->tx-data))

  (it "returns an error message if the email address is already taken"
    (let [db-val :the-database
          email "an-email-address@domain.com"
          stub-name :duplicate-email-msg
          expected-msg (format "The given email (%s) is already taken" email)
          e-stub (stub stub-name {:return expected-msg})]
      (with-redefs [sut/duplicate-email-msg e-stub]
        (should= [false expected-msg]
                 (@fut db-val {"email" email, "name" "n"}))
        (should-have-invoked stub-name {:times 1, :with [db-val email]}))))

  (context "(ignoring email uniqueness checks)"
    (with fut2
      (fn [& args]
        (with-redefs [sut/duplicate-email-msg (stub :duplicate-email-msg
                                                    {:return nil})]
          (apply @fut args))))

    (it "returns an error message if required params are missing"
      (should= [false
                (str "There were some problems with your request:\n"
                     "Error on field email: value is required\n"
                     "Error on field name: value is required\n")]
               (@fut2 :db {}))
      (should= [false
                (str "There were some problems with your request:\n"
                     "Error on field name: value is required\n")]
               (@fut2 :db {"email" "e"})))

    (context "with a found counselor-id"
      (with db-val :the-database)
      (with email "person@email.com")
      (with counselor-name "Socrates")

      (with base-params {"email" @email
                         "name" @counselor-name})
      (with base-account-tx-map {:db/id "account"
                                 :account/email @email
                                 :account/full-name @counselor-name
                                 :account/active? true
                                 :account/state :account-state/initialized
                                 :account/type :account-type/counselor})
      (with base-counselor-tx-map {:db/id "counselor"
                                   :counselor/account "account"})

      (it "returns a datom referencing a new account entity"
        (let [tx-data (second (@fut2 @db-val @base-params))]
          (should-contain [:db/add "counselor" :counselor/account
                           "counselor,:counselor/account"]
                          tx-data)))

      (it "includes an encrypted password in the account tx-map of returned value"
        (let [tx-data (second (@fut2 @db-val @base-params))]
          (should-contain [:db/add "counselor,:counselor/account"
                           :account/password]
                          (map #(subvec % 0 3) tx-data))
          (should-be string?
                     (->> tx-data
                          (filter #(= :account/password (nth % 2)))
                          first
                          last))))

      (it "includes the raw password in the metadata of the returned value"
        (let [tx-data (second (@fut2 @db-val @base-params))]
          (should-be some? (meta tx-data))
          (should-be map? (meta tx-data))
          (should-contain :password (meta tx-data))
          (should-be string? (:password (meta tx-data))))))))

(describe "the create-counselor handler"
  (with-stubs)
  (with conn :datomic-connection)
  (with db-stub-name :datomic-db)
  (with db-val :the-database-value)
  (with db-stub (stub @db-stub-name {:return @db-val}))
  (with counselor-id 4321)
  (with find-stub-name :counselor-find)
  (with find-return :counselor-data)
  (with find-stub (stub @find-stub-name {:return @find-return}))
  (with result-stub-name :counselor-result)
  (with result-stub (stub @result-stub-name {:return :counselor-result}))
  (with password "abcdefghijklmnop")
  (with tx-data (with-meta [{:db/id "account"}] {:password @password}))
  (with tx-data-stub-name :params->tx-data)
  (with tx-data-stub (stub @tx-data-stub-name {:return @tx-data}))
  (with transact-stub-name :datomic-transact)
  (with transact-stub (stub @transact-stub-name
                            {:return {:tempids {"counselor" @counselor-id}}}))
  (with duplicate-email-msg-stub-name :duplicate-email-msg)
  (with duplicate-email-msg-stub (stub @duplicate-email-msg-stub-name
                                       {:return nil}))
  (around [it] (with-redefs [d/db @db-stub
                             d/transact @transact-stub
                             sut/duplicate-email-msg @duplicate-email-msg-stub
                             sut/params->tx-data* @tx-data-stub
                             counselor/find @find-stub
                             util/counselor-result @result-stub]
                 (it)))

  (it "refuses unauthorized requests"
    (let [result (sut/handler {:datomic/conn @conn
                               :multipart-params {"email" "e", "name" "n"}})]
      (should-not-have-invoked @find-stub-name)
      (should-not-have-invoked @result-stub-name)
      (should= 403 (:status result))
      (should= "forbidden" (-> result :body :error))))

  (it "returns an error response if no email given"
    (should= {:status 400
              :body {:error (str "There were some problems with your request:\n"
                                 "Error on field email: value is required\n")}}
             (sut/handler {:datomic/conn @conn
                           :multipart-params {"name" "n"}
                           :session {:account/type :account-type/superuser}})))

  (it "returns an error response if no name given"
    (should= {:status 400
              :body
              {:error (str "There were some problems with your request:\n"
                           "Error on field name: value is required\n")}}
             (sut/handler {:datomic/conn @conn
                           :multipart-params {"email" "e"}
                           :session {:account/type :account-type/superuser}})))

  (it "calls transact with generated tx-data if given valid data"
    (sut/handler {:datomic/conn @conn
                  :multipart-params {"email" "e", "name" "n"}
                  :session {:account/type :account-type/superuser}})
    (should-have-invoked @transact-stub-name
                         {:times 1, :with [@conn {:tx-data @tx-data}]}))

  (it "returns a 200 response with counselor data given valid data"
    (let [req {:datomic/conn @conn
               :multipart-params {"email" "e", "name" "n"}
               :session {:account/type :account-type/superuser}}
          result (sut/handler req)]
      (should-have-invoked @find-stub-name
                           {:times 1, :with [@db-val @counselor-id]})
      (should-have-invoked @result-stub-name {:times 1, :with [@find-return]})
      (should= {:status 201
                :body {:counselor :counselor-result
                       :password @password}}
               result))))
