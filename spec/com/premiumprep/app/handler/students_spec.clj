(ns com.premiumprep.app.handler.students-spec
  (:require [com.premiumprep.app.handler.students :as sut]
            [com.premiumprep.app.schema :as schema]
            [datomic.client.api :as d]
            [speclj.core :refer [context describe it should-be
                                 should-have-invoked should= stub with with!
                                 with-stubs]])
  (:import java.util.Date))

(describe "the students handler"
  (with-stubs)

  (it "refuses unauthorized requests"
    (let [client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"})
          opts {:db-name "students-spec"}
          _ (d/delete-database client opts)
          _ (d/create-database client opts)
          conn (d/connect client opts)
          _ (schema/install-schema! conn)
          actual (sut/handler {:datomic/conn conn})]
      (should= 403 (:status actual))))

  (it "gets a database value from the given connection"
    (let [db :datomic-db
          conn :a-datomic-connection,
          db-stub (stub :db {:return db})
          student-list-stub (stub :student-list {:return true})
          req {:datomic/conn conn
               :session {:account/type :account-type/superuser}}]
      (with-redefs [d/db db-stub
                    sut/student-list student-list-stub]
        (sut/handler req)
        (should-have-invoked :db {:times 1, :with [conn]}))))

  (it "queries the database"
    (let [db :datomic-db
          conn :a-datomic-connection,
          q-stub (stub :q)
          db-stub (stub :db {:return db})
          req {:datomic/conn conn
               :session {:account/type :account-type/superuser}}]
      (with-redefs [d/q q-stub
                    d/db db-stub]
        (sut/handler req)
        (should-have-invoked :q {:times 1}))))

  (context "integrated with a database"
    (with client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"}))
    (with db-opts {:db-name "students-spec"})
    (with conn (do (d/delete-database @client @db-opts)
                   (d/create-database @client @db-opts)
                   (let [conn (d/connect @client @db-opts)]
                     (schema/install-schema! conn)
                     conn)))
    (with account {:db/id "account"
                   :account/email "email@email.com"
                   :account/password "should NOT be returned"
                   :account/full-name "Joseph Student"
                   :account/active? true
                   :account/state :account-state/uninitialized
                   :account/type :account-type/student})
    (with caccount {:db/id "caccount"
                    :account/password "should NOT be returned"
                    :account/email "counselor@email.com"
                    :account/full-name "The Counselor (dum dum DUM)"
                    :account/active? true
                    :account/state :account-state/initialized
                    :account/type :account-type/counselor})
    (with counselor {:db/id "counselor"
                     :counselor/account "caccount"})
    (with student {:student/account "account"
                   :student/counselor "counselor"
                   :student/joined-at (Date. 119 7 4)
                   :student/graduation-year 2024
                   :student/high-school "East Chapel Hill High"
                   :student/profile-comments []
                   :student/document-list-comments []})
    (with tx-ret
      (d/transact @conn {:tx-data [@account @caccount @counselor @student]}))
    (with counselor-id (get-in @tx-ret [:tempids "counselor"]))
    (with caccount-id (get-in @tx-ret [:tempids "caccount"]))

    (it "returns a 200 response with student info"
      @tx-ret ; for side effect :-/
      (let [counselor (dissoc @counselor :account/active?)
            estudent (assoc @student :student/account @account
                            :student/counselor counselor
                            :student/documents []
                            :student/college-lists {}
                            :issue/_student [])
            expected {:status 200
                      :body [estudent]}
            actual (sut/handler {:datomic/conn @conn
                                 :session {:account/type :account-type/superuser}})
            abody (-> actual (:body "nil"))
            astudent (first abody)
            astudent2 (dissoc astudent :db/id)
            aaccount (:student/account astudent2)]

        (should-be map? actual)
        (should= (-> expected keys sort) (-> actual keys sort))
        (should= (:status expected) (:status actual))
        (should= (:headers expected) (:headers actual))
        (should-be vector? abody)
        (should= 1 (count abody))
        (should-be map? astudent)
        (should= java.lang.String (type (:db/id astudent)))
        (should= (-> estudent keys set) (-> astudent2 keys set))
        (should= (:student/joined-at estudent) (:student/joined-at astudent2))
        (should= (:student/graduation-year estudent) (:student/graduation-year astudent2))
        (should= (:student/high-school estudent) (:student/high-school astudent2))
        (should= (-> estudent :student/account keys set (disj :account/password))
                 (-> astudent :student/account keys set))
        (should= (:account/email @account) (:account/email aaccount))
        (should= (:account/full-name @account) (:account/full-name aaccount))
        (should= (:account/type @account) (:account/type aaccount))))

    (context "with an inactive student"
      (with inactive-account (assoc @account
                                    :account/active? false
                                    :account/email "email2@email.com"))
      (with inactive-student (assoc @student :student/counselor @counselor-id))
      (with tx-ret2
        (d/transact @conn {:tx-data [@inactive-account @inactive-student]}))

      (it "returns inactive students when user is a superuser"
        @tx-ret2 ; for side effect :-/
        (should= 2
                 (-> (sut/handler
                       {:datomic/conn @conn
                        :session {:account/type :account-type/superuser}})
                     :body
                     count)))

      (it "returns only active students when user is a counselor"
        @tx-ret2 ; for side effect :-/
        (should= 1
                 (-> (sut/handler
                       {:datomic/conn @conn
                        :session {:account/type :account-type/counselor
                                  :db/id (str @caccount-id)}})
                     :body
                     count))))

    (context "with another student and counselor"
      (with new-caccount (assoc @caccount :account/email "c@email.com"))
      (with new-account (assoc @account :account/email "s@email.com"))
      (with tx-ret2
        (d/transact @conn {:tx-data
                           [@new-account @new-caccount @counselor @student]}))

      (it "returns all students when user is a superuser"
        @tx-ret ; for side effect :-/
        @tx-ret2
        (should= 2
                 (-> (sut/handler
                       {:datomic/conn @conn
                        :session {:account/type :account-type/superuser}})
                     :body
                     count)))

      (it "returns only own students when user is a counselor"
        @tx-ret ; for side effect :-/
        @tx-ret2
        (should= 1
                 (-> (sut/handler
                       {:datomic/conn @conn
                        :session {:account/type :account-type/counselor
                                  :db/id (str @caccount-id)}})
                     :body
                     count))))))
