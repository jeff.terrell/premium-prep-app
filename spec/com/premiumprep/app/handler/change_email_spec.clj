(ns com.premiumprep.app.handler.change-email-spec
  (:require [com.premiumprep.app.handler.change-email :as sut]
            [com.premiumprep.app.model.account :as account]
            [crypto.random :as crypto]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-have-invoked
                                 should-not-have-invoked should= stub with
                                 with-stubs]]
            [com.premiumprep.app.email :as email]))

(describe "the change-email handler"
  (with-stubs)
  (with token "a-nice-random-token")
  (around [it]
    (with-redefs [crypto/url-part (stub :url-part {:return @token})
                  d/db (stub :db)
                  d/transact (stub :transact)
                  email/change-email (stub :change-email)
                  email/verify-email (stub :verify-email)]
      (it)))

  (it "returns 403 for unauthorized requests"
    (should= {:status 403, :headers {}}
             (sut/handler nil)))

  (context "when session email is the same as given email"
    (with email "an-email-address@domain.com")
    (around [it]
      (with-redefs [account/find-by-email (stub :find-by-email)]
        (it)))

    (it "returns 204"
      (should= {:status 204, :headers {}}
               (sut/handler {:multipart-params {"email" @email}
                             :session {:account/email @email}})))

    (it "re-sends email with the same token"
      (let [account {:account/token @token}]
        (with-redefs [d/db (stub :db {:return :db-val})
                      crypto/url-part (stub :url-part {:return "other-token"})
                      account/find-by-email (stub :find-by-email
                                                  {:return account})]
          (sut/handler {:email-send-fn :send-email
                        :multipart-params {"email" @email}
                        :session {:account/email @email}})
          (should-have-invoked :find-by-email
                               {:times 1
                                :with [:db-val @email]})
          (should-have-invoked :verify-email
                               {:times 1
                                :with [:send-email @email @token]}))))

    (it "does not transact data to the database"
      (sut/handler {:email-send-fn :send-email
                    :multipart-params {"email" @email}
                    :session {:account/email @email}})
      (should-not-have-invoked :transact)))

  (context "when session email differs from given email"
    (with old-email "old-email-address@domain.com")
    (with new-email "new-email-address@domain.com")
    (with account-id 123454321)
    (around [it]
      (with-redefs [account/find-by-email (stub :find-by-email
                                                {:return {:db/id @account-id}})
                    d/db (stub :db {:return :db-val})
                    sut/conflict? (stub :conflict? {:return false})]
        (it)))

    (it "returns 204"
      (should= {:status 204, :headers {}}
               (sut/handler {:multipart-params {"email" @new-email}
                             :session {:account/email @old-email}})))

    (it "sends email"
      (sut/handler {:email-send-fn :send-email
                    :multipart-params {"email" @new-email}
                    :session {:account/email @old-email}})
      (should-have-invoked :change-email
                           {:times 1
                            :with [:send-email @old-email @new-email @token]}))

    (it "looks up the entity ID owning that :account/email value"
      (sut/handler {:email-send-fn :send-email
                    :multipart-params {"email" @new-email}
                    :session {:account/email @old-email}})
      (should-have-invoked :find-by-email {:times 1
                                           :with [:db-val @old-email]}))

    (it "transacts data to the database"
      (sut/handler {:datomic/conn :db-connection
                    :email-send-fn :send-email
                    :multipart-params {"email" @new-email}
                    :session {:account/email @old-email}})
      (let [tx-data [[:db/retract @account-id :account/email @old-email]
                     [:db/add @account-id :account/email @new-email]
                     [:db/add @account-id :account/state
                      :account-state/email-unverified]
                     [:db/add @account-id :account/token @token]
                     [:db/add "datomic.tx" :transaction/namespace
                      "com.premiumprep.app.handler.change-email"]]]
        (should-have-invoked :transact
                             {:times 1
                              :with [:db-connection {:tx-data tx-data}]})))

    (it "detects an email address conflict"
      (with-redefs [sut/conflict? (stub :conflict? {:return true})]
        (should= {:status 409
                  :headers {}
                  :body (str "Error: that email address is already taken.\n\n"
                             "If you forgot your password, you can reset it.\n")}
                 (sut/handler {:multipart-params {"email" @new-email}
                               :session {:account/email @old-email}}))
        (should-have-invoked :conflict? {:times 1})))))
