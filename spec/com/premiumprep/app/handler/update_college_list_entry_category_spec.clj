(ns com.premiumprep.app.handler.update-college-list-entry-category-spec
  (:require
   [com.premiumprep.app.handler.update-college-list-entry-category :as sut]
   [com.premiumprep.app.model.student :as student]
   [com.premiumprep.app.util :as util]
   [datomic.client.api :as d]
   [speclj.core :refer [around context describe it should-contain
                        should-have-invoked should-not-have-invoked should= stub
                        with with-stubs]]
   [speclj.stub :refer [first-invocation-of]]))

(describe "update-cle-category/missing-required-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-category
               'missing-required-params-error))
  (with good-params {"student-id" "123"
                     "college-id" "234"
                     "category" "likely"})

  (it "detects a missing param"
    (should= "Missing parameter: student-id"
             (@sut (dissoc @good-params "student-id")))
    (should= "Missing parameter: college-id"
             (@sut (dissoc @good-params "college-id")))
    (should= "Missing parameter: category"
             (@sut (dissoc @good-params "category"))))

  (it "detects multiple missing params"
    (should= "Missing parameters: college-id, student-id"
             (@sut (dissoc @good-params "college-id" "student-id")))))

(describe "update-cle-category/nonnumeric-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-category
               'nonnumeric-params-error))
  (with good-params {"student-id" "123"
                     "college-id" "234"})

  (it "detects a nonnumeric param"
    (should= "Non-numeric parameter: student-id"
             (@sut (assoc @good-params "student-id" "abc")))
    (should= "Non-numeric parameter: college-id"
             (@sut (assoc @good-params "college-id" "12e4"))))

  (it "detects multiple nonnumeric params"
    (should= "Non-numeric parameters: student-id, college-id"
             (@sut {}))))

(describe "update-cle-category/authorized?"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-category
               'authorized?))

  (it "rejects unauthenticated users"
    (should= false (boolean (@sut nil nil nil))))

  (it "rejects students who aren't the same student as in the request body"
    (let [id 1234
          acct-id 432423
          session {:account/type :account-type/student, :db/id (str acct-id)}
          params {"category" "likely", "student-id" (str id)}
          student {:db/id (str id "1")}]
      (with-redefs [student/find-by-account-id (stub :find {:return student})]
        (should= false (@sut :db-val session params))
        (should-have-invoked :find {:times 1, :with [:db-val acct-id]}))))

  (it "allows authorized users"
    (let [id 1234
          acct-id 432423
          session {:account/type :account-type/student, :db/id (str acct-id)}
          params {"category" "likely", "student-id" (str id)}
          student {:db/id (str id)}]
      (with-redefs [student/find-by-account-id (stub :find {:return student})]
        (should= true (@sut :db-val session params))
        (should-have-invoked :find {:times 1, :with [:db-val acct-id]})))))

(describe "update-cle-category/invalid-category-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-category
               'invalid-category-error))
  (it "detects invalid category names"
    (should= "Invalid category" (@sut {"category" "abc"})))
  (it "accepts valid category names"
    (should= nil (@sut {"category" "high-reach"}))
    (should= nil (@sut {"category" "reach"}))
    (should= nil (@sut {"category" "target"}))
    (should= nil (@sut {"category" "likely"}))
    (should= nil (@sut {"category" "uncategorized"}))))

(describe "the update-college-list-entry-category handler"
  (with-stubs)
  (it "returns status 400 if required categories are missing"
    (with-redefs [sut/missing-required-params-error (stub :stub
                                                          {:return "errmsg"})]
      (should= 400 (:status (sut/handler {:multipart-params :the-params})))
      (should-have-invoked :stub {:times 1, :with [:the-params]})))

  (context "with no missing categories"
    (around [it]
      (with-redefs [sut/missing-required-params-error (stub :mrpe)]
        (it)))

    (it "returns status 400 if any id params are not numeric"
      (with-redefs [sut/nonnumeric-params-error (stub :stub {:return "errmsg"})]
        (should= 400 (:status (sut/handler {:multipart-params :the-params})))
        (should-have-invoked :stub {:times 1, :with [:the-params]})))

    (context "with numeric id params"
      (around [it]
        (with-redefs [sut/nonnumeric-params-error (stub :nnpe)]
          (it)))

      (it "returns status 403 if request not authorized"
        (let []
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :stub {:return nil})]
            (should= 403 (:status (sut/handler {:datomic/conn :db-conn
                                                :multipart-params :the-params
                                                :session :the-session})))
            (should-have-invoked :db {:times 1, :with [:db-conn]})
            (should-have-invoked :stub
                                 {:times 1
                                  :with [:db-val :the-session :the-params]}))))

      (context "with an authorized request"
        (around [it]
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :authorized? {:return true})]
            (it)))

        (it "returns status 400 if category name invalid"
          (with-redefs [sut/invalid-category-error (stub :stub {:return "err"})]
            (should= 400
                     (:status (sut/handler {:multipart-params :the-params})))
            (should-have-invoked :stub {:times 1, :with [:the-params]})))

        (context "with a valid category name"
          (around [it]
            (with-redefs [sut/invalid-category-error (stub :ife)]
              (it)))

          (it "returns status 404 if referenced college-list-entry not found"
            (let [student-id 1234
                  college-id 2345
                  params {"student-id" (str student-id)
                          "college-id" (str college-id)
                          "category" "likely"}]
              (with-redefs [sut/find-entry (stub :stub)]
                (should= 404 (:status (sut/handler {:multipart-params params})))
                (should-have-invoked :stub
                                     {:times 1
                                      :with [:db-val student-id college-id]}))))

          (context "with a found college-list-entry"
            (with student-id 1234)
            (with college-id 2345)
            (with params {"student-id" (str @student-id)
                          "college-id" (str @college-id)
                          "category" "target"})
            (with atype :account-type/superuser)
            (with sut #(sut/handler {:datomic/conn :db-conn
                                     :multipart-params @params
                                     :session {:account/type @atype}}))
            (with entry-id 3456)

            (around [it]
              (with-redefs [sut/find-entry (stub :stub {:return @entry-id})]
                (it)))

            (it "calls happy-path fn and returns its returned value"
              (with-redefs [sut/happy-path (stub :stub {:return :hprv})]
                (should= :hprv (@sut))
                (should-have-invoked :stub
                                     {:times 1
                                      :with [@atype :db-conn @student-id
                                             @entry-id @params]})))))))))

(describe "update-cle-category/happy-path"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.update-college-list-entry-category
               'happy-path))
  (with student-id 6789)
  (with entry-id 9876)
  (with new-category :college-list-category/target)
  (with params {"category" (name @new-category)})
  (with atype :account-type/superuser)
  (with response (@sut @atype :db-conn @student-id @entry-id @params))

  (around [it]
    (with-redefs [d/db (stub :db {:return :db-val})
                  d/transact (stub :transact)]
      (it)))

  (it "calls lookup-path to get the current category"
    (with-redefs [sut/lookup-path (stub :lookup-path)
                  sut/tx-data (stub :tx-data)
                  student/find (stub :find-student)]
      @response
      (should-have-invoked :lookup-path
                           {:times 1
                            :with [:db-val @entry-id
                                   [:college-list-entry/category :db/ident]]})))

  (it "returns status 200 response"
    (with-redefs [sut/lookup-path (stub :lookup-path)
                  sut/tx-data (stub :tx-data)
                  d/db (stub :db)
                  d/transact (stub :transact)
                  student/find (stub :find-student)]
      (should= 200 (:status @response))))

  (it "returns an updated college list"
    (with-redefs [d/pull (stub :pull)
                  student/find (stub :student)
                  student/listify-college-list-entries (stub :listify)
                  util/student-result (stub :result {:return {:student/college-lists :new-list}})
                  sut/tx-data (stub :tx-data)]
      (should= :new-list (:body @response))))

  (context "with a found current category"
    (with old-category :college-list-category/likely)
    (around [it]
      (with-redefs [student/find (stub :find-student)
                    sut/lookup-path (stub :lookup-path {:return @old-category})]
        (it)))

    (it "calls tx-data"
      (with-redefs [sut/tx-data (stub :tx-data)]
        @response
        (should-have-invoked :tx-data
                             {:times 1
                              :with [:db-val @student-id @entry-id @old-category
                                     @new-category]})))

    (it "does not call transact when category is unchanged"
      (with-redefs [sut/lookup-path (stub :lookup-path {:return @new-category})]
        @response
        (should-not-have-invoked :transact)))

    (context "with a changed category"
      (with old-index 1)
      (around [it]
        (with-redefs [sut/lookup (stub :lookup {:return @old-index})
                      sut/lookup-path (stub :lookup-path
                                            {:return @old-category})]
          (it)))

      (it "calls the constituent tx-data generating fns"
        (with-redefs [sut/shift-datoms (stub :shift-datoms {:return [:datoms1]})
                      sut/entry-datoms (stub :entry-datoms {:return [:datoms2]})]
          @response
          (should-have-invoked :shift-datoms)
          (should-have-invoked :entry-datoms)
          (let [invocation (first-invocation-of :transact)
                tx-data (-> invocation second :tx-data)]
            (should-contain :datoms1 tx-data)
            (should-contain :datoms2 tx-data))))

      (it "shifts later colleges in the old category down"
        (with-redefs [sut/entries-beyond-index (stub :beyond
                                                     {:return [:beyond1
                                                               :beyond2]})
                      sut/entry-datoms (stub :entry-datoms)]
          @response
          (should-have-invoked :transact)
          (let [invocation (first-invocation-of :transact)
                tx-data (-> invocation second :tx-data)]
            (should-contain [:db/retract :beyond1 :college-list-entry/index 2]
                            tx-data)
            (should-contain [:db/add :beyond1 :college-list-entry/index 1]
                            tx-data)
            (should-contain [:db/retract :beyond2 :college-list-entry/index 3]
                            tx-data)
            (should-contain [:db/add :beyond2 :college-list-entry/index 2]
                            tx-data))))

      (it "sets the found entry's index to the max index + 1 in the new category"
        (with-redefs [sut/shift-datoms (stub :shift-datoms)
                      sut/next-index-for-list (stub :next-index {:return 5})]
          @response
          (should-have-invoked :transact)
          (let [invocation (first-invocation-of :transact)
                tx-data (-> invocation second :tx-data)]
            (should-contain [:db/retract @entry-id :college-list-entry/index 1]
                            tx-data)
            (should-contain [:db/add @entry-id :college-list-entry/index 5]
                            tx-data))))

      (it "includes datoms to update the entry's category"
        (with-redefs [sut/shift-datoms (stub :shift-datoms)
                      sut/next-index-for-list (stub :next-index {:return 5})]
          @response
          (should-have-invoked :transact)
          (let [invocation (first-invocation-of :transact)
                tx-data (-> invocation second :tx-data)]
            (should-contain [:db/retract @entry-id :college-list-entry/category @old-category]
                            tx-data)
            (should-contain [:db/add @entry-id :college-list-entry/category @new-category]
                            tx-data)))))))
