(ns com.premiumprep.app.handler.download-document-spec
  (:require [com.premiumprep.app.handler.download-document :as sut]
            [com.premiumprep.app.model.document :as doc]
            [com.premiumprep.app.storage :as storage]
            [datomic.client.api :as d]
            [speclj.core :refer [describe it should= should-have-invoked stub
                                 with with-stubs]]))

(describe "the download-document handler"
  (with-stubs)

  (it "returns 404 when the document can't be found"
    (with-redefs [d/db (stub :db {:return :db-val})
                  doc/find (stub :doc-find)]
      (should= {:status 404}
               (sut/handler {:reitit.core/match {:path-params {:id "1234"}}}))
      (should-have-invoked :doc-find {:times 1, :with [:db-val 1234]})))

  (it "returns 403 when the requester is unauthorized"
    (with-redefs [d/db (stub :db {:return :db-val})
                  doc/find (stub :doc-find {:return :document})
                  sut/authorized? (stub :authorized? {:return false})]
      (should= {:status 403}
               (sut/handler {:reitit.core/match {:path-params {:id "1234"}}}))
      (should-have-invoked :authorized?)))

  (it "returns 200 and calls `retrieve` otherwise"
    (with-redefs [d/db (stub :db {:return :db-val})
                  doc/find (stub :doc-find
                                 {:return {:document/content-type
                                           "application/json"}})
                  storage/retrieve (stub :retrieve {:return :document-contents})
                  sut/authorized? (stub :authorized? {:return true})]
      (should= {:status 200
                :headers {"content-type" "application/json"}
                :body :document-contents}
               (sut/handler {:reitit.core/match {:path-params {:id "1234"}}}))
      (should-have-invoked :retrieve))))
