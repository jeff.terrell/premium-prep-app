(ns com.premiumprep.app.handler.counselors-spec
  (:require [com.premiumprep.app.handler.counselors :as sut]
            [com.premiumprep.app.schema :as schema]
            [datomic.client.api :as d]
            [speclj.core :refer [describe it should-be should-have-invoked
                                 should= stub with-stubs]]))

(describe "the counselors handler"
  (with-stubs)

  (it "refuses unauthorized requests"
    (let [client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"})
          opts {:db-name "counselors-spec"}
          _ (d/delete-database client opts)
          _ (d/create-database client opts)
          conn (d/connect client opts)
          _ (schema/install-schema! conn)
          actual (sut/handler {:datomic/conn conn})]
      (should= 403 (:status actual))))

  (it "gets a database value from the given connection"
    (let [db :datomic-db
          conn :a-datomic-connection,
          db-stub (stub :db {:return db})
          counselor-list-stub (stub :counselor-list {:return [:counselor]})
          req {:datomic/conn conn
               :session {:account/type :account-type/superuser}}]
      (with-redefs [d/db db-stub
                    sut/counselor-list counselor-list-stub]
        (sut/handler req)
        (should-have-invoked :db {:times 1, :with [conn]}))))

  (it "queries the database"
    (let [db :datomic-db
          conn :a-datomic-connection,
          q-stub (stub :q)
          db-stub (stub :db {:return db})
          req {:datomic/conn conn
               :session {:account/type :account-type/superuser}}]
      (with-redefs [d/q q-stub
                    d/db db-stub]
        (sut/handler req)
        (should-have-invoked :q {:times 1}))))

  (it "returns a 200 response with counselor info"
    (let [client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"})
          opts {:db-name "counselors-spec"}
          _ (d/delete-database client opts)
          _ (d/create-database client opts)
          conn (d/connect client opts)
          _ (schema/install-schema! conn)
          email "email@email.com"
          name "Joseph Counselor"
          account {:db/id "account"
                   :account/email email
                   :account/password "should NOT be returned"
                   :account/full-name name
                   :account/active? true
                   :account/type :account-type/counselor
                   :account/state :account-state/initialized}
          counselor-id "counselor"
          counselor {:db/id counselor-id
                     :counselor/account "account"}
          student {:db/id "student"
                   :student/counselor counselor-id}
          tx-data [account counselor student]
          {tempids :tempids} (d/transact conn {:tx-data tx-data})
          {student-id "student", counselor-id counselor-id} tempids
          ecounselor (-> counselor
                         (assoc :db/id (str counselor-id))
                         (assoc :student/_counselor [{:db/id (str student-id)}])
                         (assoc :counselor/account account))
          expected {:status 200
                    :body [ecounselor]}
          actual (sut/handler {:datomic/conn conn
                               :session {:account/type :account-type/superuser}})
          abody (:body actual "nil")
          acounselor (first abody)
          aaccount (:counselor/account acounselor)]

      (should-be map? actual)
      (should= (-> expected keys sort) (-> actual keys sort))
      (should= (:status expected) (:status actual))
      (should= (:headers expected) (:headers actual))
      (should-be vector? abody)
      (should= 1 (count abody))
      (should-be map? acounselor)
      (should-be string? (:db/id acounselor))
      (should= (:db/id ecounselor) (:db/id acounselor))
      (should= (-> ecounselor keys set) (-> acounselor keys set))
      (should= (-> ecounselor :counselor/account keys set
                   (disj :account/password))
               (-> aaccount keys set))
      (should= email (:account/email aaccount))
      (should= name (:account/full-name aaccount))
      (should= :account-type/counselor (:account/type aaccount))
      (should= :account-state/initialized (:account/state aaccount))
      (should= (:student/_counselor ecounselor)
               (:student/_counselor acounselor)))))
