(ns com.premiumprep.app.handler.upload-document-spec
  (:require [clojure.java.io :as io]
            [com.premiumprep.app.handler.upload-document :as sut]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.storage :as storage]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-be
                                 should-have-invoked should= stub with
                                 with-stubs]])
  (:import java.util.Date))

(describe "slugify"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.upload-document
                         'slugify))
  (it "seems reasonable"
    (should= "étienne" (@sut "Étienne"))
    (should= "this-is-a-test-and-stuff"
             (@sut "  this is  a TEST! and stuff...…"))))

(describe "storage-name"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.upload-document
                         'storage-name))
  (with-stubs)
  (it "calls slugify on the given name and includes the given student eid"
    (with-redefs [sut/slugify (stub :slugify {:return "slugified"})]
      (should= "1234.slugified" (@sut 1234 "name")))))

(describe "name-conflict?"
  (with-stubs)
  (with sut @(ns-resolve 'com.premiumprep.app.handler.upload-document
                         'name-conflict?))
  (it "queries the database"
    (with-redefs [d/q (stub :q)]
      (@sut :db-val :storage-name)
      (should-have-invoked :q))))

(describe "transact-upload-metadata"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.upload-document
                         'transact-upload-metadata))
  (with-stubs)

  (it "calls d/transact with reasonable-looking tx-data"
    (let [name "user-assigned name"
          student-eid 1234
          storage-name "1234.slugified"
          content-type "application/octet-stream"
          uploaded-at (new Date 120 5 15 2 3 4)]
      (with-redefs [d/transact (stub :transact)
                    sut/uploaded-at (stub :uploaded-at {:return uploaded-at})]
        (@sut :db-connection student-eid name storage-name content-type)
        (should-have-invoked
          :transact
          {:times 1
           :with [:db-connection
                  {:tx-data [[:db/add student-eid :student/documents "doc"]
                             {:db/id "doc"
                              :document/name name
                              :document/content-type content-type
                              :document/uploaded-at uploaded-at
                              :document/s3-path storage-name}
                             [:db/add "datomic.tx" :transaction/namespace
                              "com.premiumprep.app.handler.upload-document"]]}]}))))

  (it "returns a document map"
    (let [name "user-assigned name"
          student-eid 1234
          storage-name "1234.slugified"
          content-type "application/octet-stream"
          uploaded-at (new Date 120 5 15 2 3 4)]
      (with-redefs [d/transact (stub :transact)
                    sut/uploaded-at (stub :uploaded-at {:return uploaded-at})]
        (let [result (@sut :db-connection student-eid name storage-name
                      content-type)]
          (should-be map? result)
          (should= #{:db/id :document/name :document/content-type
                     :document/uploaded-at}
                   (-> result keys set)))))))

(describe "the upload-document handler"
  (with-stubs)

  (around [it]
    (with-redefs [d/db (stub :db {:return :db-val})
                  sut/name-conflict? (stub :name-conflict?)]
      (it)))

  (it "rejects unauthorized requests"
    (should= 403 (:status (sut/handler nil))))

  (context "with authorization"
    (with account-id 12341234)
    (with session {:db/id (str @account-id)
                   :account/type :account-type/superuser})
    (with student-id 43214321)
    (with student {:db/id @student-id})
    (with storage-config {:storage/type :storage-type/none})
    (with sut (fn [req]
                (sut/handler
                  (assoc req :session @session
                         :storage-config @storage-config))))
    (around
      [it]
      (with-redefs [student/find-by-account-id (stub :find-by-account-id
                                                     {:return @student})
                    sut/storage-name (stub :storage-name
                                           {:return :storage-name})
                    storage/store (stub :store)
                    sut/transact-upload-metadata (stub
                                                   :transact-upload-metadata)]
        (it)))

    (it "calculates a stable name"
      (let [content-type "application/octet-stream"
            file {:content-type content-type :filename "a name of an uploaded file"}
            name "a user-assigned document name"]
        (@sut {:multipart-params {"name" name
                                  "student-id" (str @student-id)
                                  "file" file
                                  "link" nil}})
        (should-have-invoked :storage-name
                             {:times 1
                              :with [@student-id name]})))

    (it "checks for name conflicts"
      (let [content-type "application/octet-stream"
            file {:content-type content-type :filename "a name of an uploaded file"}
            name "a user-assigned document name"]
        (@sut {:multipart-params {"name" name "link" nil "file" file}})
        (should-have-invoked :name-conflict?
                             {:times 1
                              :with [:db-val :storage-name]})))

    (it "returns 409 with explanatory body if name conflict"
      (with-redefs [sut/name-conflict? (stub :name-conflict? {:return true})
                    sut/blank-document-name-params-error (stub :blank-document-name-params-error {:return false})
                    sut/missing-required-params-error (stub :missing-required-params-error {:return false})]
        (should= {:status 409
                  :body {:error (str "There was a conflict with the name of"
                                     " another of your documents."
                                     " Please choose another name.")}}
                 (@sut nil))))

    (context "when there is no name conflict"
      (it "returns 201 with the document body"
        (let [content-type "application/octet-stream"
              file {:content-type content-type :filename "a name of an uploaded file"}
              name "a user-assigned document name"
              multipart-params {"file" file "name" name}
              result (@sut {:multipart-params multipart-params})]
          (should= 201 (:status result))))

      (it "stores the given file"
        (@sut {:multipart-params {"file"       {:tempfile     :file-object
                                                :content-type "text/plain"
                                                :filename     "a name of an uploaded file"}
                                  "link"       nil
                                  "name"       "a user-assigned document name"
                                  "student-id" (str @student-id)}})
        (should-have-invoked :store
                             {:times 1
                              :with [@storage-config
                                     :storage-name
                                     :file-object
                                     "text/plain"]}))

      (it "transacts metadata to the database"
        (let [content-type "application/octet-stream"
              name "the user assigned name.doc"]
          (@sut {:datomic/conn :db-connection
                 :multipart-params {"name" name
                                    "link" nil
                                    "file" {:content-type content-type
                                            :filename "a name of an uploaded file"
                                            :tempfile (io/file "hello")}
                                    "student-id" (str @student-id)}})
          (should-have-invoked :transact-upload-metadata
                               {:times 1
                                :with [:db-connection
                                       @student-id
                                       name
                                       :storage-name
                                       content-type]}))))))
