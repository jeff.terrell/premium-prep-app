(ns com.premiumprep.app.handler.verify-token-spec
  (:require [com.premiumprep.app.handler.verify-token :as sut]
            [com.premiumprep.app.model.account :as account]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-have-invoked
                                 should= stub with with-stubs]]))

(describe "the verify-token handler"
  (with-stubs)

  (it "gets a database value from the given connection"
    (let [db-conn :db-connection
          db-stub-name :db]
      (with-redefs [d/db (stub db-stub-name)
                    account/find-by-token (stub :find-by-token)]
        (sut/handler {:datomic/conn db-conn})
        (should-have-invoked db-stub-name {:times 1, :with [db-conn]}))))

  (it "looks up the given token"
    (let [token "a-token"
          db-val :the-db-val
          find-stub-name :find-by-token]
      (with-redefs [d/db (stub :db {:return db-val})
                    account/find-by-token (stub find-stub-name)]
        (sut/handler {:query-params {"token" token}})
        (should-have-invoked find-stub-name {:times 1, :with [db-val token]}))))

  (it "returns a 404 if token not found"
    (let [token "a-token"
          db-val :the-db-val
          find-stub-name :find-by-token]
      (with-redefs [d/db (stub :db {:return db-val})
                    account/find-by-token (stub find-stub-name {:return nil})]
        (let [result (sut/handler {:query-params {"token" token}})]
          (should= 404 (:status result))
          (should= "not found" (-> result :body :error))))))

  (it "returns a 200 if token found"
    (let [token "a-token"
          db-val :the-db-val
          find-stub-name :find-by-token]
      (with-redefs [d/db (stub :db {:return db-val})
                    account/find-by-token (stub find-stub-name {:return true})]
        (should= 200 (:status (sut/handler {:query-params {"token" token}}))))))

  (it "returns corresponding email address if token found"
    (let [token "a-token"
          db-val :the-db-val
          find-stub-name :find-by-token
          email "an-email-address@emale.com"
          account {:account/email email}
          body {:email email}
          expected {:status 200, :body body}]
      (with-redefs [d/db (stub :db {:return db-val})
                    account/find-by-token (stub find-stub-name
                                                {:return account})]
        (should= expected (sut/handler {:query-params {"token" token}}))))))
