(ns com.premiumprep.app.handler.forgot-password-spec
  (:require [com.premiumprep.app.email :as email]
            [com.premiumprep.app.handler.forgot-password :as sut]
            [com.premiumprep.app.model.account :as account]
            [com.premiumprep.app.model.student :as student]
            [crypto.random :as random]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-contain
                                 should-have-invoked should= stub with
                                 with-stubs]]
            [speclj.stub :refer [first-invocation-of]]))

(describe "the forgot-password handler"
  (with-stubs)

  (it "always returns 204 (so as not to leak whether an email address exists)"
    (with-redefs [d/db (stub :db)
                  student/find-by-email (stub :find-by-email)]
      (should= {:headers {}, :status 204} (sut/handler nil))))

  (it "attempts to find a student with that email address"
    (let [db-val :the-database
          email "an-email-address@domain.com"
          req {:multipart-params {"email" email}}]
      (with-redefs [d/db (stub :db {:return db-val})
                    student/find-by-email (stub :find-by-email)]
        (sut/handler req)
        (should-have-invoked :db)
        (should-have-invoked :find-by-email {:times 1, :with [db-val email]}))))

  (it "returns a 403 when found account state is uninitialized"
    (let [account-eid 123412341234
          student-map {:student/account
                       {:db/id account-eid
                        :account/state {:db/ident :account-state/uninitialized}}}
          email "an-email-address@domain.com"
          req {:multipart-params {"email" email}}
          msg (str "Error: you cannot reset the password of an uninitialized "
                   "account. Please check your email for the initial welcome "
                   "email, and click the link inside it. Then you will be "
                   "able to set a password.")]
      (with-redefs [d/db (stub :db {:return :the-database})
                    student/find-by-email (stub :find-by-email
                                                {:return student-map})]
        (should= {:headers {}, :status 403, :body msg}
                 (sut/handler req)))))

  (context "when a student with that email address is found"
    (with account-eid 123412341234)
    (with student-map {:student/account {:db/id @account-eid}})
    (with email "an-email-address@domain.com")
    (with req {:multipart-params {"email" @email}})
    (around [it]
            (with-redefs [d/db (stub :db {:return :the-database})
                          student/find-by-email (stub :find-by-email
                                                      {:return @student-map})]
              (it)))

    (it "generates a new token value"
      (with-redefs [random/url-part (stub :random-url-part)
                    account/save-token (stub :save-token)
                    email/forgot-password (stub :email)]
        (sut/handler @req)
        (should-have-invoked :random-url-part {:times 1, :with [64]})))

    (it "transacts the token value in the database for later retrieval"
      (let [conn :db-connection
            token :a-random-token]
        (with-redefs [random/url-part (stub :random-url-part {:return token})
                      account/save-token (stub :save-token)
                      email/forgot-password (stub :email)]
          (sut/handler (assoc @req :datomic/conn conn))
          (should-have-invoked :save-token
                               {:times 1, :with [conn @account-eid token]}))))

    (it "sends an email with the token value to the given address"
      (let [token "Dp02-rHCh1qGs8CUQd4flo7Lhoe_b9ciHLjFC3EQPfZJC2Plf9t_w"
            email-send-stub-name :the-email-send-fn
            email-send-fn (stub email-send-stub-name)]
        (with-redefs [random/url-part (stub :random-url-part {:return token})
                      account/save-token (stub :save-token)]
          (sut/handler (assoc @req :email-send-fn email-send-fn))
          (should-have-invoked email-send-stub-name {:times 1})
          (let [invocation (first-invocation-of email-send-stub-name)
                [to _ body] invocation]
            (should= @email to)
            (should-contain token body)))))))
