(ns com.premiumprep.app.handler.create-custom-field-spec
  (:refer-clojure :exclude [name])
  (:require [com.premiumprep.app.handler.create-custom-field :as sut]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-have-invoked
                                 should= stub with with-stubs]]))

(describe "create-custom-field/missing-required-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.create-custom-field
               'missing-required-params-error))
  (with good-params {"entry-id" "234"
                     "name" "location"
                     "value" "someplace wonderful"})

  (it "detects a missing param"
    (should= "Missing parameter: entry-id"
             (@sut (dissoc @good-params "entry-id")))
    (should= "Missing parameter: name"
             (@sut (dissoc @good-params "name")))
    (should= "Missing parameter: value"
             (@sut (dissoc @good-params "value"))))

  (it "detects multiple missing fields"
    (should= "Missing parameters: name, value"
             (@sut (dissoc @good-params "name" "value")))))

(describe "create-custom-field/nonnumeric-params-error"
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.create-custom-field
               'nonnumeric-params-error))
  (with good-params {"entry-id" "123"})

  (it "detects a nonnumeric param"
    (should= "Non-numeric parameter: entry-id"
             (@sut (assoc @good-params "entry-id" "abc")))))

(describe "create-custom-field/authorized?"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.create-custom-field
               'authorized?))

  (it "rejects unauthenticated users"
    (should= false (boolean (@sut nil nil nil))))

  (it "rejects students for counselor-only fields"
    (should= false (boolean (@sut nil {:account/type :account-type/student}
                             {"field" "counselor-notes"}))))

  (it "rejects students who aren't the same student as in the request body"
    (let [id 1234
          acct-id 432423
          session {:account/type :account-type/student, :db/id (str acct-id)}
          params {"field" "student-notes", "student-id" (str id)}
          student {:db/id (str id "1")}]
      (with-redefs [sut/account-id (stub :account-id {:return (inc acct-id)})]
        (should= false (@sut :db-val session params))
        (should-have-invoked :account-id {:times 1, :with [:db-val params]}))))

  (it "allows authorized users"
    (let [id 1234
          acct-id 432423
          session {:account/type :account-type/student, :db/id (str acct-id)}
          params {"field" "student-notes", "student-id" (str id)}
          student {:db/id (str id)}]
      (with-redefs [sut/account-id (stub :account-id {:return acct-id})]
        (should= true (@sut :db-val session params))
        (should-have-invoked :account-id {:times 1, :with [:db-val params]})))))

(describe "the create-custom-field handler"
  (with-stubs)
  (it "returns status 400 if required fields are missing"
    (with-redefs [sut/missing-required-params-error (stub :stub
                                                          {:return "errmsg"})]
      (should= 400 (:status (sut/handler {:multipart-params :the-params})))
      (should-have-invoked :stub {:times 1, :with [:the-params]})))

  (context "with no missing fields"
    (around [it]
      (with-redefs [sut/missing-required-params-error (stub :mrpe)]
        (it)))

    (it "returns status 400 if any id fields are not numeric"
      (with-redefs [sut/nonnumeric-params-error (stub :stub {:return "errmsg"})]
        (should= 400 (:status (sut/handler {:multipart-params :the-params})))
        (should-have-invoked :stub {:times 1, :with [:the-params]})))

    (context "with numeric id fields"
      (around [it]
        (with-redefs [sut/nonnumeric-params-error (stub :nnpe)]
          (it)))

      (it "returns status 403 if request not authorized"
        (let []
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :stub {:return nil})]
            (should= 403 (:status (sut/handler {:datomic/conn :db-conn
                                                :multipart-params :the-params
                                                :session :the-session})))
            (should-have-invoked :db {:times 1, :with [:db-conn]})
            (should-have-invoked :stub
                                 {:times 1
                                  :with [:db-val :the-session :the-params]}))))

      (context "with an authorized request"
        (around [it]
          (with-redefs [d/db (stub :db {:return :db-val})
                        sut/authorized? (stub :authorized? {:return true})]
            (it)))

        (it "returns status 404 if referenced college-list-entry not found"
          (let [entry-id 2345
                params {"entry-id" (str entry-id)
                        "name" "student-notes"
                        "value" "new notes"}]
            (with-redefs [sut/entry-exists? (stub :stub)]
              (should= 404 (:status (sut/handler {:multipart-params params})))
              (should-have-invoked :stub
                                   {:times 1
                                    :with [:db-val entry-id]}))))

        (context "with a found college-list-entry"
          (with entry-id 2345)
          (with name "hackathons")
          (with value "4 last year")
          (with params {"entry-id" (str @entry-id)
                        "name" @name
                        "value" @value})
          (with sut #(sut/handler {:datomic/conn :db-conn
                                   :multipart-params @params}))

          (around [it]
            (with-redefs [sut/entry-exists? (stub :stub {:return true})]
              (it)))

          (it "calls happy-path fn and returns its returned value"
            (with-redefs [sut/happy-path (stub :stub {:return :hprv})]
              (should= :hprv (@sut))
              (should-have-invoked :stub
                                   {:times 1
                                    :with [:db-conn @entry-id @name
                                           @value]}))))))))

(describe "create-custom-field/happy-path"
  (with-stubs)
  (with sut @(ns-resolve
               'com.premiumprep.app.handler.create-custom-field
               'happy-path))
  (with entry-id 9876)
  (with name "hackathons")
  (with value "4 last year")
  (with params {"entry-id" @entry-id
                "name" @name
                "value" @value})
  (with response (@sut :db-conn @entry-id @name @value))

  (around [it]
    (with-redefs [d/transact (stub :transact)]
      (it)))

  (it "returns status 201 response"
    (should= 201 (:status @response)))

  (it "calls transact"
    @response
    (let [tx-data [[:db/add @entry-id :college-list-entry/custom-fields
                    "custom-field"]
                   {:db/id "custom-field"
                    :custom-field/name @name
                    :custom-field/value @value}
                   [:db/add "datomic.tx" :transaction/namespace
                    "com.premiumprep.app.handler.create-custom-field"]]]
      (should-have-invoked :transact
                           {:times 1
                            :with [:db-conn {:tx-data tx-data}]})))

  (it "returns entry data for response body"
    (let [field-id 43214321]
      (with-redefs [d/transact (stub :tx {:return
                                          {:tempids {"custom-field" field-id}}})]
        (should= {:db/id (str field-id)
                  :custom-field/name @name
                  :custom-field/value @value}
                 (:body @response))))))
