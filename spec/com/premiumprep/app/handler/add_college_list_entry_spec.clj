(ns com.premiumprep.app.handler.add-college-list-entry-spec
  (:require [com.premiumprep.app.handler.add-college-list-entry :as sut]
            [com.premiumprep.app.model.college :as college]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :as util]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should= should-be
                                 should-not= should-have-invoked stub with
                                 with-stubs]]))

(describe "add-college-list-entry/advisor?"
  (with-stubs)

  (with sut @(ns-resolve 'com.premiumprep.app.handler.add-college-list-entry
                         'advisor?))

  (it "returns falsy when student-id is nil"
    (should= false (boolean (@sut :db-val nil :account-id))))

  (it "returns true when needed"
    (let [student-id 123
          account-id 234
          student {:student/counselor {:counselor/account {:db/id account-id}}}]
      (with-redefs [student/find (stub :find {:return student})]
        (should= true (@sut :db-val (str student-id) (str account-id)))))))

(describe "add-college-list-entry/authorized?"
  (with-stubs)

  (with sut @(ns-resolve 'com.premiumprep.app.handler.add-college-list-entry
                         'authorized?))

  (it "accepts superusers regardless of student id"
    (should= true (@sut :db-val nil {:account/type :account-type/superuser})))

  (it "returns false if account type is neither superuser nor counselor"
    (should= false (@sut :db-val nil {:account/type :account-type/student})))

  (it "defers to advisor check if account type is counselor"
    (with-redefs [sut/advisor? (stub :advisor? {:return :advisor-return})]
      (let [student-id 123
            session-id 234]
        (should= :advisor-return (@sut :db-val student-id
                                  {:account/type :account-type/counselor
                                   :db/id session-id}))
        (should-have-invoked :advisor?
                             {:times 1
                              :with [:db-val student-id session-id]})))))

(describe "add-college-list-entry/request-error"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.add-college-list-entry
                         'request-error))

  (it "detects when any param is missing"
    (should=
      "The following required parameters are missing: category, college-id, student-id"
      (@sut {}))
    (should= "The following required parameter is missing: category"
             (@sut {"college-id" "a"
                    "student-id" "432"}))
    (should= "The following required parameter is missing: category"
             (@sut {"college-id" "a", "category" "", "student-id" "432"})))

  (it "detects when either ID field is not numeric"
    (should= "The college-id parameter is not numeric"
             (@sut {"college-id" "foo", "category" "reach", "student-id" "432"}))
    (should= "The student-id parameter is not numeric"
             (@sut {"college-id" "21", "category" "reach", "student-id" "foo"})))

  (it "detects when category is invalid"
    (should= "The category parameter is invalid"
             (@sut {"college-id" "123", "category" "foo", "student-id" "432"})))

  (it "returns nil given valid params"
    (should= nil (@sut {"college-id" "123"
                        "category" "reach"
                        "student-id" "432"}))))

(describe "the add-college-list-entry handler"
  (with-stubs)
  (around [it]
    (with-redefs [d/db (stub :db {:return :db-val})]
      (it)))

  (it "checks authorization and returns 403 if unauthorized"
    (with-redefs [sut/authorized? (stub :authorized? {:return false})]
      (let [student-id 1243
            params {"student-id" student-id}
            request {:multipart-params params, :session :the-session}]
        (should= 403 (:status (sut/handler request)))
        (should-have-invoked :authorized?
                             {:times 1
                              :with [:db-val student-id :the-session]}))))

  (context "with an authorized request"
    (around [it]
      (with-redefs [sut/authorized? (stub :authorized? {:return true})]
        (it)))

    (it "checks request validity and returns 400 if invalid"
      (with-redefs [sut/request-error (stub :stub {:return :err-msg})]
        (let [response (sut/handler {:multipart-params :some-params})]
          (should= 400 (:status response))
          (should= :err-msg (-> response :body :error)))))

    (context "with a valid request"
      (with student-id 12341234)
      (with college-id 43214321)
      (with params {"student-id" (str @student-id)
                    "college-id" (str @college-id)
                    "category" "reach"})
      (with sut (fn [req]
                  (sut/handler (assoc req :multipart-params @params))))
      (around [it]
        (with-redefs [sut/request-error (stub :request-error)
                      sut/next-index-for-list (stub :ni4l {:return 1})]
          (it)))

      (it "tries to find the referenced student and returns 404 if not found"
        (with-redefs [student/find (stub :find)]
          (should= 404 (:status (@sut nil)))
          (should-have-invoked :find {:times 1, :with [:db-val @student-id]})))

      (context "with a found student"
        (with existing-college-id 43210987)
        (with category :college-list-category/reach)
        (with date-added (java.util.Date.))
        (with college-list-entries
          [{:college-list-entry/category {:db/ident @category}
            :college-list-entry/index 0
            :college-list-entry/date-added @date-added
            :college-list-entry/college {:db/id @existing-college-id}}])
        (with student {:student/college-list-entries @college-list-entries})
        (around [it]
          (with-redefs [student/find (stub :find-student {:return @student})]
            (it)))

        (it "tries to find the referenced college and returns 404 if not found"
          (with-redefs [college/find (stub :find)]
            (should= 404 (:status (@sut nil)))
            (should-have-invoked :find {:times 1, :with [:db-val @college-id]})))

        (context "with a found college"
          (with response (@sut nil))
          (around [it]
            (with-redefs [college/find (stub :find-college {:return true})
                          d/transact (stub :transact)]
              (it)))

          (it "checks for a conflict and returns 409 if so"
            (with-redefs [sut/college-already-in-list? (stub :stub
                                                             {:return true})]
              (should= 409 (:status (@sut nil)))
              (should-have-invoked :stub
                                   {:times 1
                                    :with [:db-val @student-id @college-id]})))

          (context "with no conflict"
            (around [it]
              (with-redefs [sut/college-already-in-list? (stub :conflict?
                                                               {:return false})
                            sut/next-index-for-list (stub :next/index
                                                          {:return 1})]
                (it)))

            (it "returns a status of 201"
              (should= 201 (:status @response)))

            (it "returns the updated college list"
              (with-redefs [util/student-result (stub :result
                                                      {:return
                                                       {:student/college-lists
                                                        :the-lists}})]
                (should= :the-lists
                         (:body @response))))

            (it "transacts the new college-list-entry entity correctly"
              @response ; to force the request
              (let [tx-data
                    [[:db/add @student-id :student/college-list-entries "cle"]
                     {:db/id "cle"
                      :college-list-entry/category @category
                      :college-list-entry/index 1
                      :college-list-entry/date-added @date-added
                      :college-list-entry/college @college-id}
                     [:db/add "datomic.tx" :transaction/namespace
                      "com.premiumprep.app.handler.add-college-list-entry"]]]
                (should-have-invoked :transact
                                     {:times 1
                                      :with [nil {:tx-data tx-data}]})))))))))
