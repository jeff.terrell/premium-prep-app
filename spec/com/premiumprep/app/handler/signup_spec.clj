(ns com.premiumprep.app.handler.signup-spec
  (:require
   [clojurewerkz.scrypt.core :as sc]
   [com.premiumprep.app.handler.signup :as sut]
   (com.premiumprep.app.model
     [account :as account]
     [student :as student])
   [com.premiumprep.app.schema :as schema]
   [com.premiumprep.app.validation :as validation]
   [datomic.client.api :as d]
   [speclj.core :refer [around context describe it should should-be
                        should-contain should-have-invoked
                        should-not-have-invoked should= stub with with-stubs]]))

(describe "signup/bad-token"
  (with-stubs)
  (with sut @(ns-resolve 'com.premiumprep.app.handler.signup 'bad-token))

  (it "attempts to find the given token in the given database"
    (with-redefs [account/find-by-token (stub :find-by-token)]
      (@sut :db-val :token)
      (should-have-invoked :find-by-token {:times 1, :with [:db-val :token]})))

  (it "returns nil when the token is found"
    (with-redefs [account/find-by-token (stub :find-by-token {:return true})]
      (should= nil (@sut nil nil))))

  (it "returns an explanatory error message when the token is not found"
    (with-redefs [account/find-by-token (stub :find-by-token {:return nil})]
      (should= (str "Error: your signup token could not be found."
                    " Please ensure that you are at the address "
                    "included in your welcome email.")
               (@sut nil nil)))))

(describe "signup/mismatched-passwords"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.signup
                         'mismatched-passwords))
  (it "returns nil if passwords match"
    (should= nil (@sut "pass" "pass")))
  (it "returns an explanatory error message if passwords don't match"
    (should= "The given passwords do not match"
             (@sut "a-pass" "a-different-pass"))))

(describe "signup/error-messages"
  (with-stubs)
  (with sut @(ns-resolve 'com.premiumprep.app.handler.signup 'error-messages))
  (around [it]
    (with-redefs [sut/bad-token (stub :bad-token)
                  sut/mismatched-passwords (stub :mismatched-passwords)
                  validation/validate-params (stub :validate)]
      (it)))

  (it "calls bad-token with the right arguments"
    (let [token "arst;yulqwfpoien"
          db-val :a-database-val]
      (@sut db-val {"token" token})
      (should-have-invoked :bad-token {:times 1, :with [db-val token]})))

  (it "calls mismatched-passwords with the right arguments"
    (let [p1 "arst;yulqwfpoien"
          p2 "tisenrtierntsienrsietnresitnrsie"]
      (@sut :db-val {"password" p1, "confirmation" p2})
      (should-have-invoked :mismatched-passwords
                           {:times 1, :with [p1 p2]})))

  (it "calls validate-params with the right arguments"
    (let [fields @(ns-resolve 'com.premiumprep.app.handler.signup 'fields)
          params {"token" "a token value"}]
      (@sut :db params)
      (should-have-invoked :validate {:times 1, :with [fields params]}))))

(describe "signup/params->tx-data"
  (with-stubs)
  (with sut @(ns-resolve 'com.premiumprep.app.handler.signup 'params->tx-data))
  (with fields @(ns-resolve 'com.premiumprep.app.handler.signup 'fields))
  (with token "the-token")
  (with params {"token" @token})
  (with account-eid 43210)
  (with student-eid 12345)
  (around [it]
    (with-redefs [sut/error-messages (stub :error-messages)
                  account/find-by-token (stub :find-account
                                              {:return {:db/id @account-eid}})
                  student/find-by-account-id (stub :find-student
                                                 {:return {:db/id @student-eid}})
                  validation/fields-tx-data (stub :fields-tx-data)]
      (it)))

  (it "invokes error-messages"
    (@sut :db @params)
    (should-have-invoked :error-messages {:times 1, :with [:db @params]}))

  (it "returns false and retval of error-messages when there are errors"
    (let [err-msg "a combined error message"]
      (with-redefs [sut/error-messages (stub :error-messages {:return err-msg})]
        (should= [false err-msg] (@sut nil nil)))))

  (it "returns true and retval of fields-tx-data + 1 datom when no errors"
    (should= [true #{[:db/add @account-eid
                      :account/state :account-state/initialized]
                     [:db/add "datomic.tx" :transaction/namespace
                      "com.premiumprep.app.handler.signup"]}]
             (update (@sut :db @params) 1 set))
    (should-have-invoked :find-account {:times 1, :with [:db @token]})
    (should-have-invoked :find-student {:times 1, :with [:db @account-eid]})
    (should-have-invoked :fields-tx-data
                         {:times 1, :with [:db @student-eid @fields @params]})))

(describe "the signup handler"
  (with-stubs)

  (it "returns an explanatory error response if given invalid data"
    (with-redefs [d/db (stub :db)
                  sut/params->tx-data (stub :stub {:return [false "err-msg"]})]
      (should= {:status 400
                :headers {"content-type" "text/plain"}
                :body "err-msg"}
               (sut/handler nil))))

  (it "calls transact with generated tx-data if given valid data"
    (let [conn :db-connection
          tx-data :some-transaction-data]
      (with-redefs [d/db (stub :db)
                    sut/params->tx-data (stub :stub {:return [true tx-data]})
                    d/transact (stub :transact)]
        (sut/handler {:datomic/conn conn})
        (should-have-invoked :transact
                             {:times 1, :with [conn {:tx-data tx-data}]}))))

  (it "returns a 200 response with student data given valid data"
    (with-redefs [d/db (stub :db)
                  sut/params->tx-data (stub :stub {:return [true :tx-data]})
                  d/transact (stub :transact)]
      (should= {:status 204, :headers {}}
               (sut/handler {:datomic/conn :connection}))))

  (it "reuses a parent entity with the same email address"
    (let [client (d/client {:server-type :dev-local
                            :storage-dir :mem
                            :system "local"})
          db-name (name ::reuse-duplicate-parent-email)
          _ (d/create-database client {:db-name db-name})
          conn (d/connect client {:db-name db-name})
          _ (schema/install-schema! conn)
          student1-email "stu1@email.com"
          student2-email "stu2@email.com"
          parent-email "parent@email.com"
          token1 "abcdefg"
          token2 "hijklmnop"

          base-params {"graduation-year" "2023"
                       "city" "a city"
                       "parent1-full-name" "parent name"
                       "high-school" "high school"
                       "address" "a address"
                       "full-name" "my name"
                       "parent1-email" parent-email
                       "parent1-mobile" "1234554321"
                       "state" "ML"
                       "mobile" "5432112345"
                       "zip" "12345"
                       "password" "password"
                       "confirmation" "password"}
          make-params #(assoc base-params "token" %1)
          make-request (fn [token] {:datomic/conn conn
                                    :multipart-params (make-params token)})]

      (d/transact conn {:tx-data [{:db/id "s1acct"
                                   :account/email student1-email
                                   :account/token token1
                                   :account/type :account-type/student}
                                  {:db/id "s1", :student/account "s1acct"}
                                  {:db/id "s2acct"
                                   :account/email student2-email
                                   :account/token token2
                                   :account/type :account-type/student}
                                  {:db/id "s2", :student/account "s2acct"}]})

      (should= 204 (-> token1 make-request sut/handler :status))
      (should= 204 (-> token2 make-request sut/handler :status))
      (should= #{[student1-email] [student2-email]}
               (set (d/q {:query '{:find [?student-email]
                                   :in [$ ?parent-email]
                                   :where [[?parent :account/email ?parent-email]
                                           [?student :student/parent1 ?parent]
                                           [?student :student/account ?saccount]
                                           [?saccount :account/email ?student-email]]}
                          :args [(d/db conn) parent-email]}))))))
