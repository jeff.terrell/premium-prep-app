(ns com.premiumprep.app.handler.re-send-invite-spec
  (:require
   [com.premiumprep.app.email :as email]
   [com.premiumprep.app.handler.re-send-invite :as sut]
   [com.premiumprep.app.model.account :as account]
   [speclj.core :refer [around context describe it should= should-have-invoked
                        stub with with-stubs]]
   [datomic.client.api :as d]))

(describe "the re-send-invite handler"
  (with-stubs)
  (it "returns 404 if account id not numeric"
    (should= 404 (:status (sut/handler {:reitit.core/match {:path-params
                                                            {:id "abc"}}}))))

  (context "with a numeric account id"
    (with account-id 1234)
    (with sut (fn [req]
                (sut/handler (merge {:reitit.core/match
                                     {:path-params {:id (str @account-id)}}}
                                    req))))

    (it "returns 403 status if not authorized"
      (should= 403 (:status (@sut nil))))

    (context "given an authorized request"
      (with sut2 (fn [req]
                   (@sut (merge {:session
                                 {:account/type :account-type/superuser}}
                                req))))

      (it "returns 404 status if specified account not found"
        (with-redefs [account/find (stub :find)
                      d/db (stub :db {:return :db-val})]
          (should= 404 (:status (@sut2 nil)))
          (should-have-invoked :find {:times 1, :with [:db-val @account-id]})))

      (context "with a known account"
        (with email "an-email-address")
        (with token "an-unguessable-token")
        (with account {:account/email @email, :account/token @token})
        (around [it]
          (with-redefs [account/find (stub :find {:return @account})
                        d/db (stub :db {:return :db-val})
                        email/welcome-student (stub :welcome-email)]
            (it)))

        (it "sends a welcome email"
          (let [email-send-fn :send-an-email-yo]
            (@sut2 {:email-send-fn email-send-fn})
            (should-have-invoked :welcome-email
                                 {:times 1
                                  :with [email-send-fn @email @token]})))

        (it "returns 204 status"
          (should= 204 (:status (@sut2 nil))))))))
