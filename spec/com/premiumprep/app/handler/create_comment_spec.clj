(ns com.premiumprep.app.handler.create-comment-spec
  (:refer-clojure :exclude [comment])
  (:require [com.premiumprep.app.handler.create-comment :as sut]
            [com.premiumprep.app.model.comment :as comment]
            [com.premiumprep.app.model.student :as student]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should= should-be
                                 should-not= should-have-invoked stub with
                                 with-stubs]]))

(describe "create-comment"
  (with-stubs)

  (it "calls d/transact with reasonable args and returns the new db/id"
    (let [text "this is a comment"
          student-id 1234
          comment-type "profile-comments"
          params {"comment" text
                  "comment-type" comment-type
                  "student-id" (str student-id)}
          attr (keyword "student" comment-type)
          commenter-id 12345
          sut @(ns-resolve 'com.premiumprep.app.handler.create-comment
                           'create-comment)
          comment-map {:db/id "comment"
                       :comment/text text
                       :comment/created-at ::created-at
                       :comment/commenter commenter-id}
          rel-datom [:db/add student-id attr "comment"]
          tx-map {:tx-data [comment-map rel-datom]}
          id 43214321]

      (with-redefs [d/transact (stub :transact
                                     {:return {:tempids {"comment" id}}})
                    sut/created-at (stub :created-at {:return ::created-at})]
        (should= id (sut :db-connection params commenter-id))
        (should-have-invoked :transact
                             {:times 1, :with [:db-connection tx-map]})))))

(describe "the create-comment handler"
  (with-stubs)
  (with student-id 12)
  (with student-acct-id 123)
  (with counselor-id 1234)
  (with counselor-acct-id 12345)
  (with email "iama-student@email.com")
  (with full-name "Iama Student")
  (with student {:db/id @student-id
                 :student/account {:db/id @student-acct-id
                                   :account/email @email
                                   :account/full-name @full-name}
                 :student/counselor {:db/id @counselor-id
                                     :counselor/account {:db/id
                                                         @counselor-acct-id}}})
  (around [it]
    (with-redefs [d/db (stub :db {:return :db-val})
                  student/find (stub :find-student {:return @student})]
      (it)))

  (it "returns 404 if given student not given or not found"
    (with-redefs [student/find (stub :find-student {:return nil})]
      (should= 404 (:status (sut/handler nil)))
      (should= 404 (:status (sut/handler
                              {:multipart-params {"student-id" "1"}})))))

  (it "rejects counselors who don't counsel the given student"
    (should= 403 (:status (sut/handler
                            {:multipart-params {"student-id" "1"}
                             :session {:db/id (str @counselor-acct-id "0")}}))))

  (it "accepts counselors who do counsel the given student"
    (should-not= 403 (:status (sut/handler
                                {:session {:db/id (str @counselor-acct-id)}}))))

  (it "rejects students who aren't the given student"
    (should= 403 (:status (sut/handler
                            {:multipart-params {"student-id" "1"}
                             :session {:db/id (str @student-acct-id "0")}}))))

  (it "accepts students who are the given student"
    (should-not= 403 (:status (sut/handler
                                {:session {:db/id (str @student-acct-id)}}))))

  (it "accepts administrators"
    (should-not= 403 (:status (sut/handler
                                {:multipart-params {"student-id" "1"}
                                 :session {:account/type :account-type/superuser
                                           :db/id "1"}}))))

  (context "with an authorized user"
    (with sut (fn [req]
                (sut/handler (merge {:session {:db/id (str @student-acct-id)}}
                                    req))))

    (it "returns 400 with explanation if given text is empty"
      (let [params {"comment-type" "profile-comments"
                    "student-id" "1"}
            response (@sut {:multipart-params params})]
        (should= 400 (:status response))
        (should= "Error: no comment text supplied."
                 (-> response :body :error))))

    (it "returns 400 with explanation if given comment-type is invalid"
      (let [params {"comment" "something"
                    "comment-type" "invalid-value"
                    "student-id" "1"}
            response (@sut {:multipart-params params})]
        (should= 400 (:status response))
        (should= "Error: comment-type param should be either profile-comments or document-list-comments."
                 (-> response :body :error))))

    (context "with a valid request"
      (with comment-text "some comment text")
      (with params {"comment" @comment-text
                    "comment-type" "profile-comments"
                    "student-id" "1"})
      (with new-id 987)
      (with comment {:db/id @new-id
                     :comment/text @comment-text
                     :comment/created-at ::created-at
                     :comment/commenter {:db/id 9876
                                         :account/full-name (str @full-name "_")
                                         :account/email (str @email "_")}})
      (with sut2 (fn []
                   (with-redefs [comment/find (stub :find-comment
                                                    {:return @comment})
                                 sut/create-comment (stub :create-comment
                                                          {:return @new-id})
                                 sut/created-at (stub :created-at
                                                      {:return ::created-at})
                                 sut/send-email (stub :send-email)]
                     (sut/handler {:datomic/conn :db-connection
                                   :multipart-params @params
                                   :session {:db/id (str @student-acct-id)}}))))

      (it "creates a comment"
        (@sut2)
        (should-have-invoked
          :create-comment
          {:times 1, :with [:db-connection @params @student-acct-id]}))

      (it "returns 201"
        (should= 201 (:status (@sut2))))

      (it "returns created comment as data"
        (let [body-val (:body (@sut2))]
          (should= (-> @comment
                       (update :db/id str)
                       (update-in [:comment/commenter :db/id] str))
                   body-val)
          (should-have-invoked :find-comment
                               {:times 1, :with [:db-val @new-id]}))))))
