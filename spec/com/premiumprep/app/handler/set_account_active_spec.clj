(ns com.premiumprep.app.handler.set-account-active-spec
  (:refer-clojure :exclude [name])
  (:require [com.premiumprep.app.handler.set-account-active :as sut]
            [com.premiumprep.app.model.session :as session]
            [com.premiumprep.app.schema :as schema]
            [datomic.client.api :as d]
            [speclj.core :refer [describe it should-be should-have-invoked
                                 should= stub with]]))

(def email "email@email.com")
(def pass "this is a passphrase")
(def name "Obi-Wan Kenobi")

(defn transact-account [active?]
  (let [client (d/client {:server-type :dev-local
                          :storage-dir :mem
                          :system "local"})
        opts {:db-name "set-account-active-spec"}
        _ (d/delete-database client opts)
        _ (d/create-database client opts)
        conn (d/connect client opts)
        _ (schema/install-schema! conn)
        account {:db/id "account"
                 :account/email email
                 :account/password pass
                 :account/full-name name
                 :account/active? active?
                 :account/type :account-type/counselor}
        tx-data [account]
        {tempids :tempids} (d/transact conn {:tx-data tx-data})
        {account-id "account"} tempids]
    [conn account-id]))

(defn make-req [conn id atype active?]
  {:datomic/conn conn
   :reitit.core/match {:path-params {:id (str id)}
                       :data {:active? active?}}
   :session {:account/type atype}})

(describe "the set-account-active handler"
  (it "returns forbidden for non-superuser sessions"
    (let [[conn id] (transact-account true)
          req (make-req conn id :account-type/counselor false)]
      (should= 403 (:status (sut/handler req)))))

  (it "returns not found when given account not found"
    (let [[conn id] (transact-account true)
          non-id (dec id)
          req (make-req conn non-id :account-type/superuser false)]
      (should= 404 (:status (sut/handler req)))))

  (it "returns success for authorized session and found account"
    (let [[conn id] (transact-account true)
          req (make-req conn id :account-type/superuser false)]
      (should= 204 (:status (sut/handler req)))))

  (it "makes a transaction on the database to deactivate a user"
    (let [[conn id] (transact-account true)
          req (make-req conn id :account-type/superuser false)]
      (sut/handler req)
      (let [active? (d/pull (d/db conn) [:account/active?] id)]
        (should= {:account/active? false} active?))))

  (it "makes a transaction on the database to reactivate a user"
    (let [[conn id] (transact-account true)
          _ (d/transact conn {:tx-data [[:db/add id :account/active? false]]})
          req (make-req conn id :account-type/superuser true)]
      (sut/handler req)
      (let [active? (d/pull (d/db conn) [:account/active?] id)]
        (should= {:account/active? true} active?))))

  (it "is idempotent"
    (let [[conn id] (transact-account false)
          req (make-req conn id :account-type/superuser false)]
      (should= 204 (:status (sut/handler req)))))

  (it "also retracts any existing sessions when deactivating a user"
    (let [[conn id] (transact-account true)
          session-id (session/create conn id)
          req (make-req conn id :account-type/superuser false)
          get-sessions #(mapv :session/id
                              (session/find-all-for-account-id (d/db conn) id))]
      (should= [session-id] (get-sessions))
      (sut/handler req)
      (should= [] (get-sessions)))))
