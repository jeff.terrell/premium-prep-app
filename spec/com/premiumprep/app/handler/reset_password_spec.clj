(ns com.premiumprep.app.handler.reset-password-spec
  (:require [com.premiumprep.app.handler.reset-password :as sut]
            [com.premiumprep.app.model.account :as account]
            [crypto.random :as random]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-have-invoked
                                 should= stub with with-stubs]]))

(describe "the reset-password handler"
  (with-stubs)

  (it "gets a database value from the given connection"
    (let [db-conn :db-connection
          db-stub-name :db]
      (with-redefs [d/db (stub db-stub-name)
                    account/find-by-token (stub :find-by-token)]
        (sut/handler {:datomic/conn db-conn})
        (should-have-invoked db-stub-name {:times 1, :with [db-conn]}))))

  (it "looks up the given token"
    (let [token "a-token"
          db-val :the-db-val
          find-stub-name :find-by-token]
      (with-redefs [d/db (stub :db {:return db-val})
                    account/find-by-token (stub find-stub-name)]
        (sut/handler {:multipart-params {"token" token}})
        (should-have-invoked find-stub-name {:times 1, :with [db-val token]}))))

  (it "returns a 404 with explanatory body if token not found"
    (let [token "a-token"
          db-val :the-db-val
          find-stub-name :find-by-token
          headers {"content-type" "text/plain"}
          body (str "Error: the given token could not be found in the "
                    "database.\n\nPlease ensure that you are at the address "
                    "included in your password reset email. If so, you may "
                    "need to send a new password reset email.\n")
          expected {:status 404, :headers headers, :body body}]
      (with-redefs [d/db (stub :db {:return db-val})
                    account/find-by-token (stub find-stub-name {:return nil})]
        (should= expected (sut/handler {:multipart-params {"token" token}})))))

  (it "returns 400 w/ explanatory body if token found but passwords don't match"
    (let [token "a-token"
          find-stub-name :find-by-token
          headers {"content-type" "text/plain"}
          body "Error: the given passwords do not match. Please try again.\n"
          expected {:status 400, :headers headers, :body body}
          params {"token" token, "password" "a", "confirmation" "b"}]
      (with-redefs [d/db (stub :db {:return :the-db-val})
                    account/find-by-token (stub find-stub-name {:return true})]
        (should= expected (sut/handler {:multipart-params params})))))

  (it "returns a 403 when found account state is uninitialized"
    (let [token "a-token"
          find-stub-name :find-by-token
          account-map {:account/state {:db/ident :account-state/uninitialized}}
          headers {"content-type" "text/plain"}
          body (str "Error: you cannot reset a password for an uninitialized "
                    "account.\nPlease find the email from us with a link for "
                    "setting up your account.\n")
          expected {:status 403, :headers headers, :body body}
          params {"token" token, "password" "a", "confirmation" "a"}]
      (with-redefs [d/db (stub :db {:return :the-db-val})
                    account/find-by-token (stub find-stub-name
                                                {:return account-map})]
        (should= expected (sut/handler {:multipart-params params})))))

  (context "given a found token and matching passwords"
    (with token "BZ6HboGh9FkRRXdGu8eb5dZ2kkagT-7-3QzMp-cbgtc")
    (with db-val :the-db-val)
    (with password "JVti4SCXOvXFqmIFG758iw")
    (with params {"token" @token
                  "password" @password
                  "confirmation" @password})
    (with account-eid 123412342134)
    (with account-map {:db/id @account-eid})
    (around [it]
      (with-redefs [d/db (stub :db {:return :the-db-val})
                    account/find-by-token (stub :find-by-token
                                                {:return @account-map})]
        (it)))

    (it "transacts a new password for user holding the token"
      (with-redefs [account/save-password (stub :save-password)]
        (sut/handler {:datomic/conn :db-connection, :multipart-params @params})
        (should-have-invoked :save-password
                             {:times 1
                              :with [:db-connection @account-eid @password]})))

    (it "returns a 204"
      (with-redefs [account/save-password (stub :save-password)]
        (should= {:headers {}, :status 204}
                 (sut/handler {:multipart-params @params}))))))
