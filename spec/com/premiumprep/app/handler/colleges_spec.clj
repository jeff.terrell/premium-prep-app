(ns com.premiumprep.app.handler.colleges-spec
  (:require [com.premiumprep.app.handler.colleges :as sut]
            [com.premiumprep.app.model.college :as college]
            [com.premiumprep.app.schema :as schema]
            [datomic.client.api :as d]
            [speclj.core :refer [around context describe it should-be
                                 should-have-invoked should= stub with
                                 with-stubs]]))

(describe "get-colleges/authorized?"
  (with sut @(ns-resolve 'com.premiumprep.app.handler.colleges 'authorized?))
  (it "accepts superusers"
    (should= true (@sut {:account/type :account-type/superuser})))
  (it "accepts counselors"
    (should= true (@sut {:account/type :account-type/counselor})))
  (it "accepts students"
    (should= true (@sut {:account/type :account-type/student})))
  (it "rejects parents"
    (should= false (@sut {:account/type :account-type/parent})))
  (it "rejects nil sessions"
    (should= false (@sut nil))))

(describe "the get-colleges handler"
  (with-stubs)

  (it "should check authorization and return 403 if failed"
    (with-redefs [sut/authorized? (stub :authorized? {:return false})]
      (should= 403 (:status (sut/handler {:session :the-session})))
      (should-have-invoked :authorized? {:times 1, :with [:the-session]})))

  (context "with an authorized request"
    (with id 12347890)
    (around [it]
      (with-redefs [sut/authorized? (stub :authorized? {:return true})
                    d/db (stub :db {:return :db-val})
                    college/all (stub :get-all-colleges
                                      {:return [{:db/id @id}]})]
        (it)))

    (it "gets all colleges"
      (sut/handler nil)
      (should-have-invoked :get-all-colleges {:times 1, :with [:db-val]}))

    (it "returns a 200 status"
      (should= 200 (:status (sut/handler nil))))

    (it "stringifies college ids"
      (should= (str @id)
               (-> nil sut/handler :body first :db/id)))))
