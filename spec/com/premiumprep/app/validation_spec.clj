(ns com.premiumprep.app.validation-spec
  (:require
   [com.premiumprep.app.validation :as sut]
   [datomic.client.api :as d]
   [speclj.core :refer [around context describe it should= should-have-invoked
                        should-not-have-invoked stub with with-stubs]]))

(defn- return-sequentially [vs]
  (let [a (atom 0)]
    (fn [& args]
      (let [v (nth vs @a)]
        (swap! a inc)
        v))))

(describe "validation/validate-long"
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'validate-long))
  (it "accepts valid long strings"
    (should= nil (@sut "311"))
    (should= nil (@sut "0")))
  (it "rejects empty strings"
    (should= "the given value () is not numeric" (@sut "")))
  (it "rejects negative longs (for now at least)"
    (should= "the given value (-123) is not numeric" (@sut "-123")))
  (it "rejects invalid long strings"
    (should= "the given value (abc) is not numeric" (@sut "abc"))))

(describe "validation/validate-float"
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'validate-float))
  (it "accepts valid int strings"
    (should= nil (@sut "311"))
    (should= nil (@sut "0")))
  (it "accepts valid float strings"
    (should= nil (@sut "3.11"))
    (should= nil (@sut ".311"))
    (should= nil (@sut "0.311")))
  (it "rejects empty strings"
    (should= "the given value () is not numeric" (@sut "")))
  (it "rejects negative numbers (for now at least)"
    (should= "the given value (-123.0) is not numeric" (@sut "-123.0")))
  (it "rejects invalid int strings"
    (should= "the given value (abc) is not numeric" (@sut "abc"))))

(describe "validation/validate-tel"
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'validate-tel))
  (it "accepts valid phone numbers"
    (should= nil (@sut "5558675309")))
  (it "ignores nonnumeric characters"
    (should= nil (@sut "(555) 867-5309")))
  (it "rejects blank strings"
    (should= "the given value ( ) is not a valid phone number" (@sut " ")))
  (it "rejects strings with too few digits"
    (should= "the given value ((55) 867-5309) is not a valid phone number"
             (@sut "(55) 867-5309"))))

(describe "validation/validate-param"
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'validate-param))
  (it "detects missing required values"
    (should= "Error on field k: value is required"
             (@sut :k {:required? true} nil))
    (should= "Error on field k: value is required"
             (@sut :k {:required? true} "")))
  (it "doesn't try to validate when required value is missing"
    (should= "Error on field k: value is required"
             (@sut :k {:required? true, :validator sut/validate-long} nil)))
  (it "detects invalid values"
    (should= "Error on field k: the given value (abc) is not numeric"
             (@sut :k {:validator sut/validate-long} "abc")))
  (it "returns nil if required value is present and valid"
    (should= nil
             (@sut :k {:required? true, :validator sut/validate-long} "123")))
  (it "returns nil if non-required value is missing"
    (should= nil (@sut :k {} nil))
    (should= nil (@sut :k {} "")))
  (it "returns nil if value is present and field has no validator"
    (should= nil (@sut :k {:required? true} "123"))
    (should= nil (@sut :k {:required? false} "123"))))

(describe "validation/validate-params"
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'validate-params))
  (it "returns nil when there are no errors"
    (should= nil (@sut {:f1 :field1} {:f1 "abc"})))
  (it "detects missing values"
    (should= "There were some problems with your request:\nError on field f1: value is required\n"
             (@sut {:f1 {:required? true}} {})))
  (it "combines multiple error messages"
    (should= "There were some problems with your request:\nError on field f1: value is required\nError on field f2: the given value (abc) is not numeric\n"
             (@sut {:f1 {:required? true},
                    :f2 {:validator sut/validate-long}}
              {:f2 "abc"}))))

(describe "validation/tx-data"
  (with-stubs)
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'tx-data))

  (it "handles single-item paths correctly"
    (with-redefs [d/pull (stub :pull)]
      (should= [[:db/add :student-eid :the/attr :value]]
               (@sut nil :student-eid [:the/attr] :value))))

  (it "does not re-transact unchanged values"
    (let [val "value"
          attr :the/attr]
      (with-redefs [d/pull (stub :pull {:return {attr val}})]
        (should= nil (@sut nil :student-eid [attr] val)))))

  (it "retracts datoms when given value is blank"
    (let [val "value"
          attr :the/attr]
      (with-redefs [d/pull (stub :pull {:return {attr val}})]
        (should= [[:db/retract :student-eid attr val]]
                 (@sut nil :student-eid [attr] "")))))

  (it "handles adding enum values correctly"
    (let [val :the/val
          attr :the/attr
          pull (fn [_db [attr] entity]
                 (case entity
                   :student-eid {attr {:db/id 1234}}
                   1234 {:db/ident :other/val}))]
      (with-redefs [d/pull (stub :pull {:invoke pull})]
        (should= [[:db/add :student-eid attr val]]
                 (@sut nil :student-eid [attr] val)))))

  (it "handles removing enum values correctly"
    (let [val :the/val
          attr :the/attr
          pull (fn [_db [attr] entity]
                 (case entity
                   :student-eid {attr {:db/id 1234}}
                   1234 {:db/ident val}))]
      (with-redefs [d/pull (stub :pull {:invoke pull})]
        (should= [[:db/retract :student-eid attr val]]
                 (@sut nil :student-eid [attr] nil)))))

  (context "with a 2-item vector attr"
    (with attr1 :student/psat)
    (with attr2 :sat/total-score)
    (with path [@attr1 @attr2])
    (with ref-id 12341234)

    (it "correctly uses an existing ref from the database"
      (with-redefs [d/pull (stub :pull {:return {@attr1 {:db/id @ref-id}}})]
        (should= [[:db/add @ref-id @attr2 :value]]
                 (@sut :db :student-eid @path :value))
        (should-have-invoked :pull {:times 1
                                    :with [:db [@attr1] :student-eid]})))

    (it "correctly uses a tempid"
      (let [tmp-id ":student-eid,:student/psat"]
        (with-redefs [d/pull (stub :pull)]
          (should= [[:db/add :student-eid @attr1 tmp-id]
                    [:db/add tmp-id @attr2 :value]]
                   (@sut :db :student-eid @path :value))
          (should-have-invoked :pull {:times 1
                                      :with [:db [@attr1] :student-eid]})))))

  (context "with a 3-item vector attr"
    (with attr1 :student/sat-superscore)
    (with attr2 :sat-superscore/sat)
    (with attr3 :sat/total-score)
    (with path [@attr1 @attr2 @attr3])
    (with ref1-id 12341234)
    (with ref2-id 23452345)

    (it "correctly uses two tempids"
      (let [tmp1-id ":student-eid,:student/sat-superscore"
            tmp2-id ":student-eid,:student/sat-superscore,:sat-superscore/sat"]
        (with-redefs [d/pull (stub :pull)]
          (should= [[:db/add :student-eid @attr1 tmp1-id]
                    [:db/add tmp1-id @attr2 tmp2-id]
                    [:db/add tmp2-id @attr3 :value]]
                   (@sut :db :student-eid @path :value)))))

    (it "correctly mixes a tempid and an existing ref"
      (let [tmp2-id (str @ref1-id ",:sat-superscore/sat")]
        (with-redefs [d/pull (stub :pull
                                   {:invoke (return-sequentially
                                              [{@attr1 {:db/id @ref1-id}}
                                               nil])})]
          (should= [[:db/add @ref1-id @attr2 tmp2-id]
                    [:db/add tmp2-id @attr3 :value]]
                   (@sut :db :student-eid @path :value)))))

    (it "correctly uses two existing refs"
      (with-redefs [d/pull (stub :pull
                                 {:invoke (return-sequentially
                                            [{@attr1 {:db/id @ref1-id}}
                                             {@attr2 {:db/id @ref2-id}}
                                             nil])})]
        (should= [[:db/add @ref2-id @attr3 :value]]
                 (@sut :db :student-eid @path :value))))))

(describe "validation/field-tx-data"
  (with-stubs)
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'field-tx-data))
  (around [it]
    (with-redefs [sut/tx-data (stub :tx-data)]
      (it)))

  (it "parses the value if field has parse fn"
    (@sut :db :student-eid {:parse (stub :parse-fn)} :value)
    (should-have-invoked :parse-fn {:times 1, :with [:value]}))

  (it "transforms the parsed value if field has tx-transform fn"
    (@sut :db :student-eid {:parse identity, :tx-transform (stub :xform)} :value)
    (should-have-invoked :xform {:times 1, :with [:value]}))

  (it "calls tx-data with certain arguments"
    (let [path [:attr1 :attr2]
          field {:path path
                 :parse identity
                 :tx-transform (stub :xform {:return :xformd})}]
      (@sut :db :student-eid field :value)
      (should-have-invoked :tx-data {:times 1
                                     :with [:db :student-eid path :xformd]}))))

(describe "validation/fields-tx-data"
  (with-stubs)
  (with sut @(ns-resolve 'com.premiumprep.app.validation 'fields-tx-data))
  (around [it]
    (with-redefs [sut/field-tx-data (stub :field-tx-data {:return [:fact]})]
      (it)))

  (it "calls field-tx-data for each field, but only if field in params map"
    (@sut :db :student-eid {:k1 :field1, :k2 :field2} {:k1 :param1})
    (should-have-invoked :field-tx-data {:times 1})
    (should-have-invoked :field-tx-data
                         {:times 1, :with [:db :student-eid :field1 :*]})
    (should-not-have-invoked :field-tx-data
                             {:times 1, :with [:db :student-eid :field2 :*]}))

  (it "gets values by field key from the params map"
    (let [key :the-key
          val :the-value
          field {:key key}]
      (@sut :db :student-eid {key field} {key val})
      (should-have-invoked :field-tx-data
                           {:times 1, :with [:db :student-eid field val]})))

  (it "concatenates field-tx-data return values"
    (with-redefs [sut/field-tx-data (stub :pull
                                          {:invoke (return-sequentially
                                                     [[:fact1] [:fact2]])})]
      (should= [:fact1 :fact2]
               (@sut :db :student-eid
                {:k1 :field1, :k2 :field2}
                {:k1 :param1, :k2 :param2}))))

  (it "deduplicates returned facts"
    (should= [:fact]
             (@sut :db :student-eid
              {:k1 :field1, :k2 :field2}
              {:k1 :param1, :k2 :param2}))))
