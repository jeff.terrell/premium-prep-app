(ns com.premiumprep.app.routes
  (:require
   [clojure.java.io :as io]
   (com.premiumprep.app.handler
     [add-college-list-entry :as add-college-list-entry]
     [change-email :as change-email]
     [change-password :as change-password]
     [colleges :as colleges]
     [counselors :as counselors]
     [create-college :as create-college]
     [create-comment :as create-comment]
     [create-counselor :as create-counselor]
     [create-custom-field :as create-custom-field]
     [create-issue :as create-issue]
     [create-student :as create-student]
     [current-user :as current-user]
     [delete-college-list-entry :as delete-college-list-entry]
     [delete-custom-field :as delete-custom-field]
     [delete-document :as delete-document]
     [download-document :as download-document]
     [forgot-password :as forgot-password]
     [index :as index]
     [issues :as issues]
     [login :as login]
     [logout :as logout]
     [healthcheck :as healthcheck]
     [me :as me]
     [re-send-invite :as re-send-invite]
     [reset-password :as reset-password]
     [set-account-active :as set-account-active]
     [set-issue-state :as set-issue-state]
     [signup :as signup]
     [student :as student]
     [students :as students]
     [update-college-list-entry-category :as update-college-list-entry-category]
     [update-college-list-entry-field :as update-college-list-entry-field]
     [update-counselor :as update-counselor]
     [update-custom-field :as update-custom-field]
     [update-issue :as update-issue]
     [update-student :as update-student]
     [update-student-profile :as update-student-profile]
     [upload-document :as upload-document]
     [verify-email :as verify-email]
     [verify-token :as verify-token])
   (reitit
     [core :as r]
     [ring :as ring])))

(def router
  (ring/router
    [["/" {:get index/handler, :name ::index}]
     ["/healthcheck" {:get healthcheck/handler, :name ::healthcheck}]
     ["/api"
      ["/account/:id/re-send-invite" {:put re-send-invite/handler
                                      :name ::re-send-invite}]
      ["/change-email" {:post change-email/handler, :name ::change-email}]
      ["/change-password" {:put change-password/handler
                           :name ::change-password}]
      ["/college" {:post create-college/handler, :name ::create-college}]
      ["/college-list-entry" {:post add-college-list-entry/handler
                              :delete delete-college-list-entry/handler
                              :name ::college-list-entry}]
      ["/college-list-entry-category"
       {:put update-college-list-entry-category/handler
        :name ::update-college-list-entry-category}]
      ["/college-list-entry-field" {:put update-college-list-entry-field/handler
                                    :name ::update-college-list-entry-field}]
      ["/colleges" {:get colleges/handler, :name ::colleges}]
      ["/comment" {:post create-comment/handler, :name ::create-comment}]
      ["/counselor" {:post create-counselor/handler, :name ::create-counselor}]
      ["/counselor/:id" {:put update-counselor/handler, :name ::update-counselor}]
      ["/counselors" {:get counselors/handler, :name ::counselors}]
      ["/current-user" {:get current-user/handler, :name ::current-user}]
      ["/custom-field" {:post create-custom-field/handler
                        :delete delete-custom-field/handler
                        :name ::custom-field}]
      ["/custom-field/:id" {:put update-custom-field/handler
                            :name ::update-custom-field}]
      ["/deactivate-account/:id" {:put set-account-active/handler
                                  :name ::deactivate-account
                                  :active? false}]
      ["/document" {:post upload-document/handler,
                    :delete delete-document/handler
                    :name ::upload-document}]
      ["/document/:id" {:get download-document/handler
                        :name ::download-document}]
      ["/forgot-password" {:put forgot-password/handler
                           :name ::forgot-password}]
      ["/issue" {:post create-issue/handler, :name ::create-issue}]
      ["/issue/:id" {:put update-issue/handler, :name ::update-issue}]
      ["/issues" {:get issues/handler, :name ::list-issues}]
      ["/login" {:post login/handler, :name ::login}]
      ["/logout" {:delete logout/handler, :name ::logout}]
      ["/me/student" {:get me/handler, :name ::me-student}]
      ["/reactivate-account/:id" {:put set-account-active/handler
                                  :name ::reactivate-account
                                  :active? true}]
      ["/reset-password" {:post reset-password/handler, :name ::reset-password}]
      ["/set-issue-state" {:put set-issue-state/handler
                           :name ::set-issue-state}]
      ["/signup" {:post signup/handler, :name ::signup}]
      ["/student" {:post create-student/handler, :name ::create-student}]
      ["/student/:id" {:get student/handler, :name ::student
                       :put update-student/handler}]
      ["/student/:id/profile" {:put update-student-profile/handler}]
      ["/students" {:get students/handler, :name ::students}]
      ["/verify-email" {:get verify-email/handler, :name ::verify-email}]
      ["/verify-token" {:get verify-token/handler, :name ::verify-token}]]]))

(defn default-handler [req]
  (let [{:keys [uri]} req
        rel-path (str "public" uri)
        resource (io/resource rel-path)]
    (if resource
      {:status 200
       :body (io/input-stream resource)}
      (index/handler req))))

(def base-app (ring/ring-handler router default-handler))
