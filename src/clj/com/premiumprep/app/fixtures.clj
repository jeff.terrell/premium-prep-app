(ns com.premiumprep.app.fixtures
  (:require [clojurewerkz.scrypt.core :as sc]
            [clojure.instant :refer [read-instant-date]]
            [datomic.client.api :as d])
  (:import java.net.URI))

(defn install-fixtures! [conn]
  (let [account1-tempid "account1"
        account2-tempid "account2"
        student1-tempid "student1"
        student2-tempid "student2"
        counselor1-tempid "counselor1"
        counselor2-tempid "counselor2"
        counselor1-acct-id "counselor1-acct"
        encrypt-pass #(sc/encrypt % 16384 8 1)

        base-account {:account/mobile "919.123.4567"
                      :account/active? true
                      :account/state :account-state/initialized
                      :account/address-street "123 Any St."
                      :account/address-city "Anytown"
                      :account/address-state "MZ"
                      :account/address-zip 12345}

        account1 (merge base-account
                        {:db/id account1-tempid
                         :account/type :account-type/student
                         :account/email "kyptin+student1@gmail.com"
                         :account/password (encrypt-pass "password")
                         :account/full-name "Student One"})

        account2 (merge base-account
                        {:db/id account2-tempid
                         :account/type :account-type/student
                         :account/email "kyptin+student2@gmail.com"
                         :account/password (encrypt-pass "password")
                         :account/full-name "Student Two"})

        counselor1 {:db/id counselor1-tempid
                    :counselor/account
                    (merge base-account
                           {:db/id counselor1-acct-id
                            :account/type :account-type/counselor
                            :account/email "kyptin+counselor1@gmail.com"
                            :account/password (encrypt-pass "counselor")
                            :account/full-name "Ronald Hawkins"
                            :account/mobile "919.123.4567"})}

        counselor2 {:db/id counselor2-tempid
                    :counselor/account
                    (merge base-account
                           {:db/id "counselor2-acct"
                            :account/type :account-type/counselor
                            :account/email "kyptin+counselor2@gmail.com"
                            :account/password (encrypt-pass "counselor")
                            :account/full-name "Counselor 2"
                            :account/mobile "919.765.4321"})}

        make-student #(hash-map :db/id %1
                                :student/account %2
                                :student/counselor %3
                                :student/joined-at %4
                                :student/graduation-year %5
                                :student/high-school %6
                                :student/parent1
                                (merge base-account
                                       {:account/full-name "Parent Name"
                                        :account/email "kyptin+parent1@gmail.com"
                                        :account/type :account-type/parent}))

        student1 (make-student student1-tempid
                               account1-tempid
                               counselor1-tempid
                               #inst "2020-01-02Z"
                               2023
                               "East Wickham High")

        student2 (make-student student2-tempid
                               account2-tempid
                               counselor2-tempid
                               #inst "2019-02-03Z"
                               2022
                               "West Wickham High")

        date-str #(-> % .toInstant .getEpochSecond)
        make-doc #(let [date (read-instant-date (format "2019-%02d-13Z" (inc %1)))
                        s3-path (str %2 "." (date-str date))]
                    (hash-map :document/name %2
                              :document/content-type "application/octet-stream"
                              :document/uploaded-at date
                              :document/s3-path s3-path))

        doc-names ["document 0"
                   "document 1"
                   "document 2"
                   "document 3"
                   "document 4"
                   "My Strategic Report"
                   "My Timeline"
                   "My Essay"
                   "My Report Card"
                   "My Strategic Report - Update 1"]

        docs (vec (map-indexed make-doc doc-names))
        student1 (assoc student1 :student/documents docs)
        comment1 {:comment/commenter counselor1-acct-id
                  :comment/created-at #inst "2020-06-18T16:37:25Z"
                  :comment/text "Did you get your ACT scores back yet? When you do, be sure to update the score here in your profile."}
        comment2 {:comment/commenter account1-tempid
                  :comment/created-at #inst "2020-06-18T16:43:42Z"
                  :comment/text "Oh yeah. Thanks for the reminder. I just updated it :)"}
        student1 (assoc student1 :student/profile-comments [comment1 comment2])

        comment3 {:comment/commenter counselor1-acct-id
                  :comment/created-at #inst "2020-06-12T13:21:58Z"
                  :comment/text "I still need to see your essay. If you are stuck, let me know, and I might be able to help."}
        comment4 {:comment/commenter account1-tempid
                  :comment/created-at #inst "2020-06-15T11:41:02Z"
                  :comment/text "Got it! Finally. Also uploaded my latest transcript."}
        comment5 {:comment/commenter counselor1-acct-id
                  :comment/created-at #inst "2020-06-15T14:12:06Z"
                  :comment/text "This looks great! Nice work!"}
        comments [comment3 comment4 comment5]
        student1 (assoc student1 :student/document-list-comments comments)

        make-college #(hash-map :db/id %1
                                :college/name %2
                                :college/enrollment %3
                                :college/admit-rate %4
                                :college/average-sat %5
                                :college/average-act %6)
        college1 (make-college "college1" "American U" "about 1,700"
                               "https://news.ycombinator.com" "1100-1300" "26-32")
        college2 (make-college "college2" "University of American Samoa" "7321"
                               "57%" "1100-1300" "26-32")
        college3 (make-college "college3" "University of the Highlands & Islands"
                               "1,220" "52%" "1200" "28")
        college4 (make-college "college4" "Gee-Tri-C" "520" "82%" "900" "24")
        college5 (make-college "college5" "Markham Polytechnic" "1,720" "28%"
                               "1300" "31")
        college6 (make-college "college6" "UNC-X" "1,505" "38%"
                               "1150" "27")

        make-cl-entry #(hash-map
                         :db/id %1
                         :college-list-entry/college %2
                         :college-list-entry/category (keyword
                                                        "college-list-category"
                                                        (name %3))
                         :college-list-entry/index %4)
        cl-entry1 (make-cl-entry "cle1" "college1" :high-reach 0)
        cl-entry1 (assoc cl-entry1 :college-list-entry/date-added (read-instant-date "2020-12-21"))
        cl-entry1 (assoc cl-entry1 :college-list-entry/early-decision? true)
        cl-entry2 (make-cl-entry "cle2" "college2" :reach 0)
        cl-entry2 (assoc cl-entry2 :college-list-entry/date-added (read-instant-date "2020-12-21"))
        cl-entry3 (make-cl-entry "cle3" "college3" :target 0)
        cl-entry3 (assoc cl-entry3 :college-list-entry/date-added (read-instant-date "2020-12-21"))
        cl-entry4 (make-cl-entry "cle4" "college4" :target 1)
        cl-entry4 (assoc cl-entry4 :college-list-entry/date-added (read-instant-date "2020-12-21"))
        cl-entry5 (make-cl-entry "cle5" "college5" :likely 0)
        cl-entry5 (assoc cl-entry5 :college-list-entry/date-added (read-instant-date "2020-12-21"))
        cl-entry5 (assoc cl-entry5 :college-list-entry/custom-fields
                         [{:custom-field/name "Underwater Basket Weaving Team"
                           :custom-field/value "30 members strong; meets 2x/week"}
                          {:custom-field/name "Hackathons"
                           :custom-field/value "4 last year"}])
        student1 (assoc student1 :student/college-list-entries
                        ["cle1" "cle2" "cle3" "cle4" "cle5"])

        issue1 {:db/id "issue1"
                :issue/created-at #inst "2020-06-12T11:31:59Z"
                :issue/student student1-tempid
                :issue/text "Counselor hasn't seen this issue yet. And it's really long. Really, really long. Like even longer than this. Yeah. Counselor hasn't seen this issue yet. And it's really long. Really, really long. Like even longer than this. Yeah. Counselor hasn't seen this issue yet. And it's really long. Really, really long. Like even longer than this. Yeah. Counselor hasn't seen this issue yet. And it's really long. Really, really long. Like even longer than this. Yeah. Counselor hasn't seen this issue yet. And it's really long. Really, really long. Like even longer than this. Yeah."
                :issue/state :issue-state/new}
        issue2 {:db/id "issue2"
                :issue/created-at #inst "2020-06-12T11:31:59Z"
                :issue/student student1-tempid
                :issue/text "Counselor has read this issue."
                :issue/counselor-touched-at #inst "2020-06-13T11:31:59Z"
                :issue/state :issue-state/read}
        issue3 {:db/id "issue3"
                :issue/created-at #inst "2020-06-12T11:31:59Z"
                :issue/student student1-tempid
                :issue/text "Counselor needs to discuss this issue."
                :issue/counselor-touched-at #inst "2020-06-14T11:31:59Z"
                :issue/state :issue-state/needs-discussion}
        issue4 {:db/id "issue4"
                :issue/created-at #inst "2020-06-12T11:31:59Z"
                :issue/student student1-tempid
                :issue/text "Counselor thinks this issue is resolved."
                :issue/counselor-touched-at #inst "2020-06-15T11:31:59Z"
                :issue/state :issue-state/counselor-resolved}
        issue5 {:db/id "issue5"
                :issue/created-at #inst "2020-06-12T11:31:59Z"
                :issue/student student1-tempid
                :issue/text "Admin closed this issue"
                :issue/counselor-touched-at #inst "2020-06-15T11:31:59Z"
                :issue/state :issue-state/closed}

        extra-student-attrs {:student/academic-interest "Computer Science"
                             :student/gpa-weighted-9 3.700000047683716
                             :student/gpa-unweighted-9 3.53
                             :student/class-rank-9 78
                             :student/sat-subject-tests "haven't taken any yet"
                             :student/ap-tests "haven't taken any yet"
                             :student/psat {:sat/total-score 1100
                                            :sat/ebrw-score 500
                                            :sat/math-score 600}
                             :student/pact {:act/composite-score 28
                                            :act/english-score 29
                                            :act/reading-score 30
                                            :act/science-score 27
                                            :act/math-score 26}
                             :student/sat-superscore
                             {:sat-superscore/sat {:sat/total-score 1100
                                                   :sat/ebrw-score 500
                                                   :sat/math-score 600}
                              :sat-superscore/ebrw-date #inst "2019-04-02Z"
                              :sat-superscore/math-date #inst "2019-04-02Z"}
                             :student/college-planning-app-info
                             {:college-planning-app/type :college-planning-app-type/kickstart
                              :college-planning-app/username "kickstart-user"
                              :college-planning-app/password "kickstart-pass"
                              :college-planning-app/hs-login-page-url (URI. "http://url.com")}
                             :student/extracurricular-activities "squash"
                             :student/current-courses "lots and lots"
                             :student/prospective-courses "this n that"}
        student1 (merge student1 extra-student-attrs)

        tx-data [college1 college2 college3 college4 college5 college6
                 cl-entry1 cl-entry2 cl-entry3 cl-entry4 cl-entry5
                 account1 counselor1 student1
                 account2 counselor2 student2
                 issue1 issue2 issue3 issue4 issue5]]

    (d/transact conn {:tx-data (conj tx-data
                                     [:db/add "datomic.tx" :transaction/namespace
                                      "com.premiumprep.app.fixtures"])}))
  (println "Installed fixtures."))
