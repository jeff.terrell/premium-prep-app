(ns com.premiumprep.app.validation
  (:require [clojure.instant :as instant]
            [clojure.string :as str]
            [clojurewerkz.scrypt.core :as sc]
            [datomic.client.api :as d])
  (:import (java.net URI URISyntaxException)))

(defn validate-boolean [s]
  (when-not (#{"true" "false"} s)
    (format "the given value (%s) is not 'true' or 'false'" s)))

(defn validate-long [s]
  (when-not (re-matches #"\d+" s)
    (format "the given value (%s) is not numeric" s)))

(defn validate-float [s]
  (when-not (re-matches #"\d+|\d+\.\d+|0?\.\d+" s)
    (format "the given value (%s) is not numeric" s)))

(defn validate-tel [s]
  (let [numbers (str/replace s #"[^\d]" "")]
    (when (< (count numbers) 10)
      (format "the given value (%s) is not a valid phone number" s))))

(defn validate-date [s]
  (let [iso8601-re #"\d{4}-\d{2}-\d{2}"]
    (when-not (re-matches iso8601-re s)
      (format "the given value (%s) is not a valid date" s))))

(defn validate-date-short [s]
  (let [iso8601-re #"\d{4}-\d{2}"]
    (when-not (re-matches iso8601-re s)
      (format "the given value (%s) is not a valid short date (i.e Year & Month, e.g 2019-08)" s))))

(defn validate-password [s]
  (when (> 8 (count s))
    "password is too short; it should be at least 8 characters"))

(defn validate-uri [s]
  (try
    (new URI s)
    nil
    (catch URISyntaxException _
      "malformed URL")))

(defn parse-boolean [s]
  (when-not (str/blank? s)
    (= "true" s)))

(defn parse-date [s]
  (when-not (str/blank? s)
    (instant/read-instant-date (str s "Z"))))

(defn parse-float [s]
  (when-not (str/blank? s)
    (Float/parseFloat s)))

(defn parse-long [s]
  (when-not (str/blank? s)
    (Long/parseLong s)))

(defn parse-uri [s]
  (when-not (str/blank? s)
    (new URI s)))

(defn encrypt-password [s]
  (sc/encrypt s 16384 8 1))

(defn validate-param [key field value]
  (let [{:keys [required? validator]} field
        make-msg #(format "Error on field %s: %s" (name key) %)]
    (if (and required? (str/blank? value))
      (make-msg "value is required")
      (when-not (str/blank? value)
        (when-let [valid-msg (when validator (validator value))]
          (make-msg valid-msg))))))

(defn validate-params [fields params]
  (let [err-msgs (keep (fn [[key field]]
                         (validate-param key field (get params key)))
                       fields)]
    (when (not-empty err-msgs)
      (apply str "There were some problems with your request:\n"
             (map #(str % "\n") err-msgs)))))

(defn- blank? [v]
  (or (nil? v) (and (string? v) (str/blank? v))))

(defn- lookup-attr [db entity attr]
  (let [val (-> (d/pull db [attr] entity) (get attr))]
    (if (map? val)
      (-> (d/pull db [:db/ident] (:db/id val)) (get :db/ident))
      val)))

(defn- tx-data [db entity path value]
  (if (= 1 (count path))
    ;; base case: no further nesting, i.e. this is the leaf datom
    (let [attr (first path)]
      (if (and (string? entity) (not (blank? value)))
        [[:db/add entity attr value]]
        (let [old-val (when (not (string? entity))
                        (lookup-attr db entity attr))
              old-blank? (blank? old-val)
              new-blank? (blank? value)]
          (when (not= value old-val)
            (cond
              (and (not old-blank?) new-blank?) [[:db/retract entity attr
                                                  old-val]]
              new-blank? nil
              :else [[:db/add entity attr value]])))))

    ;; recursive case: leaf datom is at least one ref away
    (let [[attr & path] path
          tmp-id (str/join "," [entity attr])
          ref-id (when-not (string? entity)
                   (-> (d/pull db [attr] entity) (get attr) :db/id))
          id (or ref-id tmp-id)
          found? (some? ref-id)
          new-reference [:db/add entity attr id]
          other-facts (tx-data db id path value)]
      (if found?
        other-facts
        (when other-facts
          (cons new-reference other-facts))))))

(defn- field-tx-data [db student-eid field raw-value]
  (let [{:keys [path parse tx-transform]} field
        maybe-parse (or parse identity)
        maybe-transform (or tx-transform identity)
        value (some-> raw-value maybe-parse maybe-transform)]
    (when path
      (tx-data db student-eid path value))))

(defn fields-tx-data [db student-eid fields params]
  (let [field-tx-data (partial field-tx-data db student-eid)]
    (vec
      (distinct
        (apply concat
               (for [[key field] fields
                     :when (contains? params key)]
                 (field-tx-data field (get params key))))))))
