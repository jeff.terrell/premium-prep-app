(ns com.premiumprep.app.handler.index
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn- get-app-source-url [frontend-env-name]
  (condp = frontend-env-name
    :dev "/cljs-out/dev-main.js"
    :test "/premium-prep-app.min-e2e.js"
    :prod "/premium-prep-app.min.js"
    "/premium-prep-app.min.js"))

(defn handler [{:keys [frontend-env-name]}]
  (let [body (-> "public/index.html" io/resource slurp)
        body (str/replace body "APP_SOURCE_URL"
                          (get-app-source-url frontend-env-name))]
    {:status 200
     :headers {"Content-Type" "text/html"}
     :body body}))
