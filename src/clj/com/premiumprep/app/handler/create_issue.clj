(ns com.premiumprep.app.handler.create-issue
  (:require [clojure.string :as str]
            [com.premiumprep.app.email :as email]
            [com.premiumprep.app.model.issue :as issue]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d])
  (:import java.util.Date))

(defn- authorized? [student session]
  (when (and (not-empty session) (:db/id session))
    (let [{:keys [db/id account/type]} session
          id (Long/parseLong id)]
      (or (= :account-type/superuser type)
          (= id (-> student :student/counselor :counselor/account :db/id))))))

(defn- error-message [params]
  (when (str/blank? (get params "text"))
    "Error: no issue text supplied."))

(defn- created-at [] (new Date)) ; fn exists to aid testing

(defn- create-issue [conn params]
  (let [{:strs [student-id text]} params
        student-id (Long/parseLong student-id)
        issue-map {:db/id "issue"
                   :issue/text text
                   :issue/created-at (created-at)
                   :issue/state :issue-state/new
                   :issue/student student-id}
        tx-datom [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.create-issue"]
        tx-ret (d/transact conn {:tx-data [issue-map tx-datom]})]
    (get-in tx-ret [:tempids "issue"])))

(defn handler
  [{:keys [datomic/conn email-send-fn multipart-params session]}]
  (let [db (d/db conn)
        {:strs [student-id text]} multipart-params
        student (when student-id (student/find db (Long/parseLong student-id)))]
    (if-not student
      (error-response 404 "not found")
      (if-not (authorized? student session)
        (error-response 403 "forbidden")
        (if-let [err-msg (error-message multipart-params)]
          (error-response 400 err-msg)
          (let [issue-id (create-issue conn multipart-params)
                issue-map (issue/find (d/db conn) issue-id)
                issue-text (:issue/text issue-map)
                counselor-email (-> student :student/counselor
                                    :counselor/account :account/email)
                student-name (-> student :student/account :account/full-name)
                student-path (str "/student/" (:db/id student))]
            (email/new-issue email-send-fn counselor-email issue-text
                             student-name student-path)
            {:status 201
             :body (-> issue-map
                       (update :db/id str)
                       (update-in [:issue/student :db/id] str)
                       (update :issue/state :db/ident))}))))))
