(ns com.premiumprep.app.handler.set-account-active
  (:require [com.premiumprep.app.model.account :as account]
            [com.premiumprep.app.model.session :as session]
            [datomic.client.api :as d]))

(defn- set-account-active? [conn id active?]
  (d/transact conn
              {:tx-data [[:db/add id :account/active? active?]
                         [:db/add "datomic.tx" :transaction/namespace
                          "com.premiumprep.app.handler.set-account-active"]]}))

(defn handler
  [{:keys [datomic/conn reitit.core/match session]}]
  (let [{:keys [path-params data]} match
        id (-> path-params :id Long/parseLong)
        active? (:active? data)
        atype (:account/type session)]
    (cond
      (not= atype :account-type/superuser) {:status 403, :headers {}}
      (not (account/find (d/db conn) id)) {:status 404, :headers {}}
      :else (do
              (set-account-active? conn id active?)
              (when-not active? (session/destroy-for-account-id conn id))
              {:status 204, :headers {}}))))
