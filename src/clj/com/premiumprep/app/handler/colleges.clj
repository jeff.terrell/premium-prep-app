(ns com.premiumprep.app.handler.colleges
  (:require [com.premiumprep.app.model.college :as college]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d]))

(defn- authorized? [session]
  (boolean (#{:account-type/superuser
              :account-type/counselor
              :account-type/student}
             (:account/type session))))

(defn handler
  [{:keys [datomic/conn session]}]
  (if-not (authorized? session)
    (error-response 403 "forbidden")
    (let [colleges (college/all (d/db conn))]
      {:status 200
       :body (vec
               (for [college colleges]
                 (update college :db/id str)))})))
