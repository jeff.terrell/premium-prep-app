(ns com.premiumprep.app.handler.student
  (:require [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response
                                              maybe-remove-eliminated-entries
                                              student-result]]
            [datomic.client.api :as d]))

(defn authorized? [session id]
  (when (and session id (map? session))
    (let [{:keys [account/type account/state]} session]
      (or (= :account-type/superuser type)
          (and (= :account-type/counselor type)
               (not= :account-state/email-unverified state))
          (and (= :account-type/student type)
               (not= :account-state/email-unverified state)
               (= (str id) (:db/id session)))))))

(defn handler
  [{:keys [datomic/conn reitit.core/match session]}]
  (let [id (-> match :path-params :id Long/parseLong)
        atype (:account/type session)
        info (->> id (student/find (d/db conn))
                  ;; FIXME: we're not handling the 404 case here...whoops
                  (maybe-remove-eliminated-entries atype)
                  student/listify-college-list-entries student-result)
        aid (-> info :student/account :db/id)]
    (if-not (authorized? session aid)
      (error-response 403 "forbidden")
      {:status 200
       :body (if (#{:account-type/superuser :account-type/counselor}
                   (:account/type session))
               info
               (dissoc info :issue/_student))})))
