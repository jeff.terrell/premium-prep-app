(ns com.premiumprep.app.handler.logout
  (:require [com.premiumprep.app.model.session :as session]))

(defn- logout [conn session]
  (let [{:keys [db/id]} session]
    (when session
      (session/destroy-for-account-id conn (Long/parseLong id)))))

(defn handler
  [{:keys [datomic/conn session]}]
  (when (not-empty session)
    (logout conn session))
  {:status 204, :headers {}, :session nil})
