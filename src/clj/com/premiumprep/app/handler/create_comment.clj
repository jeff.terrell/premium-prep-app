(ns com.premiumprep.app.handler.create-comment
  (:require [clojure.string :as str]
            [com.premiumprep.app.email :as email]
            [com.premiumprep.app.model.comment :as comment]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d])
  (:import java.util.Date))

(defn- authorized? [student session]
  (when (and (not-empty session) (:db/id session))
    (let [{:keys [db/id account/type]} session
          id (Long/parseLong id)]
      (or (= :account-type/superuser type)
          (= id (-> student :student/counselor :counselor/account :db/id))
          (= id (-> student :student/account :db/id))))))

(defn- error-message [params]
  (let [{:strs [comment comment-type student-id]} params
        valid-type? #{"profile-comments" "document-list-comments"}]
    (cond
      (str/blank? comment) "Error: no comment text supplied."
      (not (valid-type? comment-type)) (str "Error: comment-type param should be"
                                            " either profile-comments or"
                                            " document-list-comments."))))

(defn- created-at [] (new Date)) ; fn exists to aid testing

(defn- create-comment [conn params commenter-id]
  (let [{:strs [comment comment-type student-id]} params
        student-id (Long/parseLong student-id)
        attr (keyword "student" comment-type)
        comment-map {:db/id "comment"
                     :comment/text comment
                     :comment/created-at (created-at)
                     :comment/commenter commenter-id}
        rel-datom [:db/add student-id attr "comment"]
        tx-datom [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.create-comment"]
        tx-ret (d/transact conn {:tx-data [comment-map rel-datom]})]
    (get-in tx-ret [:tempids "comment"])))

(defn- lookup-recipients [student recipient-types]
  (letfn [(type->paths [type]
            (case type
              :student [[:student/account]]
              :parents [[:student/parent1] [:student/parent2]]
              :counselor [[:student/counselor :counselor/account]]))
          (lookup-recipient-type [type]
            (into {}
                  (for [path (type->paths type)
                        :let [email (get-in student (conj path :account/email))]
                        :when email]
                    [type email])))]
    (reduce merge (map lookup-recipient-type recipient-types))))

(defn- send-email [email-send-fn session student comment-map comment-type]
  (let [{:keys [account/type]} session
        recipient-types (case type
                          :account-type/superuser [:counselor]
                          :account-type/counselor [:student :parents]
                          :account-type/student [:counselor :parents])
        recipients-by-type (lookup-recipients student recipient-types)]
    (email/comment-created email-send-fn student comment-map
                           recipients-by-type comment-type)))

(defn handler
  [{:keys [datomic/conn email-send-fn multipart-params session]}]
  (let [db (d/db conn)
        {:strs [comment comment-type student-id]} multipart-params
        student (when student-id (student/find db (Long/parseLong student-id)))]
    (if-not student
      (error-response 404 "not found")
      (if-not (authorized? student session)
        (error-response 403 "forbidden")
        (if-let [err-msg (error-message multipart-params)]
          (error-response 400 err-msg)
          (let [commenter-id (Long/parseLong (:db/id session))
                comment-id (create-comment conn multipart-params commenter-id)
                comment-map (comment/find (d/db conn) comment-id)]
            (send-email email-send-fn session student comment-map
                        (keyword (get multipart-params "comment-type")))
            {:status 201
             :body (-> comment-map
                       (update :db/id str)
                       (update-in [:comment/commenter :db/id] str))}))))))
