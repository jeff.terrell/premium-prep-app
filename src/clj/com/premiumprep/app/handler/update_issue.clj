(ns com.premiumprep.app.handler.update-issue
  (:require
    [clojure.string :as str]
    [com.premiumprep.app.model.issue :as issue]
    [com.premiumprep.app.util :refer [error-response]]
    [datomic.client.api :as d])
  (:import (java.util Date)))

(defn- not-found-msg [db issue-id]
  (when-not (issue/find db issue-id)
    "Feedback not found."))

(defn- error-message [db issue-id params]
  (if-let [not-found-msg (not-found-msg db issue-id)]
    not-found-msg
    (when (str/blank? (get params "action-taken"))
      "Error: no issue action-taken text supplied.")))

(defn- params->tx-data [db issue-id param]
  (if-let [err-msg (error-message db issue-id param)]
    [false err-msg]
    [true  [{:db/id issue-id
             :issue/action-taken  (get param "action-taken")
             :issue/counselor-touched-at (new Date)}
            [:db/add "datomic.tx" :transaction/namespace
             "com.premiumprep.app.handler.update-issue"]]]))

(defn success-response [conn id tx-data]
  (let [_ (d/transact conn {:tx-data tx-data})
        body-data (-> (issue/find (d/db conn) id)
                      (update :db/id str)
                      (update-in [:issue/student :db/id] str)
                      (update :issue/state :db/ident))]
    {:status 200
     :body body-data}))

(defn handler
  [{:keys [datomic/conn multipart-params reitit.core/match session]}]
  (if (#{:account-type/superuser :account-type/counselor} (:account/type session))
    (let [issue-id (-> match :path-params :id)
          [ok? result] (params->tx-data (d/db conn)
                                        (Long/parseLong issue-id)
                                        multipart-params)]
      (if ok?
        (success-response conn (Long/parseLong issue-id) result)
        (error-response 400 result)))
    (error-response 400 "forbidden")))
