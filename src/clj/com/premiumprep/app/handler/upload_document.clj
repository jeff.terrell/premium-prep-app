(ns com.premiumprep.app.handler.upload-document
  (:require [clojure.string :as str]
            [com.premiumprep.app.storage :refer [store]]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d])
  (:import (java.nio.file Files Paths StandardCopyOption)
           java.util.Date
           java.util.regex.Pattern
           (java.net URI)))

(defn- blank-document-name-params-error [params]
  (let [{:strs [name]} params]
    (when (str/blank? name)
      "Document name cannot be blank")))

(defn- missing-required-params-error [params]
  (let [{:strs [file link]} params]
    (when (str/blank? link)
      (when (str/blank? (:filename file))
        "Missing either a Document link or Document File"))))

(defn- authorized? [db session student-id]
  (let [{:keys [db/id account/type]} session]
    (or (#{:account-type/superuser :account-type/counselor} type)
       (and (= :account-type/student type)
            id
            (string? id)
            (= student-id
               (-> (student/find-by-account-id db (Long/parseLong id))
                   :db/id str))))))

(def ^:private alphanumeric-re
  ;; This was kinda tricky.
  ;; Cf. https://stackoverflow.com/a/9500409/202292
  ;; and https://stackoverflow.com/a/10894689/202292
  (Pattern/compile "(?u)[^\\p{Alpha}\\p{Digit}]"
                   Pattern/UNICODE_CHARACTER_CLASS))

(defn- slugify [s]
  (-> s
      (str/replace alphanumeric-re "-")
      (str/replace #"--+" "-")
      (str/replace #"-$" "")
      (str/replace #"^-" "")
      .toLowerCase))

(defn- storage-name [student-id name]
  (str student-id "." (slugify name)))

(defn- name-conflict? [db storage-name]
  (boolean
    (ffirst
      (d/q {:query '{:find [?doc]
                     :in [$ ?path]
                     :where [[?doc :document/s3-path ?path]]}
            :args [db storage-name]}))))

(defn- uploaded-at [] (new Date)) ; fn exists to aid testing

(defn- transact-link-metadata
  [conn student-eid name storage-name document-uri]
  (let [doc-map {:db/id "doc"
                 :document/name name
                 :document/uploaded-at (uploaded-at)
                 :document/link document-uri
                 :document/s3-path storage-name}
        ref-datom [:db/add student-eid :student/documents "doc"]
        tx-datom [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.upload-document"]
        result (d/transact conn {:tx-data [ref-datom doc-map tx-datom]})
        doc-eid (-> result :tempids (get "doc"))]
    (-> doc-map
        (assoc :db/id (str doc-eid))
        (dissoc :document/s3-path)
        (update-in [:document/link] str))))

(defn- transact-upload-metadata
  [conn student-eid name storage-name content-type]
  (let [doc-map {:db/id "doc"
                 :document/name name
                 :document/content-type content-type
                 :document/uploaded-at (uploaded-at)
                 :document/s3-path storage-name}
        ref-datom [:db/add student-eid :student/documents "doc"]
        tx-datom [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.upload-document"]
        result (d/transact conn {:tx-data [ref-datom doc-map tx-datom]})
        doc-eid (-> result :tempids (get "doc"))]
    (-> doc-map
        (assoc :db/id (str doc-eid))
        (dissoc :document/s3-path))))

(defn handler
  [{:keys [datomic/conn multipart-params session storage-config] :as req}]
  (let [db (d/db conn)]
    (if-not (authorized? db session (get multipart-params "student-id"))
      (error-response 403 "forbidden")
      (if-let [err-msg (blank-document-name-params-error multipart-params)]
        (error-response 400 err-msg)
        (if-let [err-msg (missing-required-params-error multipart-params)]
          (error-response 400 err-msg)
       ;; multipart-params will look like this:
       #_{"name"       "arstarst"
          "file"       {:filename     "tmpfile"
                        :content-type "application/octet-stream"
                        :tempfile     "an io/file object"
                        :size         18}
          "student-id" "12341234"}
       (let [{:strs [file link name student-id]} multipart-params
             student-id (when student-id (Long/parseLong student-id))
             storage-name (storage-name student-id name)
             conflict? (name-conflict? db storage-name)
             {:keys [tempfile content-type]} file]
         (if conflict?
           (error-response
             409 (str "There was a conflict with the name of another of your"
                      " documents. Please choose another name."))
           (if (str/blank? link)
             (do
               (store storage-config storage-name tempfile content-type)
               {:status 201
                :body   (transact-upload-metadata conn student-id name storage-name
                                                  content-type)})
             {:status 201
              :body   (transact-link-metadata conn student-id name storage-name (URI. link))}))))))))
