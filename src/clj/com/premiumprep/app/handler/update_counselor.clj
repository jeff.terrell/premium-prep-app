(ns com.premiumprep.app.handler.update-counselor
  (:require
   [com.premiumprep.app.model.counselor :as counselor]
   [com.premiumprep.app.model.counselor-fields :refer [fields-for-update]]
   [com.premiumprep.app.util :refer [counselor-result error-response]]
   [com.premiumprep.app.validation :refer [fields-tx-data validate-params]]
   [datomic.client.api :as d]))

(defn- not-found-msg [db counselor-id]
  (when-not (counselor/find db counselor-id)
    "No counselor with that ID was found."))

(defn- error-messages
  [db counselor-id params]
  (if-let [not-found-msg (not-found-msg db counselor-id)]
    not-found-msg
    (validate-params fields-for-update params)))

(defn- params->tx-data
  "Convert the given params (with string keys) into a tx-data sequence suitable
  for calling d/transact. If successful, return a vector of true and the tx-data
  sequence. If any validation check fails, instead return a vector of false and
  a sequence of error message strings (one for each error) suitable for display
  to the end user."
  [db id params]
  (if-let [err-msg (validate-params fields-for-update params)]
    [false err-msg]
    [true (conj (fields-tx-data db id fields-for-update params)
                [:db/add "datomic.tx" :transaction/namespace
                 "com.premiumprep.app.handler.update-counselor"])]))

(defn success-response [conn counselor-id tx-data]
  (d/transact conn {:tx-data tx-data})
  {:status 200
   :body (counselor-result (counselor/find (d/db conn) counselor-id))})

(defn handler
  [{:keys [datomic/conn multipart-params reitit.core/match session]}]
  (if (= :account-type/superuser (:account/type session))
    (let [counselor-id (-> match :path-params :id)
          [ok? result] (params->tx-data (d/db conn)
                                        (Long/parseLong counselor-id)
                                        multipart-params)]
      (if ok?
        (success-response conn (Long/parseLong counselor-id) result)
        (error-response 400 result)))
    (error-response 400 "forbidden")))
