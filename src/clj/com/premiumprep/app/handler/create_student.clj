(ns com.premiumprep.app.handler.create-student
  (:require [clojure.instant :refer [read-instant-date]]
            [clojure.string :as str]
            [com.premiumprep.app.email :as email]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :as u :refer [student-result]]
            [crypto.random :refer [url-part]]
            [datomic.client.api :as d])
  (:import (java.util Date)))

(defn- missing-params [params]
  (let [required-params ["email" "counselor-id"]
        param-missing? #(str/blank? (get params %))]
    (set (filter param-missing? required-params))))

(defn- missing-params-msg
  "Given a seq of missing params, return a suitable error string."
  [params]
  (when (not-empty params)
    (let [multiple? (-> params rest seq)]
      (format "The following required parameter%s %s missing: %s"
              (if multiple? "s" "")
              (if multiple? "are" "is")
              (str/join ", " (sort params))))))

(defn- nonnumeric-counselor-id-msg [params]
  (let [cid (get params "counselor-id")]
    (when (and (not (str/blank? cid))
               (not (re-matches #"\d+" cid)))
      (format "The given counselor-id value (%s) is not numeric" cid))))

(defn- counselor-id-not-found-msg [db params]
  (let [cid (get params "counselor-id")]
    (when (and (not (str/blank? cid))
               (re-matches #"\d+" cid))
      (let [cid (Long/parseLong cid)
            query '{:find [?counselor-eid]
                    :in [$ ?counselor-eid]
                    :where [[?counselor-eid :counselor/account ?account-eid]
                            [?account-eid :account/type ?atype-eid]
                            [?atype-eid :db/ident :account-type/counselor]]}
            results (d/q {:query query, :args [db cid]})]
        (when (empty? results)
          (format
            "The given counselor-id value (%s) is not found in the database"
            cid))))))

(defn- duplicate-email-msg [db email]
  (let [query '{:find [?account]
                :in [$ ?email]
                :where [[?account :account/email ?email]]}
        results (d/q {:query query, :args [db email]})]
    (when (not-empty results)
      (format "The given email (%s) is already taken" email))))

(defn- error-messages
  "If there are any errors with the given params, return a vector of error
  messages; otherwise, return nil."
  [db params]
  (let [{:strs [email counselor-id]} params
        missing-params (missing-params params)
        nonnumeric-msg (when-not (contains? missing-params "counselor-id")
                         (nonnumeric-counselor-id-msg params))
        not-found-msg (when (and (not (contains? missing-params "counselor-id"))
                                 (nil? nonnumeric-msg))
                        (counselor-id-not-found-msg db params))
        err-msgs (vec (remove nil? [(missing-params-msg missing-params)
                                    nonnumeric-msg
                                    not-found-msg
                                    (duplicate-email-msg db email)]))]
    (when (not-empty err-msgs)
      err-msgs)))

(defn- maybe-assoc
  "If the given value is not blank, assoc it to the given map at the given key."
  [m k v]
  (let [present? #(if (string? %)
                    (not (str/blank? %))
                    (some? v))]
    (if (present? v)
      (assoc m k v)
      m)))

(defn- params->tx-data*
  "Convert the given valid params into a tx-data sequence."
  [params]
  (let [{:strs [email counselor-id]} params
        counselor-eid (Long/parseLong counselor-id)
        parse #(when-not (str/blank? %) (Long/parseLong %))
        account {:db/id "account"
                 :account/email email
                 :account/active? true
                 :account/token (url-part 64)
                 :account/state :account-state/uninitialized
                 :account/type :account-type/student}
        student {:db/id "student"
                 :student/account "account"
                 :student/counselor counselor-eid
                 :student/joined-at (new Date)}
        tx-datom [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.create-student"]]
    [account student tx-datom]))

(defn- params->tx-data
  "Convert the given params (with string keys) into a tx-data sequence suitable
  for calling d/transact. If successful, return a vector of true and the tx-data
  sequence. If any validation check fails, instead return a vector of false and
  a sequence of error message strings (one for each error) suitable for display
  to the end user."
  [db params]
  (if-let [err-msgs (error-messages db params)]
    [false err-msgs]
    [true (params->tx-data* params)]))

(defn error-response [err-msgs]
  (let [intro "The given data generated the following errors:\n"
        msg->line #(format "- %s\n" %)]
    (u/error-response 400 (apply str intro (map msg->line err-msgs)))))

(defn success-response [conn tx-data]
  (let [{:keys [tempids]} (d/transact conn {:tx-data tx-data})
        student-eid (get tempids "student")
        student-data (student-result (student/find (d/db conn) student-eid))]
    {:status 201
     :body student-data}))

(defn handler
  [{:keys [datomic/conn email-send-fn multipart-params session]}]
  (if (= :account-type/superuser (:account/type session))
    (let [[ok? result] (params->tx-data (d/db conn) multipart-params)]
      (if ok?
        (do
          (let [{:keys [account/token account/email]} (first result)]
            (email/welcome-student email-send-fn email token))
          (success-response conn result))
        (error-response result)))
    (u/error-response 403 "forbidden")))
