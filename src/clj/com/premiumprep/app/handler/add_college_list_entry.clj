(ns com.premiumprep.app.handler.add-college-list-entry
  (:require [clojure.string :as str]
            [com.premiumprep.app.model.college :as college]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response
                                              maybe-remove-eliminated-entries
                                              student-result]]
            [datomic.client.api :as d]))

(defn- advisor? [db student-id account-id]
  (let [numeric? #(re-matches #"\d+" %)]
    (when (and student-id (numeric? student-id)
               account-id (numeric? account-id))
      (when-let [student (student/find db (Long/parseLong student-id))]
        (= (Long/parseLong account-id)
           (-> student :student/counselor :counselor/account :db/id))))))

(defn- authorized?
  [db student-id {:keys [db/id account/type] :as _session}]
  (cond
    (= type :account-type/superuser) true
    (not= type :account-type/counselor) false
    :else (advisor? db student-id id)))

(defn- missing-params [params]
  (let [required-params ["category" "college-id" "student-id"]
        param-missing? #(str/blank? (get params %))]
    (set (filter param-missing? required-params))))

(defn- missing-params-msg
  "Given a seq of missing params, return a suitable error string."
  [params]
  (when (not-empty params)
    (let [multiple? (-> params rest seq)]
      (format "The following required parameter%s %s missing: %s"
              (if multiple? "s" "")
              (if multiple? "are" "is")
              (str/join ", " (sort params))))))

(defn- request-error [params]
  (let [missing (missing-params params)
        nonnumeric? #(not (re-matches #"\d+" (get params %)))
        invalid? (delay
                   (not
                     (#{"high-reach" "reach" "target" "likely" "uncategorized"}
                       (get params "category"))))]
    (or (missing-params-msg missing)
        (when (nonnumeric? "college-id")
          "The college-id parameter is not numeric")
        (when (nonnumeric? "student-id")
          "The student-id parameter is not numeric")
        (when @invalid? "The category parameter is invalid"))))

(defn- college-already-in-list? [db student-id college-id]
  (let [query '{:find [?entry ?eliminated]
                :in [$ ?student ?college]
                :where [[?student :student/college-list-entries ?entry]
                        [?entry :college-list-entry/college ?college]
                        [?entry :college-list-entry/eliminated? ?eliminated]]}
        results (d/q {:query query
                     :args [db student-id college-id]})]
    (cond
      (empty? results) false
      (not-any? true? (map second results)) true
      :else (->> results
                 (filter second)
                 ffirst))))

(defn- next-index-for-list [db student-id category]
  (let [query '{:find [(max ?index)]
                :in [$ ?student ?category]
                :where [[?student :student/college-list-entries ?entry]
                        [?entry :college-list-entry/category ?category]
                        [?entry :college-list-entry/index ?index]]}
        result (d/q {:query query, :args [db student-id category]})]
    (if (empty? result)
      0
      (-> result ffirst inc))))

(defn- success-response [db atype student-id]
  {:status 201
   :body (->> student-id
              (student/find db)
              (maybe-remove-eliminated-entries atype)
              student/listify-college-list-entries
              student-result
              :student/college-lists)})

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (let [db (d/db conn)]
    (if-not (authorized? db (get multipart-params "student-id") session)
      (error-response 403 "forbidden")
      (if-let [err-msg (request-error multipart-params)]
        (error-response 400 err-msg)

        (let [{:strs [category college-id student-id]} multipart-params
              category (keyword "college-list-category" category)
              college-id (Long/parseLong college-id)
              student-id (Long/parseLong student-id)
              date-added (java.util.Date.)
              atype (:account/type session)
              index (next-index-for-list db student-id category)]

          (if-let [student (student/find db student-id)]
            (if-let [college (college/find db college-id)]
              (if-let [v (college-already-in-list? db student-id college-id)]
                (if (true? v)
                  (error-response
                    409 "That college is already in a list for that student")

                  (let [txdata [[:db/add v :college-list-entry/eliminated? false]
                                [:db/add v :college-list-entry/date-added date-added]
                                [:db/add v :college-list-entry/category category]
                                [:db/add v :college-list-entry/index index]
                                [:db/add "datomic.tx" :transaction/namespace
                                 "com.premiumprep.app.handler.add-college-list-entry"]]]
                    (d/transact conn {:tx-data txdata})
                    (success-response (d/db conn) atype (:db/id student))))

                (let [tx-data [[:db/add student-id :student/college-list-entries
                                "cle"]
                               {:db/id "cle"
                                :college-list-entry/category category
                                :college-list-entry/date-added date-added
                                :college-list-entry/index index
                                :college-list-entry/college college-id}
                               [:db/add "datomic.tx" :transaction/namespace
                                "com.premiumprep.app.handler.add-college-list-entry"]]]
                  (d/transact conn {:tx-data tx-data})
                  (success-response (d/db conn) atype (:db/id student))))
              (error-response 404 "not found"))
            (error-response 404 "not found")))))))
