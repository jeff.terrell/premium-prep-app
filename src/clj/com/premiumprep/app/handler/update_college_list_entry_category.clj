(ns com.premiumprep.app.handler.update-college-list-entry-category
  (:require [clojure.set :as set]
            [clojure.string :as str]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response
                                              maybe-remove-eliminated-entries
                                              student-result]]
            [datomic.client.api :as d]))

(defn- missing-required-params-error [params]
  (let [required #{"student-id" "college-id" "category"}
        missing (set/difference required (-> params keys set))]
    (case (count missing)
      0 nil
      1 (str "Missing parameter: " (first missing))
      (str "Missing parameters: " (str/join ", " (sort missing))))))

(defn- nonnumeric-params-error [params]
  (let [numeric? #(and % (re-matches #"\d+" %))
        nn-params (remove (comp numeric? params)
                          ["student-id" "college-id"])]
    (case (count nn-params)
      0 nil
      1 (str "Non-numeric parameter: " (first nn-params))
      (str "Non-numeric parameters: " (str/join ", " nn-params)))))

(defn- authorized? [db session params]
  (when-let [{:keys [db/id account/type]} session]
    (cond
      (#{:account-type/superuser :account-type/counselor} type) true
      (not= type :account-type/student) false
      :else (when-let [student (student/find-by-account-id
                                 db (Long/parseLong id))]
              (= (str (:db/id student))
                 (get params "student-id"))))))

(defn- invalid-category-error [params]
  (let [valid-category-name? #{"high-reach"
                               "reach"
                               "target"
                               "likely"
                               "uncategorized"}
        category-name (get params "category")]
    (when-not (valid-category-name? category-name)
      "Invalid category")))

(defn- find-entry [db student-id college-id]
  (let [query '{:find [?entry]
                :in [$ ?student ?college]
                :where [[?student :student/college-list-entries ?entry]
                        [?entry :college-list-entry/college ?college]]}
        result (d/q {:query query, :args [db student-id college-id]})]
    (ffirst result)))

(defn- lookup [db entity attr]
  (let [result (get (d/pull db [attr] entity) attr)]
    (if (map? result)
      (:db/id result)
      result)))

(defn- lookup-path [db entity path]
  (reduce (fn [entity attr] (lookup db entity attr))
          entity path))

(defn- next-index-for-list [db student-id category]
  (let [query '{:find [(max ?index)]
                :in [$ ?student ?category]
                :where [[?student :student/college-list-entries ?entry]
                        [?entry :college-list-entry/category ?category]
                        [?entry :college-list-entry/index ?index]]}
        result (d/q {:query query, :args [db student-id category]})]
    (if (empty? result)
      0
      (-> result ffirst inc))))

(defn- retract&add-datoms [eid attr old-val new-val]
  (when (not= old-val new-val)
    [[:db/retract eid attr old-val]
     [:db/add eid attr new-val]]))

(defn- entry-datoms [db student-id entry-id old-category old-index new-category]
  (let [new-index (next-index-for-list db student-id new-category)]
    (concat
      (retract&add-datoms entry-id :college-list-entry/category
                          old-category new-category)
      (retract&add-datoms entry-id :college-list-entry/index
                          old-index new-index))))

(defn- entries-beyond-index [db student-id category index]
  (let [query '{:find [?entry]
                :in [$ ?student ?category ?index-in]
                :where [[?student :student/college-list-entries ?entry]
                        [?entry :college-list-entry/category ?category]
                        [?entry :college-list-entry/index ?index]
                        [(< ?index-in ?index)]]}
        results (d/q {:query query
                      :args [db student-id category index]})]
    (mapv first results)))

(defn- shift-datoms [db student-id old-category old-index]
  (let [later-entries (entries-beyond-index db student-id old-category old-index)
        later-indexes (for [i (-> later-entries count range)] (+ old-index i))]
    (apply concat
           (for [[entry index] (map list later-entries later-indexes)]
             [[:db/retract entry :college-list-entry/index (inc index)]
              [:db/add     entry :college-list-entry/index index]]))))

(defn- tx-data [db student-id entry-id old-category new-category]
  (when (not= old-category new-category)
    (let [old-index (lookup db entry-id :college-list-entry/index)
          entry-datoms (entry-datoms db student-id entry-id old-category
                                     old-index new-category)
          shift-datoms (shift-datoms db student-id old-category old-index)
          tx-datom [:db/add "datomic.tx" :transaction/namespace
                    "com.premiumprep.app.handler.update-college-list-entry-category"]]
      (conj (vec (concat entry-datoms shift-datoms))
            tx-datom))))


(defn- happy-path [atype conn student-id entry-id params]
  (let [db (d/db conn)
        new-category (keyword "college-list-category" (get params "category"))
        old-category (lookup-path db entry-id
                                  [:college-list-entry/category :db/ident])
        tx-data (tx-data db student-id entry-id old-category new-category)]
    (when tx-data (d/transact conn {:tx-data tx-data}))
    {:status 200
     :body (->> student-id (student/find (d/db conn))
                (maybe-remove-eliminated-entries atype)
                student/listify-college-list-entries student-result
                :student/college-lists)}))

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-let [err-msg (missing-required-params-error multipart-params)]
    (error-response 400 err-msg)
    (if-let [err-msg (nonnumeric-params-error multipart-params)]
      (error-response 400 err-msg)
      (let [db (d/db conn)]
        (if-not (authorized? db session multipart-params)
          (error-response 403 "unauthorized")
          (if-let [err-msg (invalid-category-error multipart-params)]
            (error-response 400 err-msg)
            (let [parse #(Long/parseLong (get multipart-params %))
                  student-id (parse "student-id")
                  college-id (parse "college-id")
                  atype (:account/type session)]
              (if-let [entry-id (find-entry db student-id college-id)]
                (happy-path atype conn student-id entry-id multipart-params)
                (error-response 404 "college list entry not found")))))))))
