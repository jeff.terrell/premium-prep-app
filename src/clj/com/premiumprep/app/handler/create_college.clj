(ns com.premiumprep.app.handler.create-college
  (:require [clojure.string :as str]
            [com.premiumprep.app.email :as email]
            [com.premiumprep.app.model.college :as college]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d]))

(defn- authorized? [session]
  (= :account-type/superuser (:account/type session)))

(defn- missing-params [params]
  (let [required-params ["name" "enrollment" "admit-rate" "average-sat"
                         "average-act"]
        param-missing? #(str/blank? (get params %))]
    (set (filter param-missing? required-params))))

(defn- missing-params-msg
  "Given a seq of missing params, return a suitable error string."
  [params]
  (when (not-empty params)
    (let [multiple? (-> params rest seq)]
      (format "The following required parameter%s %s missing: %s"
              (if multiple? "s" "")
              (if multiple? "are" "is")
              (str/join ", " (sort params))))))

(defn- bad-request-message [params]
  (-> params missing-params missing-params-msg))

(defn- college-name-conflict? [db name]
  (boolean (college/find-by-name db name)))

(defn handler
  [{:keys [datomic/conn email-send-fn multipart-params session]}]
  (if-not (authorized? session)
    (error-response 403 "forbidden")
    (if-let [error-msg (bad-request-message multipart-params)]
      (error-response 400 error-msg)
      (if (college-name-conflict? (d/db conn) (get multipart-params "name"))
        (error-response 409 "There is already a college with that name")
        (let [{:strs [name enrollment admit-rate average-sat
                      average-act]} multipart-params
              tx-data [{:db/id "college"
                        :college/name name
                        :college/enrollment enrollment
                        :college/admit-rate admit-rate
                        :college/average-sat average-sat
                        :college/average-act average-act}
                       [:db/add "datomic.tx" :transaction/namespace
                        "com.premiumprep.app.handler.create-college"]]
              {{id "college"} :tempids} (d/transact conn {:tx-data tx-data})]
          (email/college-added email-send-fn multipart-params)
          {:status 201
           :body (update (college/find (d/db conn) id) :db/id str)})))))
