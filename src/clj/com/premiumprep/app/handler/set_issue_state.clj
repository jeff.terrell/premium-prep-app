(ns com.premiumprep.app.handler.set-issue-state
  (:require
   [clojure.string :as str]
   [com.premiumprep.app.model.issue :as issue]
   [com.premiumprep.app.model.student :as student]
   [com.premiumprep.app.util :refer [error-response]]
   [datomic.client.api :as d])
  (:import java.util.Date))

(defn- parse-long [str] ; redef-able var for testing purposes
  (Long/parseLong str))

(defn- now [] ; again, redef-able var for testing purposes
  (new Date))

(defn- bad-request-message [multipart-params]
  (let [{:strs [issue-id to-state]} multipart-params]
    (cond
      (and (str/blank? issue-id)
           (str/blank? to-state)) "missing issue-id and to-state params"
      (str/blank? issue-id) "missing issue-id param"
      (str/blank? to-state) "missing to-state param"
      (not (re-matches #"\d+" issue-id)) "non-numeric issue-id"
      (not (#{"new"
              "read"
              "needs-discussion"
              "counselor-resolved"
              "closed"} to-state)) "invalid to-state value"
      :else nil)))

(defn- authorized? [session advising-counselor-acct-id]
  (cond
    (empty? session) false
    (= :account-type/superuser (:account/type session)) true
    (= :account-type/student (:account/type session)) false
    (not= :account-type/counselor (:account/type session)) false
    (= (str advising-counselor-acct-id) (:db/id session)) true
    :else false))

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-let [err-message (bad-request-message multipart-params)]
    (error-response 400 err-message)
    (let [{:strs [issue-id to-state]} multipart-params
          issue-id (parse-long issue-id)
          db (d/db conn)]

      (if-let [issue (issue/find db issue-id)]
        (let [counselor-id (->> issue
                                :issue/student
                                :db/id
                                (student/find db)
                                :student/counselor :counselor/account :db/id)]
          (if-not (authorized? session counselor-id)
            (error-response 403 "forbidden")

            (let [new-state (keyword "issue-state" to-state)
                  update-ts? (= :account-type/counselor (:account/type session))
                  tx-data [[:db/add issue-id :issue/state new-state]]
                  current-ts (now)
                  tx-data (if update-ts?
                            (conj tx-data [:db/add issue-id
                                           :issue/counselor-touched-at
                                           current-ts])
                            tx-data)
                  issue (if update-ts?
                          (assoc issue :issue/counselor-touched-at current-ts)
                          issue)
                  issue (-> issue
                            (assoc :issue/state new-state)
                            (update :db/id str)
                            (update-in [:issue/student :db/id] str))
                  tx-datom [:db/add "datomic.tx" :transaction/namespace
                            "com.premiumprep.app.handler.set-issue-state"]]

              (d/transact conn {:tx-data (conj tx-data tx-datom)})
              {:status 200
               :body issue})))
        (error-response 404 "not found")))))
