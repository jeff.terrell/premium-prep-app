(ns com.premiumprep.app.handler.update-custom-field
  (:require [clojure.set :as set]
            [clojure.string :as str]
            [datomic.client.api :as d]))

(defn- missing-required-params-error [params]
  (let [required #{"id" "name" "value"}
        missing (set/difference required (-> params keys set))]
    (case (count missing)
      0 nil
      1 (str "Missing parameter: " (first missing))
      (str "Missing parameters: " (str/join ", " (sort missing))))))

(defn- nonnumeric-params-error [params]
  (let [numeric? #(and % (re-matches #"\d+" %))]
    (when-not (numeric? (get params "id"))
      "Non-numeric parameter: id")))

(defn- error-response [status msg]
  {:status status, :headers {}, :body msg})

(defn- lookup [db attr entity-id]
  (letfn [(pull [entity-id] (d/pull db [attr] entity-id))
          (get-attr [map] (get map attr))
          (resolve-many [result] (if (vector? result) (first result) result))
          (resolve-ref [result] (if (map? result) (:db/id result) result))]
    (some-> entity-id pull get-attr resolve-many resolve-ref)))

(defn- lookup* [db path entity-id]
  (reduce (fn [eid next-attr] (lookup db next-attr eid))
          entity-id
          path))

(defn- account-id [db params]
  (let [field-id (Long/parseLong (get params "id"))]
    (lookup* db [:college-list-entry/_custom-fields
                 :student/_college-list-entries
                 :student/account]
             field-id)))

(defn- authorized? [db session params]
  (when-let [{:keys [db/id account/type]} session]
    (let [counselor-only-field? #{"early-decision?"
                                  "interest-demonstrated"
                                  "counselor-notes"}]
      (cond
        (#{:account-type/superuser :account-type/counselor} type) true
        (not= type :account-type/student) false
        (counselor-only-field? (get params "field")) false
        :else (when-let [account-id (account-id db params)]
                (= id (str account-id)))))))

(defn- field-exists? [db field-id]
  (ffirst
    (d/q {:query '{:find [?field]
                   :in [$ ?field]
                   :where [[?field :custom-field/name _]]}
          :args [db field-id]})))

(defn- happy-path [conn field-id name value]
  (let [tx-datom [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.update-custom-field"]]
    (d/transact conn {:tx-data [{:db/id field-id
                                 :custom-field/name name
                                 :custom-field/value value}
                                tx-datom]}))
  {:status 204, :headers {}})

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-let [err-msg (missing-required-params-error multipart-params)]
    (error-response 400 err-msg)
    (if-let [err-msg (nonnumeric-params-error multipart-params)]
      (error-response 400 err-msg)
      (let [db (d/db conn)]
        (if-not (authorized? db session multipart-params)
          (error-response 403 "unauthorized")
          (let [field-id (Long/parseLong (get multipart-params "id"))]
            (if-not (field-exists? db field-id)
              (error-response 404 "custom field not found")
              (let [{:strs [name value]} multipart-params]
                (happy-path conn field-id name value)))))))))
