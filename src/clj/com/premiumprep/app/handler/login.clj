(ns com.premiumprep.app.handler.login
  (:require [clojurewerkz.scrypt.core :as sc]
            [com.premiumprep.app.util :refer [flatten-account-enums]]
            [datomic.client.api :as d])
  (:import (java.io ByteArrayOutputStream)))

(defn authenticate [db email password]
  (let [query '{:find [(pull ?account
                             [:db/id :account/email :account/password
                              {:account/type [:db/ident]}
                              {:account/state [:db/ident]}])]
                :in [$ ?email]
                :where [[?account :account/email ?email]
                        [?account :account/active? true]]}
        account (ffirst (d/q {:query query, :args [db email]}))]
    (when (and account
               (sc/verify password (:account/password account)))
      (-> account
          flatten-account-enums
          (dissoc :account/password)))))

(defn handler
  [{:keys [datomic/conn multipart-params]}]
  (let [{:strs [email password]} multipart-params]
    (if-let [result (authenticate (d/db conn) email password)]
      {:status 200
       :body (update result :db/id str)
       :session (:db/id result)}
      {:status 403})))
