(ns com.premiumprep.app.handler.download-document
  (:require [com.premiumprep.app.model.document :as doc]
            [com.premiumprep.app.storage :refer [retrieve]]
            [datomic.client.api :as d]))

(defn- authorized? [session document]
  (let [{:keys [db/id account/type]} session]
    (cond
      (nil? type) false
      (= :account-type/superuser type) true
      :else (let [id (Long/parseLong id)]
              (cond
                (= :account-type/counselor type) (= id (:counselor/account-id
                                                        document))
                (= :account-type/student type) (= id (:student/account-id
                                                      document)))))))

(defn handler
  [{:keys [datomic/conn reitit.core/match session storage-config] :as req}]
  (let [{:keys [path-params]} match
        doc-id (-> path-params :id Long/parseLong)
        db (d/db conn)]
    (if-let [document (doc/find db doc-id)]
      (if-not (authorized? session document)
        {:status 403}
        {:status 200
         :headers {"content-type" (:document/content-type document)}
         :body (retrieve storage-config (:document/s3-path document))})
      {:status 404})))
