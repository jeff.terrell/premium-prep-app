(ns com.premiumprep.app.handler.change-password
  (:require [com.premiumprep.app.model.account :as account]))

(def ^:private errors
  {:missing-password "Error: you did not provide a password."
   :password-too-short "Error: your password must be at least 8 characters."})

(defn- bad-request [err]
  {:status 400
   :headers {}
   :body (errors err)})

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-not session
    {:status 403, :headers {}}
    (let [password (multipart-params "password")
          account-id (Long/parseLong (:db/id session))]
      (cond
        (not password) (bad-request :missing-password)
        (> 8 (count password)) (bad-request :password-too-short)
        :else (do
                (account/save-password conn account-id password)
                {:status 204, :headers {}})))))
