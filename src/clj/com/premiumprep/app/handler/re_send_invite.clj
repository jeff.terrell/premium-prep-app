(ns com.premiumprep.app.handler.re-send-invite
  (:require [com.premiumprep.app.email :as email]
            [com.premiumprep.app.model.account :as account]
            [datomic.client.api :as d]))

(defn handler
  [{:keys [datomic/conn email-send-fn reitit.core/match session]}]
  (try
    (let [{:keys [path-params data]} match
          id (-> path-params :id Long/parseLong)
          atype (:account/type session)]
      (if-not (#{:account-type/superuser :account-type/counselor} atype)
        {:status 403, :headers {}}
        (if-let [account (account/find (d/db conn) id)]
          (let [{:keys [account/token account/email]} account]
            (email/welcome-student email-send-fn email token)
            {:status 204, :headers {}})
          {:status 404, :headers {}})))
    (catch NumberFormatException _
      {:status 404, :headers {}})))
