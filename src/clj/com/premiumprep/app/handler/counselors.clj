(ns com.premiumprep.app.handler.counselors
  (:require [com.premiumprep.app.model.counselor :as model]
            [com.premiumprep.app.util :refer [counselor-result error-response]]
            [datomic.client.api :as d]))

(defn counselor-list [db]
  (->> (model/all db)
       (map counselor-result)
       vec))

(defn handler
  [{:keys [session datomic/conn]}]
  (if (= :account-type/superuser (:account/type session))
    {:status 200
     :body (counselor-list (d/db conn))}
    (error-response 403 "forbidden")))
