(ns com.premiumprep.app.handler.students
  (:require [com.premiumprep.app.model.student :as model]
            [com.premiumprep.app.util :refer [student-result error-response]]
            [datomic.client.api :as d]))

(defn- authorized? [session]
  (when (and session (map? session))
    (let [{:keys [account/type account/state]} session]
      (or (= :account-type/superuser type)
          (and (= :account-type/counselor type)
               (not= :account-state/email-unverified state))))))

(defn student-list-for-counselor [db counselor-acct-id]
  (->> (model/find-all-by-counselor-account-id db counselor-acct-id)
       (map student-result)
       (filter (comp :account/active? :student/account))
       vec))

(defn student-list-for-superuser [db]
  (->> (model/all db)
       (map student-result)
       vec))

(defn student-list [db session]
  (let [counselor-request? (= :account-type/counselor (:account/type session))
        counselor-acct-id (when counselor-request? (:db/id session))]
    (if counselor-request?
      (student-list-for-counselor db (Long/parseLong counselor-acct-id))
      (student-list-for-superuser db))))

(defn handler
  [{:keys [datomic/conn session]}]
  (if (authorized? session)
    {:status 200
     :body (student-list (d/db conn) session)}
    (error-response 403 "forbidden")))
