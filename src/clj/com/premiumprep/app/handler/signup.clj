(ns com.premiumprep.app.handler.signup
  (:require [com.premiumprep.app.model.account :as account]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.model.student-fields :refer [fields]]
            [com.premiumprep.app.validation :refer [fields-tx-data
                                                    validate-params]]
            [datomic.client.api :as d]))

(defn- bad-token [db token]
  (when-not (account/find-by-token db token)
    (str "Error: your signup token could not be found."
         " Please ensure that you are at the address "
         "included in your welcome email.")))

(defn- mismatched-passwords [p1 p2]
  (when (not= p1 p2)
    "The given passwords do not match"))

(defn- error-messages
  "If there are any errors with the given params, return a vector of error
  messages; otherwise, return nil."
  [db params]
  (if-let [bad-token-msg (bad-token db (get params "token"))]
    bad-token-msg
    (if-let [mismatched-msg (mismatched-passwords (get params "password")
                                                  (get params "confirmation"))]
      mismatched-msg
      (validate-params fields params))))

(defn- params->tx-data
  "Convert the given params (with string keys) into a tx-data sequence suitable
  for calling d/transact. If successful, return a vector of true and the tx-data
  sequence. If any validation check fails, instead return a vector of false and
  a sequence of error message strings (one for each error) suitable for display
  to the end user."
  [db params]
  (if-let [err-msg (error-messages db params)]
    [false err-msg]
    [true (let [token (get params "token")
                {account-eid :db/id} (account/find-by-token db token)
                {student-eid :db/id} (student/find-by-account-id db account-eid)]
            (conj (fields-tx-data db student-eid fields params)
                  [:db/add account-eid
                   :account/state :account-state/initialized]
                  [:db/add "datomic.tx" :transaction/namespace
                   "com.premiumprep.app.handler.signup"]))]))

(defn error-response [err-msg]
  {:status 400
   :headers {"content-type" "text/plain"}
   :body err-msg})

(defn success-response [conn tx-data]
  (d/transact conn {:tx-data tx-data})
  {:status 204, :headers {}})

(defn handler
  [{:keys [datomic/conn multipart-params]}]
  (let [[ok? result] (params->tx-data (d/db conn) multipart-params)]
    (if ok?
      (success-response conn result)
      (error-response result))))
