(ns com.premiumprep.app.handler.create-counselor
  (:require
   [clojurewerkz.scrypt.core :as sc]
   [com.premiumprep.app.model.counselor :as counselor]
   [com.premiumprep.app.model.counselor-fields :refer [fields-for-create]]
   [com.premiumprep.app.util :refer [counselor-result error-response]]
   [com.premiumprep.app.validation :refer [fields-tx-data validate-params]]
   [crypto.random :refer [base64]]
   [datomic.client.api :as d]))

(defn- duplicate-email-msg [db email]
  (let [query '{:find [?account]
                :in [$ ?email]
                :where [[?account :account/email ?email]]}
        results (d/q {:query query, :args [db email]})]
    (when (not-empty results)
      (format "The given email (%s) is already taken" email))))

(defn- error-messages
  [db params]
  (if-let [conflict-msg (duplicate-email-msg db (get params "email"))]
    conflict-msg
    (validate-params fields-for-create params)))

(defn- params->tx-data*
  "Convert the given valid params into a tx-data sequence."
  [db params]
  (let [pw-length 24 ; 24 bytes = 32 characters
        password (base64 pw-length)
        encrypted-password (sc/encrypt password 16384 8 1)
        counselor-eid "counselor"
        ;; mimic logic from validation/tx-data:
        account-eid (str counselor-eid ",:counselor/account")]
    (with-meta
      (conj (fields-tx-data db counselor-eid fields-for-create params)
            [:db/add account-eid :account/password encrypted-password]
            [:db/add account-eid :account/active? true]
            [:db/add account-eid :account/state :account-state/initialized]
            [:db/add account-eid :account/type :account-type/counselor]
            [:db/add "datomic.tx" :transaction/namespace
             "com.premiumprep.app.handler.create-counselor"])
      {:password password})))

(defn- params->tx-data
  "Convert the given params (with string keys) into a tx-data sequence suitable
  for calling d/transact. If successful, return a vector of true and the tx-data
  sequence. If any validation check fails, instead return a vector of false and
  a sequence of error message strings (one for each error) suitable for display
  to the end user."
  [db params]
  (if-let [err-msg (error-messages db params)]
    [false err-msg]
    [true (params->tx-data* db params)]))

(defn success-response [conn tx-data]
  (let [{:keys [tempids]} (d/transact conn {:tx-data tx-data})
        counselor-eid (get tempids "counselor")
        password (:password (meta tx-data))
        counselor-data (counselor-result (counselor/find (d/db conn) counselor-eid))
        body-data {:counselor counselor-data
                   :password password}]
    {:status 201
     :body body-data}))

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if (= :account-type/superuser (:account/type session))
    (let [[ok? result] (params->tx-data (d/db conn) multipart-params)]
      (if ok?
        (success-response conn result)
        (error-response 400 result)))
    (error-response 403 "forbidden")))
