(ns com.premiumprep.app.handler.update-student
  (:require [clojure.string :as str]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :as u :refer [student-result]]
            [datomic.client.api :as d]))

(defn- invalid-id-msg [id]
  (when-not (re-matches #"\d+" id)
    (str "Invalid id in request path: " id)))

(defn- missing-params [params]
  (let [required-params ["email" "counselor-id" "active"]
        param-missing? #(str/blank? (get params %))]
    (set (filter param-missing? required-params))))

(defn- missing-params-msg
  "Given a seq of missing params, return a suitable error string."
  [params]
  (when (not-empty params)
    (let [multiple? (-> params rest seq)]
      (format "The following required parameter%s %s missing: %s"
              (if multiple? "s" "")
              (if multiple? "are" "is")
              (str/join ", " (sort params))))))

(defn- nonnumeric-counselor-id-msg [params]
  (let [cid (get params "counselor-id")]
    (when (and (not (str/blank? cid))
               (not (re-matches #"\d+" cid)))
      (format "The given counselor-id value (%s) is not numeric" cid))))

(defn- nonboolean-active [params]
  (let [active (get params "active")]
    (when-not (#{"true" "false"} active)
      (format "The given active value (%s) is not boolean" active))))

(defn- counselor-id-not-found-msg [db params]
  (let [cid (get params "counselor-id")]
    (when (and (not (str/blank? cid))
               (re-matches #"\d+" cid))
      (let [cid (Long/parseLong cid)
            query '{:find [?counselor-eid]
                    :in [$ ?counselor-eid]
                    :where [[?counselor-eid :counselor/account ?account-eid]
                            [?account-eid :account/type ?atype-eid]
                            [?atype-eid :db/ident :account-type/counselor]]}
            results (d/q {:query query, :args [db cid]})]
        (when (empty? results)
          (format
            "The given counselor-id value (%s) is not found in the database"
            cid))))))

(defn- error-messages
  "If there are any errors with the given params, return a vector of error
  messages; otherwise, return nil."
  [db id params]
  (let [invalid-id-msg (invalid-id-msg id)
        {:strs [email counselor-id active]} params
        missing-params (missing-params params)
        nonboolean-msg (when-not (contains? missing-params "active")
                         (nonboolean-active params))
        nonnumeric-msg (when-not (contains? missing-params "counselor-id")
                         (nonnumeric-counselor-id-msg params))
        not-found-msg (when (and (not (contains? missing-params "counselor-id"))
                                 (nil? nonnumeric-msg))
                        (counselor-id-not-found-msg db params))
        err-msgs (vec (remove nil? [(missing-params-msg missing-params)
                                    nonboolean-msg
                                    nonnumeric-msg
                                    not-found-msg]))]
    (when (not-empty err-msgs)
      err-msgs)))

(defn- params->tx-data*
  "Convert the given valid params into a tx-data sequence."
  [db student-eid params]
  (let [{:strs [email counselor-id active]} params
        account-eid (->> student-eid (student/find db) :student/account :db/id)
        counselor-eid (Long/parseLong counselor-id)]
    [[:db/add account-eid :account/email email]
     [:db/add account-eid :account/active? (read-string active)]
     [:db/add student-eid :student/counselor counselor-eid]
     [:db/add "datomic.tx" :transaction/namespace
      "com.premiumprep.app.handler.update-student"]]))

(defn- params->tx-data
  "Convert the given params (with string keys) into a tx-data sequence suitable
  for calling d/transact. If successful, return a vector of true and the tx-data
  sequence. If any validation check fails, instead return a vector of false and
  a sequence of error message strings (one for each error) suitable for display
  to the end user."
  [db id params]
  (if-let [err-msgs (error-messages db id params)]
    [false err-msgs]
    [true (params->tx-data* db (Long/parseLong id) params)]))

(defn error-response [err-msgs]
  (let [intro "The given data generated the following errors:\n"
        msg->line #(format "- %s\n" %)]
    (u/error-response 400 (apply str intro (map msg->line err-msgs)))))

(defn success-response [conn id tx-data]
  (let [_ (d/transact conn {:tx-data tx-data})
        body-data (student-result (student/find (d/db conn) id))]
    {:status 200
     :body body-data}))

(defn handler
  [{:keys [datomic/conn multipart-params reitit.core/match session]}]
  (if (= :account-type/superuser (:account/type session))
    (let [sid (-> match :path-params :id)
          [ok? result] (params->tx-data (d/db conn) sid multipart-params)]
      (if ok?
        (success-response conn (Long/parseLong sid) result)
        (error-response result)))
    (u/error-response 403 "forbidden")))
