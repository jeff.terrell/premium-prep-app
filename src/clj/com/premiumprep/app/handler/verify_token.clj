(ns com.premiumprep.app.handler.verify-token
  (:require [com.premiumprep.app.model.account :as account]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d]))

(defn- found [email]
  {:status 200
   :body {:email email}})

(defn- not-found []
  (error-response 404 "not found"))

(defn handler
  [{:keys [datomic/conn query-params]}]
  (let [{:strs [token]} query-params]
    (if-let [account (account/find-by-token (d/db conn) token)]
      (found (:account/email account))
      (not-found))))
