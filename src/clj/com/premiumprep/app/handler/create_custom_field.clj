(ns com.premiumprep.app.handler.create-custom-field
  (:require [clojure.set :as set]
            [clojure.string :as str]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d]))

(defn- missing-required-params-error [params]
  (let [required #{"entry-id" "name" "value"}
        missing (set/difference required (-> params keys set))]
    (case (count missing)
      0 nil
      1 (str "Missing parameter: " (first missing))
      (str "Missing parameters: " (str/join ", " (sort missing))))))

(defn- nonnumeric-params-error [params]
  (let [numeric? #(and % (re-matches #"\d+" %))]
    (when-not (numeric? (get params "entry-id"))
      "Non-numeric parameter: entry-id")))

(defn- lookup [db attr entity-id]
  (letfn [(pull [entity-id] (d/pull db [attr] entity-id))
          (get-attr [map] (get map attr))
          (resolve-many [result] (if (vector? result) (first result) result))
          (resolve-ref [result] (if (map? result) (:db/id result) result))]
    (some-> entity-id pull get-attr resolve-many resolve-ref)))

(defn- account-id [db params]
  (let [entry-id (Long/parseLong (get params "entry-id"))
        student-id (lookup db :student/_college-list-entries entry-id)]
    (lookup db :student/account student-id)))

(defn- authorized? [db session params]
  (when-let [{:keys [db/id account/type]} session]
    (let [counselor-only-field? #{"early-decision?"
                                  "interest-demonstrated"
                                  "counselor-notes"}]
      (cond
        (#{:account-type/superuser :account-type/counselor} type) true
        (not= type :account-type/student) false
        (counselor-only-field? (get params "field")) false
        :else (when-let [account-id (account-id db params)]
                (= id (str account-id)))))))

(defn- entry-exists? [db entry-id]
  (ffirst
    (d/q {:query '{:find [?entry]
                   :in [$ ?entry]
                   :where [[?entry :college-list-entry/college _]]}
          :args [db entry-id]})))

(defn- happy-path [conn entry-id name value]
  (let [tmp-id "custom-field"
        ref [:db/add entry-id :college-list-entry/custom-fields tmp-id]
        entity-map {:db/id tmp-id
                    :custom-field/name name
                    :custom-field/value value}
        tx-datom [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.create-custom-field"]
        {:keys [tempids]} (d/transact conn {:tx-data [ref entity-map tx-datom]})
        real-id (get tempids tmp-id)
        info (assoc entity-map :db/id (str real-id))] ; you're a real boy now
    {:status 201
     :body info}))

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-let [err-msg (missing-required-params-error multipart-params)]
    (error-response 400 err-msg)
    (if-let [err-msg (nonnumeric-params-error multipart-params)]
      (error-response 400 err-msg)
      (let [db (d/db conn)]
        (if-not (authorized? db session multipart-params)
          (error-response 403 "unauthorized")
          (let [entry-id (Long/parseLong (get multipart-params "entry-id"))]
            (if-not (entry-exists? db entry-id)
              (error-response 404 "college list entry not found")
              (let [{:strs [name value]} multipart-params]
                (happy-path conn entry-id name value)))))))))
