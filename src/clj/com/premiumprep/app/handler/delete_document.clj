(ns com.premiumprep.app.handler.delete-document
  (:require [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d]))

(defn- missing-required-params-error [params]
  (when-not (contains? params "document-id")
    "Missing parameter: document-id"))

(defn- nonnumeric-params-error [params]
  (let [numeric? #(and % (re-matches #"\d+" %))]
    (when-not (numeric? (get params "document-id"))
      "Non-numeric parameter: document-id")))

(defn- authorized? [db session student-id]
  (let [{:keys [db/id account/type]} session]
    (or (#{:account-type/superuser :account-type/counselor} type)
        (and (= :account-type/student type)
             id
             (string? id)
             (= student-id
                (-> (student/find-by-account-id db (Long/parseLong id))
                    :db/id str))))))

(defn doc-exists? [db doc-id]
  (ffirst
    (d/q {:query '{:find [?doc-entity]
                   :in [$ ?doc-entity]
                   :where [[?doc-entity :document/name _]]}
          :args [db doc-id]})))

(defn- happy-path [conn doc-id]
  (let [deleted-doc (d/pull (d/db conn) '[*] doc-id)
        tx-data [[:db/retractEntity doc-id]
                 [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.delete-document"]]]
    (d/transact conn
                {:tx-data tx-data})
    {:status 200
     :body   deleted-doc}))

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-let [err-msg (missing-required-params-error multipart-params)]
    (error-response 400 err-msg)
    (if-let [err-msg (nonnumeric-params-error multipart-params)]
      (error-response 400 err-msg)
      (let [db (d/db conn)]
        (if-not (authorized? db session (get multipart-params "student-id"))
          (error-response 403 "forbidden")
          (let [doc-id (Long/parseLong (get multipart-params "document-id"))]
            (if-not (doc-exists? db doc-id)
             (error-response 404 "Document not found")
             (happy-path conn doc-id))))))))
