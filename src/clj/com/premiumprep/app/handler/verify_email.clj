(ns com.premiumprep.app.handler.verify-email
  (:require [com.premiumprep.app.model.account :as account]
            [datomic.client.api :as d]
            [com.premiumprep.app.email :as email]))

(defn- verify-email [email-send-fn conn token account]
  (let [{:keys [db/id account/email]} account
        tx-data [[:db/retract id :account/token token]
                 [:db/add id :account/state :account-state/initialized]
                 [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.verify-email"]]]
    (d/transact conn {:tx-data tx-data})
    (email/verified-email email-send-fn email)
    {:status 204, :headers {}}))

(defn handler
  [{:keys [datomic/conn email-send-fn query-params]}]
  (let [token (query-params "token")]
    (if-let [account (account/find-by-token (d/db conn) token)]
      (verify-email email-send-fn conn token account)
      {:status 400
       :headers {}
       :body (str "Error: the given token could not be found in the "
                  "database.\n\nPlease ensure that you are at the "
                  "address included in your verification email. If so, "
                  "you may need to send a new verification email.\n")})))
