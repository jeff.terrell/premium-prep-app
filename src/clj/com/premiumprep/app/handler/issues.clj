(ns com.premiumprep.app.handler.issues
  (:require [com.premiumprep.app.model.issue :as issue]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d]))

(defn- authorized? [session]
  (boolean
    (#{:account-type/superuser :account-type/counselor}
      (:account/type session))))

(defn- issue-result [issue]
  (-> issue
      (update :db/id str)
      (update-in [:issue/student :db/id] str)
      (update :issue/state :db/ident)))

(defn- filter-issues [db session issues]
  (let [{:keys [account/type db/id]} session]
    (if (= type :account-type/superuser)
      issues
      (let [counselor-ref (d/pull db [:counselor/_account] (Long/parseLong id))
            counselor-id (-> counselor-ref :counselor/_account first :db/id)
            student-refs (d/pull db [:student/_counselor] counselor-id)
            student-ids (mapv :db/id (:student/_counselor student-refs))
            authorized-student? (set student-ids)]
        (filterv #(-> % :issue/student :db/id authorized-student?)
                 issues)))))

(defn handler
  [{:keys [datomic/conn session]}]
  (if-not (authorized? session)
    (error-response 403 "not found")
    {:status 200
     :body (let [db (d/db conn)
                 issues (filter-issues db session (issue/all db))]
             (mapv issue-result issues))}))
