(ns com.premiumprep.app.handler.update-college-list-entry-field
  (:require [clojure.instant :as instant]
            [clojure.set :as set]
            [clojure.string :as str]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response]]
            [datomic.client.api :as d]))

(defn- missing-required-params-error [params]
  (let [required #{"student-id" "college-id" "field" "value"}
        missing (set/difference required (-> params keys set))]
    (case (count missing)
      0 nil
      1 (str "Missing parameter: " (first missing))
      (str "Missing parameters: " (str/join ", " (sort missing))))))

(defn- nonnumeric-params-error [params]
  (let [numeric? #(and % (re-matches #"\d+" %))
        nn-params (remove (comp numeric? params)
                          ["student-id" "college-id"])]
    (case (count nn-params)
      0 nil
      1 (str "Non-numeric parameter: " (first nn-params))
      (str "Non-numeric parameters: " (str/join ", " nn-params)))))

(defn- authorized? [db session params]
  (when-let [{:keys [db/id account/type]} session]
    (let [counselor-only-field? #{"early-decision?"
                                  "interest-demonstrated"
                                  "counselor-notes"}]
      (cond
        (#{:account-type/superuser :account-type/counselor} type) true
        (not= type :account-type/student) false
        (counselor-only-field? (get params "field")) false
        :else (when-let [student (student/find-by-account-id
                                   db (Long/parseLong id))]
                (= (str (:db/id student))
                   (get params "student-id")))))))

(defn- invalid-field-error [params]
  (let [valid-field-name? #{"visited?"
                            "early-decision?"
                            "location"
                            "setting"
                            "app-round"
                            "app-deadline"
                            "app-submitted?"
                            "program-major"
                            "interest-demonstrated"
                            "counselor-notes"
                            "student-notes"
                            "rd-admit-rate"
                            "ed-admit-rate"
                            "hs-admit-rate"
                            "hs-avg-sat"
                            "hs-avg-act"
                            "hs-avg-gpa"
                            "testing-requirements"
                            "scholarship-info"
                            "scholarship-deadlines"
                            "admissions-contacts"
                            "portfolio-requirements"
                            "portfolio-status"
                            "financial-aid-forms-required"
                            "financial-aid-deadline"
                            "first-outcome"
                            "final-outcome"
                            "scholarship?"
                            "scholarship-amount"
                            "scholarship-name"
                            "matriculation?"}
        field-name (get params "field")]
    (when-not (valid-field-name? field-name)
      "Invalid field")))

(defn- find-entry [db student-id college-id]
  (ffirst
    (d/q {:query '{:find [?entry]
                   :in [$ ?student ?college]
                   :where [[?student :student/college-list-entries ?entry]
                           [?entry :college-list-entry/college ?college]]}
          :args [db student-id college-id]})))

(def ^:private boolean-field? #{"visited?"
                                "early-decision?"
                                "app-submitted?"
                                "scholarship?"
                                "matriculation?"})

(def ^:private instant-field? #{"app-deadline"})

(defn- invalid-value-error
  [{:strs [field value]}]
  (let [boolean? #{"true" "false"}
        iso8601-re #"\d{4}-\d{2}-\d{2}"
        instant? #(re-matches iso8601-re %)]
    (cond
      (boolean-field? field) (when-not (boolean? value) "Invalid boolean value")
      (instant-field? field) (when-not (instant? value)
                               "Invalid instant value"))))

(defn- happy-path [conn entry-id params]
  (let [{:strs [field value]} params
        attr (keyword "college-list-entry" field)
        val (cond
              (boolean-field? field) (read-string value)
              (instant-field? field) (instant/read-instant-date (str value "Z"))
              :else value)]
    (d/transact conn {:tx-data [[:db/add entry-id attr val]
                                [:db/add "datomic.tx" :transaction/namespace
                                 "com.premiumprep.app.handler.update-college-list-entry-field"]]})
    {:status 204, :headers {}}))

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-let [err-msg (missing-required-params-error multipart-params)]
    (error-response 400 err-msg)
    (if-let [err-msg (nonnumeric-params-error multipart-params)]
      (error-response 400 err-msg)
      (let [db (d/db conn)]
        (if-not (authorized? db session multipart-params)
          (error-response 403 "unauthorized")
          (if-let [err-msg (invalid-field-error multipart-params)]
            (error-response 400 err-msg)
            (let [parse #(Long/parseLong (get multipart-params %))
                  student-id (parse "student-id")
                  college-id (parse "college-id")]
              (if-let [entry-id (find-entry db student-id college-id)]
                (if-let [err-msg (invalid-value-error multipart-params)]
                  (error-response 400 err-msg)
                  (happy-path conn entry-id multipart-params))
                (error-response 404 "college list entry not found")))))))))
