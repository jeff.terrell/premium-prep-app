(ns com.premiumprep.app.handler.change-email
  (:require [com.premiumprep.app.email :as email]
            [com.premiumprep.app.model.account :as account]
            [crypto.random :refer [url-part]]
            [datomic.client.api :as d]))

(def ^:private ok-response
  {:status 204, :headers {}})

(def ^:private conflict-response
  {:status 409
   :headers {}
   :body (str "Error: that email address is already taken.\n\n"
              "If you forgot your password, you can reset it.\n")})

(defn- conflict? [db email]
  (boolean (account/find-by-email db email)))

(defn- re-send-verification-email [db email-send-fn email]
  (let [account (account/find-by-email db email)
        existing-token (:account/token account)]
    (email/verify-email email-send-fn email existing-token)
    ok-response))

(defn- change-email [conn email-send-fn old-email new-email token]
  (let [account-id (:db/id (account/find-by-email (d/db conn) old-email))
        tx-data [[:db/retract account-id :account/email old-email]
                 [:db/add account-id :account/email new-email]
                 [:db/add account-id :account/state
                  :account-state/email-unverified]
                 [:db/add account-id :account/token token]
                 [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.change-email"]]]
    (d/transact conn {:tx-data tx-data})
    (email/change-email email-send-fn old-email new-email token)
    ok-response))

(defn handler
  [{:keys [datomic/conn email-send-fn multipart-params session]}]
  (if-not session
    {:status 403, :headers {}}
    (let [old-email (:account/email session)
          new-email (multipart-params "email")
          db (d/db conn)]
      (if (= old-email new-email)
        (re-send-verification-email db email-send-fn old-email)
        (if (conflict? db new-email)
          conflict-response
          (change-email
            conn email-send-fn old-email new-email (url-part 64)))))))
