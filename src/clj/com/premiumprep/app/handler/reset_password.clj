(ns com.premiumprep.app.handler.reset-password
  (:require [clojure.string :as str]
            [com.premiumprep.app.model.account :as account]
            [datomic.client.api :as d]))

(defn- save-password-return-success [conn account-eid password]
  (account/save-password conn account-eid password)
  {:headers {}, :status 204})

(defn- uninitialized-account-error []
  {:status 403
   :headers {"content-type" "text/plain"}
   :body "Error: you cannot reset a password for an uninitialized account.\nPlease find the email from us with a link for setting up your account.\n"})

(defn- mismatched-passwords []
  {:status 400
   :headers {"content-type" "text/plain"}
   :body "Error: the given passwords do not match. Please try again.\n"})

(defn- token-not-found []
  {:status 404
   :headers {"content-type" "text/plain"}
   :body (str "Error: the given token could not be found in the database.\n\n"
              "Please ensure that you are at the address included in your"
              " password reset email."
              " If so, you may need to send a new password reset email.\n")})

(defn handler
  [{:keys [datomic/conn multipart-params]}]
  (let [{:strs [token password confirmation]} multipart-params]
    (if-let [account (account/find-by-token (d/db conn) token)]
      (if (= :account-state/uninitialized (-> account :account/state :db/ident))
        (uninitialized-account-error)
        (if (and (not (str/blank? password)) (= password confirmation))
          (save-password-return-success conn (:db/id account) password)
          (mismatched-passwords)))
      (token-not-found))))
