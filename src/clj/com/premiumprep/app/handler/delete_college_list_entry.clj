(ns com.premiumprep.app.handler.delete-college-list-entry
  (:require [clojure.set :as set]
            [clojure.string :as str]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response
                                              maybe-remove-eliminated-entries
                                              student-result]]
            [datomic.client.api :as d]))

(defn- missing-required-params-error [params]
  (let [required #{"student-id" "college-id"}
        missing (set/difference required (-> params keys set))]
    (case (count missing)
      0 nil
      1 (str "Missing parameter: " (first missing))
      (str "Missing parameters: " (str/join ", " (sort missing))))))

(defn- nonnumeric-params-error [params]
  (let [numeric? #(and % (re-matches #"\d+" %))
        nn-params (remove (comp numeric? params)
                          ["student-id" "college-id"])]
    (case (count nn-params)
      0 nil
      1 (str "Non-numeric parameter: " (first nn-params))
      (str "Non-numeric parameters: " (str/join ", " nn-params)))))

(defn- authorized? [session]
  (boolean
    (#{:account-type/superuser :account-type/counselor}
      (:account/type session))))

(defn- find-entry [db student-id college-id]
  (let [query '{:find [?entry]
                :in [$ ?student ?college]
                :where [[?student :student/college-list-entries ?entry]
                        [?entry :college-list-entry/college ?college]]}
        result (d/q {:query query, :args [db student-id college-id]})]
    (ffirst result)))

(defn- happy-path [atype conn student-id entry-id]
  (let [tx-data [[:db/add entry-id :college-list-entry/eliminated? true]
                 [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.handler.delete-college-list-entry"]]]
    (d/transact conn {:tx-data tx-data})
    {:status 200
     :body (->> student-id (student/find (d/db conn))
                (maybe-remove-eliminated-entries atype)
                student/listify-college-list-entries student-result
                :student/college-lists)}))

(defn handler
  [{:keys [datomic/conn multipart-params session]}]
  (if-let [err-msg (missing-required-params-error multipart-params)]
    (error-response 400 err-msg)
    (if-let [err-msg (nonnumeric-params-error multipart-params)]
      (error-response 400 err-msg)
      (let [db (d/db conn)]
        (if-not (authorized? session)
          (error-response 403 "unauthorized")
          (let [parse #(Long/parseLong (get multipart-params %))
                student-id (parse "student-id")
                college-id (parse "college-id")]
            (if-let [entry-id (find-entry db student-id college-id)]
              (happy-path (:account/type session) conn student-id entry-id)
              (error-response 404 "college list entry not found"))))))))
