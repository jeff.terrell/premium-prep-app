(ns com.premiumprep.app.handler.update-student-profile
  (:require [com.premiumprep.app.model.account :as account]
            [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.model.student-fields :refer [fields]]
            [com.premiumprep.app.util :refer [error-response
                                              maybe-remove-eliminated-entries
                                              student-result]]
            [com.premiumprep.app.validation :refer [fields-tx-data
                                                    validate-params]]
            [datomic.client.api :as d]))

(defn- bad-student-id [db student-eid]
  (try
    (when-not (student/find db (Long/parseLong student-eid))
      "Error: student not found.")
    (catch NumberFormatException e
      "Error: student not found.")))

(defn- error-messages
  "If there are any errors with the given params, return a vector of error
  messages; otherwise, return nil."
  [db params]
  (if-let [bad-student-msg (bad-student-id db (get params "student-id"))]
    bad-student-msg
    (validate-params fields params)))

(defn- params->tx-data
  "Convert the given params (with string keys) into a tx-data sequence suitable
  for calling d/transact. If successful, return a vector of true and the tx-data
  sequence. If any validation check fails, instead return a vector of false and
  a sequence of error message strings (one for each error) suitable for display
  to the end user."
  [db params]
  (if-let [err-msg (error-messages db params)]
    [false err-msg]
    [true (let [student-eid (Long/parseLong (get params "student-id"))]
            (conj (fields-tx-data db student-eid fields params)
                  [:db/add "datomic.tx" :transaction/namespace
                   "com.premiumprep.app.handler.update-student-profile"]))]))

(defn success-response [atype conn tx-data account-type student-id]
  (d/transact conn {:tx-data tx-data})
  (let [info (->> student-id
                  (student/find (d/db conn))
                  (maybe-remove-eliminated-entries atype)
                  student/listify-college-list-entries
                  student-result)
        issues? (#{:account-type/superuser :account-type/counselor} account-type)
        info (if issues? info (dissoc info :issue/_student))]
    {:status 200
     :body info}))

(defn handler
  [{:keys [datomic/conn multipart-params reitit.core/match session]}]
  (let [[ok? result] (params->tx-data (d/db conn) multipart-params)
        atype (:account/type session)]
    (if ok?
      (success-response atype conn result (:account/type session)
                        (-> match :path-params :id Long/parseLong))
      (error-response 400 result))))
