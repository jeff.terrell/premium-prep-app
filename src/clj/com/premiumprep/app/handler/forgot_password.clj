(ns com.premiumprep.app.handler.forgot-password
  (:require [com.premiumprep.app.email :as email]
            [com.premiumprep.app.model.account :as account]
            [com.premiumprep.app.model.student :as student]
            [crypto.random :refer [url-part]]
            [datomic.client.api :as d]))

(def ok-response {:headers {}, :status 204})

(def uninitialized-account-response
  {:status 403
   :headers {}
   :body (str "Error: you cannot reset the password of an uninitialized account."
              " Please check your email for the initial welcome email, and click"
              " the link inside it. Then you will be able to set a password.")})

(defn handler
  [{:keys [datomic/conn email-send-fn multipart-params]}]
  (let [{:strs [email]} multipart-params
        student (student/find-by-email (d/db conn) email)]
    (if-not student
      ok-response
      (if (= :account-state/uninitialized
             (-> student :student/account :account/state :db/ident))
        uninitialized-account-response
        (let [account-eid (-> student :student/account :db/id)
              token (url-part 64)]
          (account/save-token conn account-eid token)
          (email/forgot-password email-send-fn email token)
          ok-response)))))
