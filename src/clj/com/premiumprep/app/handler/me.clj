(ns com.premiumprep.app.handler.me
  (:require [com.premiumprep.app.model.student :as student]
            [com.premiumprep.app.util :refer [error-response
                                              maybe-remove-eliminated-entries
                                              student-result]]
            [datomic.client.api :as d]))

(defn authorized? [session]
  (when (and session (map? session))
    (let [{:keys [account/type account/state]} session]
      (and (= :account-type/student type)
           (not= :account-state/email-unverified state)))))

(defn handler
  [{:keys [datomic/conn session]}]
  (if-not (authorized? session)
    (error-response 403 "forbidden")
    (let [{:keys [db/id]} session
          id (Long/parseLong id)
          atype (:account/type session)
          mree #(maybe-remove-eliminated-entries %2 %1)
          info (-> (student/find-by-account-id (d/db conn) id)
                   (mree atype)
                   student/listify-college-list-entries
                   student-result
                   (dissoc :issue/_student))]
      {:status 200
       :body info})))
