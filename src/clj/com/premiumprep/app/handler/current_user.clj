(ns com.premiumprep.app.handler.current-user
  (:require [datomic.client.api :as d]))

(defn handler
  [{:keys [session]}]
  (if (and session (not-empty session))
    {:status 200, :body session}
    {:status 404}))
