(ns com.premiumprep.app.import
  "This namespace is supporting import of college data into the database.

  For details, including a link to the spreadsheet used for import, see
  https://trello.com/c/ImG5mJzU/70-70-8-import-college-info-from-spreadsheet

  The goal here is not necessarily a fully automated script, but an approach
  that works in an interactive REPL."

  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [datomic.client.api :as d]
            [com.premiumprep.app.schema :as schema])
  (:import java.io.File))

(def fields
  [{:n 0, :label :college/name}
   {:n 2, :label :college/enrollment}
   {:n 3, :label :college/admit-rate}
   {:n 8, :label :college/average-sat}
   {:n 9, :label :college/average-act}])

(defn process-field-value [vals field]
  (let [{:keys [n label]} field
        val (try (nth vals n) (catch IndexOutOfBoundsException _ ""))]
    (cond
      (str/blank? val) [nil {:error :missing, :label label, :value val}]
      :else [[label val] nil])))

(comment
  (process-field-value ["" "about 1,976"] (second fields))
  )

(defn line->record [line]
  (let [vals (str/split line #"\t")
        processed-vals (map (partial process-field-value vals) fields)
        errors (keep second processed-vals)
        label+val-pairs (keep first processed-vals)
        {:keys [college/name] :as to-return} (into {} label+val-pairs)
        err-str {:missing "WARN_MISSING_VALUE", :invalid "WARN_INVALID_VALUE"}]
    [to-return
     (vec (for [{:keys [error label value]} errors]
            [(err-str error) name label value]))]))

(comment
  (line->record "Agnes Scott College\tabout 1,976\t\t\t\t70%\t\t1090-1320\t23-29\t\t\t\t\t\t")
  (line->record "Agnes Scott College\tabout 1,976\t\t\t\t75%\t\t1090\t23\t\t\t\t\t\t")
  (line->record "Agnes Scott College\tenrollment\t\t\t\tmore info\t\tsomething\tinvalid\t\t\t\t\t\t")
  (line->record "Bard College\t1900\t\t\t\t\t\t\t\t\t\t\t\t\t")
  )

(defn read-data [infile]
  (assert (= File (type infile)))
  (assert (.isFile infile))
  (with-open [reader (io/reader infile)]
    (let [lines (line-seq reader)
          col-names (str/split (first lines) #"\t")]
      (assert (= "Name" (first col-names)))
      (assert (= "Undergraduate Enrollment" (nth col-names 2)))
      (assert (= "2019 Admit Rate" (nth col-names 3)))
      (assert (= "2019 Mid 50% SAT" (nth col-names 8)))
      (assert (= "2019 Mid 50% ACT" (nth col-names 9)))
      (let [val+err-pairs (mapv line->record (rest lines))]
        (with-meta (mapv first val+err-pairs)
                   {:errors (->> val+err-pairs
                                 (mapcat second)
                                 vec)})))))

(defn print-errors [errors]
  (doseq [error (sort-by first errors)]
    (println (str/join "\t" (update error 2 name)))))

(defn read-and-transact-colleges [conn]
  ;; First, visit the "College Info Master List" spreadsheet and download a TSV
  ;; of the data. Rename that file 'colleges.tsv' and move it to the
  ;; `resources/` directory in the repo.
  (let [data (-> "colleges.tsv" io/resource io/file read-data)
        _ (-> data meta :errors print-errors)
        add-tempid (fn [i college] (assoc college :db/id (str "college" i)))
        tx-data (conj (vec (map-indexed add-tempid data))
                      [:db/add "datomic.tx" :transaction/namespace
                       "com.premiumprep.app.import"])]
    (assert (distinct? (map :college/name data)))
    (d/transact conn {:tx-data tx-data})))

(comment ; test locally
  (require '[datomic.client.api :as d])
  (require '[com.premiumprep.app.schema :as schema] :reload)
  (let [client (d/client {:server-type :dev-local
                          :storage-dir :mem
                          :system "local"})
        db-name "import-test"
        _ (d/create-database client {:db-name db-name})
        conn (d/connect client {:db-name db-name})]
    (schema/install-schema! conn)
    (time (do (read-and-transact-colleges conn) nil))
    (let [results (d/q {:query '{:find [?college-name]
                                 :where [[_ :college/name ?college-name]]}
                        :args [(d/db conn)]})]
      (prn 'first10 (take 10 results))
      (prn 'n (count results))))
  )

(comment ; import to production
  ;; assumes `datomic -p pp client access pp-app` proxy is running
  (require '[datomic.client.api :as d] :reload)
  (def cfg {:server-type :ion
            :region "us-east-1"
            :system "pp-app"
            ;:creds-profile "pp" ; Provide AWS profile if you are not using the default
            :endpoint "http://entry.pp-app.us-east-1.datomic.net:8182/"
            :proxy-port 8182})
  (def client (d/client cfg))
  (def conn (d/connect client {:db-name "pp-app"}))
  (require '[com.premiumprep.app.schema :as schema])
  (schema/install-schema! conn)
  (time (do (read-and-transact-colleges conn) nil))
  (let [results (d/q {:query '{:find [?college-name]
                               :where [[_ :college/name ?college-name]]}
                      :args [(d/db conn)]})]
    (prn 'first10 (take 10 results))
    (prn 'n (count results)))
  )
