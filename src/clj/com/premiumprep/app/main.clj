(ns com.premiumprep.app.main
  (:require (com.premiumprep.app
              [config :as config]
              [system :as system]))
  (:gen-class))

(defn -main [& args]
  (system/start-system config/system-config))
