(ns com.premiumprep.app.system
  "Start a 'system' of related (and maybe stateful, running) components."
  (:require
   [clojure.spec.alpha :as s]
   (com.premiumprep.app
     [fixtures :as fixtures]
     [schema :as schema]
     [server :as server])
   [datomic.client.api :as d]
   [integrant.core :as ig]))

;;; Specify required arguments to system components and their types with
;;; `ig/pre-init-spec`:

(s/def ::name string?)
(s/def ::email string?)
(s/def ::password string?)
(s/def ::db-name string?)
(s/def ::use-fixtures (s/nilable boolean?))
(s/def ::superuser (s/keys :req-un [::name ::email ::password]))

(defmethod ig/pre-init-spec :datomic/conn [_]
  (s/keys :req-un [::db-name]
          :opt-un [::superuser ::use-fixtures]))

(defmethod ig/pre-init-spec :email/send-fn [_] ifn?)

(s/def :storage/type #{:storage-type/local
                       :storage-type/s3})
(s/def :storage/path string?)
(defmethod ig/pre-init-spec :documents/config [_]
  (s/keys :req [:storage/type :storage/path]))

(s/def ::allowed-origin string?)
(s/def ::handler fn?)
(s/def ::log-level #{:none :oneline :body})
(s/def ::port number?)
(s/def ::frontend-env-name #{:prod :dev :test})

(defmethod ig/pre-init-spec :server/http [_]
  (s/keys :req-un [::allowed-origin ::handler]
          :opt-un [::log-level ::port ::frontend-dev-name]))

;;; Provide default arguments for system components with `ig/prep-key` (which
;;; requires calling `ig/prep` before `ig/init`):

(defmethod ig/prep-key :server/http [_ config]
  (merge {:port 3000} config))

;;; Start system components using `ig/init-key`:

(defmethod ig/init-key :datomic/client [_ config]
  (let [local? (:local? config)]
    (d/client
      (if local?
        {:server-type :dev-local
         :storage-dir :mem
         :system "local"}
        {:server-type :cloud
         :region "us-east-1"
         :system "pp-app"
         :endpoint "http://entry.pp-app.us-east-1.datomic.net:8182/"}))))

(defmethod ig/init-key :datomic/conn [_ config]
  (let [{:keys [client db-name superuser use-fixtures]} config
        {:keys [name email password]} superuser]
    (d/create-database client {:db-name db-name})
    (let [conn (d/connect client {:db-name db-name})]
      (schema/install-schema! conn)
      ;; So dev will get a superuser installed automatically. Prod won't.
      (when (and superuser name)
        (schema/install-superuser! conn name email password))
      (when use-fixtures (fixtures/install-fixtures! conn))
      conn)))

(defmethod ig/init-key :documents/config [_ config] config)
(defmethod ig/init-key :email/send-fn [_ config] config)

(defmethod ig/init-key :server/http [_ config]
  (server/start-server config))

;;; Halt system components, if needed, using `ig/halt-key!`:

(defmethod ig/halt-key! :server/http [_ server]
  (server/stop-server server))

(defn start-system
  "Given a system config, start the system."
  [system-config]
  (let [port (-> system-config :server/http :port)
        system (-> system-config ig/prep ig/init)]
    (println "System started.")
    (when port
      (println "HTTP server listening on port" port))
    system))

(defn stop-system
  "Stop a running system returned from `start-system`."
  [system]
  (ig/halt! system))

(comment ;; Update schema in production
  ;; Assumes that datomic CLI tools are installed
  ;; and that a `datomic client access` process is running.
  (require '[datomic.client.api :as d] :reload)
  (def cfg {:server-type :ion
            :region "us-east-1"
            :system "pp-app"
            :creds-profile "pp"
            :endpoint "http://entry.pp-app.us-east-1.datomic.net:8182/"
            :proxy-port 8182})
  (def client (d/client cfg))
  ;; (d/delete-database client {:db-name "pp-app"})
  ;; (d/create-database client {:db-name "pp-app"})
  (def conn (d/connect client {:db-name "pp-app"}))
  (require '[com.premiumprep.app.schema :as schema] :reload-all)
  (require '[com.premiumprep.app.fixtures :as fixtures] :reload-all)
  (schema/install-schema! conn)
  (->> (d/q {:query '{:find [?identifier]
                      :where [[_ :db/ident ?identifier]]}
             :args [(d/db conn)]})
       (map first)
       sort)
  ;; (schema/install-superuser! conn name email password)
  ;; (fixtures/install-fixtures! conn)
  ;; then switch over to import namespace with a comment for importing data
  )
