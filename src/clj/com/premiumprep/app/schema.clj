(ns com.premiumprep.app.schema
  (:require [clojurewerkz.scrypt.core :as sc]
            [datomic.client.api :as d]))

(defn- attr
  ([ident type cardinality] (attr ident type cardinality nil))
  ([ident type cardinality doc] (attr ident type cardinality doc nil))
  ([ident type cardinality doc more]
   (let [type (keyword "db.type" (name type))
         cardinality (keyword "db.cardinality" (name cardinality))
         more (if doc (assoc more :db/doc doc) more)]
     (merge {:db/ident ident
             :db/valueType type
             :db/cardinality cardinality}
            more))))

(defn- component
  "A component attribute"
  ([ident cardinality]
   (dissoc (attr ident :ref cardinality "" {:db/isComponent true}) :db/doc))
  ([ident cardinality doc]
   (attr ident :ref cardinality doc {:db/isComponent true})))

(def session-schema
  [(attr :session/id :uuid :one "A random (unguessable) UUID session identifier")
   (attr :session/account :ref :one "The account for a session")])

(def account-schema
  [(attr :account/email :string :one "The email address of an account"
         {:db/unique :db.unique/identity
          ;; FIXME: this should work in production but fails in dev
          ;; :db.attr/preds 'com.premiumprep.app.schema/email?
          })
   (attr :account/password :string :one "The encrypted password of an account; not present for unverified accounts")
   (attr :account/full-name :string :one "The full name of the person with the account")
   (attr :account/preferred-name :string :one "The preferred name of the person with the account")
   (attr :account/mobile :string :one "The mobile phone # of the person with the account")
   (attr :account/work-phone :string :one "The work phone # of the person with the account")
   (attr :account/home-phone :string :one "The home phone # of the person with the account")
   (attr :account/token :string :one "An unguessable token, used for account verification and password resets"
         {:db/unique :db.unique/identity})
   (attr :account/address-street :string :one "The street address of the person with the account")
   (attr :account/address-city :string :one "The city of the person with the account")
   (attr :account/address-state :string :one "The state of the person with the account")
   (attr :account/address-zip :long :one "The zip code of the person with the account")
   (attr :account/country :string :one "The country of the person with the account")

   (attr :account/active? :boolean :one "Whether the account is active or deactivated")
   (attr :account/state :ref :one "What state is the account in?")
   {:db/ident :account-state/uninitialized}
   {:db/ident :account-state/email-unverified}
   {:db/ident :account-state/initialized}

   (attr :account/type :ref :one "What kind of account is this?")
   {:db/ident :account-type/superuser}
   {:db/ident :account-type/counselor}
   {:db/ident :account-type/parent}
   {:db/ident :account-type/student}])

(def counselor-schema
  [(attr :counselor/account :ref :one "The account entity of a counselor")])

(def student-schema
  [(attr :student/account :ref :one "The account entity of a student")
   (attr :student/counselor :ref :one "The counselor entity for a student")
   (attr :student/joined-at :instant :one "When the student created an account")
   (attr :student/graduation-year :long :one "When the student plans to graduate high school")
   (attr :student/high-school :string :one "The high school the student attends")
   (attr :student/current-college :string :one "The current college the student attends (for transfer students)")
   (attr :student/parent1 :ref :one "The account entity for the first parent of the student")
   (attr :student/parent2 :ref :one "The account entity for the second parent of the student")
   (component :student/documents :many "The documents for a student, e.g. essays, timelines, transcripts, etc.")
   (component :student/college-list-entries :many "The college list entries for the student")
   (component :student/profile-comments :many "Comments attached to the student's profile.")
   (component :student/document-list-comments :many "Comments attached to the student's document list.")

   (attr :student/academic-interest :string :one "Academic interest or potential major")
   (attr :student/gpa-weighted-9 :float :one)
   (attr :student/gpa-unweighted-9 :float :one)
   (attr :student/gpa-weighted-9-10 :float :one)
   (attr :student/gpa-unweighted-9-10 :float :one)
   (attr :student/gpa-weighted-9-11 :float :one)
   (attr :student/gpa-unweighted-9-11 :float :one)
   (attr :student/class-rank-9 :long :one)
   (attr :student/class-rank-9-10 :long :one)
   (attr :student/class-rank-9-11 :long :one)
   (attr :student/class-rank-9-of :long :one)
   (attr :student/class-rank-9-10-of :long :one)
   (attr :student/class-rank-9-11-of :long :one)
   (component :student/psat :one)
   (component :student/pact :one)
   (component :student/sat-superscore :one)
   (component :student/sat1 :one)
   (component :student/sat2 :one)
   (component :student/sat3 :one)
   (component :student/sat4 :one)
   (component :student/act-superscore :one)
   (component :student/act1 :one)
   (component :student/act2 :one)
   (component :student/act3 :one)
   (component :student/act4 :one)
   (attr :student/sat-subject-tests :string :one)
   (attr :student/ap-tests :string :one)
   (component :student/college-planning-app-info :one)
   (attr :student/extracurricular-activities :string :one)
   (attr :student/current-courses :string :one)
   (attr :student/prospective-courses :string :one)])

(def sat
  [(attr :sat/total-score :long :one)
   (attr :sat/ebrw-score :long :one)
   (attr :sat/math-score :long :one)
   (attr :sat/date :instant :one)])

(def sat-super
  [(component :sat-superscore/sat :one)
   (attr :sat-superscore/ebrw-date :instant :one)
   (attr :sat-superscore/math-date :instant :one)])

(def act
  [(attr :act/composite-score :long :one)
   (attr :act/english-score :long :one)
   (attr :act/reading-score :long :one)
   (attr :act/math-score :long :one)
   (attr :act/science-score :long :one)
   (attr :act/date :instant :one)])

(def act-super
  [(component :act-superscore/act :one)
   (attr :act-superscore/english-date :instant :one)
   (attr :act-superscore/reading-date :instant :one)
   (attr :act-superscore/math-date :instant :one)
   (attr :act-superscore/science-date :instant :one)])

(def college-planning-app
  [(attr :college-planning-app/type :ref :one)
   (attr :college-planning-app/username :string :one)
   (attr :college-planning-app/password :string :one)
   (attr :college-planning-app/hs-login-page-url :uri :one)
   {:db/ident :college-planning-app-type/naviance}
   {:db/ident :college-planning-app-type/scoir}
   {:db/ident :college-planning-app-type/maia}
   {:db/ident :college-planning-app-type/kickstart}])

(def document-schema
  [(attr :document/name :string :one "The user-specified name of the document")
   (attr :document/content-type :string :one "The content-type of the uploaded document")
   (attr :document/uploaded-at :instant :one "When the document was uploaded")
   (attr :document/link :uri :one "The path to the document data in an external location")
   (attr :document/s3-path :string :one "The path to the document data within the configured S3 document bucket"
         {:db/unique :db.unique/identity})])

(def college-schema
  [(attr :college/name :string :one "The name of the college"
         {:db/unique :db.unique/identity})
   (attr :college/enrollment :string :one "The enrollment of the college, as entered")
   (attr :college/admit-rate :string :one "The admission rate of the college, as entered")
   (attr :college/average-sat :string :one "The average SAT of the college, as entered; might be a mid-50% range like 1030-1320 or the mean like 1150")
   (attr :college/average-act :string :one "The average ACT of the college, as entered; might be a mid-50% range like 30-34 or the mean like 32")])

(def college-list-entries-schema
  [(attr :college-list-entry/category :ref :one "The list that this entry is in")
   {:db/ident :college-list-category/high-reach}
   {:db/ident :college-list-category/reach}
   {:db/ident :college-list-category/target}
   {:db/ident :college-list-category/likely}
   {:db/ident :college-list-category/uncategorized}
   (attr :college-list-entry/index :long :one "The 0-based index of this entry within its list")
   (attr :college-list-entry/college :ref :one "The college entity for this entry")
   (attr :college-list-entry/visited? :boolean :one "Whether the student has visited the college")
   (attr :college-list-entry/date-added :instant :one "When the college was added to the student")
   (attr :college-list-entry/early-decision? :boolean :one "Whether the student has applied via the early decision mechanism")
   (attr :college-list-entry/eliminated? :boolean :one)

   ;; detailed info, only visible in the expanded view for the entry
   (attr :college-list-entry/location :string :one "Where the college is located")
   (attr :college-list-entry/setting :string :one "The setting of the college")
   (attr :college-list-entry/app-round :string :one "The application round of the student's application")
   (attr :college-list-entry/app-deadline :instant :one "The application deadline for the college")
   (attr :college-list-entry/app-submitted? :boolean :one "Whether the student has submitted an application for this college")
   (attr :college-list-entry/program-major :string :one "The program or major the student plans to apply to or mention in their application")
   (attr :college-list-entry/interest-demonstrated :string :one "How the student has demonstrated interest in this college")
   (attr :college-list-entry/counselor-notes :string :one "Notes about this entry that only the counselor can see")
   (attr :college-list-entry/student-notes :string :one "Student notes about this entry")
   (attr :college-list-entry/rd-admit-rate :string :one "The regular decision (i.e. non-ED) admit rate, presumably as a percent")
   (attr :college-list-entry/ed-admit-rate :string :one "The early decision (ED) admit rate, presumably as a percent")
   (attr :college-list-entry/hs-admit-rate :string :one) ;; not sure what this means actually, and ditto for the other HS fields directly below
   (attr :college-list-entry/hs-avg-sat :string :one)
   (attr :college-list-entry/hs-avg-act :string :one)
   (attr :college-list-entry/hs-avg-gpa :string :one)
   (attr :college-list-entry/testing-requirements :string :one) ;; at this point I'd only be guessing about what these mean
   (attr :college-list-entry/scholarship-info :string :one)
   (attr :college-list-entry/scholarship-deadlines :string :one)
   (attr :college-list-entry/admissions-contacts :string :one)
   (attr :college-list-entry/portfolio-requirements :string :one)
   (attr :college-list-entry/portfolio-status :string :one)
   (attr :college-list-entry/financial-aid-forms-required :string :one)
   (attr :college-list-entry/financial-aid-deadline :string :one)
   (attr :college-list-entry/first-outcome :string :one)
   (attr :college-list-entry/final-outcome :string :one)
   (attr :college-list-entry/scholarship? :boolean :one)
   (attr :college-list-entry/scholarship-amount :string :one)
   (attr :college-list-entry/scholarship-name :string :one)
   (attr :college-list-entry/matriculation? :boolean :one)
   (component :college-list-entry/custom-fields :many)])

(def custom-field-schema
  [(attr :custom-field/name :string :one)
   (attr :custom-field/value :string :one)])

(def comment-schema
  [(attr :comment/commenter :ref :one "The account entity of the commenter")
   (attr :comment/created-at :instant :one "When the comment was created")
   (attr :comment/text :string :one "The comment text, newlines and all.")])

(def issue-schema
  [(attr :issue/created-at :instant :one "When did a superuser create this issue?")
   (attr :issue/student :ref :one "The student entity the issue refers to")
   (attr :issue/text :string :one "The administrator-defined freeform text of the issue")
   (attr :issue/action-taken :string :one "The defined action taken to resolve the issue or the discussion needed")
   (attr :issue/counselor-touched-at :instant :one "When did the counselor last touch this issue?")
   (attr :issue/state :ref :one "Whether the issue is new, read, needs-discussion, or counselor-resolved, or closed")
   {:db/ident :issue-state/new}
   {:db/ident :issue-state/read}
   {:db/ident :issue-state/needs-discussion}
   {:db/ident :issue-state/counselor-resolved}
   {:db/ident :issue-state/closed}])

(def schema
  (vec
    (concat session-schema
            account-schema
            counselor-schema
            student-schema
            document-schema
            college-schema
            college-list-entries-schema
            custom-field-schema
            comment-schema
            issue-schema
            sat sat-super
            act act-super
            college-planning-app
            [(attr :transaction/namespace :string :one "Which namespace initiated the transaction")])))

(defn install-schema! [conn]
  (d/transact conn {:tx-data schema}))

(defn install-superuser! [conn name email password]
  (assert (every? some? [name email password])
          "Did you remember to create a resources/config.edn map? (See README.)")
  (d/transact conn {:tx-data [{:account/email email
                               :account/password (sc/encrypt password 16384 8 1)
                               :account/full-name name
                               :account/active? true
                               :account/type :account-type/superuser
                               :account/state :account-state/initialized}
                              [:db/add "datomic.tx" :transaction/namespace
                               "com.premiumprep.app.schema"]]}))
