(ns com.premiumprep.app.util)

(defn error-response [status msg]
  {:status status
   :body {:error msg}})

(defn- flatten-enum
  [result enum-path]
  (update-in result enum-path :db/ident))

(defn flatten-account-enums
  ([result] (flatten-account-enums result []))
  ([result account-path]
   (-> result
       (flatten-enum (conj account-path :account/type))
       (flatten-enum (conj account-path :account/state)))))

(defn stringify-nested-ids [maps path-to-id]
  (vec
    (for [entity-map maps]
      (update-in entity-map path-to-id str))))

(defn- stringify-ids [maps]
  (stringify-nested-ids maps [:db/id]))

(defn- normalize-cpa-info [student]
  (letfn [(normalize-uri-hs-login-page-url [student]
            ;; URI objects don't convey via EDN, so stringify them.
            (if (and (contains? student :student/college-planning-app-info)
                     (contains? (:student/college-planning-app-info student)
                                :college-planning-app/hs-login-page-url))
              (update-in student
                         [:student/college-planning-app-info
                          :college-planning-app/hs-login-page-url]
                         str)
              student))

          (normalize-uri-document-uri [student]
            ;; URI objects don't convey via EDN, so stringify them.
            (if (contains? student :student/documents)
              (assoc student :student/documents (mapv #(if (:document/link %)
                                                         (update % :document/link str)
                                                         %)
                                                      (:student/documents student)))))

          (normalize-type [student]
            ;; The type can be nil, as an artifact of the nested pull pattern.
            (if (and (contains? student :student/college-planning-app-info)
                     (nil? (-> student :student/college-planning-app-info
                               :college-planning-app/type)))
              (update student :student/college-planning-app-info
                      dissoc :college-planning-app/type)
              student))

          (flatten-type [student]
            (if (and (contains? student :student/college-planning-app-info)
                     (contains? (:student/college-planning-app-info student)
                                :college-planning-app/type))
              (update-in student
                         [:student/college-planning-app-info
                          :college-planning-app/type]
                         :db/ident)
              student))]

    (-> student
        normalize-uri-hs-login-page-url
        normalize-uri-document-uri
        normalize-type
        flatten-type)))

(defn map-vals [xform-val hashmap]
  (into (empty hashmap)
        (for [[k v] hashmap]
          [k (xform-val v)])))

(defn- resultify-college-lists [student]
  (letfn [(stringify-id [entity-map]
            (update entity-map :db/id str))
          (stringify-college-id [entry]
            (update entry :college-list-entry/college stringify-id))
          (stringify-custom-field-ids [entry]
            (update entry :college-list-entry/custom-fields
                    #(mapv stringify-id %)))
          (xform-list [entries]
            (vec
              (for [entry entries]
                (-> entry
                    stringify-id
                    stringify-college-id
                    stringify-custom-field-ids))))]
    (update student :student/college-lists
            (partial map-vals xform-list))))

(defn- resultify-extra-info [student]
  (letfn [(if-key [m k f] (if (contains? m k) (update m k f) m))
          (dissoc-id [m] (dissoc m :db/id))
          (de-id-sub [m k] (if-key m k dissoc-id))
          (de-id-super [m k subk]
            (if-key m k
                    (fn [super-map]
                      (-> super-map
                          (dissoc :db/id)
                          (update subk dissoc :db/id)))))

          (de-id-all-subs [m]
            (let [subs [:student/psat :student/pact :student/sat-superscore
                        :student/sat1 :student/sat2 :student/sat3 :student/sat4
                        :student/act-superscore :student/act1 :student/act2
                        :student/act3 :student/act4 :student/parent1
                        :student/parent2]]
              (reduce de-id-sub m subs)))

          (strip-empty-ref [m k subk]
            (if (contains? m k)
              (if (nil? (get-in m [k subk]))
                (update m k dissoc subk)
                m)
              m))]

    (-> student
        (de-id-super :student/sat-superscore :sat-superscore/sat)
        (de-id-super :student/act-superscore :act-superscore/act)
        de-id-all-subs
        (strip-empty-ref :student/sat-superscore :sat-superscore/sat)
        (strip-empty-ref :student/act-superscore :act-superscore/act))))

(defn- floatify-gpa-fields [student]
  (letfn [(floatify [nil-or-double]
            (when nil-or-double
              (float nil-or-double)))
          (maybe-floatify [student field]
            (if (contains? student field)
              (update student field floatify)
              student))]
    (reduce maybe-floatify student
            [:student/gpa-weighted-9
             :student/gpa-unweighted-9
             :student/gpa-weighted-9-10
             :student/gpa-unweighted-9-10
             :student/gpa-weighted-9-11
             :student/gpa-unweighted-9-11])))

(defn student-result [student]
  (-> student
      (update :db/id str)
      (update-in [:student/account :db/id] str)
      (update-in [:student/counselor :db/id] str)
      (update-in [:student/counselor :counselor/account :db/id] str)
      (flatten-account-enums [:student/account])
      (flatten-account-enums [:student/counselor :counselor/account])
      (update :student/documents stringify-ids)
      resultify-college-lists
      (update :student/profile-comments stringify-ids)
      (update :student/document-list-comments stringify-ids)
      (update :student/profile-comments stringify-nested-ids
              [:comment/commenter :db/id])
      (update :student/document-list-comments stringify-nested-ids
              [:comment/commenter :db/id])
      (update :issue/_student stringify-ids)
      (floatify-gpa-fields)
      (resultify-extra-info)
      (normalize-cpa-info)))

(defn counselor-result [counselor]
  (-> counselor
      (update :db/id str)
      (update-in [:counselor/account :db/id] str)
      (update :student/_counselor
              (partial mapv #(update % :db/id str)))
      (flatten-account-enums [:counselor/account])))

(defn maybe-remove-eliminated-entries [atype student]
  (if (#{:account-type/superuser :account-type/counselor :account-type/student} atype)
    student
    (let [filter-entries (partial remove :college-list-entry/eliminated?)]
      (update student :student/college-list-entries filter-entries))))
