(ns com.premiumprep.app.email)

(defn- welcome-student-body [token]
  (str "Welcome to Premium Prep!\n\n"
       "We have set you up with an account on our web-based College Planning system. You and your counselor will use this system to organize your process, build your college list, and keep track of pertinent information and key documents.\n\n"
       "Please use the following link to complete the setup, create a password, and enter the required information: "
       ;; FIXME: should use route data to generate the path here
       "https://app.premiumprep.com/signup?token=" token "\n\n"
       "Thanks so much,\n"
       "The Premium Prep Team\n"))

(defn welcome-student [send-email email token]
  (let [subject "Welcome to Premium Prep"]
    (send-email email subject (welcome-student-body token))))

(defn- forgot-password-body [token]
  (str "Somebody initiated a password reset on the Premium Prep app for this email address.\n\n"
       "If it wasn't you, please ignore this email.\n\n"
       "Otherwise, you can go here to reset your password:\n"
       "https://app.premiumprep.com/reset-password?token=" token "\n\n"
       "Thanks,\n"
       "The Premium Prep Team\n"))

(defn forgot-password [send-email email token]
  (send-email email "Reset your Premium Prep password"
              (forgot-password-body token)))

(defn- email-changed-body [new-email]
  (str "Somebody changed the email address associated with your Premium Prep account.\n\n"
       "It used to be this address, but now it is " new-email ".\n\n"
       "If you didn't do this, your account may be compromised. Reply to this email and ask for help.\n\n"
       "Otherwise, please check your other email account to find an email verification link.\n\n"
       "(If you made this change in mistake, you can log in with the same password and the email address above and change your email address back to this one.)\n\n"
       "Thanks,\n"
       "The Premium Prep Team\n"))

(defn- verify-email-body [token]
  (str "You either changed your Premium Prep email address to this one or else requested that we re-send this email.\n\n"
       "To continue using the Premium Prep web app, you need to verify that you own this email address.\n\n"
       "To do so, just visit this link:\n"
       "https://app.premiumprep.com/verify-email?token=" token "\n\n"
       "If you don't know what any of this means, somebody may have mistyped their email address. Feel free to reply to this email and let us know, and we'll take care of it.\n\n"
       "Thanks,\n"
       "The Premium Prep Team\n"))

(defn change-email [send-email old-email new-email token]
  (send-email old-email "Premium Prep email changed"
              (email-changed-body new-email))
  (send-email new-email "[Premium Prep] Verify this email address"
              (verify-email-body token)))

(defn verify-email [send-email email token]
  (send-email email "[Premium Prep] Verify this email address"
              (verify-email-body token)))

(defn- verified-email-body []
  (str "You have successfully verified this email address.\n\n"
       "You may now continue to use the Premium Prep app.\n\n"
       "Thanks,\n"
       "The Premium Prep Team\n"))

(defn verified-email [send-email email]
  (send-email email "[Premium Prep] Email address verified"
              (verified-email-body)))

(defn- new-issue-body [issue-text student-name student-path]
  (str "An administrator has created an issue for your student, " student-name
       ".\n\n"
       "Here's the issue text:\n\n"
       issue-text
       "\n\nSee details for " student-name " at\n"
       "https://app.premiumprep.com" student-path
       "\n\nThanks,\n"
       "The Premium Prep Team\n"))

(defn new-issue
  [send-email counselor-email issue-text student-name student-path]
  (send-email counselor-email "[Premium Prep] New issue created for your student"
              (new-issue-body issue-text student-name student-path)))

(defn- college-added-body
  [{:strs [name enrollment admit-rate average-sat average-act]}]
  (str "An administrator (maybe you) has created a new college.\n\n"
       "Remember to add a college with the same info to the master spreadsheet,"
       " so that later imports of college data will not drop the college.\n\n"
       "The college info follows:\n"
       "\n- Name: " name
       "\n- Enrollment: " enrollment
       "\n- Admit Rate: " admit-rate
       "\n- Average SAT: " average-sat
       "\n- Average ACT: " average-act))

(defn college-added [send-email college-info]
  (send-email "alevine@premiumprep.com"
              "[Premium Prep] New college added to database"
              (college-added-body college-info)))

(defn- comment-created-body [relation-type comment-map student comment-type]
  (let [{:keys [comment/text comment/commenter]} comment-map
        {:keys [account/full-name db/id]} commenter
        student-name (-> student :student/account :account/full-name)
        student-id (:db/id student)
        student-posted? (= id (-> student :student/account :db/id))
        noun (if (= comment-type :profile-comments)
               "profile"
               "document list")
        rel-str #(str "the " noun " of your " %1 ", " student-name)
        to-txt (cond
                 (= relation-type :student) (str "your " noun)
                 student-posted? (str "their " noun)
                 (= relation-type :counselor) (rel-str "student")
                 :else (rel-str "child"))]
    (str full-name " posted a comment to " to-txt ".\n"
         "You can view the comment on the Premium Prep web app here:\n"
         "https://app.premiumprep.com/student/" student-id "\n\n"
         "The comment text follows.\n\n"
         (:comment/text comment-map) "\n")))

(defn comment-created
  [send-email student comment-map recipients-by-type comment-type]
  (let [subject "[Premium Prep] New Comment Posted"]
    (doseq [[relation-type to-email] recipients-by-type]
      (let [body (comment-created-body relation-type comment-map student
                                       comment-type)]
        (send-email to-email subject body)))))
