(ns com.premiumprep.app.config
  (:require
   [com.premiumprep.app.routes :refer [base-app]]
   [config.core :refer [env]]
   [integrant.core :as ig]
   [postal.core :refer [send-message]]))

(def ^:private email-from-addr "notifications@premiumprep.com")

(defn ^:private make-email-send-fn []
  (let [email-type (env :email-type :email-type/stdout-print)]
    (case email-type
      :email-type/stdout-print (fn [to subject body]
                                 (prn 'EMAIL to subject body))
      :email-type/smtp (let [smtp-config {:user (env :smtp-username)
                                          :pass (env :smtp-password)
                                          :host (env :smtp-server)
                                          :port (env :smtp-port)}]
                         (fn [to subject body]
                           (send-message smtp-config
                                         {:from email-from-addr, :to to
                                          :subject subject, :body body})))
      (throw (new RuntimeException
                  (format "Invalid value for :email-type (%s). See README."
                          (pr-str email-type)))))))

(def ^:private superuser-config
  (let [[name email password] (map env [:superuser-name
                                        :superuser-email
                                        :superuser-password])]
    (when (or name email password)
      {:name name
       :email email
       :password password})))

(def ^:private conn-config
  (let [base {:client (ig/ref :datomic/client)
              :db-name "pp-app"
              :use-fixtures (env :use-fixtures true)}]
    (if superuser-config
      (assoc base :superuser superuser-config)
      base)))

(def system-config
  {:datomic/client {:local? (env :datomic-is-local true)}
   :datomic/conn conn-config
   :documents/config #:storage{:type (env :storage-type :storage-type/local)
                               :path (env :storage-path
                                          "/tmp/premiumprep-files/")}
   :email/send-fn (make-email-send-fn)
   :server/http {:allowed-origin (env :allowed-origin "http://localhost:3000")
                 :conn (ig/ref :datomic/conn)
                 :email-send-fn (ig/ref :email/send-fn)
                 :frontend-env-name (env :frontend-env-name :dev)
                 :handler base-app
                 :log-level (env :log-level :oneline)
                 :port (:port env 3000)
                 :secure-cookies? true
                 :storage (ig/ref :documents/config)}})
