(ns com.premiumprep.app.storage
  (:require [clojure.java.io :as io])
  (:import com.amazonaws.regions.Regions
           com.amazonaws.services.s3.AmazonS3ClientBuilder
           (com.amazonaws.services.s3.model
             GetObjectRequest ObjectMetadata PutObjectRequest S3Object)
           java.io.InputStream
           (java.nio.file Files Paths StandardCopyOption)))

(defn- ->Path [path]
  (let [empty-more-args (into-array String [])]
    (Paths/get (if (string? path) path (.getAbsolutePath path))
               empty-more-args)))

(defmulti store (fn [cfg _ _ _] (:storage/type cfg)))

(defmethod store :storage-type/local
  [cfg storage-file-name source-file _]
  (let [base-path (:storage/path cfg)
        opts (into-array StandardCopyOption
                         [StandardCopyOption/REPLACE_EXISTING])]
    (Files/copy (->Path source-file)
                (->Path (str base-path storage-file-name))
                opts)))

(defmethod store :storage-type/s3
  [cfg object-name file-name content-type]
  (let [bucket-name (:storage/path cfg)
        s3-client (AmazonS3ClientBuilder/defaultClient)
        metadata (doto (new ObjectMetadata) (.setContentType content-type))
        file (io/file file-name)
        request (doto (new PutObjectRequest bucket-name object-name file)
                  (.setMetadata metadata))]
    (.putObject s3-client request)))

(defmethod store :storage-type/none [_ _ _ _])

(defmulti retrieve (fn [cfg _] (:storage/type cfg)))

(defmethod retrieve :storage-type/local
  [cfg storage-file-name]
  (let [base-path (:storage/path cfg)]
    ;; Ring's StreamableResponseBody protocol is implemented for File objects.
    ;; Cf. https://github.com/ring-clojure/ring/blob/master/ring-core/src/ring/core/protocols.clj
    (io/file base-path storage-file-name)))

(defmethod retrieve :storage-type/s3
  [cfg object-name]
  (let [bucket-name (:storage/path cfg)
        s3-client (AmazonS3ClientBuilder/defaultClient)
        object-request (new GetObjectRequest bucket-name object-name)
        s3-object (.getObject s3-client object-request)]
    (.getObjectContent s3-object)))

(defmethod retrieve :storage-type/none [_ _])
