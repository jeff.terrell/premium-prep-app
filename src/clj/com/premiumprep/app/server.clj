(ns com.premiumprep.app.server
  (:require
   (com.premiumprep.app
     [config :refer [system-config]]
     [middleware :as mdw]
     [routes :refer [base-app]])
   [com.premiumprep.app.handler.index :as index]
   [ring.adapter.jetty :as jetty]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [ring.middleware.format :refer [wrap-restful-format]]))

(defn make-handler
  ([base-handler log-level conn]
   (let [allowed-origin (get-in system-config
                                [:server/http :allowed-origin])
         storage (get system-config :documents/storage)]
     (make-handler base-handler allowed-origin storage (constantly false) :prod
                   log-level conn true)))

  ([base-handler allowed-origin storage email-send-fn frontend-env-name
    log-level conn secure-cookies?]
   (let [ten-years (* 60 60 24 365 10)
         cookie-attrs {:http-only true ; disallow access from JS
                       :max-age ten-years ; use real cookies, not session cookies
                       :secure secure-cookies? ; HTTPS only (except localhost)
                       :same-site :lax}
         session-cfg {:store (mdw/->DatomicSessionStore conn)
                      :cookie-attrs cookie-attrs}
         defaults-cfg (-> site-defaults
                          (assoc-in [:security :anti-forgery] false)
                          (assoc :session session-cfg))]

     (-> base-handler
         (wrap-restful-format :formats [:transit-json])
         (wrap-defaults defaults-cfg)
         (mdw/wrap-inject :email-send-fn email-send-fn)
         (mdw/wrap-inject :frontend-env-name frontend-env-name)
         (mdw/wrap-inject :storage-config storage)
         (mdw/wrap-datomic-connection conn)
         mdw/wrap-catch
         (mdw/wrap-cors allowed-origin)
         (mdw/wrap-log log-level)
         mdw/wrap-time))))

(defn start-server
  [{:keys [allowed-origin conn storage email-send-fn
           frontend-env-name handler log-level port secure-cookies?] :as config}]
  (let [wrapped-handler (make-handler
                          handler allowed-origin storage email-send-fn
                          frontend-env-name log-level conn secure-cookies?)]
    (jetty/run-jetty wrapped-handler {:port port
                                      :join? false
                                      :configurator mdw/add-compression})))

(defn stop-server [server]
  (.stop server))
