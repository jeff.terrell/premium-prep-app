(ns com.premiumprep.app.middleware
  (:require
   [com.premiumprep.app.model.session :as session]
   [datomic.client.api :as d]
   [ring.middleware.session.store :refer [SessionStore read-session
                                          write-session delete-session]])
  (:import [java.util Date]
           [org.eclipse.jetty.server.handler.gzip GzipHandler]))

(defn wrap-inject [handler key value]
  (fn [request]
    (handler (assoc request key value))))

(deftype DatomicSessionStore [conn]
  SessionStore
  (read-session [_ id]
    (session/find (d/db conn) id))
  (write-session [_ _ account-eid]
    (session/create conn account-eid))
  (delete-session [_ account-eid]
    (session/delete conn account-eid)))

(defn wrap-datomic-connection
  "Ring middleware. Given a handler and a Datomic connection, add the connection
  to a request map under :datomic/conn before calling the handler, so that the
  handler can access the Datomic database given only the request map."
  [handler conn]
  (fn [request]
    (handler (assoc request :datomic/conn conn))))

(defn wrap-catch [handler]
  (fn [req]
    (try
      (handler req)
      (catch Throwable e
        {:status 500
         :headers {"Content-Type" "text/plain"}
         :body (pr-str {:error e, :request req})}))))

(defn wrap-cors
  [handler allowed-origin]
  (if-not allowed-origin
    handler
    (fn [request]
      (assoc-in (handler request)
                [:headers "Access-Control-Allow-Origin"]
                allowed-origin))))

(defmulti log-exchange (fn [log-level _ _] log-level))
(defmethod log-exchange :none [_ _ _] (constantly ""))

(defmethod log-exchange :oneline [_ request response]
  (println (format "%s %d %s"
                   (-> request :request-method name .toUpperCase)
                   (:status response)
                   (:uri request))))

(defmethod log-exchange :body [_ request response]
  (prn 'request
       ;; TODO: would be nice to use same fns as wrap-defaults to
       ;; parse multipart params so that they are visible here.
       (:request-method request)
       (:uri request))
  (println (format "  response %d %s %s"
                   (:status response)
                   (pr-str (get-in response [:headers "Content-Type"]))
                   (pr-str (:body response)))))

(defn wrap-log [handler log-level]
  (let [log-exchange (partial log-exchange log-level)]
    (fn [request]
      (let [response (handler request)]
        (log-exchange request response)
        response))))

(defn wrap-time [handler]
  (fn [request]
    (let [get-ts #(.getTime (new Date))
          start-ts (get-ts)
          response (handler request)
          end-ts (get-ts)]
      (assoc-in response [:headers "x-clj-ring-response-time-ms"]
                (str (- end-ts start-ts))))))

(defn add-compression [server]
  (.setHandler server
               (doto (GzipHandler.)
                 (.setIncludedMimeTypes (into-array ["application/javascript"
                                                     "application/json"
                                                     "application/transit+json"
                                                     "image/svg+xml"
                                                     "text/css"
                                                     "text/html"
                                                     "text/javascript"
                                                     "text/plain"]))
                 (.setMinGzipSize 1024)
                 (.setHandler (.getHandler server)))))
