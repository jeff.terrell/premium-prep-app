(ns com.premiumprep.app.model.student
  (:refer-clojure :exclude [find])
  (:require [com.premiumprep.app.model.comment :refer [comment-pattern]]
            [datomic.client.api :as d]))

(def student-pattern
  [{:student/account
    [:db/id :account/email :account/full-name :account/mobile :account/active?
     {:account/type [:db/ident]}
     {:account/state [:db/ident]}]}
   :db/id
   :student/joined-at
   :student/graduation-year
   :student/high-school
   :student/current-college
   {:student/counselor
    [:db/id
     {:counselor/account
      [:db/id :account/email :account/full-name :account/mobile
       {:account/state [:db/ident]}
       {:account/type [:db/ident]}]}]}])

(def college-list-entry-pattern
  ['*
   {:college-list-entry/category [:db/ident]}
   {:college-list-entry/custom-fields ['*]}])

(def full-student-pattern
  (let [account-info [:db/id
                      :account/email
                      ;; everything except password and token
                      :account/full-name
                      :account/preferred-name
                      :account/mobile
                      :account/work-phone
                      :account/home-phone
                      :account/address-street
                      :account/address-city
                      :account/address-state
                      :account/address-zip
                      :account/country
                      :account/active?
                      {:account/state [:db/ident]
                       :account/type [:db/ident]}]

        cpa-info [:college-planning-app/hs-login-page-url
                  :college-planning-app/username
                  :college-planning-app/password
                  {:college-planning-app/type [:db/ident]}]]

    (conj (subvec student-pattern 1)
          :student/academic-interest
          :student/gpa-weighted-9
          :student/gpa-unweighted-9
          :student/gpa-weighted-9-10
          :student/gpa-unweighted-9-10
          :student/gpa-weighted-9-11
          :student/gpa-unweighted-9-11
          :student/class-rank-9
          :student/class-rank-9-10
          :student/class-rank-9-11
          :student/class-rank-9-of
          :student/class-rank-9-10-of
          :student/class-rank-9-11-of
          :student/sat-subject-tests
          :student/ap-tests
          :student/extracurricular-activities
          :student/current-courses
          :student/prospective-courses
          :issue/_student
          {:student/account account-info}
          {:student/parent1 account-info}
          {:student/parent2 account-info}
          {:student/documents '[*]}
          {:student/college-list-entries college-list-entry-pattern}
          {:student/profile-comments comment-pattern}
          {:student/document-list-comments comment-pattern}
          {:student/college-planning-app-info cpa-info}
          {:student/sat-superscore ['* {:sat-superscore/sat ['*]}]}
          {:student/act-superscore ['* {:act-superscore/act ['*]}]}
          {:student/psat '[*]}
          {:student/pact '[*]}
          {:student/sat1 '[*]}
          {:student/sat2 '[*]}
          {:student/sat3 '[*]}
          {:student/sat4 '[*]}
          {:student/act1 '[*]}
          {:student/act2 '[*]}
          {:student/act3 '[*]}
          {:student/act4 '[*]})))

(defn find [db id]
  (let [query '{:find [(pull ?student pattern)]
                :in [$ ?student pattern]
                :where [[?student :student/account ?account]
                        [?account :account/type ?account-type]
                        [?account-type :db/ident :account-type/student]]}]
    (ffirst (d/q {:query query, :args [db id full-student-pattern]}))))

(defn find-by-account-id [db account-id]
  (let [query '{:find [(pull ?student pattern)]
                :in [$ ?account pattern]
                :where [[?student :student/account ?account]
                        [?account :account/type ?account-type]
                        [?account-type :db/ident :account-type/student]]}]
    (ffirst (d/q {:query query
                  :args [db account-id full-student-pattern]}))))

(defn find-by-email [db email]
  (let [query '{:find [(pull ?student pattern)]
                :in [$ ?email pattern]
                :where [[?account :account/email ?email]
                        [?account :account/type ?account-type]
                        [?account-type :db/ident :account-type/student]
                        [?student :student/account ?account]]}]
    (ffirst (d/q {:query query
                  :args [db email full-student-pattern]}))))

(defn find-all-by-counselor-account-id [db counselor-account-id]
  (let [query '{:find [(pull ?student pattern)]
                :in [$ ?counselor-account pattern]
                :where [[?student :student/counselor ?counselor]
                        [?counselor :counselor/account ?counselor-account]
                        [?student :student/account ?student-account]
                        [?student-account :account/type ?account-type]
                        [?account-type :db/ident :account-type/student]]}
        results (d/q {:query query
                      :args [db counselor-account-id
                             full-student-pattern]})]
    (mapv first results)))

(defn all [db]
  (let [query '{:find [(pull ?student pattern)]
                :in [$ pattern]
                :where [[?student :student/account ?account]
                        [?account :account/type ?account-type]
                        [?account-type :db/ident :account-type/student]]}
        results (d/q {:query query, :args [db student-pattern]})]
    (mapv first results)))

(defn listify-college-list-entries
  [{:keys [student/college-list-entries] :as student}]
  (letfn [(order-list [entries]
            (->> entries
                 (sort-by :college-list-entry/index)
                 (mapv #(dissoc %
                                :college-list-entry/category
                                :college-list-entry/index))))
          (map-map [xform-key xform-val hashmap]
            (into (empty hashmap)
                  (for [[k v] hashmap]
                    [(xform-key k) (xform-val v)])))
          (helper [raw-entries]
            (->> raw-entries
                 (group-by :college-list-entry/category)
                 (map-map (comp keyword name :db/ident) order-list)))]
    (let [college-lists (helper college-list-entries)]
      (-> student
          (assoc :student/college-lists college-lists)
          (dissoc :student/college-list-entries)))))
