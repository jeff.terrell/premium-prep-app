(ns com.premiumprep.app.model.document
  (:refer-clojure :exclude [find])
  (:require [datomic.client.api :as d]))

(defn find [db id]
  (let [pattern ['*
                 {:student/_documents
                  [{:student/account [:db/id]
                    :student/counselor [{:counselor/account [:db/id]}]}]}]
        query '{:find [(pull ?document pattern)]
                :in [$ ?document pattern]
                :where [[?document :document/name _]]}
        document (ffirst (d/q {:query query, :args [db id pattern]}))]

    (when document
      {:db/id 17592186045492
       :document/name "arst"
       :document/content-type "application/octet-stream"
       :document/uploaded-at #inst "2020-08-07T17:29:50.643-00:00"
       :document/s3-path "17592186045457.arst"
       :student/_documents {:student/account {:db/id 17592186045454}
                            :student/counselor {:counselor/account
                                                {:db/id 17592186045456}}}}
      (let [student (:student/_documents document)
            ;; When testing, I see that student is a map. This surprises me.
            ;; Conceptually, shouldn't it be a vector? I don't know how Datomic
            ;; would know that only one entity points to this document. The
            ;; Datomic docs seem to imply that this should be a vector. I'll
            ;; pessimistically handle both cases.
            first-student (if (map? student) student (first student))
            student-id (-> first-student :student/account :db/id)
            counselor-id (-> first-student :student/counselor :counselor/account
                             :db/id)]
        (-> document
            (dissoc :student/_documents)
            (assoc :student/account-id student-id)
            (assoc :counselor/account-id counselor-id))))))

(comment
  (require '[datomic.client.api :as d])
  (require '[com.premiumprep.app.schema :as schema])
  (require '[com.premiumprep.app.fixtures :as fixtures])
  (def db-name (name ::db-name))
  (def client (doto (d/client {:server-type :dev-local
                               :storage-dir :mem
                               :system "local"})
                (d/create-database {:db-name db-name})))
  (def conn (doto (d/connect client {:db-name db-name})
              schema/install-schema!
              fixtures/install-fixtures!))
  (def doc-id
    (ffirst (d/q {:query '{:find [?doc]
                           :in [$]
                           :where [[?doc :document/name _]]}
                  :args [(d/db conn)]})))
  doc-id
  (find (d/db conn) doc-id)
  )
