(ns com.premiumprep.app.model.counselor-fields
  (:require [clojure.string :as str]
            [com.premiumprep.app.field :refer [->field]]
            [com.premiumprep.app.model.counselor-fields-common :as base]
            [com.premiumprep.app.validation
             :refer [fields-tx-data parse-boolean validate-boolean
                     validate-params validate-tel]]))

(def fields
  (assoc-in base/fields ["mobile" :validator] validate-tel))

(def fields-for-create
  (-> fields
      (assoc-in ["email" :required?] true)
      (assoc-in ["name" :required?] true)))

(def fields-for-update
  (let [name "active"]
    (-> fields
        (assoc name (->field name [:counselor/account :account/active?]))
        (assoc-in [name :validator] validate-boolean)
        (assoc-in [name :parse] parse-boolean))))

