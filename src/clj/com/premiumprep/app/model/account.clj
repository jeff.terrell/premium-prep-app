(ns com.premiumprep.app.model.account
  (:refer-clojure :exclude [find])
  (:require [clojurewerkz.scrypt.core :as sc]
            [datomic.client.api :as d]))

(def pattern '[*
               {:account/type [:db/ident]}
               {:account/state [:db/ident]}])

(defn find [db id]
  (let [query '{:find [(pull ?account pattern)]
                :in [$ ?account pattern]
                :where [[?account :account/type _]]}]
    (ffirst (d/q {:query query, :args [db id pattern]}))))

(defn find-by-email [db email]
  (let [query '{:find [(pull ?account pattern)]
                :in [$ ?email pattern]
                :where [[?account :account/email ?email]]}]
    (ffirst (d/q {:query query, :args [db email pattern]}))))

(defn find-by-token [db token]
  (let [query '{:find [(pull ?account pattern)]
                :in [$ ?token pattern]
                :where [[?account :account/token ?token]]}]
    (ffirst (d/q {:query query, :args [db token pattern]}))))

(defn save-token [conn account-eid new-token]
  (d/transact conn {:tx-data [[:db/add account-eid :account/token new-token]
                              [:db/add "datomic.tx" :transaction/namespace
                               "com.premiumprep.app.model.account"]]}))

(defn save-password [conn account-eid new-password]
  (let [encrypted-pass (sc/encrypt new-password 16384 8 1)
        tx-data [[:db/add account-eid :account/password encrypted-pass]
                 [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.model.account"]]]
    (d/transact conn {:tx-data tx-data})))
