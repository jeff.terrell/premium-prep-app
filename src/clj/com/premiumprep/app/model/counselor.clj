(ns com.premiumprep.app.model.counselor
  (:refer-clojure :exclude [find])
  (:require [datomic.client.api :as d]))

(def counselor-pattern
  '[* {:counselor/account
       [:db/id :account/email :account/full-name :account/mobile
        :account/active? :account/preferred-name
        {:account/type [:db/ident]}
        {:account/state [:db/ident]}]
       :student/_counselor [:db/id]}])

(defn find [db id]
  (let [query '{:find [(pull ?counselor pattern)]
                :in [$ ?counselor pattern]
                :where [[?counselor :counselor/account ?account]
                        [?account :account/type ?account-type]
                        [?account-type :db/ident :account-type/counselor]]}]
    (ffirst (d/q {:query query, :args [db id counselor-pattern]}))))

(defn all [db]
  (let [query '{:find [(pull ?counselor pattern)]
                :in [$ pattern]
                :where [[?counselor :counselor/account ?account]
                        [?account :account/type ?account-type]
                        [?account-type :db/ident :account-type/counselor]]}
        results (d/q {:query query, :args [db counselor-pattern]})]
    (mapv first results)))
