(ns com.premiumprep.app.model.college
  (:refer-clojure :exclude [find])
  (:require [datomic.client.api :as d]))

(defn find [db id]
  (let [query '{:find [(pull ?college [*])]
                :in [$ ?college]
                :where [[?college :college/name _]]}]
    (ffirst (d/q {:query query, :args [db id]}))))

(defn find-by-name [db name]
  (let [query '{:find [(pull ?college [*])]
                :in [$ ?name]
                :where [[?college :college/name ?name]]}]
    (ffirst (d/q {:query query, :args [db name]}))))

(defn all [db]
  (let [query '{:find [(pull ?college [*])]
                :where [[?college :college/name _]]}
        results (d/q {:query query, :args [db]})]
    (mapv first results)))
