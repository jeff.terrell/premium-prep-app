(ns com.premiumprep.app.model.student-fields
  (:require [clojure.string :as str]
            [com.premiumprep.app.model.student-fields-common :as base]
            [com.premiumprep.app.validation
             :refer [encrypt-password fields-tx-data parse-date parse-float
                     parse-long parse-uri validate-date validate-date-short validate-float
                     validate-long validate-params validate-password
                     validate-tel validate-uri]]))

(defn- validate-grad-year [graduation-year]
  (let [valid-year? #(re-matches #"2\d{3}" %)]
    (if (and (not (str/blank? graduation-year))
             (not (valid-year? graduation-year)))
      (str "The given graduation-year value (" graduation-year
           ") is not a valid year"))))

(defn- validate-cpa-type [s]
  (when-not (#{"" "naviance" "scoir" "maia" "kickstart"} s)
    (format "invalid value (%s)" s)))

(defn- parse-cpa-type [s]
  (when-not (str/blank? s)
    (keyword "college-planning-app-type" s)))

(defn- dateify-field
  [fields key]
  (update fields key merge {:validator validate-date, :parse parse-date}))

(defn- dateify-field-short
  [fields key]
  (update fields key merge {:validator validate-date-short, :parse parse-date}))

(defn- floatify-field
  [fields key]
  (update fields key merge {:validator validate-float, :parse parse-float}))

(defn- longify-field
  [fields key]
  (update fields key merge {:validator validate-long, :parse parse-long}))

(defn- telify-field
  [fields key]
  (update fields key merge {:validator validate-tel}))

(defn- xform-fields
  [fields f keys]
  (reduce f fields keys))

(def fields
  (-> base/fields
      (assoc-in ["password" :validator] validate-password)
      (assoc-in ["password" :tx-transform] encrypt-password)
      (assoc-in ["graduation-year" :validator] validate-grad-year)
      (assoc-in ["graduation-year" :parse] parse-long)
      (assoc-in ["college-planning-app-url" :validator] validate-uri)
      (assoc-in ["college-planning-app-url" :parse] parse-uri)
      (assoc-in ["college-planning-app-type" :validator] validate-cpa-type)
      (assoc-in ["college-planning-app-type" :parse] parse-cpa-type)

      (xform-fields dateify-field
        ["sat-superscore-ebrw-date" "sat-superscore-math-date"
         "act-superscore-english-date" "act-superscore-reading-date"
         "act-superscore-math-date" "act-superscore-science-date" ])

      (xform-fields dateify-field-short
                    ["sat1-date" "sat2-date" "sat3-date" "sat4-date"
                     "act1-date" "act2-date" "act3-date" "act4-date"])

      (xform-fields floatify-field
        ["gpa-weighted-9" "gpa-unweighted-9" "gpa-weighted-9-10"
         "gpa-unweighted-9-10" "gpa-weighted-9-11" "gpa-unweighted-9-11"])

      (xform-fields longify-field
        ["zip" "class-rank-9" "class-rank-9-10" "class-rank-9-11"
         "class-rank-9-of" "class-rank-9-10-of" "class-rank-9-11-of"
         "sat-superscore-total-score" "sat-superscore-ebrw-score"
         "sat-superscore-math-score" "act-superscore-total-score"
         "act-superscore-english-score" "act-superscore-reading-score"
         "act-superscore-math-score" "act-superscore-science-score"
         "psat-total-score" "psat-ebrw-score" "psat-math-score"
         "sat1-total-score" "sat1-ebrw-score" "sat1-math-score"
         "sat2-total-score" "sat2-ebrw-score" "sat2-math-score"
         "sat3-total-score" "sat3-ebrw-score" "sat3-math-score"
         "sat4-total-score" "sat4-ebrw-score" "sat4-math-score"
         "pact-composite-score" "pact-english-score" "pact-reading-score"
         "pact-math-score" "pact-science-score" "act1-composite-score"
         "act1-english-score" "act1-reading-score" "act1-math-score"
         "act1-science-score" "act2-composite-score" "act2-english-score"
         "act2-reading-score" "act2-math-score" "act2-science-score"
         "act3-composite-score" "act3-english-score" "act3-reading-score"
         "act3-math-score" "act3-science-score" "act4-composite-score"
         "act4-english-score" "act4-reading-score" "act4-math-score"
         "act4-science-score"])

      (xform-fields telify-field
        ["mobile" "parent1-mobile" "parent2-mobile" "parent1-work-phone"
         "parent1-home-phone" "parent2-work-phone" "parent2-home-phone"
         "home-phone"])))
