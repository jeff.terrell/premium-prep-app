(ns com.premiumprep.app.model.issue
  (:refer-clojure :exclude [find])
  (:require [datomic.client.api :as d]))

(def issue-pattern
  [:db/id :issue/created-at :issue/text :issue/student
   :issue/counselor-touched-at :issue/action-taken
   {:issue/state [:db/ident]}])

(defn find [db id]
  (let [query '{:find [(pull ?issue pattern)]
                :in [$ ?issue pattern]
                :where [[?issue :issue/text _]]}]
    (ffirst (d/q {:query query, :args [db id issue-pattern]}))))

(defn all [db]
  (let [query '{:find [(pull ?issue pattern)]
                :in [$ pattern]
                :where [[?issue :issue/text _]]}]
    (mapv first (d/q {:query query, :args [db issue-pattern]}))))
