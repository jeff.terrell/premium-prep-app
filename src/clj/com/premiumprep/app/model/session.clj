(ns com.premiumprep.app.model.session
  (:refer-clojure :exclude [find])
  (:require [com.premiumprep.app.model.account :as account]
            [com.premiumprep.app.util :refer [flatten-account-enums]]
            [datomic.client.api :as d])
  (:import java.util.UUID))

(defn find-all-for-account-id [db account-id]
  (when account-id
    (let [query '{:find [(pull ?session
                               [:db/id :session/id
                                {:session/account [:db/id]}])]
                  :in [$ ?account]
                  :where [[?session :session/account ?account]]}]
      (mapv first (d/q {:query query, :args [db account-id]})))))

(defn find [db id]
  (when id
    (let [query '{:find [(pull ?account
                               [:db/id :account/email
                                {:account/type [:db/ident]}
                                {:account/state [:db/ident]}])]
                  :in [$ ?session-id]
                  :where [[?session :session/id ?session-id]
                          [?session :session/account ?account]]}
          account (ffirst (d/q {:query query, :args [db (UUID/fromString id)]}))]
      (when account
        (-> account
            flatten-account-enums
            (update :db/id str))))))

(defn create
  "Transact a new session and return the randomly generated session ID."
  [conn account-eid]
  (let [key (UUID/randomUUID)
        tx-data [{:session/id key
                  :session/account account-eid}
                 [:db/add "datomic.tx" :transaction/namespace
                  "com.premiumprep.app.model.session"]]]
    (d/transact conn {:tx-data tx-data})
    key))

(defn delete
  [conn id]
  (let [query '{:find [?session-eid ?account-eid]
                :in [$ ?session-id]
                :where [[?session-eid :session/id ?session-id]
                        [?session-eid :session/account ?account-eid]]}
        session (first (d/q {:query query, :args [(d/db conn) id]}))]
    (when (not-empty session)
      (let [[session-eid account-eid] session
            tx-data [[:db/retract session-eid :session/id id]
                     [:db/retract session-eid :session/account account-eid]
                     [:db/add "datomic.tx" :transaction/namespace
                      "com.premiumprep.app.model.session"]]]
        (d/transact conn {:tx-data tx-data})
        nil))))

(defn destroy-for-account-id
  [conn account-id]
  (doseq [{session-id :session/id} (find-all-for-account-id
                                     (d/db conn) account-id)]
    (delete conn session-id)))
