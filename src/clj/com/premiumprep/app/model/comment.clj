(ns com.premiumprep.app.model.comment
  (:refer-clojure :exclude [find])
  (:require [datomic.client.api :as d]))

(def comment-pattern
  ['* {:comment/commenter [:db/id :account/email
                           :account/full-name]}])

(defn find [db id]
  (let [query '{:find [(pull ?comment pattern)]
                :in [$ ?comment pattern]
                :where [[?comment :comment/text _]]}]
    (ffirst (d/q {:query query, :args [db id comment-pattern]}))))

(comment
  (require '[datomic.client.api :as d])
  (require '[com.premiumprep.app.schema :as schema] :reload)
  (require '[com.premiumprep.app.fixtures :as fixtures] :reload)
  (def db-name (name ::db-name))
  (def client (doto (d/client {:server-type :dev-local
                               :storage-dir :mem
                               :system "local"})
                (d/create-database {:db-name db-name})))
  (def conn (doto (d/connect client {:db-name db-name})
              schema/install-schema!
              fixtures/install-fixtures!))
  (def comment-id
    (ffirst (d/q {:query '{:find [?comment]
                           :in [$]
                           :where [[?comment :comment/text _]]}
                  :args [(d/db conn)]})))
  comment-id
  (find (d/db conn) comment-id)
  )
