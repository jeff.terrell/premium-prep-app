(ns com.premiumprep.app.layout
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.header :refer [header]]
   [com.premiumprep.app.page.unverified-email
    :refer [unverified-email-page-inner]]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn standard
  [page-id & children]
  [:div {:id page-id}
   [header]
   (into [:main] children)])

(defn modal
  ([identifier child] (modal identifier child nil))
  ([identifier child last-child]
   (let [id #(hash-map :id (str identifier "-" %))]
     [:div.modal-page (id "page")
      [:header]
      [:main.blurred
       (conj [:div.modal-page-foreground (id "modal")] child)
       [:nav
        [:a {:href "#"} "Website"]
        [:a {:href "#"} "Contact Us"]]]
      last-child])))

(defn- unverified-page []
  [modal "unverified-email"
   [unverified-email-page-inner]])

(defn with-verified-email-check [verified-email-content]
  (r/with-let [account-state (rf/subscribe [::data/current-user-state])]
    (if (= :account-state/email-unverified @account-state)
      [unverified-page]
      verified-email-content)))
