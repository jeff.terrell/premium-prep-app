(ns com.premiumprep.app.page.change-email
  (:require
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn- status []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :change-email])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "⏳" ; this is an emoji character, in case you can't see it
        error [:div.error [:p error]]
        success? [:div.success [:p "Email successfully changed. "
                                [:a {:href (k/path-for [:login])}
                                 "Go home."]]]
        :else ""))))

(defn- change-email-form [email]
  [:form {:method "post"
          :action (api-url "/change-email")
          :on-submit #(do
                        (.preventDefault %)
                        (rf/dispatch [:change-email
                                      (js/FormData. (.-target %))]))}
   [:div.input-label "Current Email"]
   [:div.input-field [:input {:type "email", :name "current-email"
                        :value email, :disabled true}]]
   [:div.input-label "New Email"]
   [:div.input-field [:input {:type "email", :name "email", :required true}]]
   [:input {:type "submit", :value "Submit"}]
   [status]])

(defn change-email-page []
  (r/with-let [email (rf/subscribe [::data/email])]
    (if-not @email
      [unauthorized]
      [layout/modal "change-email"
       [:<>
        [:h3 "Change Email"]
        [change-email-form @email]]])))
