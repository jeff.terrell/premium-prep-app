(ns com.premiumprep.app.page.student
  (:require
   [clojure.string :as str]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.fragment.add-college-modal :refer [add-college-modal]]
   [com.premiumprep.app.fragment.create-issue-modal :refer [create-issue-modal]]
   [com.premiumprep.app.fragment.custom-field-modal :refer [custom-field-modal]]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.fragment.update-issue-modal :refer [update-issue-modal]]
   [com.premiumprep.app.fragment.upload-document-modal
    :refer [upload-document-modal]]
   [com.premiumprep.app.fragment.xhr :refer [xhr-status]]
   [com.premiumprep.app.layout :as layout]
   [com.premiumprep.app.page.student.colleges :as colleges]
   [com.premiumprep.app.page.student.comments :refer [collapsible-comment-list]]
   [com.premiumprep.app.page.student.custom-field :refer [custom-fields-section]]
   [com.premiumprep.app.page.student.documents :as documents]
   [com.premiumprep.app.page.student.issues :as issues]
   [com.premiumprep.app.page.student.profile :as profile]
   [com.premiumprep.app.util :as util]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(defn section-link-li [fragment-id label]
  [:li>a {:on-click #(rf/dispatch [::events/scroll-to-identifier fragment-id])
          :href "#"}
   label])

(defn intra-page-nav []
  (let [type @(rf/subscribe [::data/current-user-type])]
    [:nav.intra-page
     [:ul
      [section-link-li "profile" "Profile"]
      [section-link-li "docs" "Docs"]
      [section-link-li "college-list" "College List"]
      (when (#{:account-type/superuser :account-type/counselor} type)
        [section-link-li "feedback" "Feedback"])
      (when-not (#{:account-type/superuser :account-type/counselor} type)
        [:li>a.knack
         {:href "https://premiumprep.knack.com/premiumprep#my-services/"
          :target "_blank"}
         "My Services & Time Remaining"
         [:img {:src "/other-tab.png"
                :title "open by Adrien Coquet from the Noun Project"
                :height "16", :width "16"}]])]]))

(defn sidebar [student]
  (let [counselor (-> student :student/counselor :counselor/account)
        {:keys [account/email account/full-name account/phone]} counselor]
    [:aside
     [:h3.student-name (-> student :student/account :account/full-name)]
     [intra-page-nav]
     [:hr]
     (when counselor
       [:section.advisor-info
        [:h5 "Advisor Info:"]
        [:p.advisor-name (or full-name "(name unknown)")]
        [:p.phone-number
         (if phone
           [:a {:href (str "tel:" phone)} phone]
           "(phone unknown)")]
        [:p.advisor-email
         (if email email "(email unknown)")]
        (when email
          [:p.send-email
           [:a.button.secondary
            {:href (str "mailto:" email)} "Email Advisor"]])])]))

(defn authorized? [current-type current-id id]
  (when (and current-type id)
    (case current-type
      :account-type/superuser true
      :account-type/counselor true
      ;; FIXME: how do parents get authorized?
      :account-type/student (= id current-id))))

(defn breadcrumb [type student]
  (case type
    :account-type/student nil
    :account-type/counselor [:nav.breadcrumb
                             [:a {:href (k/path-for [:students])}
                              "< My Students"]]
    :account-type/superuser
    (let [{:keys [db/id counselor/account]} (:student/counselor student)
          {:keys [account/full-name]} account
          fname (first (str/split full-name #" "))
          label (str "< " fname "'s Students")]
      [:nav.breadcrumb
       [:a {:href (k/path-for [:counselor {:id id}])}
        label]])))

(defn h1 [type student]
  (let [cname (get-in student
                      [:student/counselor :counselor/account
                       :account/full-name])
        sacct (:student/account student)
        sname (:account/full-name sacct)]
    [:h1
     (case type
       :account-type/superuser (str cname "'s Student: " sname)
       :account-type/counselor (str "Student: " sname)
       (str "Welcome back, " (:account/preferred-name sacct sname)))]))

(defn inactive?
  "A student is assumed active until proven otherwise."
  [{:keys [student/account] :as student}]
  (false? (:account/active? account)))

(defn controls [student]
  (let [is-active? (not (inactive? student))
        account-id (get-in student [:student/account :db/id])
        f (if is-active?
            util/deactivate-account-click-fn
            util/reactivate-account-click-fn)
        label (str (if is-active? "D" "R") "eactivate Account")
        student-account-state @(rf/subscribe [::data/student-account-state
                                              (:db/id student)])
        email-unverified? (= :account-state/email-unverified
                             student-account-state)]

    [:div.controls
     [:a.button.secondary
      {:on-click (f :account-type/student account-id (:db/id student))}
      label]
     (when email-unverified?
       [:a.button
        {:on-click #(rf/dispatch [:re-send-invite
                                  (-> student :student/account :db/id)])}
        "Re-send user invite"])]))

(defn- student-page-main [id type]
  (let [student (rf/subscribe [::data/student id])
        student-xhr-status (rf/subscribe [::data/xhr :student])]
    [layout/standard "student-page"
     [sidebar @student]
     [:article
      [xhr-status :student @student-xhr-status]
      [breadcrumb type @student]
      [:header
       [h1 type @student]
       (when (= :account-type/superuser type)
         [controls @student])]
      (when @student
        [:<>
         (when (inactive? @student)
           [:p.notice "This student's account has been deactivated."])
         [profile/profile @student]
         [documents/documents-for-student id @student]
         [colleges/college-list-section type @student]
         [issues/issues id type]
         [:div {:style {:margin-top "5rem"
                        :text-align "right"}}
          [:a {:href "https://icons8.com/icon/1941/trash"
               :style {:font-size "8pt"
                       :color "#888"}}
           "Trash icon by Icons8"]]])]]))

(defn student-page [id]
  (let [type (rf/subscribe [::data/current-user-type])
        current-id (rf/subscribe [::data/current-student-id])]
    (fn [id]
      (if-not (authorized? @type @current-id id)
        [unauthorized]
        [layout/with-verified-email-check
         [:<>
          [student-page-main id @type]
          [add-college-modal id]
          [upload-document-modal id]
          [create-issue-modal id]
          [update-issue-modal]
          [custom-field-modal id]]]))))
