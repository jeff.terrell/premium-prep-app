(ns com.premiumprep.app.page.new-college
  (:require
   [clojure.string :as str]
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.layout :as layout]
   [re-frame.core :as rf]))

(defn- submit-form [event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)]
    (rf/dispatch [:create-college form-data])))

(defn- error-status [error-message]
  [:div.status.error
   [:p "There was an error with the attempt to create a counselor. The error "
    "response from the server follows:"]
   [:p (interpose [:br] (str/split error-message #"\n"))]])

(defn- success-status []
  [:div.status.success
   [:p "Success!"]])

(defn- creation-status []
  (let [create-xhr-status (rf/subscribe [::data/xhr :create-college])
        {:keys [in-flight? error success?]} @create-xhr-status]
    (cond
      in-flight? "⏳"
      error [error-status error]
      success? [success-status])))

(defn- input [name label]
  [:section
   [:label {:for name} label]
   [:input {:id name, :type "text", :name name, :required true}]])

(defn new-college-page []
  (let [current-user-type (rf/subscribe [::data/current-user-type])]
    (if-not (= :account-type/superuser @current-user-type)
      [unauthorized]
      [layout/with-verified-email-check
       [layout/standard "new-college-page"
        [:h1 "Add College"]
        [:form {:id "new-college-form"
                :method "post"
                :action (api-url "/college")
                :on-submit submit-form}
         [:div.form-inputs
          [input "name" "College Name"]
          [input "enrollment" "Enrollment"]
          [input "admit-rate" "Admit Rate (%)"]
          [input "average-sat" "Average SAT"]
          [input "average-act" "Average ACT"]]
         [:div.form-submit
          [:input {:type "submit", :value "Create"}]]]
        [creation-status]]])))
