(ns com.premiumprep.app.page.new-student
  (:require
   [clojure.string :as str]
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.select-counselor :refer [select-counselor]]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn- submit-form [event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)]
    (rf/dispatch [:create-student form-data])))

(defn- error-status [error-message]
  [:div.status.error
   [:p "There was an error with the attempt to create a student. The error "
    "response from the server follows:"]
   [:p (interpose [:br] (str/split error-message #"\n"))]])

(defn- creation-status []
  (let [create-xhr-status (rf/subscribe [::data/xhr :create-student])]
    (fn []
      (let [{:keys [in-flight? error success?]} @create-xhr-status]
        (cond
          in-flight? "⏳"
          error [error-status error]
          ;; Note: potential race condition here, if the user restarts a request
          ;; within 3 seconds of a success. Not worrying about it yet though.
          success?
          [:div.status.success
           "Success! The user will receive an email with a sign-up link."])))))

(defn new-student-page []
  (r/with-let [current-user-type (rf/subscribe [::data/current-user-type])]
    (if-not (= :account-type/superuser @current-user-type)
      [unauthorized]
      [layout/with-verified-email-check
       [layout/standard "new-student-page"
        [:h1 "New Student"]
        [:form {:method "post"
                :action (api-url "/student")
                :on-submit submit-form}
         [:div.form-inputs
          [:section
           [:label {:for "email"} "Student Email"]
           [:input#email {:type "email", :name "email"
                          :placeholder "email@example.com"}]]
          [:section
           [:label {:for "counselor-id"} "Counselor"]
           [select-counselor nil]]]
         [:div.form-submit
          [:input {:type "submit", :value "Create"}]]]
        [creation-status]]])))
