(ns com.premiumprep.app.page.student.comments
  (:require
   cljsjs.moment
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(k/reg-event-db ::toggle-visibility
  [(rf/path :page :page-type/student :comments-visibility)]
  (fn [substate [comments-type]]
    (update substate comments-type not)))

(defn- comments-summary [type comments visible?]
  [:div.comments-summary {:on-click #(rf/dispatch
                                       [::toggle-visibility type])}
   [:span
    [:span.arrow (if visible? "∧" "∨")]
    (let [n (count comments)]
      [:span.comment-count n " comment" (when (not= 1 n) "s")])]
   [:span.latest
    (when-let [latest (last comments)]
      [:<>
       "Latest Comment: from "
       (-> latest :comment/commenter :account/full-name)
       " on "
       (-> latest :comment/created-at .toLocaleDateString)])]])

(defn- interpreted-name [account-id full-name]
  (if (= account-id @(rf/subscribe [::data/current-user-id]))
    "Me"
    full-name))

(defn- comments-details [comments]
  (when (not-empty comments)
    [:div.comments-details
     (for [comment comments
           :let [{:keys [db/id comment/created-at
                         comment/text comment/commenter]} comment
                 {:keys [account/email account/full-name]} commenter
                 commenter-id (:db/id commenter)]]
       ^{:key id}
       [:div.comment
        [:div.comment-summary
         [:span.name [interpreted-name commenter-id full-name]]
         " – "
         [:span.relative-date {:title (.toLocaleString created-at)}
          (-> created-at js/moment .fromNow)]]
        [:div.comment-text text]])]))

(defn- submission-status [student-id type]
  (let [path [::data/xhr :post-comment student-id type]
        {:keys [in-flight? success? error] :as x} @(rf/subscribe path)]
    (cond
      in-flight? "⏳" ; this is an emoji character, in case you can't see it
      error [:div.status.error [:p error]]
      success? [:div.status.success "Sent!"]
      :else "")))

(defn- comment-submission [student-id type]
  (let [make-id #(str % "-for-" type)
        textarea-id (make-id "new-comment")
        submit-id (make-id "new-comment-submit")
        on-submit-click (fn [_] (.click (js/document.getElementById submit-id)))]
    [:div.comment-submission
     [:form {:id (make-id "new-comment-form")
             :method "post"
             :action (api-url "/comment")
             :on-submit #(let [form-data (js/FormData. (.-target %))
                               event [:post-comment student-id type form-data]]
                           (.preventDefault %)
                           (rf/dispatch event))}

      [:label {:for textarea-id} "New Comment"]
      [:textarea {:id textarea-id, :name "comment", :required true}]
      [:input {:type "hidden", :name "student-id", :value student-id}]
      [:input {:type "hidden", :name "comment-type", :value type}]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input {:id submit-id, :type "submit", :style {:display "none"}}]
      [:div.submit-controls
       [:a.button {:on-click on-submit-click} "Send"]
       [submission-status student-id type]]]]))

(defn collapsible-comment-list [id type comments]
  (let [visible? @(rf/subscribe [::data/comments-visible? type])]
    [:div.comment-list {:class (if visible? "expanded" "collapsed")}
     [comments-summary type comments visible?]
     [comments-details comments]
     [comment-submission id type]]))
