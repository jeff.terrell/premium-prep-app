(ns com.premiumprep.app.page.student.custom-field
  (:require
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.page.student.util :refer [garbage-can-img-data]]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(k/reg-event-fx ::show-add-custom-field-modal
  (fn [_ [entry-id student-id]]
    {:dispatch-n [[::events/set-modal-visibility true :modal-type/custom-field
                   {:entry-id entry-id, :student-id student-id}]
                  [:clear-xhr :create-custom-field]
                  [:clear-xhr :update-custom-field]]}))

(k/reg-event-fx ::show-edit-custom-field-modal
  (fn [_ [entry-id student-id field-id name value]]
    {:dispatch-n [[::events/set-modal-visibility true :modal-type/custom-field
                   {:entry-id entry-id
                    :student-id student-id
                    :field-id field-id
                    :name name
                    :value value}]
                  [:clear-xhr :create-custom-field]
                  [:clear-xhr :update-custom-field]]}))

(defn custom-fields-grid [student-id entry-id fields]
  (let [edit-name ::show-edit-custom-field-modal
        delete-name :delete-custom-field
        edit-fn (fn [id name value]
                  #(rf/dispatch [edit-name entry-id student-id id name value]))
        delete-fn (fn [id] #(rf/dispatch [delete-name entry-id student-id id]))]
    [:div.custom-fields-grid
     (for [{:keys [custom-field/name custom-field/value db/id]} fields]
       ^{:key id}
       [:<>
        [:span.name name]
        [:span.value value]
        [:span.icon.edit {:on-click (edit-fn id name value)} "🖉"]
        [:span.icon.delete {:on-click (delete-fn id)}
         [:img {:src garbage-can-img-data
                :width 16, :height 16}]]])]))

(defn custom-fields-section [id entry]
  (let [{:keys [college-list-entry/custom-fields]} entry
        show-modal-event [::show-add-custom-field-modal (:db/id entry) id]]
    [:section.custom-fields [:header [:h4 "Custom Fields"]]
     (if (empty? custom-fields)
       [:div.custom-fields-grid "None"]
       [custom-fields-grid id (:db/id entry) custom-fields])
     [:div
      [:a.button.secondary {:on-click #(rf/dispatch show-modal-event)}
       "Add Custom Field"]]]))
