(ns com.premiumprep.app.page.student.profile
  (:require
   cljsjs.moment
   [clojure.string :as str]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.page.student.comments :refer [collapsible-comment-list]]
   [com.premiumprep.app.page.student.util :refer
    [maybe-link sort-by-fn dispatch-button]]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(k/reg-event-db ::expand-profile
  [(rf/path :page :page-type/student)]
  (fn [_ _] {:show-full-profile? true}))

(k/reg-event-db ::collapse-profile
  [(rf/path :page :page-type/student)]
  (fn [_ _] {:show-full-profile? false}))

(defn- show-full-profile-toggle-button [show-full?]
  [:div.paging
   (if show-full?
     [dispatch-button "full-profile" [::collapse-profile] "Hide Details"]
     [dispatch-button "full-profile" [::expand-profile] "Show Details"])])

(defn profile-header [id]
  [:header
   [:h3 "Profile"]
   [:a.button.secondary {:href (k/path-for [:edit-student {:id id}])}
    "Edit Profile"]])

(defn profile-field
  ([label value] (profile-field nil label value))
  ([class label value]
   [:div (when class {:class class})
    [:span.field-name (str label ":")]
    [:span.field-value (or value "N/A")]]))

(defn grid-field
  ([label value] (grid-field nil label value))
  ([class label value]
   [:<>
    [:span.field-name (when class {:class class}) (str label ":")]
    [:span.field-value (or value "N/A")]]))

(defn common-profile-fields [student]
  (let [{:keys [db/id student/account student/graduation-year
                student/high-school]} student
        {:keys [account/full-name account/preferred-name]} account]
    [:<>
     [profile-field "name" "Name" full-name]
     [profile-field "grad-year" "HS Grad Year" graduation-year]
     [profile-field "nickname" "Preferred Name" preferred-name]
     [profile-field "high-school" "High School" high-school]]))

(defn collapsed-profile-section [student]
  (let [{:keys [db/id]} student]
    [:section.profile
     [profile-header id]
     [:div
      [common-profile-fields student]]]))

(defn test-label
  ([score] (test-label score :no-date))
  ([score date]
   (cond
     (not score) "N/A"
     (= :no-date date) score
     (not date) (str score " (no date given)")
     :else (let [local-inst (-> date js/moment.utc. (.format " 	l") str/triml)]
             (str score " (" local-inst ")")))))

(defn sat-fields [ord-label sat]
  (let [{:keys [sat/total-score sat/ebrw-score sat/math-score sat/date]} sat
        label #(str ord-label " SAT " % " Score")]
    [:<>
     [grid-field (label "Total") (test-label total-score date)]
     [grid-field "indent" (label "EBRW") ebrw-score]
     [grid-field "indent" (label "Math") math-score]]))

(defn act-fields [ord-label act]
  (let [{:keys [act/composite-score act/english-score act/reading-score
                act/science-score act/math-score act/date]} act
        label #(str ord-label " ACT " % " Score")]
    [:<>
     [grid-field (label "Composite") (test-label composite-score date)]
     [grid-field "indent" (label "English") english-score]
     [grid-field "indent" (label "Reading") reading-score]
     [grid-field "indent" (label "Math") math-score]
     [grid-field "indent" (label "Science") science-score]]))

(defn expanded-profile-section [student]
  (let [{:keys [db/id student/account student/counselor student/joined-at
                student/graduation-year student/high-school
                student/current-college student/parent1 student/parent2
                student/documents student/college-list-entries
                student/profile-comments student/document-list-comments
                student/academic-interest student/gpa-weighted-9
                student/gpa-unweighted-9 student/gpa-weighted-9-10
                student/gpa-unweighted-9-10 student/gpa-weighted-9-11
                student/gpa-unweighted-9-11 student/class-rank-9
                student/class-rank-9-10 student/class-rank-9-11
                student/class-rank-9-of student/class-rank-9-10-of
                student/class-rank-9-11-of student/psat student/pact
                student/sat-superscore student/sat1 student/sat2 student/sat3
                student/sat4 student/act-superscore student/act1 student/act2
                student/act3 student/act4 student/sat-subject-tests
                student/ap-tests student/college-planning-app-info
                student/extracurricular-activities student/current-courses
                student/prospective-courses]} student
        comments (sort-by (sort-by-fn :comment/created-at) profile-comments)
        {:keys [account/full-name account/preferred-name]} account
        show-full? @(rf/subscribe [::data/show-full-student-profile?])]

    [:section.profile
     [profile-header id]
     [:div
      [common-profile-fields student]
      [:div.expanded-fields
       [show-full-profile-toggle-button show-full?]
       [:h5 "Interest / Major"]
       [grid-field "Academic Interest or Potential Major" academic-interest]

       [:h5 "Grades"]
       [grid-field "9th Weighted GPA" gpa-weighted-9]
       [grid-field "9th Unweighted GPA" gpa-unweighted-9]
       [grid-field "9th-10th Weighted GPA" gpa-weighted-9-10]
       [grid-field "9th-10th Unweighted GPA" gpa-unweighted-9-10]
       [grid-field "9th-11th Weighted GPA" gpa-weighted-9-11]
       [grid-field "9th-11th Unweighted GPA" gpa-unweighted-9-11]
       [grid-field "9th Class Rank" class-rank-9]
       [grid-field "9th Class Size" class-rank-9-of]
       [grid-field "9th-10th Class Rank" class-rank-9-10]
       [grid-field "9th-10th Class Size" class-rank-9-10-of]
       [grid-field "9th-11th Class Rank" class-rank-9-11]
       [grid-field "9th-11th Class Size" class-rank-9-11-of]
       [grid-field "PSAT Total Score" (:sat/total-score psat)]
       [grid-field "indent" "PSAT EBRW" (:sat/ebrw-score psat)]
       [grid-field "indent" "PSAT Math" (:sat/math-score psat)]
       [grid-field "Practice ACT Composite Score" (:act/composite-score pact)]
       [grid-field "indent" "Practice ACT English" (:act/english-score pact)]
       [grid-field "indent" "Practice ACT Reading" (:act/reading-score pact)]
       [grid-field "indent" "Practice ACT Science" (:act/science-score pact)]
       [grid-field "indent" "Practice ACT Math"    (:act/math-score    pact)]

       [:h5 "Test Scores"]

       (let [{:keys [sat-superscore/sat sat-superscore/ebrw-date
                     sat-superscore/math-date]} sat-superscore
             {:keys [sat/total-score sat/ebrw-score sat/math-score
                     sat/date]} sat]
         [:<>
          [grid-field "Superscore SAT" (test-label total-score)]
          [grid-field "indent" "Superscore EBRW"
           (test-label ebrw-score ebrw-date)]
          [grid-field "indent" "Superscore Math"
           (test-label math-score math-date)]])

       [sat-fields "1st" sat1]
       [sat-fields "2nd" sat2]
       [sat-fields "3rd" sat3]
       [sat-fields "4th" sat4]

       (let [{:keys [act-superscore/act act-superscore/english-date
                     act-superscore/reading-date act-superscore/science-date
                     act-superscore/math-date]} act-superscore
             {:keys [act/composite-score act/english-score act/reading-score
                     act/science-score act/math-score act/date]} act]
         [:<>
          [grid-field "Superscore ACT" (test-label composite-score)]
          [grid-field "indent" "Superscore English"
           (test-label english-score english-date)]
          [grid-field "indent" "Superscore Reading"
           (test-label reading-score reading-date)]
          [grid-field "indent" "Superscore Math"
           (test-label math-score math-date)]
          [grid-field "indent" "Superscore Science"
           (test-label science-score science-date)]])

       [act-fields "1st" act1]
       [act-fields "2nd" act2]
       [act-fields "3rd" act3]
       [act-fields "4th" act4]

       [grid-field "SAT Subject Tests" sat-subject-tests]
       [grid-field "AP Tests" ap-tests]

       [:h5 "Contact Information"]
       [grid-field "Student Mobile" (:account/mobile account)]
       [grid-field "Student Email" (:account/email account)]
       [grid-field "Student Home Phone" (:account/home-phone account)]
       [grid-field "Parent 1 Name" (:account/full-name parent1)]
       [grid-field "Parent 1 Mobile" (:account/mobile parent1)]
       [grid-field "Parent 1 Email" (:account/email parent1)]
       [grid-field "Parent 2 Name" (:account/full-name parent2)]
       [grid-field "Parent 2 Mobile" (:account/mobile parent2)]
       [grid-field "Parent 2 Email" (:account/email parent2)]
       [grid-field "Mailing Address" (:account/address-street account)]
       [grid-field "Home City, State"
                   (let [{:keys [account/address-city
                                 account/address-state]} account]
                     (str address-city ", " address-state))]

       [:h5 "College Planning App Info"]
       (let [{:keys [college-planning-app/type
                     college-planning-app/username
                     college-planning-app/password
                     college-planning-app/hs-login-page-url]
              } college-planning-app-info
             type->str {:college-planning-app-type/naviance "Naviance"
                        :college-planning-app-type/scoir "SCOIR"
                        :college-planning-app-type/maia "Maia"
                        :college-planning-app-type/kickstart "Kickstart"}
             type-str (type->str type)]
         [:<>
          [grid-field "Type" type-str]
          [grid-field "Username" username]
          [grid-field "Password" password]
          [grid-field "Link to HS's login page"
           [:a {:href hs-login-page-url} hs-login-page-url]]])

       [:h5 "Other"]
       [grid-field "Extracurricular Activities" [maybe-link
                                                 extracurricular-activities]]
       [grid-field "Current Courses" current-courses]
       [grid-field "Prospective Courses" prospective-courses]]]]))

(defn profile [student]
  (let [{:keys [db/id student/profile-comments]} student
        comments (sort-by (sort-by-fn :comment/created-at) profile-comments)
        show-full? @(rf/subscribe [::data/show-full-student-profile?])]
    [:div#profile.profile-and-comments
     (if show-full?
       [expanded-profile-section student]
       [collapsed-profile-section student])
     [show-full-profile-toggle-button show-full?]
     [collapsible-comment-list id :profile-comments comments]]))
