(ns com.premiumprep.app.page.student.colleges
  (:require
   cljsjs.moment
   [clojure.string :as str]
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.page.student.custom-field :refer [custom-fields-section]]
   [com.premiumprep.app.page.student.util :refer [maybe-link]]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(k/reg-event-db ::toggle-eliminated-colleges-visibility
  [(rf/path :page :page-type/student :show-eliminated-colleges?)]
  (fn [currently-visible? _] (not currently-visible?)))

(defn pencil [attrs]
  [:span.pencil attrs "🖉"])

(defn entry-field-editor
  [id entry type field]
  (let [attr (keyword "college-list-entry" (name field))
        value (get entry attr)
        input-name (name field)
        college-id (-> entry :college-list-entry/college :db/id)
        on-submit #(do
                     (.preventDefault %)
                     (rf/dispatch [:update-entry-field
                                   (js/FormData. (.-target %))]))
        form-attrs {:method "put"
                    :action (api-url "/college-list-entry-field")
                    :on-submit on-submit}
        submit-id (str "update-field-submit")
        on-submit-click (fn [_] (.click (js/document.getElementById submit-id)))
        update-xhr-status (rf/subscribe [::data/xhr :update-entry-field
                                         college-id])
        {:keys [in-flight?]} @update-xhr-status]

    (letfn [(attrs [map] (merge {:id "college-list-entry-field-input"
                                 :name "value"
                                 :default-value value}
                                map))
            (radio [label val]
              [:label
               [:input (attrs {:type "radio"
                               :default-value (str val)
                               :default-checked (= val value)})]
               label])]

      [:<>
       [:form form-attrs
        [:input {:type "hidden", :name "student-id", :value id}]
        [:input {:type "hidden", :name "college-id", :value college-id}]
        [:input {:type "hidden", :name "field", :value field}]

        (case type
          :string [:input (attrs {:type "text"})]
          :boolean [:<>
                    [radio "No" false]
                    [radio "Yes" true]]
          :instant (let [opts {:type "date"}
                         opts (if value
                                (merge opts {:default-value
                                             (-> value
                                                 (js/moment.utc.)
                                                 (.format "YYYY-MM-DD"))})
                                opts)]
                     [:input (attrs opts)])
          :notes [:textarea (attrs nil)])

        (if in-flight?
          [:span.inflight "⏳"]
          [:<>
           [:span.cancel
            {:on-click #(rf/dispatch
                          [:stop-editing-field-for-college
                           college-id])}
            "❌"]

           ;; Safari doesn't yet implement the form.requestSubmit() method, so
           ;; this is a workaround to trigger form submission from Javascript
           ;; _including_ the submit event itself, which we respond to in the
           ;; :on-submit attribute of the enclosing form element:
           [:input {:id submit-id, :type "submit", :style {:display "none"}}]
           [:span.submit {:on-click on-submit-click} "✅"]])]])))

(defn entry-field
  ([id entry type field label] (entry-field id entry type field label false))
  ([id entry type field label read-only?]
   (let [attr (keyword "college-list-entry" (name field))
         college-id (-> entry :college-list-entry/college :db/id)
         editing-field @(rf/subscribe [::data/editing-field-for-college
                                       college-id])
         editing-field? (= editing-field field)
         editing-any? (some? editing-field)
         value (get entry attr "Not Specified")
         value (if (contains? entry attr)
                 (case type
                   :boolean (case (get entry attr)
                              true "Yes"
                              false "No")
                   :instant (-> (get entry attr)
                                js/moment.utc.
                                (.format " 	l"))
                   (let [txt (get entry attr)]
                     (if (re-matches #"^https?://.*" txt)
                       [:a {:href txt} "link"]
                       txt)))
                 "Not Specified")
         notes-text-div (into [:div.notes-text]
                              (interpose [:br] (str/split (get entry attr)
                                                          #"\n")))
         event [:edit-field-for-college college-id field]
         on-click {:on-click #(rf/dispatch event)}]

     (if (= type :notes)
       [:div.notes
        (cond
          read-only? [:label label [:br] notes-text-div]
          editing-field? [entry-field-editor id entry type field]
          editing-any? [:label label [:br] notes-text-div]
          :else [:label label [pencil on-click] [:br] notes-text-div])]

       [:<>
        [:div.field-name label ":"]
        [:div.field-value
         (cond
           read-only? value
           editing-field? [entry-field-editor id entry type field]
           editing-any? value
           :else [:<> value
                  [pencil on-click]])]]))))

(defn category-editor
  [id entry category]
  (let [college-id (-> entry :college-list-entry/college :db/id)
        on-submit #(do
                     (.preventDefault %)
                     (rf/dispatch [:update-entry-category
                                   (js/FormData. (.-target %))]))
        form-attrs {:method "put"
                    :action (api-url "/college-list-entry-category")
                    :on-submit on-submit}
        submit-id "update-category-submit"
        on-submit-click (fn [_] (.click (js/document.getElementById submit-id)))
        update-xhr-status (rf/subscribe [::data/xhr :update-entry-category id])
        {:keys [in-flight?]} @update-xhr-status]

    [:form form-attrs
     [:input {:type "hidden", :name "student-id", :value id}]
     [:input {:type "hidden", :name "college-id", :value college-id}]

     [:select {:name "category", :default-value (name category)}
      [:option {:value "high-reach"} "High Reach"]
      [:option {:value "reach"} "Reach"]
      [:option {:value "target"} "Target"]
      [:option {:value "likely"} "Likely"]
      [:option {:value "uncategorized"} "Uncategorized"]]

     (if in-flight?
       [:span.inflight "⏳"]
       [:<>
        [:span.cancel
         {:on-click #(rf/dispatch
                       [:stop-editing-field-for-college college-id])}
         "❌"]

        ;; Safari doesn't yet implement the form.requestSubmit() method, so
        ;; this is a workaround to trigger form submission from Javascript
        ;; _including_ the submit event itself, which we respond to in the
        ;; :on-submit attribute of the enclosing form element:
        [:input {:id submit-id, :type "submit", :style {:display "none"}}]
        [:span.submit {:on-click on-submit-click} "✅"]])]))

(defn category-h5 [id entry list-name]
  (let [category (case list-name
                   "High Reaches" :high-reach
                   "Reaches" :reach
                   "Targets" :target
                   "Likelies" :likely
                   "Uncategorized" :uncategorized)

        label ({:high-reach "High Reach"
                :reach "Reach"
                :target "Target"
                :likely "Likely"
                :uncategorized "Uncategorized"} category)

        college-id (-> entry :college-list-entry/college :db/id)
        editing-field @(rf/subscribe
                         [::data/editing-field-for-college college-id])
        editing-field? (= editing-field :category)
        editing-any? (some? editing-field)
        event [:edit-field-for-college college-id :category]
        on-click {:on-click #(rf/dispatch event)}]

    [:h5 "Category: "
     (cond
       editing-field? [category-editor id entry category]
       editing-any? label
       :else [:<> label [pencil on-click]])]))

(defn highlights-section [id type entry]
  (let [read-only? (not (#{:account-type/superuser :account-type/counselor}
                          type))]
    [:section [:header [:h4 "Highlights"]]
     [:div.field-grid
      [entry-field id entry :string :location "Location"]
      [entry-field id entry :string :setting "Setting"]
      [entry-field id entry :string :app-round "App Round"]
      [entry-field id entry :instant :app-deadline "App Deadline"]
      [entry-field id entry :boolean :app-submitted? "App Submitted"]
      [entry-field id entry :string :program-major "Program/Major"]
      [entry-field id entry :string :interest-demonstrated
       "Interest Demonstrated"
       read-only?]
      [entry-field id entry :boolean :visited? "Visited"]
      [entry-field id entry :boolean :early-decision? "Early Decision"
       read-only?]]]))

(defn admissions-section [id entry]
  [:section [:header [:h4 "Admissions"]]
   [:div.field-grid
    [entry-field id entry :string :rd-admit-rate "RD Admit Rate"]
    [entry-field id entry :string :ed-admit-rate "ED Admit Rate"]
    [entry-field id entry :string :hs-admit-rate "HS Admit Rate"]
    [entry-field id entry :string :hs-avg-sat "HS AVG SAT"]
    [entry-field id entry :string :hs-avg-act "HS AVG ACT"]
    [entry-field id entry :string :hs-avg-gpa "HS AVG GPA"]
    [entry-field id entry :string :testing-requirements "Testing Requirements"]
    [entry-field id entry :string :scholarship-info "Scholarship Info"]
    [entry-field id entry :string :scholarship-deadlines "Scholarship Deadlines"]
    [entry-field id entry :string :admissions-contacts "Admissions Contacts"]
    [entry-field id entry :string :portfolio-requirements "Portfolio Requirements"]
    [entry-field id entry :string :portfolio-status "Portfolio Status"]]])

(defn financial-aid-section [id entry]
  [:section [:header [:h4 "Financial Aid"]]
   [:div.field-grid
    [entry-field id entry :string :financial-aid-forms-required
     "Aid Forms Required"]
    [entry-field id entry :string :financial-aid-deadline
     "Financial Aid Deadline"]]])

(defn notes-input
  ([entry type] (notes-input entry type false))
  ([entry type read-only?]
   (let [label (if (= type :student) "Student's Notes" "Counselor's Notes")
         attr (keyword "college-list-entry" (str type "-notes"))]
     [:div.notes
      [:label label (when-not read-only? [pencil nil])]
      [:br]
      [:textarea]])))

(defn outcome-section [id entry]
  [:section.outcome [:header [:h4 "Outcome"]]
   [:div.field-grid
    [entry-field id entry :string :first-outcome "First Outcome"]
    [entry-field id entry :string :final-outcome "Final Outcome"]
    [entry-field id entry :boolean :scholarship? "Scholarship"]
    (when (-> entry :college-list-entry/scholarship?)
      [:<>
      [entry-field id entry :string :scholarship-amount "Scholarship Amount"]
      [entry-field id entry :string :scholarship-name "Scholarship Name"]])
    [entry-field id entry :boolean :matriculation? "Matriculation"]]])

(defn college-details-tr [id type list-name entry]
  [:tr
   [:td.college-details {:colSpan 7}
    [:header
     [category-h5 id entry list-name]
     (when (#{:account-type/superuser :account-type/counselor} type)
       [:a.button.secondary {:on-click #(rf/dispatch
                                          [:remove-college
                                           id (-> entry
                                                  :college-list-entry/college
                                                  :db/id)])}
        "Remove College"])]
    [:div.standard-fields
     [:div.one-line-fields
      [highlights-section id type entry]
      [:hr]
      [admissions-section id entry]
      [:hr]
      [financial-aid-section id entry]]
     [:div.notes-and-outcomes
      [entry-field id entry :notes :student-notes "Student's Notes"]
      [entry-field id entry :notes :counselor-notes "Counselor's Notes"
       (not (#{:account-type/superuser :account-type/counselor} type))]
      [outcome-section id entry]]]
    [custom-fields-section id entry]]])

(defn college-row-or-rows [id type list-name entry]
  (let [college-id (-> entry :college-list-entry/college :db/id)
        {:keys [college-list-entry/eliminated? college-list-entry/date-added]} entry
        {:keys [college/name college/enrollment college/admit-rate
                college/average-sat college/average-act]
         :as college} @(rf/subscribe [::data/college college-id])
        expanded? @(rf/subscribe [::data/college-expanded? college-id])
        click-attrs {:on-click #(rf/dispatch
                                  [:toggle-college-expanded college-id])
                     :style {:cursor "pointer"}}
        class (if expanded? "expanded" "collapsed")
        class (if eliminated? (str class " eliminated") class)
        tr-attrs (assoc (when-not expanded? click-attrs) :class class)]
    [:<>
     [:tr tr-attrs
      [:td.college-name (when expanded? click-attrs)
       (if expanded? "▾" "▸") " "
       name]
      [:td (maybe-link admit-rate)]
      [:td (maybe-link average-sat)]
      [:td (maybe-link average-act)]
      [:td (maybe-link enrollment)]
      [:td (if date-added (.toLocaleDateString date-added) " ")]
      [:td (if (:college-list-entry/visited? entry) "Yes" "No")]
      [:td (when (:college-list-entry/early-decision? entry)
             [:span.flag "ED"])]]
     (when expanded?
       [college-details-tr id type list-name entry])]))

(defn college-list [id type list-name entries]
  (let [show-eliminated? @(rf/subscribe [::data/show-eliminated-colleges?])
        entries (if show-eliminated?
                  entries
                  (remove :college-list-entry/eliminated? entries))]
    (when (not-empty entries)
      [:div.college-list
       [:h5 list-name]
       [:table
        [:thead>tr
         [:th "name"]
         [:th "admit rate"]
         [:th "mid-50% SAT"]
         [:th "mid-50% ACT"]
         [:th "enrollment"]
         [:th "Date Added"]
         [:th "visit"]
         [:th ""]]
        [:tbody
         (for [entry entries]
           ^{:key (-> entry :college-list-entry/college :db/id)}
           [college-row-or-rows id type list-name entry])]]])))

(defn college-list-section
  [type {:keys [db/id student/college-lists] :as student}]
  (let [{:keys [high-reach reach target likely uncategorized]} college-lists
        modal-type :modal-type/add-college
        show-modal-event [::events/set-modal-visibility true modal-type nil]
        visible? @(rf/subscribe [::data/show-eliminated-colleges?])
        change-visibility #(rf/dispatch
                             [::toggle-eliminated-colleges-visibility])]
    [:section#college-list.college-list
     [:header
      [:h3 "College List"]
      [:div.controls
       [:label
        [:input#eliminated-visibility-toggle {:type "checkbox"
                                              :default-checked visible?
                                              :on-change change-visibility}]
        " Show Eliminated Colleges"]
       (when (#{:account-type/superuser :account-type/counselor} type)
         [:a.button.primary {:on-click #(rf/dispatch show-modal-event)}
          "Add a College"])]]
     [college-list id type "High Reaches" high-reach]
     [college-list id type "Reaches" reach]
     [college-list id type "Targets" target]
     [college-list id type "Likelies" likely]
     [college-list id type "Uncategorized" uncategorized]]))
