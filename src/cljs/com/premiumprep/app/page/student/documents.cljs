(ns com.premiumprep.app.page.student.documents
  (:require
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.page.student.comments :refer [collapsible-comment-list]]
   [com.premiumprep.app.page.student.util :refer [dispatch-button sort-by-fn garbage-can-img-data]]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(k/reg-event-db ::show-all-docs
  [(rf/path :page :page-type/student)]
  (fn [_ _] {:show-all-docs? true}))

(k/reg-event-db ::show-fewer-docs
  [(rf/path :page :page-type/student)]
  (fn [_ _] {:show-all-docs? false}))

(defn- date-str [date]
  (let [options #js {:year "numeric", :month "short", :day "numeric"}]
    (.toLocaleDateString date "en-US" options)))

(defn- document [student-id doc]
  (let [{:keys [db/id document/name document/uploaded-at document/link]} doc]
    [:li.document
     [:span.name name]
     [:span.date
      [:span.date-label "Uploaded on: "]
      [:span.date-value [date-str uploaded-at]]]
     [:span.button [:a.button.secondary {:download name
                                         :href     (or link (api-url (str "/document/" id)))
                                         :target "_blank"}
                    "Download"]]
     [:span.icon.delete {:on-click #(rf/dispatch [:delete-document student-id id])}
      [:img {:src garbage-can-img-data
             :width 20, :height 22}]]]))

(defn- document-list [student-id docs]
  [:ul.document-list
   (for [doc docs]
     ^{:key (:db/id doc)} [document student-id doc])])

(defn- show-all-docs-toggle-button [num-docs show-all?]
  [:div.paging
   (cond
     (<= num-docs 5) nil
     show-all? [dispatch-button "more-docs" [::show-fewer-docs] "View Fewer"]
     :else [dispatch-button "more-docs" [::show-all-docs]
            (str "View More (" (- num-docs 5) ")")])])

(defn- collapsible-document-list [student-id docs]
  (let [n (count docs)
        show-all? @(rf/subscribe [::data/show-all-student-docs?])]
    [:<>
     [document-list student-id (if show-all? docs (take 5 docs))]
     [show-all-docs-toggle-button n show-all?]]))

(defn- documents [id docs comments]
  (let [modal-type :modal-type/upload-document
        show-modal-event [::events/set-modal-visibility true modal-type nil]]
    [:div#docs.documents-and-comments
     [:section.documents
      [:header
       [:h3 "Docs"]
       [:div.controls
        [:a.button.primary {:on-click #(rf/dispatch show-modal-event)}
         "Upload a Document"]]]
      [collapsible-document-list id docs]]
     [collapsible-comment-list id :document-list-comments comments]]))

(defn documents-for-student [id student]
  (let [sorted-docs (->> student
                         :student/documents
                         (sort-by (sort-by-fn :document/uploaded-at))
                         reverse)
        sorted-comments (->> student
                             :student/document-list-comments
                             (sort-by (sort-by-fn :comment/created-at)))]
    [documents id sorted-docs sorted-comments]))

