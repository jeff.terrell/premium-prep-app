(ns com.premiumprep.app.page.student.util
  (:require [re-frame.core :as rf]))

(defn maybe-link [s]
  (when s
    (if (re-matches #"https?://.*" s)
      [:a {:href s} "link"]
      s)))

(defn sort-by-fn [key]
  (comp #(.getTime %) key))

(defn dispatch-button [id event label]
  [:a.button.secondary {:id id
                        :on-click #(rf/dispatch event)}
   label])

(def garbage-can-img-data
  (str "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABm"
       "JLR0QA/wD/AP+gvaeTAAAA4UlEQVRIie2WPQ6CQBCF37gWnsHKK9CpiQ2HmITGxoPoQbShIV"
       "k4hloaj2CDZ7CBsRASsvysCvEn4TWbmZe8b/MoWIJFQRBMlFI7AHMAI8O+ATgmSbLyPO/SlD"
       "OwgTKIWwFBtnOVUltbzrDJFBEKw3CajQ4zn4t+FEVOmqYnADMRISKSuiwqDlrrAx4VdaE9My"
       "9KIK117W3aiJkJeOIb/Z3IXHRVYV5Zro9V9zsgZqZiDbb5bVBX6kE9qAf1oLK++pu4dsCJzU"
       "UVyG9LEZFSRtVza52dSwDjFxmxiPhEtDGNO+rlQJ0DAuMtAAAAAElFTkSuQmCC"))
