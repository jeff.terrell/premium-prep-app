(ns com.premiumprep.app.page.student.issues
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.fragment.issue
    :refer [default-sort-key issue-controls-by-name issue-touched-td]]
   [re-frame.core :as rf]))

(def ^:private action-req {:issue-state/needs-discussion "Discussion needed: "
                           :issue-state/counselor-resolved "Resolution reached: "})

(defn sort-issues [issues]
  (sort-by default-sort-key #(compare %2 %1) issues))

(defn issue-controls-for-superuser [issue type]
  [issue-controls-by-name issue type
   [:new :read :needs-discussion :counselor-resolved :closed]])

(defn issue-controls-for-counselor [issue type]
  [issue-controls-by-name issue type
   [:new :read :needs-discussion :counselor-resolved]])

(defn issue-controls
  [type student-id issue]
  [:td.issue-controls
   [(if (= type :account-type/superuser)
      issue-controls-for-superuser
      issue-controls-for-counselor)
    issue type]])

(defn issue-tr
  [student-id type
   {:keys [db/id issue/created-at issue/text issue/state
           issue/action-taken issue/counselor-touched-at]
    :as issue}]
  (let [new? (= :issue-state/new state)
        state->text {:issue-state/new "Unread"
                     :issue-state/read "Read"
                     :issue-state/needs-discussion "Discussion Needed"
                     :issue-state/counselor-resolved "Marked as resolved"
                     :issue-state/closed "Closed"}]
    ^{:key id}
    [:tr.issue (when new? {:class "new"})
     [:td.issue-text
      text
      (when (and action-taken (action-req state))
        [:<>
         [:p [:i (action-req state)] action-taken]])]
     [issue-touched-td issue]
     [:td.issue-state (state->text state)]
     [issue-controls type student-id issue]]))

(defn issues [student-id type]
  (when (#{:account-type/superuser :account-type/counselor} type)
    (let [modal-type :modal-type/create-issue
          show-modal-event [::events/set-modal-visibility true modal-type
                            {:student-id student-id}]]
      [:section#feedback.issues
       [:header
        [:h3 "Feedback"]
        (when (= type :account-type/superuser)
          [:div.controls
           [:a.button.primary {:on-click #(rf/dispatch show-modal-event)}
            "Create Issue"]])]
       (let [issues (sort-issues
                      @(rf/subscribe [::data/issues-for-student student-id]))]
         (if (empty? issues)
           [:div "No feedback for this student"]
           [:table.issue-list
            [:thead
             [:tr
              [:th "Issue text"]
              [:th [:span {:title "by administrator"} "Created at"] " / "
               [:span {:title "by counselor"} "Last updated at"]]
              [:th "Status"]
              [:th "Set Status"]]]
            [:tbody
             (map (partial issue-tr student-id type) issues)]]))])))

