(ns com.premiumprep.app.page.verify-email
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn- status []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :verify-email])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "Loading… ⏳" ; this is an emoji character, in case you can't see it
        error [:div.error [:p error]]
        success? [:div.success [:p "Email successfully verified. "
                                [:a {:href (k/path-for [:login])}
                                 "Go home."]]]
        :else ""))))

(defn verify-email-page []
  [layout/modal "verify-email"
   [:<>
    [:h3 "Verify Email"]
    [status]]])
