(ns com.premiumprep.app.page.reset-password
  (:require
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(k/reg-event-db ::flag-mismatch-error
  [(rf/path :page :page-type/reset-password)]
  (fn [_ _]
    {:mismatch-error true}))

(k/reg-event-db ::clear-mismatch-error
  [(rf/path :page :page-type/reset-password)]
  (fn [_ _]
    {:mismatch-error false}))

(k/reg-event-fx ::submit
  (fn [_ [form-data]]
    (let [p1 (.get form-data "password")
          p2 (.get form-data "confirmation")]
      (if (= p1 p2)
        {:dispatch-n [[::clear-mismatch-error]
                      [:reset-password form-data]]}
        {:dispatch [::flag-mismatch-error]}))))

(defn- missing-token-error []
  [:div.error
   [:p "Error: no token is available at this address."]
   [:p
    "Please ensure that you are at the address included in your password reset email."
    " If so, you may need to "
    [:a {:href (k/path-for [:login])} "send a new password reset email"]
    "."]])

(defn- status []
  (r/with-let [mismatch-error (rf/subscribe [::data/mismatch-error])
               xhr-status (rf/subscribe [::data/xhr :reset-password])]
    (if @mismatch-error
      [:div.error [:p "Error: your passwords don't match."]]
      (let [{:keys [in-flight? success? error]} @xhr-status]
        (cond
         in-flight? "⏳" ; this is an emoji character, in case you can't see it
         error [:div.error [:p error]]
         success? [:div.success [:p "Password successfully reset. Now, "
                                 [:a {:href (k/path-for [:login])}
                                  "login"]
                                 "."]]
         :else "")))))

(defn- reset-password-form [token]
  (let [password-input-opts {:type "password"
                             :placeholder "********"
                             :required true}]
    [:form {:method "post"
            :action (api-url "/reset-password")
            :on-submit #(do
                          (.preventDefault %)
                          (rf/dispatch [::submit (js/FormData. (.-target %))]))}
     [:input {:type "hidden", :name "token", :value token}]
     [:div.input-label "New Password"]
     [:div.input-field [:input (assoc password-input-opts :name "password")]]
     [:div.input-label "Confirm Password"]
     [:div.input-field [:input (assoc password-input-opts :name "confirmation")]]
     [:input {:type "submit", :value "Reset Password"}]
     [status]]))

(defn- get-token []
  (let [sp (new js/URLSearchParams js/window.location.search)]
    (.get sp "token")))

(defn- form-or-missing-token-error []
  (let [token (get-token)]
    (fn []
      (if-not token
        [missing-token-error]
        [reset-password-form token]))))

(defn reset-password-page []
  [layout/modal "reset-password"
   [:<>
    [:h3 "Reset Password"]
    [form-or-missing-token-error]]])
