(ns com.premiumprep.app.page.home
  "This has the same content as the view student page, except that the student
  viewed is hardcoded to be the student who's currently logged in."
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.layout :as layout]
   [com.premiumprep.app.page.student :refer [student-page]]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn home-page []
  (r/with-let [current-id (rf/subscribe [::data/current-student-id])]
    [layout/with-verified-email-check
     (when @current-id
       [student-page @current-id])]))
