(ns com.premiumprep.app.page.login
  (:require
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.fragment.forgot-password-modal
    :refer [forgot-password-modal]]
   [com.premiumprep.app.fragment.xhr :as xhrs]
   [com.premiumprep.app.layout :as layout]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn- header []
  [:header
   [:img#logo {:src "logo.svg"
               :width 241
               :height 51
               :alt "Premium Prep Logo"}]])

(defn- forgot-password-link []
  [:div.forgot-password
   [:a {:on-click #(rf/dispatch [::events/set-modal-visibility true
                                 :modal-type/forgot-password {}])}
    "Forgot your password?"]])

(defn- login-form []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :login])]
    [:form {:method "post"
            :action (api-url "/login")
            :on-submit #(do
                          (.preventDefault %)
                          (rf/dispatch [:login (js/FormData.
                                                 (.-target %))]))}
     [:div.input-label "Your Email"]
     [:div.input-field
      [:input {:type "email", :name "email" :placeholder "email@example.com"}]]
     [:div.input-label "Your Password"]
     [:div.input-field
      [:input {:type "password", :name "password" :placeholder "********"}]]
     [:input {:type "submit", :value "Login"}]
     [xhrs/xhr-status :login @xhr-status]]))

(defn login-page []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :current-user])]
    [:<>
     [header]
     [layout/modal "login"
      [:<>
       [:h3 "Login"]
       (let [{:keys [in-flight? success?]} @xhr-status]
         (cond
           in-flight? "Loading… ⏳"
           success? (rf/dispatch [:navigate-to-authenticated-home])
           :else [:<>  ; nobody is logged in; display the login form
                  [login-form]
                  [forgot-password-link]]))]]
     [forgot-password-modal]]))
