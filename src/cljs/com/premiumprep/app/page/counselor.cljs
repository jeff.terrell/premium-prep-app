(ns com.premiumprep.app.page.counselor
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.student-edit-modal :refer [edit-student-modal]]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.layout :as layout]
   [com.premiumprep.app.page.students :refer [students-table]]
   [com.premiumprep.app.util :as util]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn profile [counselor]
  (let [{:keys [db/id counselor/account student/_counselor]} counselor
        {:keys [account/email account/full-name]} account]
    [:section.profile
     [:div.profile-group
      [:div.name (str "Name: " (or full-name "-"))]]
     [:div.profile-group
      [:div.email (str "Email: " (or email "-"))]]
     [:div.profile-group
      [:div.student-count
       (str "Number of students: " (count _counselor))]]]))

(defn authorized? [current-user-type]
  (and current-user-type
       (= :account-type/superuser current-user-type)))

(defn inactive?
  "A counselor is assumed active until proven otherwise."
  [{:keys [counselor/account] :as counselor}]
  (false? (:account/active? account)))

(defn counselor-page [id]
  (let [counselor (rf/subscribe [::data/counselor id])
        students-xhr-status (rf/subscribe [::data/xhr :students])
        students (rf/subscribe [::data/students-for-counselor id])
        current-user-id (rf/subscribe [::data/current-user-id])
        current-user-type (rf/subscribe [::data/current-user-type])]

    (if-not (authorized? @current-user-type)
      [unauthorized]
      [layout/with-verified-email-check
       (let [name (get-in @counselor
                          [:counselor/account :account/full-name] "")]
         [layout/standard "counselor-page"
          [:article
           [:nav.breadcrumb [:a {:href (k/path-for [:counselors])}
                             "< Counselors"]]
           [:header
            [:h1 name]

            [:div.controls
             (when (= :account-type/superuser @current-user-type)
               (if (inactive? @counselor)
                 [:a.button.secondary
                  (let [account-id (get-in @counselor
                                           [:counselor/account :db/id])]
                    {:on-click (util/reactivate-account-click-fn
                                 :account-type/counselor account-id id)})
                  "Reactivate Account"]
                 [:a.button.secondary
                  (let [account-id (get-in @counselor
                                           [:counselor/account :db/id])
                        deactivate-fn (util/deactivate-account-click-fn
                                        :account-type/counselor account-id id)
                        msg (str "Error: you must first reassign all students"
                                 " to a different counselor.")
                        refusal-fn #(js/window.alert msg)]
                    {:on-click (if (empty? @students)
                                 deactivate-fn
                                 refusal-fn)})
                  "Deactivate Account"]))]]

           (when @counselor
             [:<>
              (when (inactive? @counselor)
                [:p.notice "This counselor's account has been deactivated."])
              [profile @counselor]
              (let [{:keys [in-flight? success? error]} @students-xhr-status]
                (cond
                  in-flight? "⏳"
                  error (if (not-empty error)
                          [:abbr {:title (str "Error: " error)} "❌"]
                          "❌")
                  :else [:div
                         [:h3 (str name "'s Students")]
                         [students-table @students]]))])]
          [edit-student-modal]])])))
