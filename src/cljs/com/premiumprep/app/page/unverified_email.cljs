(ns com.premiumprep.app.page.unverified-email
  (:require
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn- status []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :change-email])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "⏳"
        error [:div.error [:p error]]
        success? [:div.success [:p "Verification email sent."]]
        :else ""))))

(defn- form []
  (r/with-let [email (rf/subscribe [::data/email])]
    [:form {:method "post"
            :action (api-url "/change-email")
            :on-submit #(do
                          (.preventDefault %)
                          (rf/dispatch [:change-email
                                        (js/FormData. (.-target %))]))}
     [:div.input-label "Unverified Email Address"]
     [:div.input [:input {:type "email", :name "display-email"
                          :value @email, :disabled true}]]
     [:input {:type "hidden", :name "email", :value @email}]
     [:input {:type "submit", :value "Re-send Verification Email"}]
     [status]]))

(defn unverified-email-page-inner []
  [:<>
   [:h3 "Unverified Email"]
   [:p "You must verify your email before using the site. "
    "Please check your email for a verification link."]
   [:p "If you can't find the email, you can send another one using the button below."]
   [form]])
