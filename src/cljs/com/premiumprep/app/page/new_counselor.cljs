(ns com.premiumprep.app.page.new-counselor
  (:require
   [clojure.string :as str]
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(defn- submit-form [event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)]
    (rf/dispatch [:create-counselor form-data])))

(defn- error-status [error-message]
  [:div.status.error
   [:p "There was an error with the attempt to create a counselor. The error "
    "response from the server follows:"]
   [:p (interpose [:br] (str/split error-message #"\n"))]])

(defn- success-status [password]
  [:div.status.success
   [:p "Success!"]
   [:p "Please email the counselor the following password: "
    [:tt password]]])

(defn- creation-status []
  (let [create-xhr-status (rf/subscribe [::data/xhr :create-counselor])
        new-counselor-password (rf/subscribe [::data/new-counselor-password])]
    (fn []
      (let [{:keys [in-flight? error success?]} @create-xhr-status]
        (cond
          in-flight? "⏳"
          error [error-status error]
          ;; Note: potential race condition here, if the user restarts a request
          ;; within 3 seconds of a success. Not worrying about it yet though.
          (and (not= @new-counselor-password
                     [::data/new-counselor-password])
               (not (str/blank? @new-counselor-password))) [success-status
                                                            @new-counselor-password])))))

(defn new-counselor-page []
  (let [current-user-type (rf/subscribe [::data/current-user-type])]
    (fn []
      (if-not (= :account-type/superuser @current-user-type)
        [unauthorized]
        [layout/with-verified-email-check
         [layout/standard "new-counselor-page"
          [:h1 "New Counselor"]
          [:form {:method "post"
                  :action (api-url "/counselor")
                  :on-submit submit-form}
           [:div.form-inputs
            [:section
             [:label {:for "email"} "Counselor Email"]
             [:input#email {:type "email", :name "email"
                            :placeholder "email@example.com"}]]
            [:section
             [:label {:for "name"} "Counselor Name"]
             [:input#name {:type "text", :name "name" :placeholder "First Last"}]]
            [:section
             [:label {:for "phone"} "Counselor Phone"]
             [:input#phone {:type "tel", :name "phone"
                            :placeholder "(888) 867-5309"}]]]
           [:div.form-submit
            [:input {:type "submit", :value "Create"}]]]
          [creation-status]]]))))
