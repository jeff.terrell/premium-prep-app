(ns com.premiumprep.app.page.change-password
  (:require
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(k/reg-event-db ::flag-mismatch-error
                [(rf/path :page :page-type/change-password)]
                (fn [_ _]
                  {:mismatch-error true}))

(k/reg-event-db ::clear-mismatch-error
                [(rf/path :page :page-type/change-password)]
                (fn [_ _]
                  {:mismatch-error false}))

(k/reg-event-fx ::submit
                (fn [_ [form-data]]
                  (let [p1 (.get form-data "password")
                        p2 (.get form-data "confirmation")]
                    (if (= p1 p2)
                      {:dispatch-n [[::clear-mismatch-error]
                                    [:change-password form-data]]}
                      {:dispatch [::flag-mismatch-error]}))))

(defn- status []
  (r/with-let [mismatch-error (rf/subscribe [::data/mismatch-error2])
               xhr-status (rf/subscribe [::data/xhr :change-password])]
    (if @mismatch-error
      [:div.error [:p "Error: your passwords don't match."]]
      (let [{:keys [in-flight? success? error]} @xhr-status]
        (cond
          in-flight? "⏳" ; this is an emoji character, in case you can't see it
          error [:div.error [:p error]]
          success? [:div.success
                    [:p "Password successfully changed. "
                     [:a {:href "#"
                          :on-click #(rf/dispatch
                                       [:navigate-to-authenticated-home])}
                      "Go home."]]]
          :else "")))))

(defn- change-password-form [email]
  (let [password-input-opts {:type "password"
                             :placeholder "********"
                             :required true}]
    [:<>
     [:form {:method "put"
             :action (api-url "/change-password")
             :on-submit #(do
                           (.preventDefault %)
                           (rf/dispatch [::submit
                                         (js/FormData. (.-target %))]))}
      [:div.input-label "Current Email"]
      [:div.input-field [:input {:type "email", :name "current-email"
                           :value email, :disabled true}]]
      [:div.input-label "Password"]
      [:div.input-field [:input (assoc password-input-opts :name "password")]]
      [:div.input-label "Confirm Password"]
      [:div.input-field [:input (assoc password-input-opts :name "confirmation")]]
      [:input {:type "submit", :value "Change Password"}]]
     [status]]))

(defn change-password-page []
  (r/with-let [email (rf/subscribe [::data/email])]
    (if-not @email
      [unauthorized]
      [layout/modal "change-password"
       [:<>
        [:h3 "Change Password"]
        [change-password-form @email]]])))
