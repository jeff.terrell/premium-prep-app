(ns com.premiumprep.app.page.issues
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.issue
    :refer [default-sort-key issue-controls-by-name issue-touched-td]]
   [com.premiumprep.app.fragment.table :refer [table]]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.fragment.update-issue-modal :refer [update-issue-modal]]
   [com.premiumprep.app.layout :as layout]
   [com.premiumprep.app.xhr :as xhr]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(def ^:private state->label
  {:issue-state/new "new"
   :issue-state/read "read"
   :issue-state/needs-discussion "discussion needed"
   :issue-state/counselor-resolved "resolved"
   :issue-state/closed "closed"})

(def ^:private action-req {:issue-state/needs-discussion "Discussion needed: "
                           :issue-state/counselor-resolved "Resolution reached: "})

(defn- issue->student [students-by-id issue]
  (-> issue :issue/student :db/id students-by-id))

(defn- issue->student-name [students-by-id issue]
  (->> issue
       (issue->student students-by-id)
       :student/account
       :account/full-name))

(defn- issue->counselor-name [students-by-id issue]
  (->> issue
       (issue->student students-by-id)
       :student/counselor
       :counselor/account
       :account/full-name))

(defn- issue->student-grad-year [students-by-id issue]
  (->> issue
       (issue->student students-by-id)
       :student/graduation-year))

(defn- state-td [issue]
  [:td.issue-state (-> issue :issue/state state->label)])

(defn- counselor-td [students-by-id issue]
  (let [{:keys [db/id counselor/account]} (-> issue :issue/student :db/id
                                              students-by-id :student/counselor)
        href (k/path-for [:counselor {:id id}])]
    [:td [:a {:href href} (:account/full-name account)]]))

(defn- student-td [students-by-id issue]
  (let [student-id (-> issue :issue/student :db/id)
        {:keys [account/full-name]} (-> student-id students-by-id
                                        :student/account)
        href (k/path-for [:student {:id student-id}])]
    [:td [:a {:href href} full-name]]))

(defn- issue-text-td [issue]
  (let [text (-> issue :issue/text)
        action-taken (-> issue :issue/action-taken)
        state (-> issue :issue/state)]
   [:td.issue-text
    text
    (when (and action-taken (action-req state))
      [:<>
       [:p [:i (action-req state)] action-taken]])]))

(defn- issue-controls-td
  [type issue]
  [:td.issue-controls
   [issue-controls-by-name issue type
    [:new :read :needs-discussion :counselor-resolved :closed]]])

(defn issues-page []
  (let [issues (rf/subscribe [::data/issues])
        students (rf/subscribe [::data/students])
        current-user-type (rf/subscribe [::data/current-user-type])]
    (when (and @issues @students) ; wait for students and issues to load
      (if-not (= :account-type/superuser @current-user-type)
        [unauthorized]
        [layout/standard "issues-page"
         [:<>
          [:header [:h1 "All Feedback"]]
          (if (empty? @issues)
            [:div "No feedback yet."]
            [table
             {:table-class "issues"
              :page-type   :page-type/issues
              :columns     {:counselor {:label       "Counselor"
                                        :sort-key-fn (partial issue->counselor-name
                                                              @students)
                                        :->markup    (partial counselor-td @students)}
                            :student   {:label       "Student"
                                        :sort-key-fn (partial issue->student-name
                                                              @students)
                                        :->markup    (partial student-td @students)}
                            :grad-year {:label       "Grad year"
                                        :sort-key-fn (partial issue->student-grad-year
                                                              @students)}
                            :text      {:label       "Text"
                                        :sort-key-fn :issue/text
                                        :->markup    issue-text-td}
                            :timestamp {:label       "Created/Updated at"
                                        :sort-key-fn #(or (:issue/counselor-touched-at
                                                            %)
                                                          (:issue/created-at %))
                                        :->markup    issue-touched-td}
                            :state     {:label       "Status"
                                        :sort-key-fn default-sort-key
                                        :->markup    state-td}
                            :controls  {:label    "Set Status"
                                        :->markup (partial issue-controls-td
                                                           @current-user-type)}}}
             (vals @issues)])
          [update-issue-modal]]]))))
