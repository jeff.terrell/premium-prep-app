(ns com.premiumprep.app.page.students
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.fragment.student-edit-modal :refer [edit-student-modal]]
   [com.premiumprep.app.fragment.table :refer [table]]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.layout :as layout]
   [com.premiumprep.app.xhr :as xhr]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn student-name-td
  [{:keys [db/id student/account] :as _}]
  [:td.student-name
   [:a {:href (k/path-for [:student {:id id}])}
    (:account/full-name account)]])

(defn counselor-td [student]
  (let [counselor (:student/counselor student)
        {:keys [db/id counselor/account]} counselor
        {:keys [account/full-name]} account]
    [:td.counselor-name [:a {:href (k/path-for [:counselor {:id id}])}
                         full-name]]))

(defn since-td
  [{:keys [student/joined-at]}]
  (let [;; e.g. 7/4/19 2:43 PM
        opts #js {:dateStyle "short", :timeStyle "short"}
        ->str #(.toLocaleString % #js {} opts)
        str (if joined-at (->str joined-at) "-")]
    [:td str]))

(defn grad-td
  [{:keys [student/graduation-year]}]
  [:td (or graduation-year "-")])

(defn student-controls [student]
  (let [current-user-type (rf/subscribe [::data/current-user-type])
        id (:db/id student)]
    [:td.controls
     ;; TODO: consider passing this in as a prop instead, for performance
     (when (= @current-user-type :account-type/superuser)
       [:a {:on-click #(rf/dispatch [::events/open-edit-student-modal id])}
        "Edit"])
     [:a {:href (k/path-for [:student {:id id}])} "View >"]]))

(def columns
  {:name          {:label "student name"
                   :sort-key-fn #(-> % :student/account :account/full-name)
                   :->markup student-name-td}
   :counselor     {:label "counselor"
                   :sort-key-fn #(-> % :student/counselor :counselor/account
                                     :account/full-name)
                   :->markup counselor-td}
   :since         {:label "student since"
                   :sort-key-fn :student/joined-at
                   :->markup since-td}
   :grad-year     {:label "hs grad year"
                   :sort-key-fn :student/graduation-year
                   :->markup grad-td}
   :controls      {:label ""
                   :->markup student-controls}})

(def superuser-columns
  [:name :counselor :since :grad-year :controls])

(def counselor-columns
  [:name :since :grad-year :last-comment :controls])

(defn table-opts [current-user-type page-type]
  {:columns (select-keys columns
                         (if (= :account-type/superuser current-user-type)
                           superuser-columns
                           counselor-columns))
   :page-type page-type
   :table-class "students"
   :tr-class-fn (fn [student]
                  (when-not (-> student :student/account :account/active?)
                    "inactive"))})

(defn students-table
  ([students] (students-table :page-type/counselor counselor-columns students))
  ([cols students] (students-table :page-type/students cols students))
  ([page-type cols students]
   (when (not-empty students)
     (let [current-user-type @(rf/subscribe [::data/current-user-type])]
       [table (table-opts current-user-type page-type) students]))))

(defn active-status-filter []
  (let [filter-value (rf/subscribe [::data/students-active-status-filter])]
    [:label
     [:select.active-status-filter
      {:defaultValue (when @filter-value (name @filter-value))
       :on-change #(rf/dispatch [::events/set-students-active-status-filter
                                 (-> % .-target .-value keyword)])}
      [:option {:value "all"} "Show: All Students"]
      [:option {:value "active-only"} "Show: Active Students"]]]))

(defn filtered-students-table [cols students]
  (let [filter-val (rf/subscribe [::data/students-active-status-filter])]
    [students-table cols
     (filter (if (= :active-only @filter-val)
               (comp :account/active? :student/account)
               identity)
             students)]))

(defn students-main-for-superuser [students]
  [:<>
   [:header
    [:h1 "All Students"]
    [:div.controls
     [active-status-filter]
     [:a.button {:href (k/path-for [:new-student])} "New Student"]]]
   (when (not-empty students)
     [filtered-students-table superuser-columns (vals students)])])

(defn students-main-for-counselor [students]
  [:<>
   [:header [:h1 "My Students"]]
   (when (not-empty students)
     (students-table counselor-columns (vals students)))])

(defn students-main [current-user-type students]
  (if (= :account-type/superuser current-user-type)
    [students-main-for-superuser students]
    [students-main-for-counselor students]))

(defn students-page []
  (let [students (rf/subscribe [::data/students])
        current-user-type (rf/subscribe [::data/current-user-type])]
    (fn []
      (if-not (#{:account-type/superuser :account-type/counselor}
                @current-user-type)
        [unauthorized]
        [layout/with-verified-email-check
         [layout/standard "students-page"
          [students-main @current-user-type @students]
          [edit-student-modal]]]))))
