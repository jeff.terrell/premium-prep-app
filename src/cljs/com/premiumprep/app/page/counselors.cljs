(ns com.premiumprep.app.page.counselors
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.fragment.counselor-edit-modal :refer [edit-counselor-modal]]
   [com.premiumprep.app.fragment.table :refer [table]]
   [com.premiumprep.app.fragment.unauthorized :refer [unauthorized]]
   [com.premiumprep.app.fragment.xhr :refer [xhr-status]]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [re-frame.core :as rf]))

(defn counselor-controls [counselor]
  (let [id (:db/id counselor)]
    [:td.controls
     [:a {:on-click #(rf/dispatch [::events/open-edit-counselor-modal id])}
      "Edit"]
     [:a {:href (k/path-for [:counselor {:id id}])} "View >"]]))

(def ^:private columns
  {:name {:label "counselor"
          :sort-key-fn #(-> % :counselor/account :account/full-name)
          :->markup (fn [{id :db/id, {name :account/full-name} :counselor/account}]
                      [:td.counselor-name
                       [:a {:href (k/path-for [:counselor {:id id}])} name]])}
   :#students {:label "number of students"
               :sort-key-fn #(-> % :student/_counselor count)
               :->markup (fn [counselor]
                           [:td.num-students
                            (-> counselor :student/_counselor count)])}
   :controls {:label "actions"
              :->markup counselor-controls}})

(def ^:private table-opts
  {:columns columns
   :page-type :page-type/counselors
   :table-class "counselors"
   :tr-class-fn (fn [counselor]
                  (when-not (-> counselor :counselor/account :account/active?)
                    "inactive"))})

(defn counselors-page []
  (let [counselors @(rf/subscribe [::data/counselors-vec])
        counselors-xhr-status @(rf/subscribe [::data/xhr :counselors])
        current-user-type @(rf/subscribe [::data/current-user-type])]
    (if-not (= :account-type/superuser current-user-type)
      [unauthorized]
      (when (and counselors (not= counselors [::data/counselors-vec]))
        [layout/with-verified-email-check
         [layout/standard "counselors-page"
          [xhr-status :counselors counselors-xhr-status]
          [:header
           [:h1 "Counselors"]
           (when (= :account-type/superuser current-user-type)
             [:a.button {:href (k/path-for [:new-counselor])} "Invite Counselor"])]
          (if (empty? counselors)
            [:div "No counselors yet."]
            [table table-opts counselors])
          [edit-counselor-modal]]]))))
