(ns com.premiumprep.app.page.edit-student
  (:require
   [clojure.string :as str]
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.student-form
    :refer [basic-info college-planning-app-info contact-info grades misc
            parent-info status test-scores]]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn- bad-student-error []
  [:div.error
   [:p "Error: the given student could not be found."]])

(defn- error-status [_ error-message]
  [:div.status.error {:style {:margin-top "1rem"}}
   [:p "There was an error. The error response from the server follows:"]
   [:p (interpose [:br] (str/split error-message #"\n"))]])

(defn- success-status [student]
  [:div.status.success {:style {:margin-top "1rem"}}
   [:p "Update successful! Now, "
    [:a {:href (k/path-for [:student {:id (:db/id student)}])}
     "go back"]
    "."]])

(defn edit-student-form [student]
  (let [{:keys [db/id]} student]
    [layout/standard "edit-student-page"
     [:h1 "Edit Student Profile"]
     [:form.student {:method "post"
                     :action (api-url (str "/student/" id "/edit"))
                     :on-submit #(do
                                   (.preventDefault %)
                                   (rf/dispatch [:update-student-profile id
                                                 (js/FormData. (.-target %))]))}
      [:input {:type "hidden", :name "student-id", :value id}]
      [basic-info student]
      [grades student]
      [test-scores student]
      [contact-info student false]
      [parent-info student 1 true]
      [parent-info student 2 false]
      [college-planning-app-info student]
      [misc student]
      [:div.form-submit
       [:input {:type "submit", :value "Update"}]]
      [status student :update-student-profile success-status error-status]]]))

(defn edit-student-page [id]
  (let [status @(rf/subscribe [::data/xhr :student])
        {:keys [in-flight? success? error]} status
        student @(rf/subscribe [::data/student id])]
    (cond
      in-flight? "⏳ Loading…"
      error [bad-student-error]
      :else [edit-student-form student])))
