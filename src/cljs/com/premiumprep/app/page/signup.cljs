(ns com.premiumprep.app.page.signup
  (:require
   [clojure.string :as str]
   [com.premiumprep.app.config :refer [api-url]]
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.inputs :refer [field]]
   [com.premiumprep.app.fragment.student-form
    :refer [basic-info college-planning-app-info contact-info grades misc
            parent-info status test-scores]]
   [com.premiumprep.app.layout :as layout]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn- bad-token-error []
  [:div.error
   [:p "Error: your signup token could not be found."]
   [:p "Please ensure that you are at the address included in your welcome "
    "email."]])

(defn- error-status [_ error-message]
  [:div.status.error {:style {:margin-top "1rem"}}
   [:p "There was an error with the attempt to sign up. The error "
    "response from the server follows:"]
   [:p (interpose [:br] (str/split error-message #"\n"))]])

(defn- success-status [_]
  [:div.status.success {:style {:margin-top "1rem"}}
   [:p "Signup successful! Now, "
    [:a {:href (k/path-for [:login])}
     "login"]
    "."]])

(defn password []
  [:<>
   [:h3 "Password"]
   [field nil "password" "password" "Password" true nil]
   [field nil "confirmation" "password" "Confirm Password" true nil]])

(defn- signup-form []
  (r/with-let [token (rf/subscribe [::data/signup-token])
               email (rf/subscribe [::data/signup-email])]
    [layout/standard "signup-page"
     [:h1 "Set Up Your Premium Prep Account"]
     [:form.student {:method "post"
                     :action (api-url "/signup")
                     :on-submit #(do
                                   (.preventDefault %)
                                   (rf/dispatch [:signup
                                                 (js/FormData. (.-target %))]))}
      [:input {:type "hidden", :name "token", :value @token}]
      [basic-info nil]
      [contact-info {:student/account {:account/email @email}} true]
      [password nil]
      [college-planning-app-info nil]
      [grades nil]
      [test-scores nil]
      [parent-info nil 1 true]
      [parent-info nil 2 false]
      [misc nil]
      [:div.form-submit
       [:input {:type "submit", :value "Create"}]]
      [status nil :signup success-status error-status]]]))

(defn signup-page []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :verify-token])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "⏳ Loading…"
        error [bad-token-error]
        success? [signup-form]
        :else ""))))
