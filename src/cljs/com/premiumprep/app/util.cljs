(ns com.premiumprep.app.util
  (:require
   [re-frame.core :as rf]))

(defn by-id
  "Given an id function and a seq of xs, return a map from (id x) to x. If there
  are multiple xs with the same ID, the last one wins."
  [f xs]
  (into {}
        (for [x xs]
          [(f x) x])))

(defn deactivate-account-click-fn [account-type account-id id]
  (fn [_click-event]
    (let [confirmation-text "Are you sure you want to deactivate this account?"
          confirmed? (js/window.confirm confirmation-text)]
      (when confirmed?
        (rf/dispatch [:deactivate-account account-type account-id id])))))

(defn reactivate-account-click-fn [account-type account-id id]
  (fn [_click-event]
    (let [confirmation-text "Are you sure you want to reactivate this account?"
          confirmed? (js/window.confirm confirmation-text)]
      (when confirmed?
        (rf/dispatch [:reactivate-account account-type account-id id])))))
