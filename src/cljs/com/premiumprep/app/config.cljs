(ns com.premiumprep.app.config)

(goog-define DEBUG false)
(goog-define PREMIUMPREP_API_HOST "")

(js/console.log "api-host" PREMIUMPREP_API_HOST)

(defn api-url [path]
  (str PREMIUMPREP_API_HOST "/api" path))
