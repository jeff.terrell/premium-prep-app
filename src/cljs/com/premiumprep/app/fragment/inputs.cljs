(ns com.premiumprep.app.fragment.inputs
  (:require
   cljsjs.moment
   [com.premiumprep.app.model.student-fields-common :refer [fields]]))

(defn- value
  [student name type]
  (let [{:keys [path]} (get fields name)
        raw-val (get-in student path)]
    (cond
      (= type "date") (when raw-val
                        (.format (js/moment.utc raw-val) "YYYY-MM-DD"))
      (= type "month") (when raw-val
                         (.format (js/moment.utc raw-val) "YYYY-MM"))
      :else raw-val)))

(defn field
  ([student name type label]
   (field student name type label false))
  ([student name type label required?]
   (field student name type label required? nil))
  ([student name type label required? placeholder]
   (field student name type label required? placeholder {}))
  ([student name type label required? placeholder extra-opts]
   (field student name type label required? placeholder extra-opts nil))
  ([student name type label required? placeholder extra-opts abbr]
   [:<>
    [:label {:for name} (if required? (str label " *") label)]
    (let [value (value student name type)
          input [:input (merge {:id name, :type type, :name name
                                :defaultValue value, :placeholder placeholder,
                                :required required?}
                               extra-opts)]]
      (if abbr
        [:abbr {:title abbr} input]
        input))]))

(defn textarea [student name label]
  [:<>
   [:label {:for name} label]
   [:textarea {:id name, :name name, :defaultValue (value student name "text")}]])
