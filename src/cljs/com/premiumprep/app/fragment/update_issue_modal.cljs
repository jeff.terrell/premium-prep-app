(ns com.premiumprep.app.fragment.update-issue-modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn- submit-update-issue-form [issue control event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)
        id (:db/id issue)]
    (rf/dispatch [:update-issue id control form-data])))

(def control->label
  {:needs-discussion "Discussion needed: "
   :counselor-resolved "Resolution reached: "})

(defn- update-issue-main-section [issue control]
  (let [{:keys [db/id issue/action-taken issue/text]} issue
        control-name (control->label control)]
   [:section
    [:div
     [:p control-name]
     [:textarea {:name "action-taken", :required true} action-taken]]]))

(defn- status []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :update-issue])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "⏳" ; this is an emoji character, in case you can't see it
        error [:div.error [:p error]]
        success?
        [:div.success [:p "Issue Updated successfully."]]
        :else ""))))

(defn- update-issue-footer []
  (letfn [(on-save-click [e]
            (let [submit-el (js/document.getElementById "update-issue-submit")]
              (.click submit-el)))]
    [:footer
     [:div.buttons
      [:a.button.secondary {:on-click close} "Cancel"]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input#update-issue-submit {:type "submit", :style {:display "none"}}]
      [:a.button {:on-click on-save-click} "Update"]]
     [status]]))

(defn- update-issue-panel [issue control]
  [:form#update-issue-modal.modal-form {:on-submit
                                        (partial submit-update-issue-form issue control)}
   [header "Update Issue"]
   [update-issue-main-section issue control]
   [update-issue-footer]])

(defn update-issue-modal []
  (r/with-let [issue (rf/subscribe [::data/update-issue-modal-issue])
               control (rf/subscribe [::data/update-issue-modal-control])]
    [modal :modal-type/update-issue
    [update-issue-panel @issue @control]]))
