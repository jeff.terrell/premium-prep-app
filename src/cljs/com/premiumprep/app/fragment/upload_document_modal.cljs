(ns com.premiumprep.app.fragment.upload-document-modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn- submit-form [event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)
        student-id (.get form-data "student-id")]
    (rf/dispatch [:upload-document student-id form-data])))

(defn- main-section []
  [:section
   [:div
    [:p "Document Name"]
    [:input {:type "text", :name "name", :required true}]]
   [:div
    [:p "Document Link"]
    [:input {:type "text", :name "link"}]]
   [:div
    [:p "Document File"]
    [:input {:type "file", :name "file"}]]])

(defn- status []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :upload-document])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "⏳" ; this is an emoji character, in case you can't see it
        error [:div.error [:p error]]
        success?
        [:div.success [:p "Document uploaded successfully."]]
        :else ""))))

(defn- footer [id]
  (let [submit-id "upload-document-submit"
        on-submit-click (fn [_] (.click (js/document.getElementById submit-id)))]
    [:footer
     [:div.buttons
      [:a.button.secondary {:on-click close} "Cancel"]
      [:input {:type "hidden", :name "student-id", :value id}]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input {:id submit-id, :type "submit", :style {:display "none"}}]
      [:a.button {:on-click on-submit-click} "Submit"]]
     [status]]))

(defn- upload-document-panel [id]
  [:form#upload-document-modal.modal-form {:on-submit submit-form}
   [header "Upload Document"]
   [main-section]
   [footer id]])

(defn upload-document-modal [id]
  [modal :modal-type/upload-document
   [upload-document-panel id]])
