(ns com.premiumprep.app.fragment.add-college-modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn- submit-form [event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)
        student-id (.get form-data "student-id")]
    (rf/dispatch [:add-college-list-entry student-id form-data])))

(defn- college-select-box [student-id]
  (let [colleges (rf/subscribe [::data/colleges])
        college-id-already-in-list? @(rf/subscribe
                                       [::data/college-ids-in-student-list
                                        student-id])]
    [:select {:name "college-id", :required true}
     [:option {:value ""} "Select College"]
     (for [{:keys [db/id college/name]} (sort-by :college/name (vals @colleges))
           :when (not (college-id-already-in-list? id))]
       ^{:key id}
       [:option {:value id} name])]))

(defn- category-radio [value label]
  (let [id (str "add-college-category-" value)]
    [:<>
     [:input {:id id
              :name "category"
              :type "radio"
              :value value
              :required true}]
     [:label {:for id} label]
     [:br]]))

(defn- main-section [student-id]
  [:section
   [:div
    [:p "College"]
    [college-select-box student-id]]
   [:div
    [:p "Category"]
    [category-radio "high-reach" "High Reach"]
    [category-radio "reach" "Reach"]
    [category-radio "target" "Target"]
    [category-radio "likely" "Likely"]
    [category-radio "uncategorized" "Uncategorized"]]])

(defn- status [student-id]
  (let [xhr-status (rf/subscribe [::data/xhr :add-college-list-entry student-id])
        {:keys [in-flight? success? error]} @xhr-status]
    (cond
      in-flight? "⏳" ; this is an emoji character, in case you can't see it
      error [:div.error [:p error]]
      success?
      [:div.success [:p "College added successfully."]]
      :else "")))

(defn- footer [student-id]
  (let [submit-id "add-college-submit"
        on-submit-click (fn [_] (.click (js/document.getElementById submit-id)))]
    [:footer
     [:div.buttons
      [:a.button.secondary {:on-click close} "Cancel"]
      [:input {:type "hidden", :name "student-id", :value student-id}]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input {:id submit-id, :type "submit", :style {:display "none"}}]
      [:a.button {:on-click on-submit-click} "Submit"]]
     [status student-id]]))

(defn- add-college-panel [student-id]
  [:form#add-college-list-entry-form.modal-form {:on-submit submit-form}
   [header "Add a College"]
   [main-section student-id]
   [footer student-id]])

(defn add-college-modal [student-id]
  [modal :modal-type/add-college
   [add-college-panel student-id]])
