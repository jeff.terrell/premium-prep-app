(ns com.premiumprep.app.fragment.student-edit-modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.events :as events]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [com.premiumprep.app.fragment.select-counselor :refer [select-counselor]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn submit-edit-student-form [id event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)]
    (rf/dispatch [:update-student id form-data])))

(defn active-option [student active? label]
  (let [id (str "active-" active?)
        student-active? (get-in student [:student/account :account/active?])
        selected? (= active? student-active?)
        opts {:id id
              :type "radio"
              :name "active" ; ha
              :value (str active?)
              :defaultChecked selected?}]
    [:span.active-option
     [:input opts]
     [:label {:for id} label]]))

(defn active-options [student]
  [:div.inline-radio-options
   [active-option student true "Active"]
   [active-option student false "Inactive"]])

(defn edit-student-main-section [student]
  [:section
   [:div
    [:p "Account Status"]
    [active-options student]]
   [:div
    [:p "Account Email"]
    [:input {:type "text", :name "email"
             :defaultValue (get-in student
                                   [:student/account :account/email])}]]
   [:div
    [:p "Counselor"]
    [select-counselor (get-in student [:student/counselor :db/id])]]])

(defn edit-student-footer []
  (letfn [(on-save-click [e]
            (let [submit-el (js/document.getElementById "edit-student-submit")]
              (.click submit-el)))]
    [:footer>div.buttons
     [:a.button.secondary {:on-click close} "Cancel"]
     ;; Safari doesn't yet implement the form.requestSubmit() method, so this
     ;; is a workaround to trigger form submission from Javascript _including_
     ;; the submit event itself, which we respond to in the :on-submit
     ;; attribute of the enclosing form element:
     [:input#edit-student-submit {:type "submit", :style {:display "none"}}]
     [:a.button {:on-click on-save-click} "Save"]]))

(defn edit-student-panel [id]
  (r/with-let [student (rf/subscribe [::data/student id])]
    [:form#edit-student-modal.modal-form {:on-submit
                                          (partial submit-edit-student-form id)}
     [header "Edit Student"]
     [edit-student-main-section @student]
     [edit-student-footer]]))

(defn edit-student-modal []
  (r/with-let [student-id (rf/subscribe [::data/edit-student-modal-student-id])]
    [modal :modal-type/edit-student
     [edit-student-panel @student-id]]))
