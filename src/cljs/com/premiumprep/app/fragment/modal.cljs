(ns com.premiumprep.app.fragment.modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.events :as events]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn close [_e]
  (rf/dispatch [::events/set-modal-visibility false]))

(def ignore-clicks
  {:on-click (fn [e] (.stopPropagation e))})

(defn header [text]
  [:header
   [:h3 text]
   [:span.close-modal-x {:on-click close} "⨉"]])

(defn modal [modal-type child-hiccup]
  (r/with-let [visible? (rf/subscribe [::data/modal-visible? modal-type])]
    (when @visible?
      [:aside.modal {:on-click close}
       [:div.foreground ignore-clicks child-hiccup]])))
