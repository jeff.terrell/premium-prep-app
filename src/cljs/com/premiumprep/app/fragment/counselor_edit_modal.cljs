(ns com.premiumprep.app.fragment.counselor-edit-modal
  (:require [clojure.string :as str]
            [com.premiumprep.app.data :as data]
            [com.premiumprep.app.events :as events]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn submit-edit-counselor-form [id event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)]
    (rf/dispatch [:update-counselor id form-data])))

(defn active-option [counselor active? label]
  (let [id (str "active-" active?)
        counselor-active? (get-in counselor [:counselor/account
                                             :account/active?])
        selected? (= active? counselor-active?)
        opts {:id id
              :type "radio"
              :name "active" ; ha
              :value (str active?)
              :defaultChecked selected?}]
    [:span.active-option
     [:input opts]
     [:label {:for id} label]]))

(defn active-options [counselor]
  [:div.inline-radio-options
   [active-option counselor true "Active"]
   [active-option counselor false "Inactive"]])

(defn edit-counselor-main-section [counselor]
  [:section
   [:div
    [:p "Account Status"]
    [active-options counselor]]
   [:div
    [:p "Account Email"]
    [:input {:type "text", :name "email"
             :defaultValue (get-in counselor
                                   [:counselor/account :account/email])}]]
   [:div
    [:p "Full Name"]
    [:input {:type "text", :name "name"
             :defaultValue (get-in counselor
                                   [:counselor/account :account/full-name])}]]
   [:div
    [:p "Preferred Name"]
    [:input {:type "text", :name "preferred-name"
             :defaultValue (get-in counselor
                                   [:counselor/account
                                    :account/preferred-name])}]]
   [:div
    [:p "Mobile #"]
    [:input {:type "tel", :name "mobile"
             :defaultValue (get-in counselor
                                   [:counselor/account :account/mobile])}]]])

(defn- status []
  (let [xhr-status (rf/subscribe [::data/xhr :update-counselor])
        {:keys [in-flight? error]} @xhr-status]
    (cond
      in-flight? "⏳" ; this is an emoji character, in case you can't see it
      error [:div.status.error [:p (interpose [:br] (str/split error #"\n"))]])))

(defn edit-counselor-footer []
  (letfn [(on-save-click [e]
            (let [submit-el (js/document.getElementById "edit-counselor-submit")]
              (.click submit-el)))]
    [:footer
     [:div.buttons
      [:a.button.secondary {:on-click close} "Cancel"]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input#edit-counselor-submit {:type "submit", :style {:display "none"}}]
      [:a.button {:on-click on-save-click} "Save"]]
     [status]]))

(defn edit-counselor-panel [id]
  (r/with-let [counselor (rf/subscribe [::data/counselor id])]
    [:form#edit-counselor-modal.modal-form {:on-submit
                                            (partial submit-edit-counselor-form
                                                     id)}
     [header "Edit Counselor"]
     [edit-counselor-main-section @counselor]
     [edit-counselor-footer]]))

(defn edit-counselor-modal []
  (r/with-let [counselor-id (rf/subscribe
                              [::data/edit-counselor-modal-counselor-id])]
    [modal :modal-type/edit-counselor
     [edit-counselor-panel @counselor-id]]))
