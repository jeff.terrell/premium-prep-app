(ns com.premiumprep.app.fragment.create-issue-modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn- submit-form [event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)
        student-id (.get form-data "student-id")]
    (rf/dispatch [:create-issue student-id form-data])))

(defn- main-section []
  [:section
   [:div
    [:p "Issue Text"]
    [:textarea {:name "text", :required true}]]])

(defn- status []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :create-issue])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "⏳" ; this is an emoji character, in case you can't see it
        error [:div.error [:p error]]
        success?
        [:div.success [:p "Issue created successfully."]]
        :else ""))))

(defn- footer [student-id]
  (let [submit-id "create-issue-submit"
        on-submit-click (fn [_] (.click (js/document.getElementById submit-id)))]
    [:footer
     [:div.buttons
      [:a.button.secondary {:on-click close} "Cancel"]
      [:input {:type "hidden", :name "student-id", :value student-id}]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input {:id submit-id, :type "submit", :style {:display "none"}}]
      [:a.button {:on-click on-submit-click} "Submit"]]
     [status]]))

(defn- create-issue-panel [student-id]
  [:form#create-issue-form.modal-form {:on-submit submit-form}
   [header "Create Issue"]
   [main-section]
   [footer student-id]])

(defn create-issue-modal [student-id]
  [modal :modal-type/create-issue
   [create-issue-panel student-id]])
