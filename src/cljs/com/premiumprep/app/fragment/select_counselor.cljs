(ns com.premiumprep.app.fragment.select-counselor
  (:require [com.premiumprep.app.data :as data]
            [re-frame.core :as rf]))

(defn counselor->option [counselor]
  (let [{:keys [db/id counselor/account]} counselor
        {:keys [account/full-name]} account]
    [:option {:value id, :key id} full-name]))

(defn- top-option [counselors]
  (let [xhr @(rf/subscribe [::data/xhr :counselors])]
    [:option {:value ""}
     (cond
       (:in-flight? xhr) "(fetching counselors...)"
       (:error xhr) (str "(error fetching counselors: "
                         (:error xhr) ")")
       (empty? counselors) "(you must add a counselor first)"
       :else "--- Select a counselor ---")]))

(defn select-counselor
  [selected-id]
  (let [counselors @(rf/subscribe [::data/counselors-vec])
        select-tag [:select#counselor-id {:name "counselor-id"
                                          :defaultValue selected-id}
                    [top-option counselors]]]
    (into select-tag
          (map counselor->option counselors))))
