(ns com.premiumprep.app.fragment.xhr
  (:require com.premiumprep.app.events
            [re-frame.core :as rf]))

(defn xhr-status
  [subkey {:keys [in-flight? success? error] :as xhr-status}]
  (cond
    in-flight? "⏳" ; this is an emoji character, in case you can't see it
    error [:div.xhr-error
           (if (not-empty error)
             [:abbr {:title (str "Error: " error)} "❌"]
             "❌")]
    ;; Note: potential race condition here, if the user restarts a request
    ;; within 3 seconds of a success. Not worrying about it yet though.
    success? "✅"
    :else ""))
