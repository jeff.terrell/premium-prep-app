(ns com.premiumprep.app.fragment.custom-field-modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn- submit-form [verb event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)
        event-name (if (= verb :add)
                     :create-custom-field
                     :update-custom-field)]
    (rf/dispatch [event-name form-data])))

(defn- main-section [modal-state]
  (let [{:keys [field-id name value]} modal-state]
    [:section
     (when field-id
       [:input {:type "hidden", :name "id", :value field-id}])
     [:div
      [:p "Field Label"]
      [:input {:type "text", :name "name", :required true, :default-value name}]]
     [:div
      [:p "Field Text"]
      [:textarea {:name "value"
                  :required true
                  :default-value value}]]]))

(defn- status [verb]
  (r/with-let [initial-verb verb
               seen-create-success? (atom false)]
    (let [create-key :create-custom-field
          update-key :update-custom-field
          create-xhr @(rf/subscribe [::data/xhr create-key])
          update-xhr @(rf/subscribe [::data/xhr update-key])
          in-flight? (some :in-flight? [create-xhr update-xhr])
          error (some :error [create-xhr update-xhr])
          success? (some :success? [create-xhr update-xhr])
          verb-txt #(if (= :edit %) "updated" "added")
          txt #(str "Custom field " (verb-txt %) " successfully.")
          success-div (fn [v] [:div.success [:p (txt v)]])]
      (cond
        in-flight? "⏳"
        error [:div.error [:p error]]
        (not success?) nil
        (not= initial-verb :add) [success-div :edit]
        @seen-create-success? [success-div :edit]
        :else (do
                (reset! seen-create-success? true)
                [success-div :add])))))

(defn- footer [verb]
  (let [submit-id "custom-field-submit"
        on-submit-click (fn [_] (.click (js/document.getElementById submit-id)))]
    [:footer
     [:div.buttons
      [:a.button.secondary {:on-click close} "Cancel"]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input {:id submit-id, :type "submit", :style {:display "none"}}]
      [:a.button {:on-click on-submit-click} "Submit"]]
     [status verb]]))

(defn- custom-field-panel []
  (let [modal-state @(rf/subscribe [::data/modal-extra-state])
        verb (if (:name modal-state) :edit :add)
        verb-str (if (= verb :edit) "Edit" "Add")]
    [:form#custom-field-form.modal-form {:on-submit (partial submit-form verb)}
     [header (str verb-str " Custom Field")]
     [main-section modal-state]
     [footer verb]]))

(defn custom-field-modal []
  [modal :modal-type/custom-field
   [custom-field-panel]])
