(ns com.premiumprep.app.fragment.header
  (:require
   [com.premiumprep.app.data :as data]
   [kee-frame.core :as k]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn nav-link
  [top-or-sub path text]
  (let [href (if path (k/path-for [path]) "#")]
    [:div.nav-link {:class (name top-or-sub)
                    :on-click #(rf/dispatch [:navigate-to path])}
     [:a {:href href} text]]))

(defn- account-submenu []
  [:div#my-account.nav-link.top
   "My Account"
   [:div.sub-nav
    [nav-link :sub :change-email "Change Email"]
    [nav-link :sub :change-password "Change Password"]
    [nav-link :sub :logout "Log out"]]])

(defn header []
  (r/with-let [type (rf/subscribe [::data/current-user-type])]
    [:header
     [:a {:href "/"}
      [:img#logo {:src "/logo.svg"
                  :width 241
                  :height 51
                  :alt "Premium Prep Logo"}]]
     [:nav#top-nav
      (when (= :account-type/superuser @type)
        [:<>
         [nav-link :top :new-college "Add College"] " | "
         [nav-link :top :issues "Feedback"] " | "
         [nav-link :top :counselors "Counselors"] " | "])
      (when (#{:account-type/superuser :account-type/counselor} @type)
        [:<> [nav-link :top :students "Students"] " | "])
      [account-submenu]]]))
