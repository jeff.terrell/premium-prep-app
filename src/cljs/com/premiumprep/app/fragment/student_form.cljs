(ns com.premiumprep.app.fragment.student-form
  (:require
   [com.premiumprep.app.data :as data]
   [com.premiumprep.app.fragment.inputs :refer [field textarea]]
   [com.premiumprep.app.model.student-fields-common :refer [fields]]
   [reagent.core :as r]
   [re-frame.core :as rf]))

(defn basic-info [student]
  [:<>
   [:h3 "Basic Info"]
   [field student "full-name" "text" "Student Full Name" true "First Last"]
   [field student "preferred-name" "text" "Student Preferred Name" false nil]
   [field student "high-school" "text" "High School" true nil]
   [field student "graduation-year" "number" "High School Graduation Year"
    true nil {:min "2020"}]
   [field student "current-college" "text"
    "Current College (for transfers only)" false nil]
   [field student "academic-interest" "text"
    "Academic Interest / Potential Major" false nil]])

(defn grades [student]
  [:<>
   [:h3 "Grades"]
   [field student "gpa-weighted-9" "text" "9th Weighted GPA"]
   [field student "gpa-unweighted-9" "text" "9th Unweighted GPA"]
   [field student "gpa-weighted-9-10" "text" "9th-10th Weighted GPA"]
   [field student "gpa-unweighted-9-10" "text" "9th-10th Unweighted GPA"]
   [field student "gpa-weighted-9-11" "text" "9th-11th Weighted GPA"]
   [field student "gpa-unweighted-9-11" "text" "9th-11th Unweighted GPA"]
   [field student "class-rank-9" "number" "9th Class Rank"]
   [field student "class-rank-9-of" "number" "9th Class Size"]
   [field student "class-rank-9-10" "number" "9th-10th Class Rank"]
   [field student "class-rank-9-10-of" "number" "9th-10th Class Size"]
   [field student "class-rank-9-11" "number" "9th-11th Class Rank"]
   [field student "class-rank-9-11-of" "number" "9th-11th Class Size"]
   [field student "psat-total-score" "number" "PSAT Total Score"]
   [field student "psat-ebrw-score" "number" "PSAT EBRW"]
   [field student "psat-math-score" "number" "PSAT Math"]
   [field student "pact-composite-score" "number" "Practice ACT Composite Score"]
   [field student "pact-english-score" "number" "Practice ACT English"]
   [field student "pact-reading-score" "number" "Practice ACT Reading"]
   [field student "pact-math-score" "number" "Practice ACT Math"]
   [field student "pact-science-score" "number" "Practice ACT Science"]])

(defn test-scores [student]
  [:<>
   [:h3 "Test Scores"]
   [:p "Please include all tests you have taken and plan to take"]

   [:h4 "SAT Superscore"]
   [field student "sat-superscore-total-score" "number" "Superscore SAT"]
   [field student "sat-superscore-ebrw-score" "number" "Superscore EBRW"]
   [field student "sat-superscore-ebrw-date" "date" "Superscore EBRW Date"]
   [field student "sat-superscore-math-score" "number" "Superscore Math"]
   [field student "sat-superscore-math-date" "date" "Superscore Math Date"]

   [:h4 "1st SAT"]
   [field student "sat1-total-score" "number" "1st SAT Total Score"]
   [field student "sat1-date" "month" "1st Date" nil "YYYY-MM"]
   [field student "sat1-ebrw-score" "number" "1st EBRW"]
   [field student "sat1-math-score" "number" "1st Math"]

   [:h4 "2nd SAT"]
   [field student "sat2-total-score" "number" "2nd SAT Total Score"]
   [field student "sat2-date" "month" "2nd Date" nil "YYYY-MM"]
   [field student "sat2-ebrw-score" "number" "2nd EBRW"]
   [field student "sat2-math-score" "number" "2nd Math"]

   [:h4 "3rd SAT"]
   [field student "sat3-total-score" "number" "3rd SAT Total Score"]
   [field student "sat3-date" "month" "3rd Date" nil "YYYY-MM"]
   [field student "sat3-ebrw-score" "number" "3rd EBRW"]
   [field student "sat3-math-score" "number" "3rd Math"]

   [:h4 "4th SAT"]
   [field student "sat4-total-score" "number" "4th SAT Total Score"]
   [field student "sat4-date" "month" "4th Date" nil "YYYY-MM"]
   [field student "sat4-ebrw-score" "number" "4th EBRW"]
   [field student "sat4-math-score" "number" "4th Math"]

   [:h4 "ACT Superscore"]
   [field student "act-superscore-total-score" "number" "Superscore ACT"]
   [field student "act-superscore-english-score" "number" "Superscore English"]
   [field student "act-superscore-english-date" "date" "Superscore English Date"]
   [field student "act-superscore-reading-score" "number" "Superscore Reading"]
   [field student "act-superscore-reading-date" "date" "Superscore Reading Date"]
   [field student "act-superscore-math-score" "number" "Superscore Math"]
   [field student "act-superscore-math-date" "date" "Superscore Math Date"]
   [field student "act-superscore-science-score" "number" "Superscore Science"]
   [field student "act-superscore-science-date" "date" "Superscore Science Date"]

   [:h4 "1st ACT"]
   [field student "act1-composite-score" "number" "1st ACT Composite Score"]
   [field student "act1-date" "month" "1st Date" nil "YYYY-MM"]
   [field student "act1-english-score" "number" "1st English"]
   [field student "act1-reading-score" "number" "1st Reading"]
   [field student "act1-math-score" "number" "1st Math"]
   [field student "act1-science-score" "number" "1st Science"]

   [:h4 "2nd ACT"]
   [field student "act2-composite-score" "number" "2nd ACT Composite Score"]
   [field student "act2-date" "month" "2nd Date" nil "YYYY-MM"]
   [field student "act2-english-score" "number" "2nd English"]
   [field student "act2-reading-score" "number" "2nd Reading"]
   [field student "act2-math-score" "number" "2nd Math"]
   [field student "act2-science-score" "number" "2nd Science"]

   [:h4 "3rd ACT"]
   [field student "act3-composite-score" "number" "3rd ACT Composite Score"]
   [field student "act3-date" "month" "3rd Date" nil "YYYY-MM"]
   [field student "act3-english-score" "number" "3rd English"]
   [field student "act3-reading-score" "number" "3rd Reading"]
   [field student "act3-math-score" "number" "3rd Math"]
   [field student "act3-science-score" "number" "3rd Science"]

   [:h4 "4th ACT"]
   [field student "act4-composite-score" "number" "4th ACT Composite Score"]
   [field student "act4-date" "month" "4th Date" nil "YYYY-MM"]
   [field student "act4-english-score" "number" "4th English"]
   [field student "act4-reading-score" "number" "4th Reading"]
   [field student "act4-math-score" "number" "4th Math"]
   [field student "act4-science-score" "number" "4th Science"]

   [:h4 "Other tests"]
   [textarea student "sat-subject-tests" "SAT Subject Tests (Type, Score, Date Taken)"]
   [textarea student "ap-tests" "AP Tests (Type, Score, Date Taken)"]])

(defn contact-info [student display-email?]
  [:<>
   [:h3 "Contact Information"]
   [field student "mobile" "tel" "Student Mobile" true "(123) 456-7890"]
   (when display-email?
     [field student "email" "email" "Student Email" false nil
      {:disabled true
       :default-value (-> student :student/account :account/email)}
      "You cannot change this yet, but once you sign up you can."])
   [field student "home-phone" "tel" "Student Home Phone" false "(123) 456-7890"]
   [field student "address" "text" "Home Address" true nil]
   [field student "city" "text" "Home City" true nil]
   [field student "state" "text" "Home State" true nil]
   [field student "zip" "number" "Home Zip" true nil]
   [field student "country" "text" "Country" false nil]])

(defn parent-info [student n req?]
  (let [name #(str "parent" n "-" %)
        label #(str "Parent " n " " %)]
    [:<>
     [:h4 (str "Parent " n " Contact Info")]
     [field student (name "full-name") "text" (label "Full Name") req?
      "First Last"]
     [field student (name "preferred-name") "text" (label "Preferred Name") false
      nil]
     [field student (name "email") "email" (label "Email") req? nil]
     [field student (name "mobile") "tel" (label "Mobile") req? nil]
     [field student (name "work-phone") "tel" (label "Work Phone") false nil]
     [field student (name "home-phone") "tel" (label "Home Phone") false nil]]))

(defn college-planning-app-info [student]
  [:<>
   [:h3 "College Planning App Info (ex: Naviance, SCOIR, Kickstart, Maia Learning)"]
   [:label {:for "college-planning-app-type"} "Type"]
   [:select (let [name "college-planning-app-type"
                  {:keys [path]} (get fields name)]
              {:id name
               :name name
               :defaultValue (get-in student path)})
    [:option {:value ""} "(none)"]
    [:option {:value "naviance"} "Naviance"]
    [:option {:value "scoir"} "SCOIR"]
    [:option {:value "maia"} "Maia"]
    [:option {:value "kickstart"} "Kickstart"]]
   [field student "college-planning-app-username" "text" "Username"]
   [field student "college-planning-app-password" "text" "Password"]
   [field student "college-planning-app-url" "text" "Link to HS's login page"]])

(defn misc [student]
  [:<>
   [:h3 "Miscellaneous"]
   [textarea student "extracurricular-activities"
    (str "Extracurricular Activities (Include Activity, Position, Grades (9-12)"
         " and a brief description for each activity.)")]
   [textarea student "current-courses" "Current Courses"]
   [textarea student "prospective-courses" "Prospective Courses"]])

(defn- status-in-view [student in-flight? success? error on-success on-error]
  (let [elements (cond
                   in-flight? "⏳"
                   error [on-error student error]
                   success? [on-success student])
        scroll-to-bottom #(js/window.scrollTo 0 js/document.body.scrollHeight)]
    (js/window.setTimeout scroll-to-bottom 100) ; do it after elements appear
    elements))

(defn status [student xhr-key on-success on-error]
  (r/with-let [xhr-status (rf/subscribe [::data/xhr xhr-key])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (when (or in-flight? success? error)
        [status-in-view student in-flight? success? error on-success on-error]))))
