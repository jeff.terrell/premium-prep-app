(ns com.premiumprep.app.fragment.issue
  (:require cljsjs.moment
            [com.premiumprep.app.events :as events]
            [re-frame.core :as rf]))

(def action-req [:needs-discussion :counselor-resolved])

(def state-sort-key
  {:issue-state/new                4
   :issue-state/read               3
   :issue-state/needs-discussion   2
   :issue-state/counselor-resolved 1
   :issue-state/closed             0})

(defn default-sort-key [issue]
  (let [{:keys [issue/state issue/counselor-touched-at issue/created-at]} issue
        ts (or counselor-touched-at created-at)]
    [(state-sort-key state)
     ts ; i.e. sort by ts descending as tie-breaker
     ]))

(defn issue-touched-td [issue]
  (let [precise #(.toLocaleString %)
        relative #(-> % js/moment .fromNow)
        {:keys [db/id issue/created-at issue/text issue/state
                issue/counselor-touched-at]} issue
        new? (= :issue-state/new state)
        ts (cond new? created-at
                 counselor-touched-at counselor-touched-at
                 :else created-at)
        title (if (or new? counselor-touched-at)
                (precise ts)
                (str (precise ts) " (created)"))]
    [:td.issue-touched {:title title} (relative ts)]))

(defn issue-controls-by-name [issue type control-names]
  (when (not-empty control-names)
    (let [name->label {:new "unread"
                       :read "read"
                       :needs-discussion "discussion needed"
                       :counselor-resolved "resolved"
                       :closed "closed"}
          id (:db/id issue)
          make-event (fn [name] [:set-issue-state id name])
          dispatch-fn (fn [name] #(rf/dispatch (make-event name)))]
      [:ul
       (for [control-name control-names]
         ^{:key control-name}
         [:li {:on-click (if (and (= type :account-type/counselor) (some #(= % control-name) action-req))
                           #(rf/dispatch [::events/set-modal-visibility true :modal-type/update-issue
                                          {:issue issue :control control-name}])
                           (dispatch-fn control-name))}
          (name->label control-name)])])))
