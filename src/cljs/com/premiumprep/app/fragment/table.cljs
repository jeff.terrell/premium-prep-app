(ns com.premiumprep.app.fragment.table
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.events :as events]
            [re-frame.core :as rf]))

(defn- tr [columns tr-class-fn data-row]
  [:tr (when tr-class-fn {:class (tr-class-fn data-row)})
   (doall
     (for [[col-name {:keys [sort-key-fn ->markup]}] columns]
       (with-meta
         (if ->markup
           (->markup data-row)
           [:td (sort-key-fn data-row)])
         {:key col-name})))])

(defn- sort-data [table-opts ordering-column-id ascending? data-rows]
  (let [{:keys [columns]} table-opts
        ordering-column (get columns ordering-column-id)
        sort-fn (:sort-key-fn ordering-column (constantly true))
        sorted-data (sort-by sort-fn data-rows)
        num-reverses (+
                       (if ascending? 0 1)
                       (if (:reverse-sort ordering-column) 1 0))]
    (if (= 1 num-reverses)
      (reverse sorted-data)
      sorted-data)))

(defn- tbody [table-opts ordering-column-id ascending? data-rows]
  (let [{:keys [columns tr-class-fn]} table-opts]
    [:tbody (map (fn [data-row]
                   ^{:key (:db/id data-row)}
                   [tr columns tr-class-fn data-row])
                 (sort-data table-opts ordering-column-id ascending?
                            data-rows))]))

(defn- thead [{:keys [columns page-type]} ordering-column-id ascending?]
  (letfn [(attrs [id sort-key-fn]
            (merge
              (when sort-key-fn
                {:on-click #(rf/dispatch [::events/sort-by-column
                                          page-type id])})
              {:class (if (not= id ordering-column-id)
                        (when sort-key-fn "sortable")
                        (str "sorted "
                             (if ascending? "ascending" "descending")))}))
          (label-markup [id label]
            (cond
              (not= id ordering-column-id) label
              ascending? [:<> label [:span.direction-arrow "↓"]]
              :else      [:<> label [:span.direction-arrow "↑"]]))]
    [:thead
     [:tr
      (for [[id {:keys [label sort-key-fn]}] columns]
        ^{:key id} [:th (attrs id sort-key-fn)
                    (label-markup id label)])]]))

(defn table
  "Display a table with sorting controls. `data-rows` is a seq of maps.
  `table-opts` is a map of options that can include (required unless noted):
  - :table-class - a string with the CSS class for the top-level table element
  - :tr-class-fn - a fn to accept a data row and return a CSS class to apply to
    the tr element (not required)
  - :page-type - a keyword to help determine where the page state lives in the
    app-db, e.g. :page-type/student
  - :columns - a map defining a column. Keys are column IDs (assumed keywords)
    and values are maps whose options can include (not required unless noted):
    - :label - a string to put in the column header (required)
    - :sort-key-fn - a fn from data row map to sort key (default => not a
      sortable column)
    - :reverse-sort - a boolean for whether to reverse the sort (default false)
    - :->markup - a reagent rendering fn that gets the data row as its arg
      (defaults to a simple td element whose value is the sort key for the row;
      required if sort-key-fn not given)"

  [table-opts data-rows]
  (let [{:keys [columns page-type table-class]} table-opts
        order-by @(rf/subscribe [::data/table-sort-column page-type])
        ascending? @(rf/subscribe [::data/table-sort-ascending? page-type])
        ordering-column-id (or order-by (-> columns first key))]
    [:table {:class table-class}
     [thead table-opts ordering-column-id ascending?]
     [tbody table-opts ordering-column-id ascending? data-rows]]))
