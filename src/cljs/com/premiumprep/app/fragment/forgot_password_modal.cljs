(ns com.premiumprep.app.fragment.forgot-password-modal
  (:require [com.premiumprep.app.data :as data]
            [com.premiumprep.app.fragment.modal :refer [close header modal]]
            [reagent.core :as r]
            [re-frame.core :as rf]))

(defn submit-forgot-password-form [event]
  (.preventDefault event)
  (let [form-el (.-target event)
        form-data (js/FormData. form-el)]
    (rf/dispatch [:forgot-password form-data])))

(defn forgot-password-main-section []
  [:section
   [:div
    [:p "Account Email"]
    [:input {:type "email", :name "email", :required true}]]])

(defn- status []
  (r/with-let [xhr-status (rf/subscribe [::data/xhr :forgot-password])]
    (let [{:keys [in-flight? success? error]} @xhr-status]
      (cond
        in-flight? "⏳" ; this is an emoji character, in case you can't see it
        error [:div.error [:p error]]
        success?
        [:div.success
         [:p "If we recognized your email address, we sent you an email with a "
          "link to reset your password. If you didn't get it, you may have "
          "mistyped your email address (or forgotten which one you signed up "
          "with), or the email may be in your spam folder."]]
        :else ""))))

(defn forgot-password-footer []
  (letfn [(on-submit-click [e]
            (let [submit-el (js/document.getElementById
                              "forgot-password-submit")]
              (.click submit-el)))]
    [:footer
     [:div.buttons
      [:a.button.secondary {:on-click close} "Cancel"]
      ;; Safari doesn't yet implement the form.requestSubmit() method, so this
      ;; is a workaround to trigger form submission from Javascript _including_
      ;; the submit event itself, which we respond to in the :on-submit
      ;; attribute of the enclosing form element:
      [:input#forgot-password-submit {:type "submit", :style {:display "none"}}]
      [:a.button {:on-click on-submit-click} "Submit"]]
     [status]]))

(defn forgot-password-panel []
  [:form#forgot-password-modal.modal-form {:on-submit
                                           submit-forgot-password-form}
   [header "Forgot your password?"]
   [forgot-password-main-section]
   [forgot-password-footer]])

(defn forgot-password-modal []
  [modal :modal-type/forgot-password
   [forgot-password-panel]])
