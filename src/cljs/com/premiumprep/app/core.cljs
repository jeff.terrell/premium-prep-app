(ns ^:figwheel-hooks com.premiumprep.app.core
  (:require
   [com.premiumprep.app.config :refer [DEBUG]]
   [com.premiumprep.app.db-spec :as db-spec]
   com.premiumprep.app.events
   [com.premiumprep.app.root :as root]
   com.premiumprep.app.xhr
   [kee-frame.core :as k]
   kee-frame.scroll
   [re-frame.core :as rf]))

(defn dev-setup []
  (when DEBUG
    (enable-console-print!)
    (println "dev mode active")))

(defn start-app []
  (let [cfg {:routes root/routes
             :app-db-spec ::db-spec/db-spec
             :initial-db {}
             :root-component [root/root-component]}
        cfg (if DEBUG
              (assoc cfg :log {:level :debug})
              cfg)]
    (rf/clear-subscription-cache!)
    (k/start! cfg)
    (rf/dispatch [:current-user/load])))

(defn ^:after-load re-render []
  (start-app))

(defn ^:export init []
  (dev-setup)
  (start-app))
