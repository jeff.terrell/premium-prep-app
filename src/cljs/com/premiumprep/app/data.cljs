(ns com.premiumprep.app.data
  "Re-frame subscriptions to aspects of the app database.

  These subscriptions are divided into Layer 2 subscriptions and Layer 3
  subscriptions. To quote from [1], the four layers are:

  - Layer 1: the ground truth, i.e. the app database
  - Layer 2: extractors: subscriptions that extract data from app-db but do no
    further computation
  - Layer 3: materialized view: subscriptions that obtain data from other
    subscriptions (never app-db directly) and compute derived data from their
    inputs
  - Layer 4: view functions: the leaf nodes which compute hiccup markup

  A Clojure Spec for the app-db (Layer 1) can be found in the db-spec namespace.
  The view functions (Layer 4) are in the fragment.* and page.* namespaces. This
  namespace contains layers 2 and 3. Note that Layer 3 subscriptions can depend
  on other Layer 3 subscriptions.

  The separation between Layer 2 and Layer 3 is useful not just for logical
  separation between raw and derived values. It's also useful for performance.
  Whenever a leaf node subscribes to a subscription, the entire chain of
  dependencies is recalculated when a value changes. If the output of one step
  doesn't change across the recalculation, that step acts as a 'circuit
  breaker', and the dependent steps are not recalculated. For example, if we
  have a chain of subscriptions from an L4 leaf-node to an L3 signal to an L2
  signal to the app-db, and the app-db changes, the L2 function is always
  called. If the returned value of the L2 function is not different, then L3 and
  L4 need not be (and aren't) recomputed. It's a beautiful, performant system,
  called a 'signal graph', and it's made possible because every function is
  assumed to be pure. (So make sure any functions you give to reg-sub are pure.)

  [1] http://day8.github.io/re-frame/subscriptions/"
  (:require [re-frame.core :refer [reg-sub subscribe]]))

;;;; Layer 2 subscriptions

(reg-sub ::colleges
  (fn [db _] (:colleges db)))
(reg-sub ::counselors
  (fn [db _] (:counselors db)))
(reg-sub ::current-student-id
  (fn [db _] (:current-student-id db)))
(reg-sub ::current-user
  (fn [db _] (:current-user db)))
(reg-sub ::issues
  (fn [db _] (:issues db)))
(reg-sub ::modal
  (fn [db _] (get db :modal)))
(reg-sub ::new-counselor-password
  (fn [db _] (:new-counselor-password db)))
(reg-sub ::page
  (fn [db _] (get db :page)))
(reg-sub ::students
  (fn [db _] (:students db)))
(reg-sub ::students-active-status-filter
  (fn [db] (get db :students-active-status-filter)))
(reg-sub ::xhr-base
  (fn [db _] (:xhr db)))


;;;; Layer 3 subscriptions

;;; simple id-based lookups

(reg-sub ::college
  :<- [::colleges]
  (fn [colleges [_ id]]
    (get colleges id)))

(reg-sub ::counselor
  :<- [::counselors]
  (fn [counselors [_ id]]
    (get counselors id)))

(reg-sub ::student
  :<- [::students]
  (fn [students [_ id]]
    (get students id)))


;;; simple attribute-based lookups

(reg-sub ::current-user-id
  :<- [::current-user]
  (fn [current-user _] (:id current-user)))

(reg-sub ::current-user-state
  :<- [::current-user]
  (fn [current-user _] (:state current-user)))

(reg-sub ::current-user-type
  :<- [::current-user]
  (fn [current-user _] (:type current-user)))

(reg-sub ::email
  :<- [::current-user]
  (fn [current-user _] (:email current-user)))

(reg-sub ::page-state
  :<- [::page]
  (fn [page-substate [_ page-type]] (get page-substate page-type)))

(reg-sub ::student-account-state
  (fn [[_ id] _] (subscribe [::student id]))
  (fn [student _] (get-in student [:student/account :account/state])))

(reg-sub ::xhr
  :<- [::xhr-base]
  (fn [xhr-state [_ & subkeys]]
    (get-in xhr-state subkeys)))


;;; modal substate lookups

(reg-sub ::modal-type
  :<- [::modal]
  (fn [modal-state _]
    (get modal-state :modal/type)))

(reg-sub ::modal-visible?
  :<- [::modal-type]
  (fn [actual-modal-type [_ given-modal-type]]
    (= actual-modal-type given-modal-type)))

(reg-sub ::modal-extra-state
  :<- [::modal]
  (fn [modal-state _]
    (dissoc modal-state :modal/type)))

(reg-sub ::edit-counselor-modal-counselor-id
  :<- [::modal-visible? :modal-type/edit-counselor]
  :<- [::modal-extra-state]
  (fn [[edit-counselor-modal-visible? extra-state] _]
    (when edit-counselor-modal-visible?
      (:counselor-id extra-state))))

(reg-sub ::edit-student-modal-student-id
  :<- [::modal-visible? :modal-type/edit-student]
  :<- [::modal-extra-state]
  (fn [[edit-student-modal-visible? extra-state] _]
    (when edit-student-modal-visible?
      (:student-id extra-state))))

(reg-sub ::update-issue-modal-issue
  :<- [::modal-visible? :modal-type/update-issue]
  :<- [::modal-extra-state]
  (fn [[update-issue-modal-visible? extra-state] _]
   (when update-issue-modal-visible?
     (:issue extra-state))))

(reg-sub ::update-issue-modal-control
  :<- [::modal-visible? :modal-type/update-issue]
  :<- [::modal-extra-state]
  (fn [[update-issue-modal-visible? extra-state] _]
   (when update-issue-modal-visible?
     (:control extra-state))))


;;; page substate lookups

(reg-sub ::mismatch-error
  :<- [::page-state :page-type/reset-password]
  (fn [page-state _]
    (:mismatch-error page-state)))

;; there's a better way to do this, but I'm in a hurry
(reg-sub ::mismatch-error2
  :<- [::page-state :page-type/change-password]
  (fn [page-state _]
    (:mismatch-error page-state)))

(reg-sub ::signup-token
  :<- [::page-state :page-type/signup]
  (fn [page-state _]
    (:token page-state)))

(reg-sub ::signup-email
  :<- [::page-state :page-type/signup]
  (fn [page-state _]
    (:email page-state)))

(reg-sub ::show-all-student-docs?
  :<- [::page-state :page-type/student]
  (fn [page-state _]
    (:show-all-docs? page-state)))

(reg-sub ::show-full-student-profile?
  :<- [::page-state :page-type/student]
  (fn [page-state _]
    (:show-full-profile? page-state)))

(reg-sub ::show-eliminated-colleges?
  :<- [::page-state :page-type/student]
  (fn [page-state _]
    (:show-eliminated-colleges? page-state)))

(reg-sub ::comments-visible?
  :<- [::page-state :page-type/student]
  (fn [page-state [_ type]]
    (get-in page-state [:comments-visibility type])))

(reg-sub ::table-sort
  (fn [[_ page-type] _] (subscribe [::page-state page-type]))
  (fn [page-state _] (get page-state :table-sort)))

(reg-sub ::table-sort-column
  (fn [[_ page-type] _] (subscribe [::table-sort page-type]))
  (fn [table-sort _] (get table-sort :column)))

(reg-sub ::table-sort-ascending?
  (fn [[_ page-type] _] (subscribe [::table-sort page-type]))
  (fn [table-sort _] (get table-sort :ascending? true)))

(reg-sub ::college-details-visibility-map
  :<- [::page-state :page-type/student]
  (fn [page-state _] (:college-details-visibility page-state)))

(reg-sub ::college-expanded?
  :<- [::college-details-visibility-map]
  (fn [visibility-map [_ college-id]]
    (get visibility-map college-id)))

(reg-sub ::editing-fields-for-colleges
  :<- [::page-state :page-type/student]
  (fn [page-state _] (:editing-college-list-entry-field page-state)))

(reg-sub ::editing-field-for-college
  :<- [::editing-fields-for-colleges]
  (fn [editing-fields-map [_ college-id]]
    (get editing-fields-map college-id)))


;;; more complex reference-based lookups

(reg-sub ::counselors-vec
  :<- [::counselors]
  (fn [counselors _]
    (if (= counselors {})
      [] ; a truthy value, to indicate presence of a value for counselors page
      (vals counselors))))

(reg-sub ::students-for-counselor
  :<- [::counselors]
  :<- [::students]
  (fn [[counselors students] [_ counselor-id]]
    (when-not (or (empty? counselors) (empty? students))
      (let [counselor (get counselors counselor-id)
            student-refs (get counselor :student/_counselor)
            lookup-student-ref #(get students (:db/id %))]
        (mapv lookup-student-ref student-refs)))))

(reg-sub ::issues-for-student
  (fn [[_ id]]
    [(subscribe [::student id])
     (subscribe [::issues])])
  (fn [[student issues] _]
    (when-not (or (empty? student) (empty? issues))
      (let [student-refs (:issue/_student student)
            lookup-student-ref #(get issues (:db/id %))]
        (mapv lookup-student-ref student-refs)))))

(reg-sub ::college-ids-in-student-list
  (fn [[_ student-id]] (subscribe [::student student-id]))
  (fn [student _]
    (let [{:keys [student/college-lists]} student
          refs (apply concat (vals college-lists))]
      (->> refs
           (remove :college-list-entry/eliminated?)
           (map (comp :db/id :college-list-entry/college))
           set))))
