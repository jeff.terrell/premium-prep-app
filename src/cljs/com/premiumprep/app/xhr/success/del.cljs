(ns com.premiumprep.app.xhr.success.del
  ;; Use .del instead of .delete in the namespace to avoid a compile warning
  ;; because of the reserved `delete` keyword in JS.
  "Re-frame event definitions for events that receive success responses from the
  backend for DELETE requests."
  (:require [kee-frame.core :as k]))

(k/reg-event-fx :logout-success
  (fn [{:keys [db]} _]
    {:db (assoc-in db [:xhr :logout]
                   {:in-flight? false, :error nil, :success? true})
     :navigate-to [:login]}))

(k/reg-event-db :remove-college-success
  (fn [db [student-id college-id updated-college-lists]]
    (-> db
        (assoc-in [:xhr :remove-college student-id]
                  {:in-flight? false, :error nil, :success? true})
        (assoc-in [:students student-id :student/college-lists]
                  updated-college-lists)
        (assoc-in [:page :page-type/student :college-details-visibility
                   college-id]
                  false))))

(k/reg-event-db :delete-custom-field-success
  (fn [db [student-id category entry-index field-index]]
    (let [fields-path [:students student-id :student/college-lists category
                       entry-index :college-list-entry/custom-fields]
          splicev #(vec (concat (subvec %1 0 %2) (subvec %1 (inc %2))))]
      (-> db
          (assoc-in [:xhr :delete-custom-field]
                    {:in-flight? false, :error nil, :success? true})
          (update-in fields-path splicev field-index)))))

(k/reg-event-db :delete-document-success
  (fn [db [student-id document-id deleted-document]]
    (let [original-documents-coll (get-in db [:students student-id :student/documents])
          updated-documents-coll (filterv #(not= (-> %
                                                     (dissoc :document/s3-path)
                                                     (dissoc :document/link))
                                                 (update (-> deleted-document
                                                             (dissoc :document/s3-path)
                                                             (dissoc :document/link)) :db/id str ))
                                          original-documents-coll)]
      (-> db
          (assoc-in [:xhr :delete-document document-id]
                    {:in-flight? false, :error nil, :success? true})
          (assoc-in [:students student-id :student/documents]
                    updated-documents-coll)))))
