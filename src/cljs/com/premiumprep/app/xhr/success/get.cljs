(ns com.premiumprep.app.xhr.success.get
  "Re-frame event definitions for events that receive success responses from the
  backend for GET requests."
  (:require [com.premiumprep.app.xhr.util :refer [make-event-name]]
            [com.premiumprep.app.util :refer [by-id]]
            [kee-frame.core :as k]))

(defn reg-xhr-success-event [event-base xhr-subkey xform-fx]
  (let [event-name (make-event-name event-base :success)]
    (k/reg-event-fx event-name
      (fn [{:keys [db]} [data]]
        (let [xhr-state {:in-flight? false, :error nil, :success? true}]
          (xform-fx {:db (assoc-in db [:xhr xhr-subkey] xhr-state)}
                    data))))))

(reg-xhr-success-event :current-user/load :current-user
  (fn [fx {:keys [account/type account/state account/email db/id]}]
    (assoc-in fx [:db :current-user]
              {:email email, :type type, :id id, :state state})))

(reg-xhr-success-event :students/load :students
  (fn [fx data]
    (-> fx
        (assoc-in [:db :students-active-status-filter] :active-only)
        (assoc-in [:db :students] (by-id :db/id data)))))

(reg-xhr-success-event :counselors/load :counselors
  (fn [fx data]
    (-> fx
        (assoc-in [:db :counselors] (by-id :db/id data))
        (assoc :dispatch-later
               [{:ms 3000, :dispatch [:clear-xhr :counselors]}]))))

(reg-xhr-success-event :colleges/load :colleges
  (fn [fx data]
    (assoc-in fx [:db :colleges] (by-id :db/id data))))

(reg-xhr-success-event :student/load :student
  (fn [fx data]
    (let [id (:db/id data)]
      (-> fx
          (assoc-in [:db :students id] data)
          (assoc :dispatch-later
                 [{:ms 3000, :dispatch [:clear-xhr :student]}])))))

(reg-xhr-success-event :me-student/load :me-student
  (fn [fx data]
    (let [id (:db/id data)]
      (-> fx
          (assoc-in [:db :current-student-id] id)
          (assoc-in [:db :students id] data)))))

(reg-xhr-success-event :verify-token :verify-token
  (fn [fx data]
    (assoc-in fx [:db :page :page-type/signup :email] (:email data))))

(reg-xhr-success-event :verify-email :verify-email
                       (fn [fx _data]
                         (assoc-in fx [:db :current-user :state] :account-state/initialized)))

(reg-xhr-success-event :issues/load :issues
                       (fn [fx data]
                         (assoc-in fx [:db :issues] (by-id :db/id data))))
