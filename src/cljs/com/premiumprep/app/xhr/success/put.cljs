(ns com.premiumprep.app.xhr.success.put
  "Re-frame event definitions for events that receive success responses from the
  backend for PUT requests."
  (:require cljsjs.moment
            [kee-frame.core :as k]))

(k/reg-event-fx :update-counselor-success
  (fn [{:keys [db]} [form-data counselor]]
    (let [{:keys [db/id]} counselor]
      {:db (-> db
               (assoc-in [:xhr :update-counselor]
                         {:in-flight? false, :error nil, :success? true})
               (update :counselors assoc id counselor))
       :dispatch [:com.premiumprep.app.events/set-modal-visibility false]})))

(k/reg-event-fx :update-issue-success
  (fn [{:keys [db]} [id control issue]]
    (let [{:keys [db/id]} issue]
      {:db         (-> db
                       (assoc-in [:xhr :update-issue]
                                 {:in-flight? false, :error nil, :success? true})
                       (update :issues assoc id issue))
       :dispatch-n [[:com.premiumprep.app.events/set-modal-visibility false]
                    [:set-issue-state id control]]})))

(k/reg-event-fx :update-student-success
  (fn [{:keys [db]} [id form-data student]]
    (let [old-counselor-id (get-in db [:students id :student/counselor :db/id])
          new-counselor-id (get-in student [:student/counselor :db/id])
          {:keys [db/id]} student
          db (assoc-in db [:xhr :update-student]
                       {:in-flight? false, :error nil, :success? true})
          db (update db :students assoc id student)]
      {:db (if (= old-counselor-id new-counselor-id)
             db
             ;; FIXME: maybe extract this into its own event?
             (letfn [(the-ref? [ref]
                       (= id (:db/id ref)))
                     (disj-student-ref [refs]
                       (filterv (complement the-ref?) refs))
                     (conj-student-ref [refs]
                       (conj refs {:db/id id}))]
               (-> db
                   (update-in [:counselors old-counselor-id :student/_counselor]
                              disj-student-ref)
                   (update-in [:counselors new-counselor-id :student/_counselor]
                              conj-student-ref))))
       :dispatch [:com.premiumprep.app.events/set-modal-visibility false]})))

(k/reg-event-db :update-student-profile-success
  (fn [db [id student]]
    (let [{:keys [db/id]} student]
      (-> db
          (assoc-in [:xhr :update-student-profile]
                    {:in-flight? false, :error nil, :success? true})
          (assoc-in [:students id] student)))))

(k/reg-event-db :re-send-invite-success
  (fn [db _]
    (assoc-in db [:xhr :re-send-invite]
              {:in-flight? false, :error nil, :success? true})))

(k/reg-event-db :set-account-active-attribute-success
  (fn [db [active? account-type student-or-counselor-id]]
    (let [db (assoc-in db [:xhr :set-account-active-attribute]
                       {:in-flight? false, :error nil, :success? true})
          counselor? (= :account-type/counselor account-type)
          key1 (if counselor? :counselors :students)
          key3 (if counselor? :counselor/account :student/account)
          keys [key1 student-or-counselor-id key3 :account/active?]]
      (assoc-in db keys active?))))

(k/reg-event-db :forgot-password-success
  (fn [db [form-data]]
    (assoc-in db [:xhr :forgot-password]
              {:in-flight? false, :error nil, :success? true})))

(k/reg-event-db :change-password-success
  (fn [db _]
    (assoc-in db [:xhr :change-password]
              {:in-flight? false, :error nil, :success? true})))

(k/reg-event-fx :set-issue-state-success
  (fn [{:keys [db]} [issue]]
    (let [{:keys [db/id]} issue]
      {:db (-> db
               (assoc-in [:xhr :set-issue-state id]
                         {:in-flight? false, :error nil, :success? true})
               (assoc-in [:issues id] issue))
       :dispatch-later [{:ms 3000
                         :dispatch [:clear-xhr :set-issue-state id]}]})))

(k/reg-event-db :update-entry-field-success
  (fn [db [student-id college-id category index field value]]
    (let [attr (keyword "college-list-entry" (name field))
          boolean-field? #{"visited?"
                           "early-decision?"
                           "app-submitted?"
                           "scholarship?"
                           "matriculation?"}
          str->bool {"true" true, "false" false}
          str->inst #(.toDate (js/moment.utc % "YYYY-MM-DD"))]

      (-> db
          (assoc-in [:xhr :update-entry-field college-id]
                    {:in-flight? false, :error nil, :success? true})
          (assoc-in [:students student-id :student/college-lists category
                     index attr]
                    ;; FIXME: this field-type logic should live elsewhere.
                    ;; A better approach would be for the backend to return
                    ;; the data like always. Then the logic would only live
                    ;; there.
                    (cond
                      (boolean-field? field) (str->bool value)
                      (= field "app-deadline") (str->inst value)
                      :else value))

          ;; This logic is duplicated from the
          ;; :stop-editing-field-for-college event. Unfortunately, adding
          ;; a :dispatch effect here resulted in the transition from the
          ;; in-flight view (i.e. with an hourglass) to the display view
          ;; (i.e. with a pencil icon to start editing) wrongly showing an
          ;; edit view (i.e. with the field input) in between. I think
          ;; that's because there was time for a re-render in between this
          ;; event and the next one when I did it that way.
          (update-in [:page :page-type/student :editing-college-list-entry-field]
                     dissoc college-id)))))

(k/reg-event-db :update-entry-category-success
  (fn [db [student-id college-id updated-college-lists]]
    (-> db
        (assoc-in [:xhr :update-entry-category student-id]
                  {:in-flight? false, :error nil, :success? true})
        (assoc-in [:students student-id :student/college-lists]
                  updated-college-lists)

        ;; This logic is duplicated from the
        ;; :stop-editing-field-for-college event. Unfortunately, adding
        ;; a :dispatch effect here resulted in the transition from the
        ;; in-flight view (i.e. with an hourglass) to the display view
        ;; (i.e. with a pencil icon to start editing) wrongly showing an
        ;; edit view (i.e. with the field input) in between. I think
        ;; that's because there was time for a re-render in between this
        ;; event and the next one when I did it that way.
        (update-in [:page :page-type/student :editing-college-list-entry-field]
                   dissoc college-id))))

(k/reg-event-db :update-custom-field-success
  (fn [db [student-id category entry-index field-index name value]]
    (let [field-path [:students student-id :student/college-lists category
                      entry-index :college-list-entry/custom-fields field-index]]
      (-> db
          (assoc-in [:xhr :update-custom-field]
                    {:in-flight? false, :error nil, :success? true})
          (assoc-in (conj field-path :custom-field/name) name)
          (assoc-in (conj field-path :custom-field/value) value)
          (assoc-in [:modal :name] name)
          (assoc-in [:modal :value] value)))))
