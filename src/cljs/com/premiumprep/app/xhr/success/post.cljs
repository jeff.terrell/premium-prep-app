(ns com.premiumprep.app.xhr.success.post
  "Re-frame event definitions for events that receive success responses from the
  backend for POST requests."
  (:require [kee-frame.core :as k]))

(k/reg-event-fx :login-success
  (fn [cofx [form-data data]]
    (let [{:keys [account/type account/state account/email db/id]} data]
      {:db (-> (:db cofx)
               (assoc-in [:xhr :login]
                         {:in-flight? false, :error nil, :success? true})
               (assoc :current-user
                      {:email email, :type type, :id id, :state state}))
       :dispatch [:navigate-to-authenticated-home]
       :dispatch-later [{:ms 3000
                         :dispatch [:clear-xhr :login]}]})))

(k/reg-event-db :create-student-success
  (fn [db [form-data student]]
    (let [{:keys [db/id]} student]
      (-> db
          (assoc-in [:xhr :create-student]
                    {:in-flight? false, :error nil, :success? true})
          (update :students assoc id student)))))

(k/reg-event-db :create-counselor-success
  (fn [db [form-data data]]
    (let [{:keys [counselor password]} data
          {:keys [db/id]} counselor]
      (-> db
          (assoc-in [:xhr :create-counselor]
                    {:in-flight? false, :error nil, :success? true})
          (update :counselors assoc id counselor)
          (assoc :new-counselor-password password)))))

(k/reg-event-fx :create-college-success
  (fn [{:keys [db]} [college]]
    {:db (let [{:keys [db/id]} college]
           (-> db
               (assoc-in [:xhr :create-college]
                         {:in-flight? false, :error nil, :success? true})
               (update :colleges assoc id college)))
     :reset-form "new-college-form"}))

(k/reg-event-db :reset-password-success
  (fn [db _]
    (assoc-in db [:xhr :reset-password]
              {:in-flight? false, :error nil, :success? true})))

(k/reg-event-db :change-email-success
  (fn [db _]
    (assoc-in db [:xhr :change-email]
              {:in-flight? false, :error nil, :success? true})))

(k/reg-event-db :signup-success
  (fn [db [form-data]]
    (assoc-in db [:xhr :signup]
              {:in-flight? false, :error nil, :success? true})))

(k/reg-event-db :upload-document-success
  (fn [db [student-id doc-map]]
    (-> db
        (assoc-in [:xhr :upload-document]
                  {:in-flight? false, :error nil, :success? true})
        (update-in [:students student-id :student/documents]
                   conj doc-map))))

(k/reg-event-fx :post-comment-success
  (fn [{:keys [db]} [student-id comment-type comment]]
    {:db (let [comment-attr (keyword "student" comment-type)]
           (-> db
               (assoc-in [:xhr :post-comment student-id comment-type]
                         {:in-flight? false, :error nil, :success? true})
               (update-in [:students student-id comment-attr]
                          conj comment)))
     :dispatch-later [{:ms 3000
                       :dispatch [:clear-xhr :post-comment
                                  student-id comment-type]}]
     :reset-form (str "new-comment-form-for-" comment-type)}))

(k/reg-event-fx :create-issue-success
  (fn [{:keys [db]} [issue]]
    {:db (let [student-id (get-in issue [:issue/student :db/id])]
           (-> db
               (assoc-in [:xhr :create-issue]
                         {:in-flight? false, :error nil, :success? true})
               (update-in [:students student-id :issue/_student]
                          (fnil conj []) (select-keys issue [:db/id]))
               (assoc-in [:issues (:db/id issue)] issue)))
     :dispatch-later [{:ms 3000
                       :dispatch [:clear-xhr :create-issue]}]
     :reset-form "create-issue-form"}))

(k/reg-event-fx :add-college-list-entry-success
  (fn [{:keys [db]} [student-id college-lists]]
    {:db (-> db
             (assoc-in [:xhr :add-college-list-entry]
                       {:in-flight? false, :error nil, :success? true})
             (assoc-in [:students student-id :student/college-lists]
                       college-lists))
     :dispatch [:com.premiumprep.app.events/set-modal-visibility false]}))

(k/reg-event-db :create-custom-field-success
  (fn [db [student-id category index updated-entry]]
    (let [{:keys [db/id custom-field/name custom-field/value]} updated-entry]
      (-> db
          (assoc-in [:xhr :create-custom-field]
                    {:in-flight? false, :error nil, :success? true})
          (update-in [:students student-id :student/college-lists category index
                      :college-list-entry/custom-fields]
                     conj updated-entry)
          (assoc-in [:modal :name] name)
          (assoc-in [:modal :value] value)
          (assoc-in [:modal :field-id] id)))))
