(ns com.premiumprep.app.xhr.send
  "Re-frame event definitions for events that send requests to the backend."
  (:require
   ;; Require these namespaces for their side effect of registering re-frame
   ;; events, not for anything we need to call or reference here.
   com.premiumprep.app.xhr.send.del
   com.premiumprep.app.xhr.send.get
   com.premiumprep.app.xhr.send.post
   com.premiumprep.app.xhr.send.put))

