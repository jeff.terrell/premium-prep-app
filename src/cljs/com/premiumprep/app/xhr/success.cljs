(ns com.premiumprep.app.xhr.success
  "Re-frame event definitions for events that receive success responses from the
  backend."
  (:require
   ;; Require these namespaces for their side effect of registering re-frame
   ;; events, not for anything we need to call or reference here.
   com.premiumprep.app.xhr.success.del
   com.premiumprep.app.xhr.success.get
   com.premiumprep.app.xhr.success.post
   com.premiumprep.app.xhr.success.put))

