(ns com.premiumprep.app.xhr.send.del
  ;; Use .del instead of .delete in the namespace to avoid a compile warning
  ;; because of the reserved `delete` keyword in JS.
  "Re-frame event definitions for events that send DELETE requests to the
  backend."
  (:require [ajax.core :as ajax]
            [com.premiumprep.app.config :refer [api-url]]
            day8.re-frame.http-fx
            [kee-frame.core :as k]))

(k/reg-event-fx :logout
  (fn [{:keys [db]} _]
    {:http-xhrio {:method :delete
                  :uri (api-url "/logout")
                  :format          (ajax/text-request-format)
                  :response-format (ajax/text-response-format)
                  :on-success [:logout-success]
                  :on-failure [:logout-failure]}
     :db (-> db
             (assoc-in [:xhr :logout] {:in-flight? true})
             (dissoc :current-user)
             ;; prevent login page from redirecting home:
             (update :xhr dissoc :login)
             (update :xhr dissoc :current-user))}))

(k/reg-event-fx :remove-college
  (fn [{db :db :as cofx} [student-id college-id]]
    {:http-xhrio {:method :delete
                  :uri (api-url "/college-list-entry")
                  :body (doto (new js/FormData)
                          (.append "student-id" student-id)
                          (.append "college-id" college-id))
                  :format :multipart
                  :response-format (ajax/transit-response-format)
                  :on-success [:remove-college-success student-id college-id]
                  :on-failure [:remove-college-failure student-id]}
     :db (assoc-in db [:xhr :remove-college student-id]
                   {:in-flight? true})}))

(k/reg-event-fx :delete-custom-field
  (fn [{db :db :as cofx} [entry-id student-id field-id]]
    (let [=entry #(= entry-id (:db/id %))
          has-entry? #(some =entry %)
          lists-by-category (get-in db [:students student-id
                                        :student/college-lists])
          [category list] (first (filter (fn [[_ list]] (has-entry? list))
                                         lists-by-category))
          [entry-index entry] (->> list
                                   (map-indexed vector)
                                   (filter (fn [[i entry]] (=entry entry)))
                                   first)
          =field (fn [[idx field]] (= field-id (:db/id field)))
          field-index (->> entry :college-list-entry/custom-fields
                           (map-indexed vector) (filter =field) ffirst)]

      {:http-xhrio {:method :delete
                    :uri (api-url "/custom-field")
                    :body (doto (new js/FormData)
                            (.append "id" field-id))
                    :response-format (ajax/text-response-format)
                    :on-success [:delete-custom-field-success student-id
                                 category entry-index field-index]
                    :on-failure [:delete-custom-field-failure]}
       :db (assoc-in db [:xhr :delete-custom-field]
                     {:in-flight? true})})))

(k/reg-event-fx :delete-document
  (fn [{db :db :as cofx} [student-id document-id]]
    {:http-xhrio {:method :delete
                  :uri (api-url "/document")
                  :body (doto (new js/FormData)
                          (.append "student-id" student-id)
                          (.append "document-id" document-id))
                  :response-format (ajax/transit-response-format)
                  :on-success [:delete-document-success student-id document-id]
                  :on-failure [:delete-document-failure]}
     :db (assoc-in db [:xhr :delete-document document-id]
                   {:in-flight? true})}))
