(ns com.premiumprep.app.xhr.send.put
  "Re-frame event definitions for events that send PUT requests to the backend."
  (:require [ajax.core :as ajax]
            [com.premiumprep.app.config :refer [api-url]]
            day8.re-frame.http-fx
            [kee-frame.core :as k]))

(k/reg-event-fx :update-counselor
  (fn [{:keys [db]} [id form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :update-counselor :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url (str "/counselor/" id))
                    :body form-data
                    :format :multipart
                    :response-format (ajax/transit-response-format)
                    :on-success [:update-counselor-success form-data]
                    :on-failure [:update-counselor-failure]}
       :db (assoc-in db [:xhr :update-counselor] {:in-flight? true})})))

(k/reg-event-fx :update-issue
  (fn [{:keys [db]} [id control form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :update-issue :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url (str "/issue/" id))
                    :body form-data
                    :format :multipart
                    :response-format (ajax/transit-response-format)
                    :on-success [:update-issue-success id control]
                    :on-failure [:update-issue-failure]}
       :db (assoc-in db [:xhr :update-issue] {:in-flight? true})})))

(k/reg-event-fx :update-student
  (fn [{:keys [db]} [id form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :update-student :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url (str "/student/" id))
                    :body form-data
                    :format :multipart
                    :response-format (ajax/transit-response-format)
                    :on-success [:update-student-success id form-data]
                    :on-failure [:update-student-failure]}
       :db (assoc-in db [:xhr :update-student] {:in-flight? true})})))

(k/reg-event-fx :update-student-profile
  (fn [{:keys [db]} [id form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :update-student-profile :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url (str "/student/" id "/profile"))
                    :body form-data
                    :format :multipart
                    :response-format (ajax/transit-response-format)
                    :on-success [:update-student-profile-success id]
                    :on-failure [:update-student-profile-failure]}
       :db (assoc-in db [:xhr :update-student-profile] {:in-flight? true})})))

(k/reg-event-fx :re-send-invite
  (fn [{:keys [db]} [account-id]]
    (when-not (get-in db [:xhr :re-send-invite :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url (str "/account/" account-id "/re-send-invite"))
                    :body ""
                    :response-format (ajax/text-response-format)
                    :on-success [:re-send-invite-success]
                    :on-failure [:re-send-invite-failure]}
       :db (assoc-in db [:xhr :re-send-invite] {:in-flight? true})})))

(k/reg-event-fx :set-account-active-attribute
  (fn [{db :db :as cofx}
       [active? account-type account-id student-or-counselor-id]]
    (assert (#{:account-type/counselor :account-type/student} account-type)
            "Error: only counselors and students can be activated/deactivated.")
    (let [base-path (if active? "/reactivate-account/" "/deactivate-account/")
          url (api-url (str base-path account-id))]
      {:http-xhrio {:method :put
                    :uri url
                    :body ""
                    :response-format (ajax/text-response-format)
                    :on-success [:set-account-active-attribute-success
                                 active? account-type student-or-counselor-id]
                    :on-failure [:set-account-active-attribute-failure]}
       :db (assoc-in db [:xhr :set-account-active-attribute]
                     {:in-flight? true})})))

(k/reg-event-fx :forgot-password
  (fn [{:keys [db]} [form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :forgot-password :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url "/forgot-password")
                    :body form-data
                    :format :multipart
                    :response-format (ajax/text-response-format)
                    :on-success [:forgot-password-success form-data]
                    :on-failure [:forgot-password-failure]}
       :db (assoc-in db [:xhr :forgot-password] {:in-flight? true})})))

(k/reg-event-fx :change-password
  (fn [{:keys [db]} [form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :change-password :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url "/change-password")
                    :body form-data
                    :format :multipart
                    :response-format (ajax/text-response-format)
                    :on-success [:change-password-success]
                    :on-failure [:change-password-failure]}
       :db (assoc-in db [:xhr :change-password] {:in-flight? true})})))

(k/reg-event-fx :set-issue-state
  (fn [cofx [issue-id to-state]]
    ;; stifle duplicate requests
    (when-not (get-in cofx [:db :xhr :set-issue-state issue-id :in-flight?])
      {:http-xhrio {:method :put
                    :uri (api-url "/set-issue-state")
                    :body (doto (new js/FormData)
                            (.append "issue-id" issue-id)
                            (.append "to-state" (name to-state)))
                    :format :multipart
                    :response-format (ajax/transit-response-format)
                    :on-success [:set-issue-state-success]
                    :on-failure [:set-issue-state-failure issue-id]}
       :db (assoc-in (:db cofx)
                     [:xhr :set-issue-state issue-id]
                     {:in-flight? true})})))

(k/reg-event-fx :update-entry-field
  (fn [{db :db :as cofx} [form-data]]
    (let [student-id (.get form-data "student-id")
          college-id (.get form-data "college-id")
          field (.get form-data "field")
          value (.get form-data "value")
          same-college? (comp (partial = college-id)
                              (comp :db/id :college-list-entry/college))
          has-college? (partial some same-college?)
          lists (get-in db [:students student-id
                            :student/college-lists])
          category (key (first (filter (comp has-college? val) lists)))
          index (ffirst (filter (comp same-college? second)
                                (map-indexed list (get lists category))))]

      {:http-xhrio {:method :put
                    :uri (api-url "/college-list-entry-field")
                    :body form-data
                    :format :multipart
                    :response-format (ajax/text-response-format)
                    :on-success [:update-entry-field-success
                                 student-id college-id category index field
                                 value]
                    :on-failure [:update-entry-field-failure college-id]}
       :db (assoc-in db [:xhr :update-entry-field college-id]
                     {:in-flight? true})})))

(k/reg-event-fx :update-entry-category
  (fn [{db :db :as cofx} [form-data]]
    (let [student-id (.get form-data "student-id")
          college-id (.get form-data "college-id")]
      {:http-xhrio {:method :put
                    :uri (api-url "/college-list-entry-category")
                    :body form-data
                    :format :multipart
                    :response-format (ajax/transit-response-format)
                    :on-success [:update-entry-category-success
                                 student-id college-id]
                    :on-failure [:update-entry-category-failure student-id]}
       :db (assoc-in db [:xhr :update-entry-category student-id]
                     {:in-flight? true})})))

(k/reg-event-fx :update-custom-field
  (fn [{db :db :as cofx} [form-data]]
    (let [entry-id (-> db :modal :entry-id)
          student-id (-> db :modal :student-id)
          =entry #(= entry-id (:db/id %))
          has-entry? #(some =entry %)
          lists-by-category (get-in db [:students student-id
                                        :student/college-lists])
          [category list] (first (filter (fn [[_ list]] (has-entry? list))
                                         lists-by-category))
          [entry-index entry] (->> list
                                   (map-indexed vector)
                                   (filter (fn [[i entry]] (=entry entry)))
                                   first)
          field-id (.get form-data "id")
          =field (fn [[idx field]] (= field-id (:db/id field)))
          field-index (->> entry :college-list-entry/custom-fields
                           (map-indexed vector) (filter =field) ffirst)
          name (.get form-data "name")
          value (.get form-data "value")]

      {:http-xhrio {:method :put
                    :uri (api-url (str "/custom-field/" field-id))
                    :body form-data
                    :format :multipart
                    :response-format (ajax/text-response-format)
                    :on-success [:update-custom-field-success student-id
                                 category entry-index field-index name value]
                    :on-failure [:update-custom-field-failure]}
       :db (assoc-in db [:xhr :update-custom-field]
                     {:in-flight? true})})))
