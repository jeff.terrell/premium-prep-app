(ns com.premiumprep.app.xhr.send.get
  "Re-frame event definitions for events that send GET requests to the backend."
  (:require [ajax.core :as ajax]
            [com.premiumprep.app.config :refer [api-url]]
            [com.premiumprep.app.xhr.util :refer [make-event-name]]
            day8.re-frame.http-fx
            [kee-frame.core :as k]
            [re-frame.core :as rf]))

(defn ^:private get-config [path key-base]
  {:method :get
   :uri (api-url path)
   :response-format (ajax/transit-response-format)
   :on-success [(make-event-name key-base :success)]
   :on-failure [(make-event-name key-base :failure)]})

(defn ^:private get-effect-map [cofx api-path event-base xhr-subkey]
  {:http-xhrio (get-config api-path event-base)
   :db (assoc-in (:db cofx) [:xhr xhr-subkey] {:in-flight? true})})

(k/reg-event-fx :current-user/load
  (fn [cofx]
    (get-effect-map cofx "/current-user" :current-user/load :current-user)))

(k/reg-event-fx :students/load
  (fn [cofx]
    (get-effect-map cofx "/students" :students/load :students)))

(k/reg-event-fx :counselors/load
  (fn [cofx]
    (get-effect-map cofx "/counselors" :counselors/load :counselors)))

(k/reg-event-fx :colleges/load
  (fn [cofx]
    ;; Don't re-fetch colleges for each student, but do fetch if there are only
    ;; a few manually added colleges.
    (when (> 5 (count (:colleges (:db cofx))))
      (get-effect-map cofx "/colleges" :colleges/load :colleges))))

(k/reg-event-fx :student/load
  (fn [cofx [id]]
    (get-effect-map cofx (str "/student/" id) :student/load :student)))

(k/reg-event-fx :me-student/load
  (fn [{db :db :as cofx} [id]]
    (when (and (not (contains? (:students db) id))
               (not= :account-state/email-unverified
                     (-> db :current-user :state)))
      (get-effect-map cofx "/me/student" :me-student/load :me-student))))

(rf/reg-cofx :search-param
  (fn [coeffects key]
    (let [sp (new js/URLSearchParams js/window.location.search)
          value (.get sp (name key))]
      (assoc coeffects key value))))

(k/reg-event-fx :verify-token
  [(rf/inject-cofx :search-param :token)]
  (fn [{:keys [token] :as cofx}]
    (assoc-in (get-effect-map cofx (str "/verify-token?token=" token)
                              :verify-token :verify-token)
              [:db :page :page-type/signup :token]
              token)))

(k/reg-event-fx :verify-email
  [(rf/inject-cofx :search-param :token)]
  (fn [cofx]
    (get-effect-map cofx (str "/verify-email?token=" (:token cofx))
                    :verify-email :verify-email)))

(k/reg-event-fx :issues/load
  (fn [cofx]
    (get-effect-map cofx "/issues" :issues/load :issues)))
