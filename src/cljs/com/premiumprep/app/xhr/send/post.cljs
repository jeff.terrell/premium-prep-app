(ns com.premiumprep.app.xhr.send.post
  "Re-frame event definitions for events that send POST requests to the
  backend."
  (:require [ajax.core :as ajax]
            [com.premiumprep.app.config :refer [api-url]]
            day8.re-frame.http-fx
            [kee-frame.core :as k]))

(defn ^:private post-config
  ([path form-data on-success on-failure]
   (post-config path form-data on-success on-failure nil))
  ([path form-data on-success on-failure overrides]
   (merge {:method :post
           :uri (api-url path)
           :body form-data
           :format :multipart
           :response-format (ajax/transit-response-format)
           :on-success on-success
           :on-failure on-failure}
          overrides)))

(k/reg-event-fx :login
  (fn [cofx [form-data]]
    {:http-xhrio (post-config "/login" form-data
                              [:login-success form-data]
                              [:login-failure])
     :db (assoc-in (:db cofx) [:xhr :login] {:in-flight? true})}))

(k/reg-event-fx :create-student
  (fn [cofx [form-data]]
    {:http-xhrio (post-config "/student" form-data
                              [:create-student-success form-data]
                              [:create-student-failure])
     :db (assoc-in (:db cofx) [:xhr :create-student] {:in-flight? true})}))

(k/reg-event-fx :create-counselor
  (fn [cofx [form-data]]
    {:http-xhrio (post-config "/counselor" form-data
                              [:create-counselor-success form-data]
                              [:create-counselor-failure])
     :db (assoc-in (:db cofx) [:xhr :create-counselor] {:in-flight? true})}))

(k/reg-event-fx :create-college
  (fn [cofx [form-data]]
    {:http-xhrio (post-config "/college" form-data
                              [:create-college-success]
                              [:create-college-failure])
     :db (assoc-in (:db cofx) [:xhr :create-college] {:in-flight? true})}))

(k/reg-event-fx :reset-password
  (fn [{:keys [db]} [form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :reset-password :in-flight?])
      {:http-xhrio (post-config "/reset-password" form-data
                                [:reset-password-success]
                                [:reset-password-failure]
                                {:response-format (ajax/text-response-format)})
       :db (assoc-in db [:xhr :reset-password] {:in-flight? true})})))

(k/reg-event-fx :change-email
  (fn [{:keys [db]} [form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :change-email :in-flight?])
      {:http-xhrio (post-config "/change-email" form-data
                                [:change-email-success]
                                [:change-email-failure]
                                {:response-format (ajax/text-response-format)})
       :db (assoc-in db [:xhr :change-email] {:in-flight? true})})))

(k/reg-event-fx :signup
  (fn [cofx [form-data]]
    {:http-xhrio (post-config "/signup" form-data
                              [:signup-success]
                              [:signup-failure]
                              {:response-format (ajax/text-response-format)})
     :db (assoc-in (:db cofx) [:xhr :signup] {:in-flight? true})}))

(k/reg-event-fx :upload-document
  (fn [cofx [student-id form-data]]
    {:http-xhrio (post-config "/document" form-data
                              [:upload-document-success student-id]
                              [:upload-document-failure])
     :db (assoc-in (:db cofx) [:xhr :upload-document] {:in-flight? true})}))

(k/reg-event-fx :post-comment
  (fn [cofx [student-id comment-type form-data]]
    ;; stifle duplicate requests
    (when-not (get-in cofx [:db :xhr :post-comment student-id comment-type
                            :in-flight?])
      {:http-xhrio (post-config "/comment" form-data
                                [:post-comment-success student-id comment-type]
                                [:post-comment-failure student-id comment-type])
       :db (assoc-in (:db cofx)
                     [:xhr :post-comment student-id comment-type]
                     {:in-flight? true})})))

(k/reg-event-fx :create-issue
  (fn [cofx [student-id form-data]]
    ;; stifle duplicate requests
    (when-not (get-in cofx [:db :xhr :create-issue :in-flight?])
      {:http-xhrio (post-config "/issue" form-data
                                [:create-issue-success]
                                [:create-issue-failure])
       :db (assoc-in (:db cofx) [:xhr :create-issue] {:in-flight? true})})))

(k/reg-event-fx :add-college-list-entry
  (fn [{:keys [db]} [student-id form-data]]
    ;; stifle duplicate requests
    (when-not (get-in db [:xhr :add-college-list-entry student-id :in-flight?])
      {:http-xhrio (post-config "/college-list-entry" form-data
                                [:add-college-list-entry-success student-id]
                                [:add-college-list-entry-failure])
       :db (assoc-in db [:xhr :add-college-list-entry] {:in-flight? true})})))

(k/reg-event-fx :create-custom-field
  (fn [{db :db :as cofx} [form-data]]
    (let [entry-id (-> db :modal :entry-id)
          student-id (-> db :modal :student-id)
          =entry #(= entry-id (:db/id %))
          has-entry? #(some =entry %)
          lists-by-category (get-in db [:students student-id
                                        :student/college-lists])
          [category list] (first (filter (fn [[_ list]] (has-entry? list))
                                         lists-by-category))
          index (->> list
                     (map-indexed vector)
                     (filter (fn [[i entry]] (=entry entry)))
                     ffirst)]

      {:http-xhrio (post-config "/custom-field" (doto form-data
                                                  (.append "entry-id" entry-id))
                                [:create-custom-field-success student-id
                                 category index]
                                [:create-custom-field-failure])
       :db (assoc-in db [:xhr :create-custom-field]
                     {:in-flight? true})})))
