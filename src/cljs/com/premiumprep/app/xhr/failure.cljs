(ns com.premiumprep.app.xhr.failure
  "Re-frame event definitions for events that receive failure responses from the
  backend."
  (:require [com.premiumprep.app.xhr.util :refer [make-event-name]]
            [kee-frame.core :as k]))

(defn reg-xhr-failure-event [event-base xhr-subkey]
  (let [event-name (make-event-name event-base :failure)]
    (k/reg-event-db event-name
      (fn [db [error]]
        (assoc-in db [:xhr xhr-subkey]
                  {:in-flight? false
                   :error (let [response (:response error)]
                            (if (map? response)
                              (:error response)
                              response))
                   :success? false})))))

(reg-xhr-failure-event :current-user/load :current-user)
(reg-xhr-failure-event :students/load :students)
(reg-xhr-failure-event :counselors/load :counselors)
(reg-xhr-failure-event :colleges/load :colleges)
(reg-xhr-failure-event :student/load :student)
(reg-xhr-failure-event :me-student/load :me-student)
(reg-xhr-failure-event :verify-token :verify-token)
(reg-xhr-failure-event :verify-email :verify-email)
(reg-xhr-failure-event :issues/load :issues)
(reg-xhr-failure-event :logout :logout)
(reg-xhr-failure-event :create-student :create-student)
(reg-xhr-failure-event :update-counselor :update-counselor)
(reg-xhr-failure-event :update-student :update-student)
(reg-xhr-failure-event :update-student-profile :update-student-profile)
(reg-xhr-failure-event :create-counselor :create-counselor)
(reg-xhr-failure-event :create-college :create-college)
(reg-xhr-failure-event :re-send-invite :re-send-invite)
(reg-xhr-failure-event :set-account-active-attribute
                       :set-account-active-attribute)
(reg-xhr-failure-event :forgot-password :forgot-password)
(reg-xhr-failure-event :reset-password :reset-password)
(reg-xhr-failure-event :change-password :change-password)
(reg-xhr-failure-event :change-email :change-email)
(reg-xhr-failure-event :signup :signup)
(reg-xhr-failure-event :upload-document :upload-document)
(reg-xhr-failure-event :create-issue :create-issue)
(reg-xhr-failure-event :add-college-list-entry :add-college-list-entry)
(reg-xhr-failure-event :create-custom-field :create-custom-field)
(reg-xhr-failure-event :update-custom-field :update-custom-field)
(reg-xhr-failure-event :delete-custom-field :delete-custom-field)
(reg-xhr-failure-event :delete-document :delete-document)
(reg-xhr-failure-event :update-issue :update-issue)

(k/reg-event-db :login-failure
  (fn [db [error]]
    (assoc-in db [:xhr :login]
              {:in-flight? false
               :error (:status-text error)
               :success? false})))

(k/reg-event-db :post-comment-failure
  (fn [db [student-id comment-type error]]
    (assoc-in db [:xhr :post-comment student-id comment-type]
              {:in-flight? false
               :error (-> error :response :error)
               :success? false})))

(k/reg-event-db :set-issue-state-failure
  (fn [db [issue-id error]]
    (assoc-in db [:xhr :set-issue-state issue-id]
              {:in-flight? false
               :error (-> error :response :error)
               :success? false})))

(k/reg-event-db :update-entry-field-failure
  (fn [db [college-id error]]
    (assoc-in db [:xhr :update-entry-field college-id]
              {:in-flight? false, :error (:response error), :success? false})))

(k/reg-event-db :update-entry-category-failure
  (fn [db [student-id error]]
    (assoc-in db [:xhr :update-entry-category student-id]
              {:in-flight? false
               :error (-> error :response :error)
               :success? false})))

(k/reg-event-db :remove-college-failure
  (fn [db [student-id error]]
    (assoc-in db [:xhr :remove-college student-id]
              {:in-flight? false
               :error (-> error :response :error)
               :success? false})))
