(ns com.premiumprep.app.xhr.util
  "Utility functions used by various XHR namespaces.")

(defn make-event-name
  "Given a base and suffix (both keywords), combine them into a new keyword.
  Ex: (make-event-name :students/load :success) => :students/load-success
  Ex: (make-event-name :verify-token :failure)  => :verify-token-failure"
  [base-keyword suffix-keyword]
  (keyword (subs (str base-keyword "-" (name suffix-keyword)) 1)))
