(ns com.premiumprep.app.db-spec.document
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :document/name string?)
(s/def :document/content-type string?)
(s/def :document/uploaded-at inst?)
(s/def :document/link string?)   ; actually a URI
(s/def ::document
  (s/keys :req [:db/id :document/name :document/uploaded-at]
          :opt [:document/content-type :document/link]))
