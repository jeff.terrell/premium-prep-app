(ns com.premiumprep.app.db-spec.student
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.account :as account]
            [com.premiumprep.app.db-spec.college-list-entry :as cl-entry]
            [com.premiumprep.app.db-spec.comment :as comment]
            [com.premiumprep.app.db-spec.custom-field :as custom-field]
            [com.premiumprep.app.db-spec.db :as db]
            [com.premiumprep.app.db-spec.document :as document]))

(s/def :student/documents (s/coll-of ::document/document :kind vector?))
(s/def :student/account ::account/account)
(s/def :student/counselor (s/keys :req [:counselor/account]))
(s/def :student/joined-at inst?)
(s/def :student/graduation-year int?)
(s/def :student/high-school string?)
(s/def :student/current-college string?)
(s/def :student/academic-interest string?)
(s/def :student/gpa-weighted-9 float?)
(s/def :student/gpa-unweighted-9 float?)
(s/def :student/gpa-weighted-9-10 float?)
(s/def :student/gpa-unweighted-9-10 float?)
(s/def :student/gpa-weighted-9-11 float?)
(s/def :student/gpa-unweighted-9-11 float?)
(s/def :student/class-rank-9 int?)
(s/def :student/class-rank-9-10 int?)
(s/def :student/class-rank-9-11 int?)
(s/def :student/class-rank-9-of int?)
(s/def :student/class-rank-9-10-of int?)
(s/def :student/class-rank-9-11-of int?)
(s/def :student/sat-subject-tests string?)
(s/def :student/ap-tests string?)
(s/def :student/home-phone string?)
(s/def :student/extracurricular-activities string?)
(s/def :student/current-courses string?)
(s/def :student/prospective-courses string?)
(s/def :student/custom-fields
  (s/coll-of ::custom-field/custom-field :kind vector?))

(s/def :student/college-list (s/coll-of ::cl-entry/entry :kind vector?))
(s/def :student/college-list-type
  #{:high-reach :reach :target :likely :uncategorized})

(s/def :student/college-lists
  (s/nilable (s/every-kv :student/college-list-type
                         :student/college-list)))

(s/def :student/profile-comments ::comment/list)
(s/def :student/document-list-comments ::comment/list)

(s/def :college-planning-app/type #{:college-planning-app-type/naviance
                                    :college-planning-app-type/scoir
                                    :college-planning-app-type/maia
                                    :college-planning-app-type/kickstart})
(s/def :college-planning-app/username string?)
(s/def :college-planning-app/password string?)
(s/def :college-planning-app/hs-login-page-url string?) ; actually a URI
(s/def :student/college-planning-app-info
  (s/keys :opt [:college-planning-app/type
                :college-planning-app/username
                :college-planning-app/password
                :college-planning-app/hs-login-page-url]))

(s/def :sat/total-score int?)
(s/def :sat/ebrw-score int?)
(s/def :sat/math-score int?)
(s/def :sat/date inst?)
(s/def :student/sat
  (s/keys :opt [:sat/total-score :sat/ebrw-score :sat/math-score :sat/date]))
(s/def :student/psat :student/sat)
(s/def :student/sat1 :student/sat)
(s/def :student/sat2 :student/sat)
(s/def :student/sat3 :student/sat)
(s/def :student/sat4 :student/sat)
(s/def :sat-superscore/sat :student/sat)
(s/def :sat-superscore/ebrw-date inst?)
(s/def :sat-superscore/math-date inst?)
(s/def :student/sat-superscore
  (s/keys :opt [:sat-superscore/sat :sat-superscore/ebrw-date
                :sat-superscore/math-date]))

(s/def :act/composite-score int?)
(s/def :act/english-score int?)
(s/def :act/reading-score int?)
(s/def :act/math-score int?)
(s/def :act/science-score int?)
(s/def :act/date inst?)
(s/def :student/act
  (s/keys :opt [:act/composite-score :act/english-score :act/reading-score
                :act/math-score :act/science-score :act/date]))
(s/def :student/pact :student/act)
(s/def :student/act1 :student/act)
(s/def :student/act2 :student/act)
(s/def :student/act3 :student/act)
(s/def :student/act4 :student/act)
(s/def :act-super/act :student/act)
(s/def :act-super/english-date inst?)
(s/def :act-super/reading-date inst?)
(s/def :act-super/math-date inst?)
(s/def :act-super/science-date inst?)
(s/def :student/act-superscore
  (s/keys :opt [:act-super/act :act-super/english-date :act-super/reading-date
                :act-super/math-date :act-super/science-date]))

(s/def ::student
  (s/keys :req [:db/id :student/account]
          :opt [:student/counselor :student/joined-at :student/graduation-year
                :student/high-school :student/documents :student/college-lists
                :student/profile-comments :student/document-list-comments
                :issue/_student :student/current-college
                :student/academic-interest :student/gpa-weighted-9
                :student/gpa-unweighted-9 :student/gpa-weighted-9-10
                :student/gpa-unweighted-9-10 :student/gpa-weighted-9-11
                :student/gpa-unweighted-9-11 :student/class-rank-9
                :student/class-rank-9-10 :student/class-rank-9-11
                :student/class-rank-9-of :student/class-rank-9-10-of
                :student/class-rank-9-11-of :student/sat-subject-tests
                :student/ap-tests :student/home-phone
                :student/extracurricular-activities :student/current-courses
                :student/prospective-courses :student/college-planning-app-info
                :student/psat :student/sat-superscore :student/sat1
                :student/sat2 :student/sat3 :student/sat4 :student/pact
                :student/act-superscore :student/act1 :student/act2
                :student/act3 :student/act4 :student/custom-fields]))

(s/def ::students (s/every-kv :db/id ::student))
