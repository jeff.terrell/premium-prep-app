(ns com.premiumprep.app.db-spec.comment
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.account :as account]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :comment/created-at inst?)
(s/def :comment/text string?)
(s/def :comment/commenter
  (s/keys :req [:db/id account/email account/full-name]))
(s/def ::comment
  (s/keys :req [:db/id :comment/created-at :comment/text :comment/commenter]))
(s/def ::list (s/coll-of ::comment :kind vector?))
