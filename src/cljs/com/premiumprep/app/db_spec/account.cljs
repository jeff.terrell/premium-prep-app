(ns com.premiumprep.app.db-spec.account
  (:refer-clojure :exclude [type])
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :account/email string?)
;; Also export a var so that it's clear when connections to this spec from
;; elsewhere are intended rather than accidental uses of the same keyword.
(def email :account/email)

(s/def :account/full-name string?)
(def full-name :account/full-name)

(s/def :account/preferred-name string?)
(s/def :account/mobile (s/nilable string?))
(s/def :account/active? boolean?)

(s/def :account/state #{:account-state/initialized
                        :account-state/uninitialized
                        :account-state/email-unverified})
(def state :account/state)

(s/def :account/type #{:account-type/superuser :account-type/counselor
                       :account-type/parent :account-type/student})
(def type :account/type)

(s/def ::account
  (s/keys :opt [:db/id :account/email :account/full-name :account/preferred-name
                :account/type :account/state :account/active?]))
