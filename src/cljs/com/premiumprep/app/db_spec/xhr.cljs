(ns com.premiumprep.app.db-spec.xhr
  (:require [cljs.spec.alpha :as s]))

(s/def ::in-flight boolean?)
(s/def ::success boolean?)
(s/def ::error (constantly true))
(s/def ::xhr-status (s/keys :req-un [::in-flight?]
                            :opt-un [::success? ::error]))
(s/def :xhr/login ::xhr-status)
(s/def :xhr/student ::xhr-status)
(s/def :xhr/students ::xhr-status)
(s/def ::xhr (s/keys :opt-un [:xhr/login :xhr/students :xhr/student]))
