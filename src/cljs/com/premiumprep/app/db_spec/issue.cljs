(ns com.premiumprep.app.db-spec.issue
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :issue/text string?)
(s/def :issue/action-taken string?)
(s/def :issue/created-at inst?)
(s/def :issue/counselor-touched-at inst?)
(s/def :issue/state #{:issue-state/new
                      :issue-state/read
                      :issue-state/needs-discussion
                      :issue-state/counselor-resolved
                      :issue-state/closed})
(s/def :issue/student ::db/reference)
(s/def ::issue
  (s/keys :req [:db/id :issue/created-at :issue/text :issue/state]
          :opt [:issue/counselor-touched-at :issue/student :issue/action-taken]))
(s/def ::list (s/coll-of ::issue :kind vector?))
(s/def :issue/_student (s/coll-of ::db/reference :kind vector?))
(s/def ::issues (s/every-kv :db/id ::issue))
