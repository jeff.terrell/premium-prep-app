(ns com.premiumprep.app.db-spec.db
  (:require [cljs.spec.alpha :as s]))

(s/def :db/id string?)

(s/def ::reference (s/keys :req [:db/id]))
