(ns com.premiumprep.app.db-spec.modal
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.db :as db]
            [com.premiumprep.app.db-spec.issue :as issue]))

(s/def ::counselor-id :db/id)
(s/def ::entry-id :db/id)
(s/def ::field-id :db/id)
(s/def ::student-id :db/id)
(s/def ::name string?)
(s/def ::value string?)

(s/def :modal/type #{:modal-type/hidden
                     :modal-type/add-college
                     :modal-type/create-issue
                     :modal-type/custom-field
                     :modal-type/edit-counselor
                     :modal-type/edit-student
                     :modal-type/forgot-password
                     :modal-type/update-issue
                     :modal-type/upload-document})

(defmulti modal-type :modal/type)

(s/def ::type-only (s/keys :req [:modal/type]))
(defmethod modal-type :modal-type/hidden [_] ::type-only)
(defmethod modal-type :modal-type/add-college [_] ::type-only)
(defmethod modal-type :modal-type/forgot-password [_] ::type-only)
(defmethod modal-type :modal-type/upload-document [_] ::type-only)

(defmethod modal-type :modal-type/create-issue [_]
  (s/keys :req [:modal/type], :req-un [::student-id]))

(defmethod modal-type :modal-type/custom-field [_]
  (s/keys :req [:modal/type]
          :opt-un [::entry-id ::student-id ::field-id ::name ::value]))

(defmethod modal-type :modal-type/edit-counselor [_]
  (s/keys :req [:modal/type], :req-un [::counselor-id]))

(defmethod modal-type :modal-type/edit-student [_]
  (s/keys :req [:modal/type], :req-un [::student-id]))

(defmethod modal-type :modal-type/update-issue [_]
  (s/keys :req [:modal/type], :opt-un [::issue/issue]))

(s/def ::modal (s/multi-spec modal-type :modal/type))
