(ns com.premiumprep.app.db-spec.custom-field
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :custom-field/name string?)
(s/def :custom-field/value string?)
(s/def ::custom-field
  (s/keys :opt [:custom-field/name :custom-field/value]))
