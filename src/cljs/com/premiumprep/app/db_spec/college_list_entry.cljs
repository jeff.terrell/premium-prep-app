(ns com.premiumprep.app.db-spec.college-list-entry
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :college-list-entry/college ::db/reference)
(s/def :college-list-entry/visited? boolean?)
(s/def :college-list-entry/date-added inst?)
(s/def :college-list-entry/early-decision? boolean?)
(s/def :college-list-entry/eliminated? boolean?)
(s/def :college-list-entry/location string?)
(s/def :college-list-entry/setting string?)
(s/def :college-list-entry/app-round string?)
(s/def :college-list-entry/app-deadline inst?)
(s/def :college-list-entry/app-submitted? boolean?)
(s/def :college-list-entry/program-major string?)
(s/def :college-list-entry/interest-demonstrated string?)
(s/def :college-list-entry/counselor-notes string?)
(s/def :college-list-entry/student-notes string?)
(s/def :college-list-entry/rd-admit-rate string?)
(s/def :college-list-entry/ed-admit-rate string?)
(s/def :college-list-entry/hs-admit-rate string?)
(s/def :college-list-entry/hs-avg-sat string?)
(s/def :college-list-entry/hs-avg-act string?)
(s/def :college-list-entry/hs-avg-gpa string?)
(s/def :college-list-entry/testing-requirements string?)
(s/def :college-list-entry/scholarship-info string?)
(s/def :college-list-entry/scholarship-deadlines string?)
(s/def :college-list-entry/admissions-contacts string?)
(s/def :college-list-entry/portfolio-requirements string?)
(s/def :college-list-entry/portfolio-status string?)
(s/def :college-list-entry/financial-aid-forms-required string?)
(s/def :college-list-entry/financial-aid-deadline string?)
(s/def :college-list-entry/first-outcome string?)
(s/def :college-list-entry/final-outcome string?)
(s/def :college-list-entry/scholarship? boolean?)
(s/def :college-list-entry/scholarship-amount string?)
(s/def :college-list-entry/scholarship-name string?)
(s/def :college-list-entry/matriculation? boolean?)

(s/def ::entry
  (s/keys :req [:db/id
                :college-list-entry/college]
          :opt [:college-list-entry/visited?
                :college-list-entry/early-decision?
                :college-list-entry/date-added
                :college-list-entry/eliminated?
                :college-list-entry/location
                :college-list-entry/setting
                :college-list-entry/app-round
                :college-list-entry/app-deadline
                :college-list-entry/app-submitted?
                :college-list-entry/program-major
                :college-list-entry/interest-demonstrated
                :college-list-entry/counselor-notes
                :college-list-entry/student-notes
                :college-list-entry/rd-admit-rate
                :college-list-entry/ed-admit-rate
                :college-list-entry/hs-admit-rate
                :college-list-entry/hs-avg-sat
                :college-list-entry/hs-avg-act
                :college-list-entry/hs-avg-gpa
                :college-list-entry/testing-requirements
                :college-list-entry/scholarship-info
                :college-list-entry/scholarship-deadlines
                :college-list-entry/admissions-contacts
                :college-list-entry/portfolio-requirements
                :college-list-entry/portfolio-status
                :college-list-entry/financial-aid-forms-required
                :college-list-entry/financial-aid-deadline
                :college-list-entry/first-outcome
                :college-list-entry/final-outcome
                :college-list-entry/scholarship?
                :college-list-entry/scholarship-amount
                :college-list-entry/scholarship-name
                :college-list-entry/matriculation?]))
