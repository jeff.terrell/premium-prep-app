(ns com.premiumprep.app.db-spec.counselor
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.account :as account]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :counselor/account ::account/account)
(s/def :student/_counselor (s/every ::db/reference))
(s/def ::counselor
  (s/keys :req [:db/id :counselor/account :student/_counselor]))

(s/def ::counselors (s/every-kv :db/id ::counselor))
