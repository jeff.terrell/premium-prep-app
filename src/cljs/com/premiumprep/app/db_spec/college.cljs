(ns com.premiumprep.app.db-spec.college
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def :college/name string?)
(s/def :college/enrollment string?)
(s/def :college/admit-rate string?)
(s/def :college/average-sat string?)
(s/def :college/average-act string?)

(s/def ::college
  (s/keys :req [:db/id]
          :opt [:college/name :college/enrollment :college/admit-rate
                :college/average-sat :college/average-act]))

(s/def ::colleges (s/every-kv :db/id ::college))
