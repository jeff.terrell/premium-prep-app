(ns com.premiumprep.app.db-spec.page
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.account :as account]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def ::column keyword?)
(s/def ::ascending? boolean?)
(s/def ::table-sort
  (s/keys :req-un [::column ::ascending?]))

(s/def ::mismatch-error boolean?)

(s/def :page-type/change-password
  (s/keys :opt-un [::mismatch-error]))

(s/def :page-type/counselor
  (s/keys :req-un [::table-sort]))

(s/def :page-type/counselors
  (s/keys :req-un [::table-sort]))

(s/def :page-type/issues
  (s/keys :req-un [::table-sort]))

(s/def :page-type/reset-password
  (s/keys :opt-un [::mismatch-error]))

(s/def ::token string?)
(s/def ::email account/email)

(s/def :page-type/signup
  (s/keys :req-un [::token]
          :opt-un [::email]))

(s/def ::document-list-comments boolean?)
(s/def ::profile-comments boolean?)
(s/def ::show-all-docs? boolean?)
(s/def ::show-full-profile? boolean?)
(s/def ::show-eliminated-colleges? boolean?)

(s/def ::comments-visibility
  (s/keys :opt-un [::profile-comments
                   ::document-list-comments]))

(s/def ::college-details-visibility (s/every-kv :db/id boolean?))

(s/def ::college-list-entry-field
  #{:visited? :early-decision? :location :setting :app-round :app-deadline
    :app-submitted? :program-major :interest-demonstrated :counselor-notes
    :student-notes :rd-admit-rate :ed-admit-rate :hs-admit-rate :hs-avg-sat
    :hs-avg-act :hs-avg-gpa :testing-requirements :scholarship-info
    :scholarship-deadlines :admissions-contacts :portfolio-requirements
    :portfolio-status :financial-aid-forms-required :financial-aid-deadline
    :first-outcome :final-outcome :scholarship? :scholarship-amount :scholarship-name :matriculation? :category})

(s/def ::editing-college-list-entry-field
  (s/every-kv :db/id (s/nilable ::college-list-entry-field)))

(s/def :page-type/student
  (s/keys :opt-un [::college-details-visibility
                   ::comments-visibility
                   ::editing-college-list-entry-field
                   ::show-all-docs?
                   ::show-full-profile?
                   ::show-eliminated-colleges?]))

(s/def :page-type/students
  (s/keys :req-un [::table-sort]))

(s/def ::page (s/keys :opt [:page-type/change-password
                            :page-type/counselor
                            :page-type/counselors
                            :page-type/issues
                            :page-type/reset-password
                            :page-type/signup
                            :page-type/student
                            :page-type/students]))
