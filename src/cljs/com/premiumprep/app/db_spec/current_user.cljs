(ns com.premiumprep.app.db-spec.current-user
  (:require [cljs.spec.alpha :as s]
            [com.premiumprep.app.db-spec.account :as account]
            [com.premiumprep.app.db-spec.db :as db]))

(s/def ::current-user
  (s/nilable
    (s/keys :req-un [:db/id account/type account/state account/email])))

(s/def ::current-student-id (s/nilable :db/id))
