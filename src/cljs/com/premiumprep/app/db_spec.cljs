(ns com.premiumprep.app.db-spec
  "Kee-frame has a very nice feature where, before resetting the app-db to a new
  value, it checks whether the new value conforms to a spec. If so, it proceeds
  with the reset. If not, it refuses to reset the value and prints an error
  message to the console. This identifies likely bugs as soon as possible, when
  the re-frame effects are actually being handled. This namespace defines that
  spec; the bottom `db-spec` def is what the app-db values are actually checked
  against."
  (:require
   [cljs.spec.alpha :as s]
   [com.premiumprep.app.db-spec.college :as college]
   [com.premiumprep.app.db-spec.counselor :as counselor]
   [com.premiumprep.app.db-spec.current-user :as current-user]
   [com.premiumprep.app.db-spec.db :as db]
   [com.premiumprep.app.db-spec.issue :as issue]
   [com.premiumprep.app.db-spec.modal :as modal]
   [com.premiumprep.app.db-spec.page :as page]
   [com.premiumprep.app.db-spec.student :as student]
   [com.premiumprep.app.db-spec.xhr :as xhr] ; TODO: add missing keys
   ))

(s/def ::new-counselor-password string?)
(s/def :student-list/students-active-status-filter #{:all :active-only})

(s/def ::db-spec
  (s/keys :opt-un
          [::college/colleges
           ::counselor/counselors
           ::current-user/current-student-id ;; FIXME: move this to page state
           ::current-user/current-user
           ::issue/issues
           ::modal/modal
           ::new-counselor-password ;; FIXME: move this to page state
           ::page/page
           ::student/students
           :student-list/students-active-status-filter ;; FIXME: move this to page state
           ::xhr/xhr]))
