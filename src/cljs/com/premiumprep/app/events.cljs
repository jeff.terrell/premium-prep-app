(ns com.premiumprep.app.events
  (:require com.premiumprep.app.xhr
            [kee-frame.core :as k]
            [re-frame.core :as rf :refer [path]]))

(k/reg-event-fx :navigate-to
  (fn [cofx args]
    {:navigate-to args}))

(k/reg-event-fx :navigate-to-authenticated-home
  (fn [{:keys [db]} _]
    {:navigate-to (case (-> db :current-user :type)
                    :account-type/superuser [:issues]
                    :account-type/counselor [:students]
                    [:home])}))

(k/reg-event-db ::set-initial-sort
  (fn [db [page-type]]
    (assoc-in db [:page page-type :table-sort]
              {:column :name
               :ascending? true})))

(k/reg-event-db ::set-initial-issues-sort
  (fn [db _]
    (assoc-in db [:page :page-type/issues :table-sort]
              {:column :state
               :ascending? false})))

(k/reg-event-db ::set-students-active-status-filter
  (fn [db [value]]
    (assoc db :students-active-status-filter value)))

(k/reg-event-db ::set-modal-visibility
  [(path :modal)]
  (fn [_ [visible? modal-type extra-modal-state-map]]
    (if-not visible?
      {:modal/type :modal-type/hidden}
      (assoc extra-modal-state-map :modal/type modal-type))))

(k/reg-event-fx ::open-edit-student-modal
  (fn [_ [id]]
    {:dispatch [::set-modal-visibility true :modal-type/edit-student
                {:student-id id}]}))

(k/reg-event-fx ::open-edit-counselor-modal
  (fn [_ [id]]
    {:dispatch [::set-modal-visibility true :modal-type/edit-counselor
                {:counselor-id id}]}))

(k/reg-event-fx :deactivate-account
  (fn [_ [account-type account-id student-or-counselor-id]]
    {:dispatch [:set-account-active-attribute false account-type account-id
                student-or-counselor-id]}))

(k/reg-event-fx :reactivate-account
  (fn [_ [account-type account-id student-or-counselor-id]]
    {:dispatch [:set-account-active-attribute true account-type account-id
                student-or-counselor-id]}))

(k/reg-event-db :clear-xhr
  (fn [db [& subkeys]]
    (assoc-in db (into [:xhr] subkeys)
              {:in-flight? false, :error nil, :success? false})))

(k/reg-event-fx ::load-all-issues-data
  (fn [_]
    {:dispatch-n [[:counselors/load]
                  [:students/load]
                  [:issues/load]
                  [::set-initial-issues-sort]]}))

(k/reg-event-fx :load-counselors-and-students
  (fn [_]
    {:dispatch-n [[:counselors/load]
                  [:students/load]
                  [::set-initial-sort :page-type/students]]}))

(k/reg-event-fx :load-student-data
  (fn [_ [student-id]]
    {:dispatch-n [[:colleges/load]
                  [:issues/load]
                  [:student/load student-id]]}))

(k/reg-event-fx :home/load
  (fn [_]
    {:dispatch-n [[:colleges/load]
                  [:me-student/load]]}))

(rf/reg-fx :reset-form
  (fn [form-id]
    (-> form-id js/document.getElementById .reset)))

(k/reg-event-db ::reverse-table-sort-order
  (fn [db [page-type]]
    (update-in db [:page page-type :table-sort :ascending?] not)))

(k/reg-event-db ::change-table-sort-column
  (fn [db [page-type column]]
    (-> db
        (assoc-in [:page page-type :table-sort :column] column)
        (assoc-in [:page page-type :table-sort :ascending?] true))))

(k/reg-event-fx ::sort-by-column
  (fn [cofx [page-type new-column]]
    (let [current-column (get-in cofx [:db :page page-type :table-sort :column])]
      (if (= current-column new-column)
        {:dispatch [::reverse-table-sort-order page-type]}
        {:dispatch [::change-table-sort-column page-type new-column]}))))

(k/reg-event-fx :students/load-and-set-initial-sort
  (fn [_ _]
    {:dispatch-n [[:students/load]
                  [::set-initial-sort :page-type/students]]}))

(k/reg-event-fx :counselors/load-and-set-initial-sort
  (fn [_ _]
    {:dispatch-n [[:counselors/load]
                  [::set-initial-sort :page-type/counselors]]}))

(k/reg-event-db :toggle-college-expanded
  (fn [db [college-id]]
    (update-in db
               [:page :page-type/student :college-details-visibility college-id]
               not)))

(k/reg-event-db :edit-field-for-college
  (fn [db [college-id field]]
    (assoc-in db
              [:page :page-type/student :editing-college-list-entry-field
               college-id]
              field)))

(k/reg-event-db :stop-editing-field-for-college
  (fn [db [college-id]]
    (update-in db
               [:page :page-type/student :editing-college-list-entry-field]
               dissoc college-id)))

(k/reg-event-fx :edit-student
  (fn [{:keys [db]} [id]]
    (if (contains? (get-in db [:students id]) :student/college-lists)
      {:dispatch [:clear-xhr :update-student-profile]}
      ;; fetch student data if we started on the edit student page
      {:dispatch [:student/load id]})))

(rf/reg-fx :scroll-to-identifier
  (fn [id]
    (-> id js/document.getElementById .scrollIntoView)))

(k/reg-event-fx ::scroll-to-identifier
  (fn [_ [id]]
    {:scroll-to-identifier id}))
