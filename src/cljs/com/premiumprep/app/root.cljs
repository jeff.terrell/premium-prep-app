(ns com.premiumprep.app.root
  (:require
   [com.premiumprep.app.events :as events]
   [com.premiumprep.app.page.change-email :refer [change-email-page]]
   [com.premiumprep.app.page.change-password :refer [change-password-page]]
   [com.premiumprep.app.page.counselor :refer [counselor-page]]
   [com.premiumprep.app.page.counselors :refer [counselors-page]]
   [com.premiumprep.app.page.edit-student :refer [edit-student-page]]
   [com.premiumprep.app.page.home :refer [home-page]]
   [com.premiumprep.app.page.issues :refer [issues-page]]
   [com.premiumprep.app.page.login :refer [login-page]]
   [com.premiumprep.app.page.new-college :refer [new-college-page]]
   [com.premiumprep.app.page.new-counselor :refer [new-counselor-page]]
   [com.premiumprep.app.page.new-student :refer [new-student-page]]
   [com.premiumprep.app.page.reset-password :refer [reset-password-page]]
   [com.premiumprep.app.page.signup :refer [signup-page]]
   [com.premiumprep.app.page.student :refer [student-page]]
   [com.premiumprep.app.page.students :refer [students-page]]
   [com.premiumprep.app.page.verify-email :refer [verify-email-page]]
   com.premiumprep.app.xhr
   [kee-frame.core :as k]))

(def routes
  [["/" :login]
   ["/change-email" :change-email]
   ["/change-password" :change-password]
   ["/counselors/new" :new-counselor]
   ["/counselor/:id" :counselor]
   ["/counselors" :counselors]
   ["/home" :home]
   ["/issues" :issues]
   ["/logout" :logout]
   ["/new-college" :new-college]
   ["/reset-password" :reset-password]
   ["/signup" :signup]
   ["/students/new" :new-student]
   ["/student/:id" :student]
   ["/student/:id/edit" :edit-student]
   ["/students" :students]
   ["/verify-email" :verify-email]])

(k/reg-controller :login
  {:params #(when (= :login (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :change-email
  {:params #(when (= :change-email (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :change-password
  {:params #(when (= :change-password (-> % :data :name)) true)
   :start (fn [] [:clear-xhr :change-password])})

(k/reg-controller :new-counselor
  {:params #(when (= :new-counselor (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :counselor
  {:params (fn [{:keys [data path-params]}]
             (when (= (:name data) :counselor)
               (:id path-params)))
   :start  (fn [_ id] [:students/load-and-set-initial-sort])})

(k/reg-controller :counselors
  {:params #(when (= :counselors (-> % :data :name)) true)
   :start [:counselors/load-and-set-initial-sort]})

(k/reg-controller :home
  {:params #(when (= :home (-> % :data :name)) true)
   :start (fn [] [:home/load])})

(k/reg-controller :issues
  {:params #(when (= :issues (-> % :data :name)) true)
   :start (fn [] [::events/load-all-issues-data])})

(k/reg-controller :logout
  {:params #(when (= :logout (-> % :data :name)) true)
   :start (fn [] [:logout])})

(k/reg-controller :new-college
  {:params #(when (= :new-college (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :reset-password
  {:params #(when (= :reset-password (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :signup
  {:params #(when (= :signup (-> % :data :name)) true)
   :start (fn [] [:verify-token])})

(k/reg-controller :new-student
  {:params #(when (= :new-student (-> % :data :name)) true)
   :start [:counselors/load]
   :stop [:clear-xhr :create-student]})

(k/reg-controller :student
  {:params (fn [{:keys [data path-params]}]
             (when (= (:name data) :student)
               (:id path-params)))
   :start  (fn [_ id] [:load-student-data id])})

(k/reg-controller :edit-student
  {:params (fn [{:keys [data path-params]}]
             (when (= (:name data) :edit-student)
               (:id path-params)))
   :start  (fn [_ id] [:edit-student id])})

(k/reg-controller :students
  {:params #(when (= :students (-> % :data :name)) true)
   :start (fn [] [:load-counselors-and-students])})

(k/reg-controller :verify-email
  {:params #(when (= :verify-email (-> % :data :name)) true)
   :start (fn [] [:verify-email])})

(defn root-component []
  (k/switch-route (fn [route] (-> route :data :name))
                  :login [login-page]
                  :change-password [change-password-page]
                  :change-email [change-email-page]
                  :new-counselor [new-counselor-page]
                  :counselor (fn [{{id :id} :path-params :as r}] [counselor-page id])
                  :counselors [counselors-page]
                  :home [home-page]
                  :issues [issues-page]
                  :logout "Logging out…"
                  :new-college [new-college-page]
                  :reset-password [reset-password-page]
                  :signup [signup-page]
                  :new-student [new-student-page]
                  :student (fn [{{id :id} :path-params :as r}] [student-page id])
                  :edit-student (fn [{{id :id} :path-params :as r}] [edit-student-page id])
                  :students [students-page]
                  :verify-email [verify-email-page]
                  nil [login-page]))
