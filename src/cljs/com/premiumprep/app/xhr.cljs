(ns com.premiumprep.app.xhr
  (:require
   ;; require these namespaces for their side effect of registering re-frame
   ;; events:
   com.premiumprep.app.xhr.failure
   com.premiumprep.app.xhr.send
   com.premiumprep.app.xhr.success))
