(ns com.premiumprep.app.model.counselor-fields-common
  (:require [com.premiumprep.app.field :refer [->fields]]))

(def fields
  (->fields
    ["email" [:counselor/account :account/email]]
    ["name" [:counselor/account :account/full-name]]
    ["preferred-name" [:counselor/account :account/preferred-name]]
    ["mobile" [:counselor/account :account/mobile]]))
