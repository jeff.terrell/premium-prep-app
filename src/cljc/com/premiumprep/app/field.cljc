(ns com.premiumprep.app.field)

(defn ->field
  ([key path] (->field key path false))
  ([key path required?]
   {:key key, :path path, :required? required?}))

(defn ->fields
  [& field-arglists]
  (into {}
        (for [arglist field-arglists]
          (let [field (apply ->field arglist)]
            [(:key field) field]))))
