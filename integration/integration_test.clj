(ns integration-test
  (:require [app-driver :as a :refer [*d*]]
            [clojure.string :as str]
            [datomic.client.api :as d]
            [etaoin.api :as et]
            [etaoin.keys :as keys]
            [speclj.core :refer [around before context describe it should
                                 should-not should= with]]))

(describe "The login page"
  (around [it] (a/with-system it))
  (before
    (a/go-home *d*)
    (et/wait-visible *d* {:tag :input, :name "email"})
    (should (et/has-text? *d* "Login")))

  (it "navigates to a different page upon successful login"
    (a/fill-and-submit-login-form *d* "" "")
    (et/wait-has-text *d* :app "❌")
    (should (et/has-text? *d* "❌")) ; indicates failure when auth fails
    (a/login-as-superuser *d*)
    ;; this is the place that admins get redirected to; other users go elsewhere
    (should (a/at-issues-page? *d*)))

  (it "refuses logins to deactivated accounts, even with the right credentials"
    (let [conn (a/get-db-conn)
          ;; FIXME: brittle copies of values from .schema ns
          cemail "kyptin+counselor2@gmail.com"
          cpassword "counselor"
          [cacct-id cid] (first
                           (d/q {:query
                                 '{:find [?account ?c]
                                   :in [$ ?email]
                                   :where [[?account :account/email ?email]
                                           [?c :counselor/account ?account]]}
                                 :args [(d/db conn) cemail]}))
          tx-data (fn [active?] [[:db/add cacct-id :account/active? active?]])]
      (d/transact conn {:tx-data (tx-data false)})
      (a/fill-and-submit-login-form *d* cemail cpassword)
      (et/wait-has-text *d* :app "❌")
      (should (et/has-text? *d* "❌"))
      (d/transact conn {:tx-data (tx-data true)})
      (a/fill-and-submit-login-form *d* cemail cpassword)
      (et/wait-absent *d* {:tag :input, :name "email"})
      (should (a/at-students-page? *d*))))

  (it "automatically logs user in based on session"
    (a/login-as-superuser *d*)
    (a/go-home *d*)
    ;; skips login page and goes directly to default admin page
    (et/wait-exists *d* {:tag :h1, :fn/text "All Feedback"})
    (should (a/at-issues-page? *d*))))

(describe "A superuser"
  (around [it] (a/with-system it))
  (before (a/login-as-superuser *d*))

  (it "can add a new college"
    (et/click *d* {:tag :a, :fn/text "Add College"})
    (et/wait-exists *d* {:tag :h1, :fn/text "Add College"})
    (et/fill *d* :name "University of the Highlands and Islands")
    (et/fill *d* :enrollment "2,033")
    (et/fill *d* :admit-rate "50%")
    (et/fill *d* :average-sat "unknown")
    (should= 0 (count (a/get-sent-emails)))
    (et/fill *d* :average-act "unknown" keys/enter)
    (et/wait-exists *d* {:css "div.success"})
    (should= 1 (count (a/get-sent-emails))))

  (context "at the counselors page"
    (before (a/go-to-counselors-page *d*))

    (it "can edit a counselor"
      (let [first-counselor-tr "table.counselors tbody tr:first-child "
            first-edit-link (str first-counselor-tr "td.controls a:first-child")
            first-counselor-name (str first-counselor-tr "td.counselor-name")]
        (et/click *d* {:css first-edit-link})
        (et/wait-visible *d* {:tag :h3, :fn/text "Edit Counselor"})
        (et/clear *d* {:tag :input, :name "mobile"})
        (et/fill *d* {:tag :input, :name "mobile"} "abc")
        (et/click *d* {:tag :a, :fn/text "Save"})
        (et/wait-visible *d* {:css "div.status.error"})
        (et/clear *d* {:tag :input, :name "mobile"})
        (et/fill *d* {:tag :input, :name "mobile"} "919.765.4321")
        (et/clear *d* {:tag :input, :name "name"})
        (et/fill *d* {:tag :input, :name "name"}, "Booyah")
        (et/click *d* {:tag :a, :fn/text "Save"})
        (et/wait-invisible *d* {:tag :h3, :fn/text "Edit Counselor"})
        (should= "Booyah" (et/get-element-text *d* {:css first-counselor-name}))
        (et/click *d* {:css first-edit-link})
        (et/wait-visible *d* {:tag :h3, :fn/text "Edit Counselor"})
        (et/clear *d* {:tag :input, :name "name"})
        (et/fill *d* {:tag :input, :name "name"}, "Counselor 2")
        (et/click *d* {:tag :a, :fn/text "Save"})
        (et/wait-invisible *d* {:tag :h3, :fn/text "Edit Counselor"})))

    (it "can view details of a counselor"
      (et/wait-exists *d* {:tag :a, :fn/text "Ronald Hawkins"})
      (et/click *d* {:tag :a, :fn/text "Ronald Hawkins"})
      (et/wait-exists *d* {:tag :a, :fn/text "< Counselors"})
      (should (a/at-counselor-page? *d*)))

    (it "can create a counselor"
      (let [existing-email "kyptin+counselor2@gmail.com"
            new-name "Created Counselor"
            new-email "created2@student.com"]
        (a/logout *d*)
        (et/wait-visible *d* {:css "#login-page input[name=email]"})
        (et/fill *d* {:tag :input, :name "email"} a/superuser-email)
        (et/fill *d* {:tag :input, :name "password"} a/superuser-password keys/enter)
        (et/wait-visible *d* {:tag "a", :fn/text "Counselors"})
        (et/click *d* {:tag "a", :fn/text "Counselors"})
        (et/wait-visible *d* {:tag "a", :fn/text "Invite Counselor"})
        (et/click *d* {:tag "a", :fn/text "Invite Counselor"})
        (et/wait-visible *d* {:tag "h1", :fn/text "New Counselor"})
        (et/fill *d* {:tag :input, :name "name"} new-name)
        ;; test that duplicate emails are not allowed, with visible error message
        (et/fill *d* {:tag :input, :name "email"} existing-email keys/enter)
        (et/wait-visible *d* {:css "div.status.error"})
        (should (.contains (et/get-element-text *d* {:css "div.status.error"})
                           (format "The given email (%s) is already taken"
                                   existing-email)))
        ;; continue with creation, with everything valid this time
        (et/clear *d* {:tag :input, :name "email"})
        (et/fill *d* {:tag :input, :name "email"} new-email)
        (should (et/visible? *d* {:css "div.form-submit input[type=submit]"}))
        (et/click *d* {:css "div.form-submit input[type=submit]"})
        (et/wait-visible *d* {:css "div.status.success"})
        (should (.startsWith (et/get-element-text *d* {:css "div.status.success"})
                             "Success!")))))

  (context "at a counselor page"
    (before (a/go-to-counselor-page *d*))

    (it "can reassign a student to a different counselor"
      (a/reassign-first-student *d*)
      (should (et/absent? *d* {:css "table.students"})))

    (it "can deactivate and reactivate the counselor's account"
      (a/reassign-first-student *d*) ;; gotta do this before deactivating
      (et/click *d* {:tag :a :fn/text "Deactivate Account"})
      (et/wait-has-alert *d*)
      (should (et/has-alert? *d*))
      (et/dismiss-alert *d*)
      (should (a/at-counselor-page? *d*))
      (et/click *d* {:tag :a :fn/text "Deactivate Account"})
      (et/wait-has-alert *d*)
      (should (et/has-alert? *d*))
      (et/accept-alert *d*)
      (et/wait-exists *d* {:tag :p
                           :fn/has-text " account has been deactivated."})
      (should (.startsWith (et/get-element-text *d* {:css "main article p.notice"})
                           "This counselor's account has been deactivated."))
      (let [counselor-name (et/get-element-text *d* {:css "main h1"})]
        (et/click *d* {:css "main article nav.breadcrumb a"})
        (et/wait-exists *d* {:tag :a, :fn/text counselor-name})
        (should (et/exists? *d* {:css "table tr.inactive"
                                 :fn/has-text counselor-name}))

        ;; and reactivate
        (et/click *d* {:css "table tr.inactive td.counselor-name a"})
        (et/wait-exists *d* {:tag :a :fn/text "Reactivate Account"})
        (should (a/at-counselor-page? *d*))
        (et/click *d* {:tag :a :fn/text "Reactivate Account"})
        (et/wait-has-alert *d*)
        (et/dismiss-alert *d*)
        (et/click *d* {:tag :a :fn/text "Reactivate Account"})
        (et/accept-alert *d*)
        (et/wait-absent *d* {:css "main article p.notice"})
        (et/click *d* {:css "main article nav.breadcrumb a"})
        (et/wait-absent *d* {:css "table tr.inactive"})
        (should (et/has-text? *d* counselor-name))
        (should (et/absent? *d* {:css "table tr.inactive"})))))

  (context "at the students page"
    (before (a/go-to-students-page *d*))

    (it "can see the student list"
      (should (a/at-students-page? *d*)))

    (it "can sort the table by different columns"
      (should (et/visible? *d* {:css "th.sorted.ascending:first-child"}))
      (should (et/visible? *d* {:css "th.sortable:nth-child(2)"}))
      (et/click *d* {:css "th.sorted.ascending:first-child"})
      (et/wait-visible *d* {:css "th.sorted.descending:first-child"})
      (et/click *d* {:css "th.sortable:nth-child(2)"})
      (et/wait-visible *d* {:css "th.sorted.ascending:nth-child(2)"})
      (should (et/visible? *d* {:css "th.sortable:first-child"})))

    (it "can navigate to info page for a student"
      (et/click *d* {:tag :a, :fn/text "Student Two"})
      (et/wait-exists *d* {:tag :h1, :fn/has-text "Student: Student Two"})
      (should (a/at-student-page? *d*)))

    (it "can create a student"
      (let [existing-email "kyptin+student1@gmail.com"
            new-name "Created Student"
            new-email "created@student.com"]
        (et/wait-visible *d* {:tag "a", :fn/text "New Student"})
        (et/click *d* {:tag "a", :fn/text "New Student"})
        (et/wait-visible *d* {:tag "h1", :fn/text "New Student"})
        (should (et/visible? *d* {:css "select#counselor-id"}))
        (et/click *d* {:css "select#counselor-id"})
        (should (et/visible? *d* {:tag :option, :fn/text "Ronald Hawkins"}))
        (et/click *d* {:tag :option, :fn/text "Ronald Hawkins"})
        ;; test that duplicate emails are not allowed, with visible error message
        (et/fill *d* {:tag :input, :name "email"} existing-email keys/enter)
        (et/wait-visible *d* {:css "div.status.error"})
        (should (.contains (et/get-element-text *d* {:css "div.status.error"})
                           (format "The given email (%s) is already taken"
                                   existing-email)))
        ;; continue with creation, with everything valid this time
        (et/clear *d* {:tag :input, :name "email"})
        (et/fill *d* {:tag :input, :name "email"} new-email)
        (should (et/visible? *d* {:css "div.form-submit input[type=submit]"}))
        (et/click *d* {:css "div.form-submit input[type=submit]"})
        (et/wait-visible *d* {:css "div.status.success"})
        (should (.startsWith (et/get-element-text *d* {:css "div.status.success"})
                             "Success!"))

        ;; password reset attempt blocked for unverified account
        (a/logout *d*)
        (et/wait-visible *d* {:css "div.forgot-password a"})
        (et/click *d* {:css "div.forgot-password a"})
        (et/wait-visible *d* {:tag :h3, :fn/text "Forgot your password?"})
        (et/fill *d* {:css "#forgot-password-modal input[name=email]"}
                 new-email keys/enter)
        (et/wait-visible *d* {:css "#forgot-password-modal div.error"})
        (should (.startsWith
                  (et/get-element-text
                    *d* {:css "#forgot-password-modal div.error"})
                  "Error: you cannot reset the password of an uninitialized account"))
        (et/click *d* {:tag :a, :fn/text "Cancel"})
        (et/wait-absent *d* {:css "#forgot-password-modal"})
        (et/back *d*)

        ;; student should receive an email with the sign up link
        (should= 1 (count (a/get-sent-emails)))
        (let [{:keys [to subject body]} (first (a/get-sent-emails))
              query '{:find [?token]
                      :in [$ ?email]
                      :where [[?account :account/email ?email]
                              [?account :account/token ?token]]}
              args [(d/db (a/get-db-conn)), new-email]
              token (ffirst (d/q {:query query, :args args}))
              expected-path (str "/signup?token=" token)
              expected-url (str "https://app.premiumprep.com" expected-path)
              local-url (a/localize-url expected-url)
              password "a really secure passphrase"]
          (should= new-email to)
          (should= "Welcome to Premium Prep" subject)
          (should (.contains body expected-url))
          (a/go-to-path *d* (str expected-path "1")) ; to make token invalid
          (et/wait-visible *d* {:css "div.error"})
          (should (.startsWith (et/get-element-text *d* {:css "div.error"})
                               "Error: your signup token could not be found."))
          (a/go-to-path *d* expected-path)
          (et/wait-visible *d*
                           {:tag :h1 :fn/text "Set Up Your Premium Prep Account"})
          (should= new-email (et/get-element-attr *d* :email :value))
          (et/fill *d* :full-name "Joe Schmo")
          (et/fill *d* :mobile "(888) 555-5303")
          (et/fill *d* :high-school "Upper East Highd")
          (et/fill *d* :graduation-year "2024")
          (et/fill *d* :address "222 Any Street")
          (et/fill *d* :city "Anytown")
          (et/fill *d* :state "ML")
          (et/fill *d* :zip "12345")
          (et/fill *d* :parent1-full-name "Parental Unit")
          (et/fill *d* :parent1-email "parent@email.com")
          (et/fill *d* :parent1-mobile "919.867.5309")
          (et/fill *d* :password password)
          (et/fill *d* :confirmation (str password "2") keys/enter)
          (et/wait-visible *d* {:css "div.status.error"})
          (should (.contains (et/get-element-text *d* {:css "div.status"})
                             "The given passwords do not match"))
          (et/clear *d* :confirmation)
          (et/fill *d* :confirmation password keys/enter)
          (et/wait-visible *d* {:css "div.status.success"})
          (should (.contains (et/get-element-text *d* {:css "div.status"})
                             "Signup successful!"))
          (et/click *d* {:css "div.status a"})
          (et/wait-visible *d* {:css "#login-page input[name=email]"})
          (et/fill *d* {:css "input[name=email]"} new-email)
          (et/fill *d* {:css "input[name=password]"} password keys/enter)
          (et/wait-visible *d* {:css "#student-page"})))))

  (context "at a student page"
    (before
      (a/go-to-students-page *d*)
      (et/click *d* {:tag :a, :fn/text "Student Two"})
      (et/wait-exists *d* {:tag :h1, :fn/has-text "Student: Student Two"}))

    (it "can add, remove, and re-add a college list entry"
      (et/click *d* {:tag :a, :fn/text "Add a College"})
      (et/wait-visible *d* :add-college-list-entry-form)
      (et/click *d* {:css "#add-college-list-entry-form select"})
      (let [css "#add-college-list-entry-form option:nth-child(2)"]
        (et/wait-visible *d* {:css css})
        (let [college-name (et/get-element-text *d* {:css css})]
          (et/click *d* {:css css})
          (et/click *d* :add-college-category-reach)
          (et/click *d* {:tag :a, :fn/text "Submit"})
          (et/wait-invisible *d* :add-college-list-entry-form)
          (should (et/visible? *d* {:tag :td
                                    :class "college-name"
                                    :fn/text college-name}))))
      (et/click *d* {:css "div.college-list tr.collapsed td.college-name"})
      (et/wait-visible *d* {:css "div.college-list tr.expanded"})
      (et/click *d* {:tag :a, :fn/text "Remove College"})
      (et/wait-invisible *d* {:css "div.college-list tr.expanded"})
      (should (et/invisible? *d* {:css "div.college-list tr.eliminated"}))
      (should (et/visible? *d* :eliminated-visibility-toggle))
      (et/click *d* :eliminated-visibility-toggle)
      (et/wait-visible *d* {:css "div.college-list tr.eliminated"})
      (et/click *d* :eliminated-visibility-toggle)
      (et/wait-invisible *d* {:css "div.college-list tr.eliminated"})
      (et/click *d* {:tag :a, :fn/text "Add a College"})
      (et/wait-visible *d* :add-college-list-entry-form)
      (et/click *d* {:css "#add-college-list-entry-form select"})
      (let [css "#add-college-list-entry-form option:nth-child(2)"]
        (et/wait-visible *d* {:css css})
        (let [college-name (et/get-element-text *d* {:css css})]
          (et/click *d* {:css css})
          (et/click *d* :add-college-category-reach)
          (et/click *d* {:tag :a, :fn/text "Submit"})
          (et/wait-invisible *d* :add-college-list-entry-form)
          (should (et/visible? *d* {:tag :td
                                    :class "college-name"
                                    :fn/text college-name})))))

    (it "can create an issue (and implicitly send email to counselor)"
      (should (et/visible? *d* {:tag :a, :fn/text "Create Issue"}))
      (et/click *d* {:tag :a, :fn/text "Create Issue"})
      (et/wait-visible *d* :create-issue-form)
      (let [issue-text "this is a new issue for this student."]
        (et/fill *d* {:css "#create-issue-form textarea"} issue-text)
        (et/click *d* {:tag :a, :fn/text "Submit"})
        (et/wait-visible *d* {:css "div.success"})
        (should (= 1 (count (a/get-sent-emails))))
        (et/click *d* {:tag :a, :fn/text "Cancel"})
        (should (.contains (et/get-element-text *d* {:css "section.issues"})
                           issue-text))
        (should (et/visible? *d* {:css "tr.issue.new"}))))

    (it "can deactivate and reactivate a student's account"
      (et/click *d* {:tag :a :fn/text "Deactivate Account"})
      (et/wait-has-alert *d*)
      (should (et/has-alert? *d*))
      (et/dismiss-alert *d*)
      (should (a/at-student-page? *d*))
      (et/click *d* {:tag :a :fn/text "Deactivate Account"})
      (et/wait-has-alert *d*)
      (should (et/has-alert? *d*))
      (et/accept-alert *d*)
      (et/wait-exists *d* {:css "main article p.notice"})
      (should (.startsWith (et/get-element-text *d* {:css "main article p.notice"})
                           "This student's account has been deactivated."))
      (let [student-name (second
                           (str/split (et/get-element-text *d* {:css "main h1"})
                                      #": "))]
        (et/click *d* {:fn/text "Students"})
        (et/wait-exists *d* {:css "select.active-status-filter"})

        ;; change students active status filter
        (should-not (et/has-text? *d* student-name))
        (should (et/absent? *d* {:css "table tr.inactive"}))
        (et/click *d* {:css "select.active-status-filter"})
        (should (et/visible? *d* {:tag :option, :fn/text "Show: All Students"}))
        (et/click *d* {:tag :option, :fn/text "Show: All Students"})
        (et/wait-visible *d* {:tag :a, :fn/text student-name})
        (should (et/exists? *d* {:css "table tr.inactive"}))

        ;; as superuser, reactivate a student's account
        (et/click *d* {:css "table tr.inactive td.student-name a"})
        (et/wait-exists *d* {:tag :a :fn/text "Reactivate Account"})
        (should (a/at-student-page? *d*))
        (et/click *d* {:tag :a :fn/text "Reactivate Account"})
        (et/wait-has-alert *d*)
        (et/dismiss-alert *d*)
        (et/click *d* {:tag :a :fn/text "Reactivate Account"})
        (et/accept-alert *d*)
        (et/wait-absent *d* {:css "main article p.notice"})
        (et/click *d* {:css "main article nav.breadcrumb a"})
        (et/wait-exists *d* {:css "table.students"})
        (should (et/has-text? *d* student-name))
        (should (et/absent? *d* {:css "table tr.inactive"}))))))

(describe "A counselor"
  (around [it] (a/with-system it))
  (before (a/login-as-counselor *d*))

  (with cid (ffirst
              (d/q {:query
                    '{:find [?c]
                      :in [$ ?email]
                      :where [[?account :account/email ?email]
                              [?c :counselor/account ?account]]}
                    :args [(d/db (a/get-db-conn))
                           "kyptin+counselor1@gmail.com"]})))

  (it "can only see their own students"
    (should= 1 (count (et/query-all *d* {:css "table.students tbody tr"}))))

  (it "cannot see pages they're not authorized to see"
    (doseq [path ["/counselors/new"
                  "/students/new"
                  (str "/counselor/" @cid)]]
      (a/go-to-path *d* path)
      (et/wait-exists *d* {:tag :a, :fn/text "Go home."})
      (should (et/has-text? *d* "You are not authorized to view this page."))))

  (it "can see student info page incl. feedback"
    (et/wait-visible
      *d* {:css "table.students tbody tr:first-child td.student-name a"})
    (et/click
      *d* {:css "table.students tbody tr:first-child td.student-name a"})
    (et/wait-has-text *d* :app "Profile")
    (should (et/visible? *d* {:css "#student-page main article header h1"}))
    (should (.startsWith (et/get-element-text
                           *d* {:css "#student-page main article header h1"})
                         "Student: "))
    (should (et/visible? *d* {:css "section.issues"}))
    (should (et/visible? *d* {:css "section.issues tr.issue.new"})))

  (it "can change the state of an issue"
    (et/click
      *d* {:css "table.students tbody tr:first-child td.student-name a"})
    (et/wait-has-text *d* :app "Profile")
    (should (et/visible? *d* {:tag :li, :fn/text "read"}))
    (let [text (et/get-element-text *d* {:css "tr.issue.new td.issue-text"})]
      (et/click *d* {:tag :li, :fn/text "read"})
      (et/wait-absent *d* {:css "tr.issue.new"})
      (should (.contains (et/get-element-text *d* {:css "table.issue-list"})
                         text)))))

(describe "A student"
  (around [it] (a/with-system it))
  (before (a/login-as-student *d*))

  (with cid (ffirst
              (d/q {:query
                    '{:find [?c]
                      :in [$ ?email]
                      :where [[?account :account/email ?email]
                              [?c :counselor/account ?account]]}
                    :args [(d/db (a/get-db-conn))
                           "kyptin+counselor1@gmail.com"]})))

  (with sid (ffirst
              (d/q {:query
                    '{:find [?s]
                      :in [$ ?email]
                      :where [[?account :account/email ?email]
                              [?s :student/account ?account]]}
                    :args [(d/db (a/get-db-conn))
                           "kyptin+student1@gmail.com"]})))

  (it "cannot see feedback"
    (should-not (et/visible? *d* {:css "section.issues"}))
    (should (a/at-path? *d* "/home")))

  (it "cannot see pages they're not authorized to see"
    (doseq [path ["/counselors/new"
                  "/students/new"
                  (str "/counselor/" @cid)
                  (str "/student/" @sid)]]
      (a/go-to-path *d* path)
      (et/wait-exists *d* {:tag :a, :fn/text "Go home."})
      (should (et/has-text? *d* "You are not authorized to view this page."))))

  (it "can see profile info and toggle display of details"
    (et/wait-visible *d* {:tag :section, :class :profile})
    (should (et/visible? *d* {:css "div.grad-year"}))
    (should (et/invisible? *d* {:tag :span
                            :class "field-name"
                            :fn/has-text "AP Tests"}))
    (should= "Show Details" (et/get-element-text *d* :full-profile))
    (et/click *d* :full-profile)
    (et/wait-visible *d* {:tag :span
                          :class "field-name"
                          :fn/has-text "AP Tests"})
    (should= "Hide Details" (et/get-element-text *d* :full-profile))
    (et/click *d* :full-profile))

  (it "can edit profile fields"
    (et/click *d* {:tag :a, :fn/text "Edit Profile"})
    (et/wait-visible *d* {:tag :h1, :fn/text "Edit Student Profile"})
    (should= (et/get-element-attr *d* :full-name :value) "Student One")
    (should= (et/get-element-attr *d* :high-school :value) "East Wickham High")
    (et/fill *d* :gpa-weighted-9-10 "a" keys/enter)
    (et/wait-visible *d* {:css ".status.error"})
    (should (.contains (.toLowerCase (et/get-element-text *d* {:css ".status.error"}))
                       "gpa"))
    (et/clear *d* :gpa-weighted-9-10)
    (et/fill *d* :gpa-weighted-9-10 "3.5" keys/enter)
    (et/wait-visible *d* {:css ".status.success"})
    (et/click *d* {:tag :a, :fn/text "go back"})
    (et/wait-visible *d* {:css "div.college-list"}))

  (it "can see list of documents and toggle display of more/fewer"
    (should= 5 (count (et/query-all *d* {:css ".document-list li.document"})))
    (should (et/visible? *d* :more-docs))
    (should= "View More (5)" (et/get-element-text *d* :more-docs))
    (et/click *d* :more-docs)
    (et/wait-visible *d* {:tag :a, :id "more-docs", :fn/text "View Fewer"})
    (should= 10 (count (et/query-all *d* {:css ".document-list li.document"})))
    (should (et/visible? *d* :more-docs))
    (should= "View Fewer" (et/get-element-text *d* :more-docs))
    (et/click *d* :more-docs)
    ;; Oddly, waiting on text "View More (5)" will never succeed. Maybe because
    ;; the "View More (" and the number are different children of the tag in
    ;; React-land?
    (et/wait-has-text *d* {:tag :a, :id "more-docs"} "View More (")
    (should= 5 (count (et/query-all *d* {:css ".document-list li.document"}))))

  (it "can upload a new document"
    (let [doc-name "a shiny new document.doc"]
      (et/wait-visible *d* {:tag :a, :fn/text "Upload a Document"})
      (et/click *d* {:tag :a, :fn/text "Upload a Document"})
      (et/wait-visible *d* :upload-document-modal)
      (et/fill *d* {:tag :input, :name "name"} doc-name)
      (et/upload-file *d* {:tag :input, :name "file"} "README.md")
      (et/click *d* {:tag :a, :fn/text "Submit"})
      ;; If the test fails here, be sure you've created the directory to hold
      ;; documents, by default /tmp/premiumprep-files/.
      (et/wait-visible *d* {:css "div.success"})
      (should= "Document uploaded successfully."
               (et/get-element-text *d* {:css "div.success"}))
      (et/click *d* {:tag :a, :fn/text "Submit"})
      (et/wait-visible *d* {:css "div.error"})
      (should (.startsWith (et/get-element-text *d* {:css "div.error"})
                           "There was a conflict "))
      (et/click *d* {:tag :a, :fn/text "Cancel"})
      (et/wait-absent *d* :upload-document-modal)
      (should (et/visible? *d* {:tag :span, :class :name, :fn/text doc-name}))
      ;; can't actually test downloads in webdriver, so this will have to do:
      (should (et/visible? *d* {:tag :a, :download doc-name}))))

  (it "can expand/collapse comment lists, see comments, and add comments"
    (should (et/visible? *d* {:css ".profile-and-comments .comment-list.collapsed"}))
    (should (et/visible? *d* {:css ".documents-and-comments .comment-list.collapsed"}))
    (should-not (et/visible? *d* {:css ".profile-and-comments .comments-details"}))
    (should-not (et/visible? *d* {:css ".documents-and-comments .comments-details"}))

    (let [sel ".profile-and-comments .comment-list.collapsed .comments-summary"
          summary-text (et/get-element-text *d* {:css sel})]
      (should (.contains summary-text "∨"))
      (should (.contains summary-text "2 comments"))
      (should (.contains summary-text
                         "Latest Comment: from Student One on 6/18/2020"))
      (et/click *d* {:css sel})
      (et/wait-visible *d* {:css ".profile-and-comments .comment-list.expanded"})

      (let [sel ".profile-and-comments .comment-list.expanded .comments-summary"
            summary-text (et/get-element-text *d* {:css sel})
            text "this is a new comment"]
        (should (.contains summary-text "∧"))
        (should (et/visible? *d* {:css ".profile-and-comments .comments-details"}))
        (should (et/visible? *d* {:css ".profile-and-comments .comment-submission"}))
        (et/fill *d* {:css ".profile-and-comments textarea"} text)
        (should= 0 (count (a/get-sent-emails)))
        (et/click *d* {:css ".profile-and-comments .comment-submission a.button"})
        (et/wait-visible *d* {:css ".profile-and-comments div.success"})
        (should= 2 (count (a/get-sent-emails)))
        (should= #{"kyptin+counselor1@gmail.com" "kyptin+parent1@gmail.com"}
                 (set (map :to (a/get-sent-emails))))
        (should (.contains (et/get-element-text *d* {:css sel}) "3 comments"))
        (should (.contains (et/get-element-text
                             *d* {:css ".profile-and-comments .comments-details"})
                           text))
        (et/click *d* {:css sel})
        (et/wait-visible *d* {:css ".profile-and-comments .comment-list.collapsed"})
        (should-not (et/visible? *d*
                                 {:css ".profile-and-comments .comments-details"})))

      (let [container ".documents-and-comments"
            sel (str container " .comment-list .comments-summary")
            text "this is another new comment"]
        (should (.contains (et/get-element-text *d* {:css sel}) "3 comments"))
        (et/click *d* {:css sel})
        (et/wait-visible *d* {:css (str container " .comment-list.expanded")})
        (et/fill *d* {:css (str container " textarea")} text)
        (et/click *d* {:css (str container " .comment-submission a.button")})
        (et/wait-visible *d* {:css (str container " div.success")})
        (should= 4 (count (a/get-sent-emails)))
        (should= #{"kyptin+counselor1@gmail.com" "kyptin+parent1@gmail.com"}
                 (set (map :to (take-last 2 (a/get-sent-emails)))))
        (should (.contains (et/get-element-text *d* {:css sel}) "4 comments"))
        (should (.contains (et/get-element-text
                             *d* {:css (str container " .comments-details")})
                           text))
        (et/click *d* {:css sel})
        (et/wait-visible *d* {:css (str container " .comment-list.collapsed")})
        (should-not (et/visible? *d* {:css (str container " .comments-details")})))))

  (it "can expand/collapse a college in a college list and update fields"
    (should (et/visible? *d* {:css "div.college-list"}))
    (should (et/invisible? *d* {:css "div.college-list tr.expanded"}))
    (should (et/invisible? *d* {:css "div.college-list td.college-details"}))
    (et/click *d* {:css "tr.collapsed td.college-name"})
    (et/wait-visible *d* {:css "div.college-list tr.expanded"})
    (should (et/visible? *d* {:css "div.college-list td.college-details"})))

  (it "can see and add custom fields"
    (et/click *d* {:tag :td
                   :class "college-name"
                   :fn/text "Markham Polytechnic"})
    (et/wait-visible *d* {:css "tr.expanded"})
    (should (et/visible? *d* {:tag :span, :class "name", :fn/text "Hackathons"}))
    (et/click *d* {:tag :a, :fn/text "Add Custom Field"})
    (et/wait-visible *d* :custom-field-form)
    (et/fill *d* {:tag :input, :name "name"} "Student Clubs")
    (et/fill *d* {:tag :textarea, :name "value"} "500+")
    (et/click *d* {:tag :a, :fn/text "Submit"})
    (et/wait-visible *d* {:css ".success"})
    (et/click *d* {:tag :textarea, :name "value"})
    (et/clear *d* {:tag :textarea, :name "value"})
    (et/fill *d* {:tag :textarea, :name "value"} "600+")
    (et/click *d* {:tag :a, :fn/text "Submit"})
    (et/wait-visible *d* {:css ".success"})
    (et/click *d* {:tag :a, :fn/text "Cancel"})
    (et/wait-invisible *d* :custom-field-form)
    (should (et/visible? *d* {:tag :span, :class "name", :fn/text "Student Clubs"}))
    (should (et/visible? *d* {:tag :span, :class "value", :fn/text "600+"}))
    (should= 8 (count (et/query-all *d* {:css "span.icon.delete img"})))
    (et/click-el *d* (last (et/query-all *d* {:css "span.icon.delete img"})))
    (Thread/sleep 1000)
    (should= 7 (count (et/query-all *d* {:css "span.icon.delete img"})))
    (et/click *d* {:css "div.college-list tr.expanded td.college-name"})
    (et/wait-invisible *d* {:css "tr.expanded"}))

  (context "with an expanded college"
    (before
      (et/click *d* {:css "tr.collapsed td.college-name"})
      (et/wait-visible *d* {:css "div.college-list tr.expanded"}))

    (it "can edit a string field (location)"
      (et/click *d* {:css ".one-line-fields section:first-child span.pencil"})
      (et/wait-visible *d* :college-list-entry-field-input)
      (et/fill *d* :college-list-entry-field-input "collegetown, USA")
      (et/click *d* {:css ".one-line-fields section:first-child .cancel"})
      (et/wait-visible *d* {:css ".one-line-fields section:first-child span.pencil"})
      (et/click *d* {:css ".one-line-fields section:first-child span.pencil"})
      (et/wait-visible *d* :college-list-entry-field-input)
      (et/fill *d* :college-list-entry-field-input "College Town, USA")
      (et/click *d* {:css ".one-line-fields section:first-child span.submit"})
      (et/wait-visible *d* {:tag :div
                            :class "field-value"
                            :fn/has-text "College Town, USA"}))

    (it "can edit a date field (app-deadline)"
      (et/click *d* {:css ".one-line-fields section:first-child div.field-grid div:nth-child(8) span.pencil"})
      (et/wait-visible *d* :college-list-entry-field-input)
      (et/fill *d* :college-list-entry-field-input "07/24/2020")
      (et/click *d* {:css ".one-line-fields span.submit"})
      (et/wait-visible *d* {:tag :div, :class "field-value", :fn/has-text "7/24/2020"}))

    (it "can edit a boolean field (visited?)"
      (et/click *d* {:css ".one-line-fields section:first-child div.field-grid div:nth-child(10) span.pencil"})
      (et/wait-visible *d* :college-list-entry-field-input)
      (et/click *d* {:css "#college-list-entry-field-input[value=false]"})
      (et/click *d* {:css ".one-line-fields span.submit"})
      (et/wait-invisible *d* :college-list-entry-field-input)
      (should (.startsWith (et/get-element-text *d* {:css ".one-line-fields section:first-child div.field-grid div:nth-child(10)"})
                           "No")))

    (it "can edit a notes field (student-notes)"
      (et/click *d* {:css ".notes-and-outcomes .notes:first-child span.pencil"})
      (et/wait-visible *d* :college-list-entry-field-input)
      (et/fill *d* :college-list-entry-field-input "some notes")
      (et/click *d* {:css "span.submit"})
      (et/wait-invisible *d* :college-list-entry-field-input)
      (et/wait-visible *d* {:tag :div
                            :class "notes-text"
                            :fn/has-text "some notes"}))

    (it "can change the list a college is in"
      (should (.contains (et/get-element-text
                           *d* {:css "td.college-details > header"})
                         "High Reach"))
      (et/click *d* {:css "td.college-details header span.pencil"})
      (et/wait-visible *d* {:css "td.college-details header select"})
      (et/click *d* {:css "td.college-details header select"})
      (et/click *d* {:css "td.college-details header option[value=likely]"})
      (et/click *d* {:css "td.college-details header span.submit"})
      (et/wait-invisible *d* {:css "td.college-details header select"})
      (should (.contains (et/get-element-text
                           *d* {:css "td.college-details > header"})
                         "Likely")))))

(describe "A user"
  (around [it] (a/with-system it))
  (with semail "kyptin+student1@gmail.com")

  (it "can change their email address"
    (let [new-email "a-new-email-address@email.com"]
      (a/login-as-student *d*)
      (et/wait-visible *d* :my-account)
      (et/mouse-move-to *d* :my-account)
      (et/wait-visible *d* {:tag :a, :fn/text "Change Email"})
      (et/click *d* {:tag :a, :fn/text "Change Email"})
      (et/wait-visible *d* {:tag :h3, :fn/text "Change Email"})
      (should= @semail
               (et/get-element-attr *d* {:tag :input, :name "current-email"}
                                    :value))
      (et/fill *d* {:tag :input, :name "email"} "kyptin+student2@gmail.com" keys/enter)
      (et/wait-visible *d* {:css "div.error"})
      (should (.startsWith (et/get-element-text *d* {:css "div.error"})
                           "Error: that email address is already taken."))
      (et/clear *d* {:tag :input, :name "email"})
      (et/fill *d* {:tag :input, :name "email"} new-email keys/enter)
      (et/wait-visible *d* {:css "div.success"})
      (should (.startsWith (et/get-element-text *d* {:css "div.success"})
                           "Email successfully changed."))
      (et/click *d* {:css "div.success a"})

      ;; changing email address should send email to old and new addrs
      (should= 2 (count (a/get-sent-emails)))
      (let [to-old (first (a/get-sent-emails))
            to-new (second (a/get-sent-emails))
            old-body (:body to-old)
            new-body (:body to-new)]
        (should= @semail (:to to-old))
        (should= "Premium Prep email changed" (:subject to-old))
        (should= new-email (:to to-new))
        (should= "[Premium Prep] Verify this email address" (:subject to-new))
        (should (.startsWith old-body
                             "Somebody changed the email address associated"))
        (should (.contains new-body
                           "you need to verify that you own this email address"))
        (should-not (.contains old-body
                               "https://app.premiumprep.com/verify-email"))
        (should (.contains new-body
                           "https://app.premiumprep.com/verify-email?token=")))

      ;; redirected to "unverified email" page
      (a/go-home *d*)
      (et/wait-visible *d* {:tag :h3, :fn/text "Unverified Email"})
      (should (et/invisible? *d* {:css "div.success"}))

      ;; can re-send verification email
      (let [prod-domain "https://app.premiumprep.com"
            regex (re-pattern (str prod-domain
                                   "(/verify-email\\?token=.*?)\n"))
            email-body (-> (a/get-sent-emails) last :body)
            verify-path (second (re-find regex email-body))]
        (et/click *d* {:tag :input, :type "submit"})
        (et/wait-visible *d* {:css "div.success"})
        (should= 3 (count (a/get-sent-emails)))
        (should= new-email (:to (last (a/get-sent-emails))))
        (should= "[Premium Prep] Verify this email address"
                 (:subject (last (a/get-sent-emails))))
        (should (.contains (:body (last (a/get-sent-emails)))
                           "you need to verify that you own this email address"))
        (should (.contains (:body (last (a/get-sent-emails))) verify-path))

        ;; can verify new email address and use app
        (a/go-to-path *d* verify-path)
        (et/wait-visible *d* {:tag :h3, :fn/text "Verify Email"})
        (et/wait-visible *d* {:css "div.success"})
        (should (.startsWith (et/get-element-text *d* {:css "div.success"})
                             "Email successfully verified."))
        (et/click *d* {:css "div.success a"})
        (et/wait-visible *d* {:css "#student-page"})
        (should (a/at-path? *d* "/home")))

      ;; user can change their password
      (let [new-pass "a-super-secure-new-password"
            too-short "hello"]
        (et/wait-visible *d* :my-account)
        (et/mouse-move-to *d* :my-account)
        (et/wait-visible *d* {:tag :a, :fn/text "Change Password"})
        (et/click *d* {:tag :a, :fn/text "Change Password"})
        (et/wait-visible *d* {:tag :h3, :fn/text "Change Password"})
        (should= new-email
                 (et/get-element-attr *d* {:tag :input, :name "current-email"}
                                      :value))
        (et/fill *d* {:tag :input, :name "password"} too-short)
        (et/fill *d* {:tag :input, :name "confirmation"}
                 (str too-short "a") keys/enter)
        (et/wait-visible *d* {:css "div.error"})
        (should= (et/get-element-text *d* {:css "div.error"})
                 "Error: your passwords don't match.")
        (et/clear *d* {:tag :input, :name "password"})
        (et/clear *d* {:tag :input, :name "confirmation"})
        (et/fill *d* {:tag :input, :name "password"} too-short)
        (et/fill *d* {:tag :input, :name "confirmation"} too-short keys/enter)
        (et/wait-has-text *d* {:css "div.error"}
                          "Error: your password must be at least 8 characters.")
        (et/clear *d* {:tag :input, :name "password"})
        (et/clear *d* {:tag :input, :name "confirmation"})
        (et/fill *d* {:tag :input, :name "password"} new-pass)
        (et/fill *d* {:tag :input, :name "confirmation"} new-pass keys/enter)
        (et/wait-visible *d* {:css "div.success"})
        (should (.startsWith (et/get-element-text *d* {:css "div.success"})
                             "Password successfully changed."))
        (et/click *d* {:css "div.success a"})
        (et/wait-visible *d* :student-page)
        (a/logout *d*)
        (et/wait-visible *d* {:tag :input, :name "email"})
        (et/fill *d* {:tag :input, :name "email"} new-email)
        (et/fill *d* {:tag :input, :name "password"} new-pass keys/enter)
        (et/wait-visible *d* :student-page))))

  (it "can recover their password"
    (a/go-home *d*)
    (et/wait-visible *d* {:css "div.forgot-password a"})
    (et/click *d* {:css "div.forgot-password a"})
    (et/wait-visible *d* {:tag :h3, :fn/text "Forgot your password?"})
    (et/fill *d* {:css "#forgot-password-modal input[name=email]"}
             @semail keys/enter)
    (et/wait-visible *d* {:css "#forgot-password-modal div.success"})
    (should (.startsWith
              (et/get-element-text
                *d* {:css "#forgot-password-modal div.success"})
              "If we recognized your email address, we sent you an email"))
    (et/click *d* {:tag :a, :fn/text "Cancel"})
    (et/wait-absent *d* {:css "#forgot-password-modal"})
    (should= 1 (count (a/get-sent-emails)))

    (let [{:keys [to subject body]} (first (a/get-sent-emails))
          query '{:find [?token]
                  :in [$ ?email]
                  :where [[?account :account/email ?email]
                          [?account :account/token ?token]]}
          args [(d/db (a/get-db-conn)), @semail]
          token (ffirst (d/q {:query query, :args args}))
          token-path (str "/reset-password?token=" token)
          expected-url (str "https://app.premiumprep.com" token-path)]
      (should= @semail to)
      (should= "Reset your Premium Prep password" subject)
      (should (.contains body expected-url))

      ;; reset password / see "no token" error
      (let [path-without-token (str/replace token-path #"\?token=.*$" "")]
        (a/go-to-path *d* path-without-token)
        (et/wait-visible *d* {:tag :h3, :fn/text "Reset Password"})
        (should (et/visible? *d* {:css "#reset-password-modal div.error p"}))
        (should (et/has-text? *d* {:css "#reset-password-modal div.error p"}
                              "Error: no token is available at this address.")))

      ;; reset password / see "token not found" error
      (let [path-bad-token (str/replace token-path #".$" "")
            new-pass "arstoien"]
        (a/go-to-path *d* path-bad-token)
        (et/wait-visible *d* {:tag :h3, :fn/text "Reset Password"})
        (should (et/visible? *d*
                             {:css "#reset-password-modal input[name=password]"}))
        (et/fill *d* {:css "#reset-password-modal input[name=password]"}
                 new-pass)
        (et/fill *d* {:css "#reset-password-modal input[name=confirmation]"}
                 new-pass keys/enter)
        (et/wait-visible *d* {:css "#reset-password-modal div.error p"})
        (should (et/has-text? *d* {:css "#reset-password-modal div.error p"}
                              "Error: the given token could not be found")))

      ;; reset password / see success message
      (let [new-pass "arstoien"]
        (a/go-to-path *d* token-path)
        (et/wait-visible *d* {:tag :h3, :fn/text "Reset Password"})
        (should (et/visible? *d*
                             {:css "#reset-password-modal input[name=password]"}))
        (et/fill *d* {:css "#reset-password-modal input[name=password]"}
                 new-pass)
        (et/fill *d* {:css "#reset-password-modal input[name=confirmation]"}
                 new-pass keys/enter)
        (et/wait-visible *d* {:css "#reset-password-modal div.success p"})
        (should (et/has-text? *d* {:css "#reset-password-modal div.success p"}
                              "Password successfully reset."))
        (should (et/visible? *d* [{:css "#reset-password-modal div.success p"}
                                  {:tag :a, :href "/", :fn/text "login"}]))

        ;; reset password / login with new password
        (et/click *d* {:css "div.success a"})
        (et/wait-visible *d* {:tag :input, :type "submit"})
        (et/fill *d* {:tag :input, :name "email"} @semail)
        (et/fill *d* {:tag :input, :name "password"} new-pass keys/enter)
        (et/wait-exists *d* {:css "#student-page"})
        (should (a/at-path? *d* "/home"))))))

(comment ; to poke around manually in a REPL session
  ;; Alternatively, you can also pause for a long time before the failed
  ;; assertion in the test above to leave the browser open for inspection.
  (figwheel/stop "integration-test")
  (compile-cljs #(assoc-in % [:closure-defines 'com.premiumprep.app.config/DEBUG] true))
  (def system (start-system (a/test-system-config)))
  (def d (et/firefox))

  ;; do et/ stuff with driver `d`

  ;; et cetera...
  (et/delete-session d)
  (stop-system system)
  )
