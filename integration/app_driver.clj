(ns app-driver
  (:require [clojure.string :as str]
            (com.premiumprep.app
              [config :refer [system-config]]
              [system :refer [start-system stop-system]])
            [datomic.client.api :as d]
            [etaoin.api :as et]
            [etaoin.keys :as keys]))

(def ^:private server-port 3333)
(def ^:private base-url (str "http://localhost:" server-port))
(def ^:private db-name "pp-integration-test")
(def superuser-email "superuser@email.com")
(def superuser-password "a really secure password!!!11")
(def ^:dynamic *d*) ; the Etaoin driver
(def ^:dynamic ^:private *system*) ; the running backend system
(def ^:private sent-emails (atom nil))

(def test-system-config
  (-> system-config
      (assoc-in [:datomic/conn :db-name] db-name)
      (assoc-in [:datomic/conn :use-fixtures] true)
      (assoc-in [:datomic/conn :superuser :name] "Test Superuser")
      (assoc-in [:datomic/conn :superuser :email] superuser-email)
      (assoc-in [:datomic/conn :superuser :password] superuser-password)
      (assoc-in [:email/send-fn]
                #(swap! sent-emails conj {:to %1, :subject %2, :body %3}))
      (assoc-in [:server/http :allowed-origin] base-url)
      (assoc-in [:server/http :frontend-env-name] :test)
      (assoc-in [:server/http :log-level] :none)
      (assoc-in [:server/http :port] server-port)
      ;; must disable this b/c for e2e testing we're in a nonsecure context
      (assoc-in [:server/http :secure-cookies?] false)))

(defn with-system [body-fn]
  (binding [*system* (with-redefs [clojure.core/println (constantly nil)]
                       (start-system test-system-config))]
    (try
      (et/with-chrome-headless {:size [1200 1200]}
        driver
        (binding [*d* driver]
          (body-fn)))
      (finally
        (reset! sent-emails [])
        (d/delete-database (:datomic/client *system*) {:db-name db-name})
        (stop-system *system*)))))

(defn get-db-conn []
  (:datomic/conn *system*))

(defn get-sent-emails []
  @sent-emails)

(defn get-path [d]
  (let [url (et/get-url d)]
    (assert (.startsWith url base-url))
    (subs url (count base-url))))

(defn fill-and-submit-login-form [d email password]
  (et/wait-visible d {:tag :input, :name "email"})
  (et/clear d {:tag :input, :name "email"})
  (et/clear d {:tag :input, :name "password"})
  (et/fill d {:tag :input, :name "email"} email)
  (et/fill d {:tag :input, :name "password"} password keys/enter))

(defn login-as-superuser [d]
  (et/go d base-url)
  (fill-and-submit-login-form d superuser-email superuser-password)
  (et/wait-exists d {:tag :h1, :fn/text "All Feedback"}))

(defn login-as-counselor [d]
  (et/go d base-url)
  (fill-and-submit-login-form d "kyptin+counselor1@gmail.com" "counselor")
  (et/wait-exists d {:css "table.students"}))

(defn login-as-student [d]
  (et/go d base-url)
  (fill-and-submit-login-form d "kyptin+student1@gmail.com" "password")
  (et/wait-visible d {:css "#student-page"}))

(defn localize-url
  "Given a URL for the real app (e.g. from an email body), make it relative to the
  local server run in the testing context."
  [url]
  (str/replace url "https://app.premiumprep.com" base-url))

(defn go-to-path [d path]
  (et/go d (str base-url path)))

(defn at-path? [d path]
  (= (str base-url path) (et/get-url d)))

(defn go-home [d]
  (go-to-path d "/"))

(defn go-to-counselors-page [d]
  (et/click d {:tag :a, :fn/text "Counselors"})
  (et/wait-exists d {:tag :h1, :fn/text "Counselors"}))

(defn go-to-counselor-page [d]
  (et/click d {:tag :a, :fn/text "Counselors"})
  (et/wait-exists d {:tag :h1, :fn/text "Counselors"})
  (et/wait-exists d {:tag :a, :fn/text "Ronald Hawkins"})
  (et/click d {:tag :a, :fn/text "Ronald Hawkins"})
  (et/wait-exists d {:tag :a, :fn/text "< Counselors"}))

(defn go-to-students-page [d]
  (et/click d [{:tag :header} {:tag :nav} {:tag :a, :fn/text "Students"}])
  (et/wait-exists d {:css "main table.students"}))

(defn at-issues-page? [d]
  (and (= "/issues" (get-path d))
       (= "All Feedback" (et/get-element-text d {:css "#app h1"}))))

(defn at-counselors-page? [d]
  (and (= "/counselors" (get-path d))
       (= "Counselors" (et/get-element-text d {:css "#app h1"}))))

(defn at-counselor-page? [d]
  (and (= "< Counselors"
          (et/get-element-text d {:css "main article nav.breadcrumb a"}))
       (re-matches (re-pattern "/counselor/\\d+") (get-path d))))

(defn at-students-page? [d]
  (and (= "/students" (get-path d))
       (et/exists? d {:css "main table.students"})))

(defn at-student-page? [d]
  (and (re-matches (re-pattern "/student/\\d+") (get-path d))
       (.contains (et/get-element-text d {:css "main article header h1"})
                  "Student: ")))

(defn reassign-first-student [d]
  (let [edit-student-link (str "table.students tbody tr:first-child "
                               "td.controls a:first-child")]
    (et/wait-visible d {:css edit-student-link})
    (et/click d {:css edit-student-link})
    (et/wait-visible d {:css "#edit-student-modal"})
    (et/click d {:css "select#counselor-id"})
    (assert (et/visible? d {:tag :option, :fn/text "Counselor 2"}))
    (et/click d {:tag :option, :fn/text "Counselor 2"})
    (assert (et/visible? d {:tag :a, :fn/text "Save"}))
    (et/click d {:tag :a, :fn/text "Save"})
    (et/wait-absent d {:css "aside.modal"})))

(defn logout [d]
  (let [submenu-hover-revealer-selector :my-account
        logout-link-selector {:tag :a, :fn/text "Log out"}]
    (doto d
      (et/scroll-top)
      (et/wait-visible submenu-hover-revealer-selector)
      ;; As of 2020-06-12, Firefox geckodriver crashes with this command.
      ;; Cf. https://stackoverflow.com/q/58092837/202292
      ;; ...so I'm switching to chromedriver.
      (et/mouse-move-to submenu-hover-revealer-selector)
      (et/wait-visible logout-link-selector)
      (et/click logout-link-selector))))
