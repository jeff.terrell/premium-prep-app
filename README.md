# Premium Prep Web App

A [re-frame](https://github.com/Day8/re-frame) application to help [Premium
Prep](https://premiumprep.com) serve its users better.

## Development Mode

### Initial setup

There are a few things you need to do to get a working development environment.

1. Add a `resources/config.edn` file with your local setup. See the section
   below on configuration for details and a partial example.
2. Ensure the directory pointed to by `:storage-path` in your configuration
   exists.
3. Install Datomic dev-local by following [these
   instructions](https://docs.datomic.com/cloud/dev-local.html#getting-started).
4. Ensure you have a working `make` on your system.

### Run application

The application in development mode needs two processes running: a backend
server and a Figwheel process to compile Clojurescript (CLJS) to JS. Start the
backend server on `localhost:3000`:

    make backend

Then start the Figwheel process with the command below. Once the CLJS is
compiled, it will open up a web page with the app, with hot-reloading enabled.
This means that when you make changes to the CLJS source, they will
automatically propagate to the app running in the browser tab. Look for the
Figwheel icon to pop up briefly in the lower-left corner of the page. It will be
green if your changes compiled successfully.

    make figwheel

You can also use [entr](http://eradman.com/entrproject/) to kill and restart the
backend server whenever code changes like so:

    find src/clj -type f -name \*.clj | entr -r make backend

Pro tip: use [fd](https://github.com/sharkdp/fd) instead of find:

    fd -e clj | entr -r make backend

### Configuring the application

Configuration for your environment belongs in the git-ignored file
`resources/config.edn`. Here's an example:

```
{:allowed-origin "http://localhost:3000"
 :datomic-is-local true
 :email-type :email-type/stdout-print
 :frontend-env-name :dev
 :log-level :oneline
 :port 3000
 :storage-type :storage-type/local
 :storage-path "/tmp/premiumprep-files/"
 :superuser-name "<name>"
 :superuser-email "<email>"
 :superuser-password "<password>"
 :use-fixtures true}
```

Here's what each setting means.

- `:allowed-origin` indicates which origin the backend allows API requests from.
  The default value is shown above.

- `:datomic-is-local` indicates whether the system should use a local, in-memory
  database or try to connect to a real one. The default is `true`.

- `:email-type` controls whether to send emails for real (with
  `:email-type/smtp`) or merely print them to stdout (with
  `:email-type/stdout-print`, the default). If the former value is given, you
  must also supply values for `:smtp-server`, `:smtp-port`, `:smtp-username`,
  and `:smtp-password`.

- `:frontend-env-name` indicates which frontend bundle to serve. The default is
  `:dev`, which serves the top-level script for the development frontend code.
  The value `:prod` will serve the minified production bundle. See the
  `com.premiumprep.app.handler.index` namespace for details.

- `:log-level` indicates how much to print for each request the backend sees.
  The default value is shown above. Valid values are `:none`, `:oneline`, and
  `:body`.

- `:port` is the port for the server process to listen on. The default value is
  shown above. Note the connection between `:port` and `:allowed-origin`.

- `:storage-type` indicates which approach to use when storing and retrieving
  documents. Valid options are `:storage-type/local` (the default),
  `:storage-type/s3`, and `:storage-type/none`. If you use S3, be sure that you
  have the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`
  set in the environment of your server process, so that the [AWS default
  credentials provider
  chain](https://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/com/amazonaws/auth/DefaultAWSCredentialsProviderChain.html)
  will grant the needed S3 permissions to the process.

- `:storage-path` indicates where to store the files. For storage type local,
  this is a directory on disk. Note that you'll need to create this directory
  yourself, if it doesn't already exist. The default value is shown above. For
  storage type S3, this is the name of an S3 bucket that your credentials (see
  previous bullet) have read/write permission to.

- `:superuser-name`, `:superuser-email`, and `:superuser-password` control the
  values used for creating a superuser (a.k.a. administrator) account. Without
  such an account, you won't be able to login to the app as a superuser. If
  superuser values are not provided, no superuser will be created.

- The `:use-fixtures` value instructs the server to create fixture data in the
  database when it starts. For the development environment, this is especially
  helpful because the database is an in-memory stub of Datomic (i.e. without
  persistence) and is empty every time the server starts. Set to `false` to
  disable fixtures. Defaults to true.

Note that any of these values can be overridden by environment variables. E.g.
to override the log level when running the backend, say:

    LOG_LEVEL=":body" make backend

For more details on how environment variables and values are converted into
configuration values, see [the README of the yogthos/config
library](https://github.com/yogthos/config), which is what we use here.

## Tests

### One-time testing setup

Unfortunately, the testing framework I prefer,
[Speclj](https://github.com/slagyr/speclj), has not been updated in a couple of
years and doesn't play nice with the newer [Clojure deps.edn and
CLI](https://clojure.org/guides/deps_and_cli) tooling that we use. The fix is
simple, but must be done manually on your machine before you will be able to
run the tests.

```
clojure -R:test -e "(doseq [class '(speclj.platform.SpecFailure
                                    speclj.platform.SpecPending)]
                      (compile class))"
```

### Running tests

To run Clojure tests:

    make .test

If no Clojure source files have changed since the last time the tests passed
(i.e. the last-modified timestamp of the `.test` file), the tests will not be
run and `make` will exit with a successful status.

You can also run an auto-tester process, which will watch the source files for
changes and re-run only the relevant test files:

    make autotest

You can also use [entr](http://eradman.com/entrproject/) for stateless
auto-testing like so:

    find . -type f -name \*.clj | entr make .test

(Pro tip: use `fd -e clj` instead of `find ...` above.)

Note that this stateless approach is purer, but it takes longer to give feedback
because it restarts the JVM from scratch every time.

### Browser tests

I'm using [Etaoin](https://github.com/igrishaev/etaoin) for end-to-end (e2e)
browser tests. They're heavy, but they provide high confidence that everything
is working correctly because they actually test the app from the perspective of
a real web browser, remote controlled by the code.

#### Prerequisites

Before you run the e2e tests, make sure the directory `/tmp/premiumprep-files/`
exists.

You also need to install
[chromedriver](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver).

#### Usage

To run the e2e tests, say:

    make .e2e

This will compile a version of the frontend that points to the backend used
during e2e tests (localhost port 3333) if needed, and run the tests.

The above notes for unit testing about `entr` also apply here. Note that, if you
do this, changes to frontend code (in .cljs files) will not be accounted for.
You can run a separate `entr` loop to rebuild the frontend whenever those files
change, but note that your two `entr` loops will not coordinate so as to delay
the test runner until after the frontend code is built. (I want to create a
nicer dev flow here for e2e testing, but haven't done it yet; it's not yet been
high enough of a priority in terms of value delivered to clients.)

#### Debugging

The full test suite takes quite a while (about 3 minutes) to run, so in case of
a test failure, a faster feedback loop is useful. The way I do this is to
manually comment out all other tests with `#_` and re-run the test. It's
tedious, but so far the best option. Note that "folding" in your editor can make
it easier to find the appropriate enclosing `describe` and `context` blocks.

For a faster feedback loop, comment out all the other tests (with the `#_`
reader form). That's more tedious to set up, though.

Also note that you can show the window of the browser that's being controlled by
changing the `et/with-chrome-headless` call in the `app-driver` namespace to
`et/with-chrome`. If the window closes too quickly, try adding
`(Thread/sleep 20000)` just before the failure. Note that you can open the
browser dev tools in this window, just like you can with other browser windows.

If you're really stuck, see the comment at the end of the `integration-test`
namespace for some hints on starting a REPL session with a remote controlled
browser.

### Testing S3

You can test that the code works correctly to store and retrieve documents
to/from S3. First, create a bucket using the AWS web console. You can keep the
defaults for access mode (private) and object locking (disabled).

Then edit your app configuration (see above) to use storage type S3 and storage
path equal to your bucket name. Ensure the `AWS_ACCESS_KEY_ID` and
`AWS_SECRET_ACCESS_KEY` environment variables are set appropriately (typically
to the values stored in `~/.aws/credentials`--in fact, leaving the credentials
there and skipping the env vars should work, so long as they are your default
credentials). Then you should be able to start your app, login (e.g. as a
fixtured user), and store real documents to your S3 bucket, using the AWS web
console to inspect the bucket contents.

## Production Mode

### Production Build

To compile clojurescript to javascript:

    make .min-prod

The compiled output is available in `resources/public/premiumprep-app.min.js`,
as specified in `min-prod.cljs.edn`. Note that the backend location is defined
at compile time and assumes the script will run in a production environment.

### Testing production build locally

If you want to access the app in production mode, bypassing Figwheel's
auto-compiling and hot-reloading niceties, you'll first need to compile a
production build with the `min-local` profile:

    make .min-local

(The `min-local` profile is like the `min-prod` profile except that it points
to the local backend instead of the production backend. You can examine the
respective configuration files, `min-*.cljs.edn`, for full details.)

Then start the backend server as normal.

### Production Deploy

The code is deployed to an AWS Elastic Beanstalk environment set to run in the
same VPC that Datomic Cloud set up, so that the server can access Datomic Cloud.

To deploy, run:

    make .deploy

This will ensure that:

1. the current version of the code has not already been uploaded,
2. all tests pass, and
3. the working tree is clean

...then it will create an archive of application source code (including a
current min-prod bundle) and upload it to the Elastic Beanstalk environment.
You'll need the AWS CLI installed and credentials for an account with
appropriate access installed locally, in the typical AWS CLI way. You'll also
need the `zip` and `jq` utilities installed. (Windows users: not sure how this
will work in a command prompt or powershell window. You will probably have
better luck with WSL. Sorry.) If you have multiple AWS accounts and need to use
a non-default credentials profile for this, set the environment variables
`PREMIUMPREP_AWS_CREDS_PROFILE` to the name of the profile you use for this
account. For debugging, the git-ignored archive uploaded to Elastic Beanstalk is
retained in the repository root directory, e.g.
`premium-prep-app.<7-digit-commit-sha>.zip`.

(Pro tip: use [`direnv`](https://direnv.net/) to store the
`PREMIUMPREP_AWS_CREDS_PROFILE` value.)

Note: I recommend running `make .deploy` often, as a way to get feedback on
whether the code you wrote works. As long as you trust the test suite, it's
completely safe to run it at any time. It will only deploy if all tests pass and
the working tree is all committed. So if you have a dirty working tree (i.e.
uncommitted changes), it's a quick way to get feedback about whether everything
works.

### Troubleshooting

#### Accessing production data

You can connect to the production database from the comfort of your local
computer. First, follow the instructions
[here](https://docs.datomic.com/cloud/getting-started/get-connected.html) to
open a connection to the production database. The region is `us-east-1` and the
system name is `pp-app`.

Next, see the comments in the bottom of the `com.premiumprep.app.system`
namespace for Clojure code that will open a Datomic connection to the production
database. **Be careful!** You can wipe out production data this way. Note that
you can query the full history of the database to see the history of different
values for an entity and attribute and when they changed. See
[here](https://docs.datomic.com/cloud/tutorial/history.html) for more info.

As you query, note that (as of 2020-12-13) we are manually saving the namespace
that initiates each transaction to make it easier to understand how the data in
the database was changed.

#### Troubleshooting infrastructure issues

You can ssh in to the production environment to poke around. You'll need to ssh
first to the bastion, then on to the host from there. You need to forward
access to a running ssh access to authenticate the second hop. So, if you don't
already have an ssh-agent running, start one with `. $(ssh-agent -s)`. Then add
both the bastion key and the application host key to your agent:

    ssh-add .ssh/datomic-us-east-1-pp-app-bastion
    ssh-add .ssh/premium-prep-jeff.pem

(If you don't have these files, ask Jeff.)

Then use the AWS web console to find both needed IP addresses. You'll need the
external (public) IP of the bastion host and the internal (private) IP of the
application host. Both of these should be visible in the EC2 instances panel.
Connect to the bastion:

    ssh -A ec2-user@<bastion-ip-address>

...then on to the application host:

    ssh <application-host-ip>

Once there, you might like to check out the logs in `/var/log` and the app in
`/var/app/`. You can get root with `sudo bash`. Typically the app code will be
in `/var/app/current/`, but if something went wrong with the last deployment,
`/var/app/staging/` might have the more current code.

You can test that the app is running with `curl -v
http://localhost:8080/healthcheck`. Check that the nginx reverse proxy is
connected correctly by doing the same thing without a port number.

One more trick: find the full environment and command of the running
application process with:

    ps auxwwwe | grep -v grep | grep premiumprep
