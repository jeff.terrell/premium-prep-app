#!/bin/bash
# deploy-helper.sh - do the heavy lifting for the `make .deploy` task.

if [ -z "$MAKELEVEL" ]; then
    # Don't want to skip the protections offered by the make context, e.g. that
    # tests pass and that the min-prod bundle is built.
    echo "Do not call this script directly; call 'make .deploy' instead." >&2
    exit 2
fi

if [ -z "$(which zip 2>/dev/null)" ]; then
    echo "You must first install the 'zip' executable." >&2
    exit 2
fi

if [ -z "$(which aws 2>/dev/null)" ]; then
    echo "You must first install the 'aws' executable." >&2
    exit 2
fi

if [ -z "$(which jq 2>/dev/null)" ]; then
    echo "You must first install the 'jq' executable." >&2
    exit 2
fi

if [ -n "$(git status --short)" ]; then
    echo "You may only deploy with a clean working tree." >&2
    echo "Commit or stash your changes and try again." >&2
    exit 2
fi

sha7=$(git rev-parse @ | cut -c1-7)
zipfile="premium-prep-app.${sha7}.zip"
git archive --format=zip @ > "$zipfile"
zip -q "$zipfile" resources/public/premium-prep-app.min.js

aws="$(which aws) --region us-east-1"
if [ -n "${PREMIUMPREP_AWS_CREDS_PROFILE}" ]; then
    aws="${aws} --profile ${PREMIUMPREP_AWS_CREDS_PROFILE}"
fi

# pass attribute names as args
# Ex: `get-health HealthStatus Status Color Causes`
# Ex: `get-health All`   # special value
function get-health() {
    $aws elasticbeanstalk describe-environment-health \
         --environment-name PremiumprepprodEnv-env \
         --attribute-names "$@"
}

# pass a single attribute name, and not `All`
function get-health-str() {
    get-health "$1" | jq ".$1" | tr -d '"'
}

set -eux

$aws s3 cp "$zipfile" "s3://elasticbeanstalk-us-east-1-517329429887/${zipfile}"

$aws elasticbeanstalk create-application-version \
     --application-name "PremiumPrepProd-env" \
     --version-label "$zipfile" \
     --source-bundle "S3Bucket=elasticbeanstalk-us-east-1-517329429887,S3Key=${zipfile}"

$aws elasticbeanstalk update-environment \
     --environment-name "PremiumprepprodEnv-env" \
     --version-label "$zipfile"

set +x

echo "Initiated deployment of revision ${sha7}."
echo -n "Waiting for deployment to finish (estimated 13 minutes) "

while [ "$(get-health-str Status)" == 'Updating' ]; do
    echo -n '.'
    sleep 10
done
echo

if [ "$(get-health-str Color)" == 'Green' ]; then
    echo "Update successful."
    exit 0
fi

echo 'Update unsuccessful.'
echo
echo 'For info, see the Elastic Beanstalk (EB) web console or try one of these commands:'
echo
echo 'Get summary health info:'
echo "  $aws elasticbeanstalk describe-environment-health --environment-name PremiumprepprodEnv-env --attribute-names Status HealthStatus Color Causes"
echo
echo 'Get all health info:'
echo "  $aws elasticbeanstalk describe-environment-health --environment-name PremiumprepprodEnv-env --attribute-names All"
echo
echo 'Get recent events for the EB environment, most recent first:'
echo "  $aws elasticbeanstalk describe-events --environment-name PremiumprepprodEnv-env --max-items 20"

exit 2
